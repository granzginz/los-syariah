#Region "Revision History"
'$Header: /Tunas/AdIns.Application.Eloan.DataAccess.Channeling/Setting/FundingCompany.vb 51    8/19/04 8:28p Teddy $
'$Workfile: FundingCompany.vb $
'-----------------------------------------------------------
'$Log: /Tunas/AdIns.Application.Eloan.DataAccess.Channeling/Setting/FundingCompany.vb $
'
'51    8/19/04 8:28p Teddy
'
'50    8/18/04 7:15p Teddy
'
'49    8/18/04 3:12p Teddy
'
'48    8/12/04 7:29p Teddy
'
'47    8/09/04 8:19p Teddy
'
'46    15/06/04 11:29 Kukuh
'
'45    15/06/04 11:24 Kukuh
'
'
'36    6/11/04 11:44a Parang
'
'35    6/08/04 9:16p Reli
'
'34    24/05/04 20:12 Henry
'  
'  34    04-05-19 10:39a Teddy
'  
'  33    04/03/30 8:59p Teddy
'  
'  32    3/19/04 3:14p Kukuh
'  
'  31    04/03/19 3:10p Teddy
'  
'  30    04-03-12 9:18a Teddy
'  
'  29    04/03/09 5:30p Teddy
'  
'  28    04/02/27 2:09p Teddy
'  
'  27    04/02/25 4:23p Teddy
'  
'  26    04/02/20 11:15p Teddy
'  
'  25    04/02/19 9:34p Teddy
'  
'  24    04/02/18 6:15p Teddy
'  
'  23    04/02/16 5:41p Teddy
'  
'  22    2/16/04 11:21a Henry
'  
'  23    2/13/04 9:41p Admin
'  
'  22    2/13/04 2:45p Admin
'  
'  21    2/13/04 8:43a Lina
'  
'  20    9/16/03 4:50p Stephanus
'  
'  19    9/15/03 2:30p Stephanus
'  
'  18    9/09/03 4:18p Stephanus
'  
'  17    9/08/03 5:25p Stephanus
'  
'  16    9/08/03 12:20p Stephanus
'  
'  15    9/05/03 2:22p Henry
'  
'  19    9/05/03 10:39a Stephanus
'  
'  18    9/04/03 7:41p Stephanus
'  
'  17    9/04/03 3:16p Stephanus
'  
'  16    9/04/03 2:55p Stephanus
'  
'  15    9/02/03 1:00p Stephanus
'  
'  14    9/01/03 5:17p Stephanus
'  
'  13    9/01/03 3:32p Stephanus
'  
'  12    8/29/03 3:56p Stephanus
'  
'  11    8/28/03 11:34a Stephanus
'  
'  10    8/27/03 7:36p Stephanus
'  
'  9     8/27/03 7:35p Stephanus
'  
'  8     8/27/03 7:31p Stephanus
'  
'  7     8/27/03 2:47p Stephanus
'  
'  6     8/27/03 1:40p Stephanus
'  
'  5     8/27/03 11:03a Stephanus
'  
'  4     8/26/03 6:42p Stephanus
'  
'  3     8/26/03 6:12p Stephanus
'  
'  2     8/26/03 4:41p Stephanus
'  
'  1     8/26/03 11:05a Stephanus
'  
'  
'-----------------------------------------------------------
#End Region

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports AdIns.Framework.DataAccess
Imports System.Data.SqlClient
Imports AdIns.Application.Eloan.Business
Imports AdIns.Application.Eloan
Imports AdIns.Application.Eloan.Exceptions
#End Region


Public Class FundingCompany : Inherits DataAccessBase
    Private Const spFUNDINGCOMPANY_LIST As String = "spFUNDINGCOMPANYLIST"
    Private Const spFUNDINGCOMPANY_ADD As String = "spFUNDINGCOMPANYADD"
    Private Const spFUNDINGCOMPANY_BYID As String = "spFundingCompanyByID"
    Private Const spFUNDINGCOMPANY_EDIT As String = "spFundingCOMPANYEdit"
    Private Const spFUNDINGCOMPANY_DEL As String = "spFundingCOMPANYDelete"
    Private Const spFUNDINGCOMPANY_Report As String = "spFUNDINGCOMPANYReport"

    Private Const spFUNDINGCONTRACT_ADD As String = "spFUNDINGCONTRACTADD"
    Private Const spFUNDINGCONTRACT_BYID As String = "spFundingContractByID"
    Private Const spFUNDINGCONTRACT_EDIT As String = "spFundingCONTRACTEdit"
    Private Const spFUNDINGCONTRACT_DEL As String = "spFundingCONTRACTDelete"

    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_ADD As String = "spFUNDINGCONTRACTPlafondBranchADD"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_BYID As String = "spFundingContractPlafondBranchByID"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_EDIT As String = "spFundingCONTRACTPlafondBranchEdit"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_DEL As String = "spFundingCONTRACTPlafondbranchDelete"

    Private Const spFUNDINGCONTRACTDOC_ADD As String = "spFUNDINGCONTRACTDOCADD"
    Private Const spFUNDINGCONTRACTDOC_DEL As String = "spFundingCONTRACTDOCDelete"

    Private Const spFUNDINGCONTRACTBATCH_ADD As String = "spFUNDINGCONTRACTBATCHADD"
    Private Const spFUNDINGCONTRACTBATCHINS_ADD As String = "spFUNDINGCONTRACTBATCHINSADD"
    Private Const spFUNDINGCONTRACTBATCH_EDIT As String = "spFUNDINGCONTRACTBATCHEDIT"

    Private Const spFUNDINGUPDATEAGREEMENT As String = "spFundingUpdateAgreementSelected"
    Private Const spFUNDINGUPDATEAGREEMENTEXECUTION As String = "spFundingUpdateAgreementExecution"
    Private Const spFUNDINGUPDATEAGREEMENTSECONDEXECUTION As String = "spFUNDINGUPDATEAGREEMENTSECONDEXECUTION"

    Private Const SPFUNDINGDROWDOWNRECEIVE As String = "spFundingDrowdownReceive"
    Private Const spPaymentOutInstallmentProcess As String = "spFundingPaymentOutInstallmentProcess"

    Private Const spGETCOMBO_INPUTBANK As String = "select BankId, BankName from BANKMASTER"
    Private Const spFundingPrepaymentBPKBReplacingProcess As String = "spFundingPrepaymentBPKBReplacingProcess"
    Private Const spFundingPrepaymentPaymentOutProcess As String = "spFundingPrepaymentPaymentOutProcess"
    Private Const spFundingPaymentOutFeesProcess As String = "spFundingPaymentOutFeesProcess"
    Private Const spFundingAddFundingAgreementInst As String = "spFundingAddFundingAgreementInst"
    Private Const spFundingRescheduleAgreementInstallment As String = "spFundingRescheduleAgreementInstallment"
    Private Const spFundingUpdateContractBatchInst As String = "spFundingUpdateContractBatchInst"
    Private Const spFundingUncheckAgreement As String = "spFundingUncheckAgreement"
    Private Const spFundingChangeBatchDateProcess As String = "spFundingChangeBatchDateProcess"
    Private Const spFundingReschedulePrepaymentAgreementInstallment As String = "spFundingReschedulePrepaymentAgreementInstallment"

    Private Const SPFUNDINGCONTRACTNEGCOVMNT_GET As String = "spGetFundContractNegCov"
    Private Const SPFUNDINGCONTRACTNEGCOVMNT_UPDATE As String = "spUpdateFundContractNegCov"

#Region "FundingCompany"

#Region "FundingCompanyList"
    Public Function ListFundingCompany(ByVal customclass As Entities.FundingCompany) As Entities.FundingCompany

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingCompanyAdd"
    Public Sub FundingCompanyAdd(ByVal oCompany As Entities.FundingCompany, ByVal oClassAddress As Entities.Address, ByVal oClassPersonal As Entities.Personal)

        Dim params(22) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingCoyName", SqlDbType.VarChar, 50)
        params(2).Value = oCompany.FundingCoyName

        params(3) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(3).Value = oClassAddress.Address

        params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RT
        params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RW

        params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kelurahan
        params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kecamatan
        params(8) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.City
        params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(9).Value = oClassAddress.ZipCode
        params(10) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(10).Value = oClassAddress.AreaPhone1
        params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(11).Value = oClassAddress.Phone1
        params(12) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(12).Value = oClassAddress.AreaPhone2
        params(13) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(13).Value = oClassAddress.Phone2

        params(14) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(14).Value = oClassAddress.AreaFax
        params(15) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(15).Value = oClassAddress.Fax

        params(16) = New SqlParameter("@ContactName", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonName
        params(17) = New SqlParameter("@ContactJobTitle", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonTitle
        params(18) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
        params(18).Value = oClassPersonal.Email
        params(19) = New SqlParameter("@ContactMobilePhone", SqlDbType.VarChar, 20)
        params(19).Value = oClassPersonal.MobilePhone

        params(20) = New SqlParameter("@FundingCoBankBranchAccount", SqlDbType.VarChar, 30)
        params(20).Value = oCompany.FundingCoBankBranchAccount
        params(21) = New SqlParameter("@FundingCoBankAccountName", SqlDbType.VarChar, 50)
        params(21).Value = oCompany.FundingCoBankAccountName
        params(22) = New SqlParameter("@InterestCalculationOption", SqlDbType.Int)
        params(22).Value = oCompany.InterestCalculationOption


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exists with the same primary key !")
            'Throw New Exception(ex.Message & "1")
        End Try


    End Sub
#End Region

#Region "FundingCompanybyID"
    Public Function ListFundingCompanyByID(ByVal customclass As Entities.FundingCompany) As Entities.FundingCompany

        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingCoyID


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingCompanyEdit"
    Public Sub FundingCompanyEdit(ByVal oCompany As Entities.FundingCompany, ByVal oClassAddress As Entities.Address, ByVal oClassPersonal As Entities.Personal)

        Dim params(22) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingCoyName", SqlDbType.VarChar, 50)
        params(2).Value = oCompany.FundingCoyName

        params(3) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(3).Value = oClassAddress.Address

        params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RT
        params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RW

        params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kelurahan
        params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kecamatan
        params(8) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.City
        params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(9).Value = oClassAddress.ZipCode
        params(10) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(10).Value = oClassAddress.AreaPhone1
        params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(11).Value = oClassAddress.Phone1
        params(12) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(12).Value = oClassAddress.AreaPhone2
        params(13) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(13).Value = oClassAddress.Phone2

        params(14) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(14).Value = oClassAddress.AreaFax
        params(15) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(15).Value = oClassAddress.Fax

        params(16) = New SqlParameter("@ContactName", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonName
        params(17) = New SqlParameter("@ContactJobTitle", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonTitle
        params(18) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
        params(18).Value = oClassPersonal.Email
        params(19) = New SqlParameter("@ContactMobilePhone", SqlDbType.VarChar, 20)
        params(19).Value = oClassPersonal.MobilePhone

        params(20) = New SqlParameter("@FundingCoBankBranchAccount", SqlDbType.VarChar, 30)
        params(20).Value = oCompany.FundingCoBankBranchAccount
        params(21) = New SqlParameter("@FundingCoBankAccountName", SqlDbType.VarChar, 50)
        params(21).Value = oCompany.FundingCoBankAccountName
        params(22) = New SqlParameter("@InterestCalculationOption", SqlDbType.Int)
        params(22).Value = oCompany.InterestCalculationOption

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception("Update Failed!")
        End Try


    End Sub
#End Region

#Region "FundingCompanyDelete"
    Public Sub FundingCompanyDelete(ByVal oCompany As Entities.FundingCompany)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.Char, 20)
            params(0).Value = .FundingCoyID

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#Region "FundingCompanyReport"
    Public Function ListFundingCompanyReport(ByVal customclass As Entities.FundingCompany) As Entities.FundingCompany


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_Report).Tables(0)

        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#End Region

#Region "FundingContract"

#Region "FundingContractAdd"

    Public Sub FundingContractAdd(ByVal oCompany As Entities.FundingContract)

        Dim params(41) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@ContractName", SqlDbType.VarChar, 50)
        params(3).Value = oCompany.ContractName

        params(4) = New SqlParameter("@FlagCS", SqlDbType.Char, 1)
        params(4).Value = oCompany.FlagCS

        params(5) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(5).Value = oCompany.CurrencyID

        params(6) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.PlafondAmount

        params(7) = New SqlParameter("@ContractDate", SqlDbType.DateTime)
        params(7).Value = oCompany.ContractDate

        params(8) = New SqlParameter("@PeriodFrom", SqlDbType.DateTime)
        params(8).Value = oCompany.PeriodFrom

        params(9) = New SqlParameter("@PeriodTo", SqlDbType.DateTime)
        params(9).Value = oCompany.PeriodTo

        params(10) = New SqlParameter("@FacilityType", SqlDbType.Char, 1)
        params(10).Value = oCompany.FacilityType

        params(11) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(11).Value = oCompany.FinalMaturityDate

        params(12) = New SqlParameter("@EvaluationDate", SqlDbType.DateTime)
        params(12).Value = oCompany.EvaluationDate

        params(13) = New SqlParameter("@RateToFundingCoy", SqlDbType.Decimal)
        params(13).Value = oCompany.RateToFundingCoy

        params(14) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(14).Value = oCompany.InterestType

        params(15) = New SqlParameter("@InterestNotes", SqlDbType.VarChar, 50)
        params(15).Value = oCompany.InterestNotes

        params(16) = New SqlParameter("@LCPerDay", SqlDbType.Decimal)
        params(16).Value = oCompany.LCPerDay

        params(17) = New SqlParameter("@LCGracePeriod", SqlDbType.Int)
        params(17).Value = oCompany.LCGracePeriod

        params(18) = New SqlParameter("@FlagCoBranding", SqlDbType.Bit)
        params(18).Value = oCompany.FlagCoBranding

        params(19) = New SqlParameter("@FundingCoyPortion", SqlDbType.Int)
        params(19).Value = oCompany.FundingCoyPortion

        params(20) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
        params(20).Value = oCompany.PrepaymentPenalty

        params(21) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(21).Value = oCompany.PaymentScheme

        params(22) = New SqlParameter("@RecourseType", SqlDbType.Char, 1)
        params(22).Value = oCompany.RecourseType

        params(23) = New SqlParameter("@AcqActiveStatus", SqlDbType.Bit)
        params(23).Value = oCompany.AcqActiveStatus

        params(24) = New SqlParameter("@NegCovDate", SqlDbType.DateTime)
        params(24).Value = oCompany.NegCovDate

        params(25) = New SqlParameter("@CashHoldBack", SqlDbType.Decimal)
        params(25).Value = oCompany.CashHoldBack

        params(26) = New SqlParameter("@ContractStatus", SqlDbType.Char, 1)
        params(26).Value = oCompany.ContractStatus

        params(27) = New SqlParameter("@FloatingStart", SqlDbType.DateTime)
        params(27).Value = oCompany.FloatingStart

        params(28) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
        params(28).Value = oCompany.FacilityKind

        params(29) = New SqlParameter("@SecurityCoveragePercentage", SqlDbType.Decimal)
        params(29).Value = oCompany.SecurityCoveragePercentage

        params(30) = New SqlParameter("@SecurityCoverageType", SqlDbType.Char, 1)
        params(30).Value = oCompany.SecurityCoverageType

        params(31) = New SqlParameter("@SecurityType", SqlDbType.VarChar, 4)
        params(31).Value = oCompany.SecurityType

        params(32) = New SqlParameter("@BalanceSheetStatus", SqlDbType.Char, 1)
        params(32).Value = oCompany.BalanceSheetStatus

        params(33) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(33).Value = oCompany.ProvisionFeeAmount

        params(34) = New SqlParameter("@AdminFeePerAccount", SqlDbType.Decimal)
        params(34).Value = oCompany.AdminFeePerAccount

        params(35) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
        params(35).Value = oCompany.AdminFeeFacility

        params(36) = New SqlParameter("@AdminFeePerDrawDown", SqlDbType.Decimal)
        params(36).Value = oCompany.AdminFeePerDrawDown

        params(37) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
        params(37).Value = oCompany.CommitmentFee

        params(38) = New SqlParameter("@MaximumDateForDD", SqlDbType.Int)
        params(38).Value = oCompany.MaximumDateForDD

        params(39) = New SqlParameter("@PrepaymentType", SqlDbType.Char, 1)
        params(39).Value = oCompany.PrepaymentType

        params(40) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(40).Value = oCompany.AssetDocLocation

        params(41) = New SqlParameter("@CommitmentStatus", SqlDbType.Bit)
        params(41).Value = oCompany.CommitmentStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(Tech~icalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New exception(ex.Message & "1")
        End Try


    End Sub

#End Region

#Region "FundingSontractbyID"
    Public Function ListFundingContractByID(ByVal customclass As Entities.FundingContract) As Entities.FundingContract

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingContractNo
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingContractEdit"
    Public Sub FundingContractEdit(ByVal oCompany As Entities.FundingContract)

        Dim params(41) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@ContractName", SqlDbType.VarChar, 50)
        params(3).Value = oCompany.ContractName

        params(4) = New SqlParameter("@FlagCS", SqlDbType.Char, 1)
        params(4).Value = oCompany.FlagCS

        params(5) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(5).Value = oCompany.CurrencyID

        params(6) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.PlafondAmount

        params(7) = New SqlParameter("@ContractDate", SqlDbType.DateTime)
        params(7).Value = oCompany.ContractDate

        params(8) = New SqlParameter("@PeriodFrom", SqlDbType.DateTime)
        params(8).Value = oCompany.PeriodFrom

        params(9) = New SqlParameter("@PeriodTo", SqlDbType.DateTime)
        params(9).Value = oCompany.PeriodTo

        params(10) = New SqlParameter("@FacilityType", SqlDbType.Char, 1)
        params(10).Value = oCompany.FacilityType

        params(11) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(11).Value = oCompany.FinalMaturityDate

        params(12) = New SqlParameter("@EvaluationDate", SqlDbType.DateTime)
        params(12).Value = oCompany.EvaluationDate

        params(13) = New SqlParameter("@RateToFundingCoy", SqlDbType.Decimal)
        params(13).Value = oCompany.RateToFundingCoy

        params(14) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(14).Value = oCompany.InterestType

        params(15) = New SqlParameter("@InterestNotes", SqlDbType.VarChar, 50)
        params(15).Value = oCompany.InterestNotes

        params(16) = New SqlParameter("@LCPerDay", SqlDbType.Decimal)
        params(16).Value = oCompany.LCPerDay

        params(17) = New SqlParameter("@LCGracePeriod", SqlDbType.Int)
        params(17).Value = oCompany.LCGracePeriod

        params(18) = New SqlParameter("@FlagCoBranding", SqlDbType.Bit)
        params(18).Value = oCompany.FlagCoBranding

        params(19) = New SqlParameter("@FundingCoyPortion", SqlDbType.Int)
        params(19).Value = oCompany.FundingCoyPortion

        params(20) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
        params(20).Value = oCompany.PrepaymentPenalty

        params(21) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(21).Value = oCompany.PaymentScheme

        params(22) = New SqlParameter("@RecourseType", SqlDbType.Char, 1)
        params(22).Value = oCompany.RecourseType

        params(23) = New SqlParameter("@AcqActiveStatus", SqlDbType.Bit)
        params(23).Value = oCompany.AcqActiveStatus

        params(24) = New SqlParameter("@NegCovDate", SqlDbType.DateTime)
        params(24).Value = oCompany.NegCovDate

        params(25) = New SqlParameter("@CashHoldBack", SqlDbType.Decimal)
        params(25).Value = oCompany.CashHoldBack

        params(26) = New SqlParameter("@ContractStatus", SqlDbType.Char, 1)
        params(26).Value = oCompany.ContractStatus

        params(27) = New SqlParameter("@FloatingStart", SqlDbType.DateTime)
        params(27).Value = oCompany.FloatingStart

        params(28) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
        params(28).Value = oCompany.FacilityKind

        params(29) = New SqlParameter("@SecurityCoveragePercentage", SqlDbType.Decimal)
        params(29).Value = oCompany.SecurityCoveragePercentage

        params(30) = New SqlParameter("@SecurityCoverageType", SqlDbType.Char, 1)
        params(30).Value = oCompany.SecurityCoverageType

        params(31) = New SqlParameter("@SecurityType", SqlDbType.VarChar, 4)
        params(31).Value = oCompany.SecurityType

        params(32) = New SqlParameter("@BalanceSheetStatus", SqlDbType.Char, 1)
        params(32).Value = oCompany.BalanceSheetStatus

        params(33) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(33).Value = oCompany.ProvisionFeeAmount

        params(34) = New SqlParameter("@AdminFeePerAccount", SqlDbType.Decimal)
        params(34).Value = oCompany.AdminFeePerAccount

        params(35) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
        params(35).Value = oCompany.AdminFeeFacility

        params(36) = New SqlParameter("@AdminFeePerDrawDown", SqlDbType.Decimal)
        params(36).Value = oCompany.AdminFeePerDrawDown

        params(37) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
        params(37).Value = oCompany.CommitmentFee

        params(38) = New SqlParameter("@MaximumDateForDD", SqlDbType.Int)
        params(38).Value = oCompany.MaximumDateForDD

        params(39) = New SqlParameter("@PrepaymentType", SqlDbType.Char, 1)
        params(39).Value = oCompany.PrepaymentType

        params(40) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(40).Value = oCompany.AssetDocLocation

        params(41) = New SqlParameter("@CommitmentStatus", SqlDbType.Bit)
        params(41).Value = oCompany.CommitmentStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception("Update Failed!")
        End Try


    End Sub
#End Region

#Region "FundingContractDelete"
    Public Sub FundingContractDelete(ByVal oCompany As Entities.FundingContract)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region

#Region "FundingContractPlafondBranch"

#Region "FundingContractAdd"

    Public Sub FundingContractPlafondBranchAdd(ByVal oCompany As Entities.FundingContractBranch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oCompany.BranchId

        params(4) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(4).Value = oCompany.PlafondAmount

        params(5) = New SqlParameter("@BookAmount", SqlDbType.Decimal)
        params(5).Value = oCompany.BookAmount

        params(6) = New SqlParameter("@OSAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.OSAmount

        params(7) = New SqlParameter("@AcqStatus", SqlDbType.VarChar, 1)
        params(7).Value = oCompany.AcqStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub

#End Region

#Region "FundingContractbyID"
    Public Function ListFundingContractPlafondBranchByID(ByVal customclass As Entities.FundingContractBranch) As Entities.FundingContractBranch

        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingContractNo

        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customclass.BranchId


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingContractEdit"
    Public Sub FundingContractPlafondBranchEdit(ByVal oCompany As Entities.FundingContractBranch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oCompany.BranchId

        params(4) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(4).Value = oCompany.PlafondAmount

        params(5) = New SqlParameter("@BookAmount", SqlDbType.Decimal)
        params(5).Value = oCompany.BookAmount

        params(6) = New SqlParameter("@OSAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.OSAmount

        params(7) = New SqlParameter("@AcqStatus", SqlDbType.VarChar, 1)
        params(7).Value = oCompany.AcqStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub
#End Region

#Region "FundingContractDelete"
    Public Sub FundingContractPlafondBranchDelete(ByVal oCompany As Entities.FundingContractBranch)

        Dim params(1) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = .BranchId

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region

#Region "FundingContractDoc"

#Region "FundingContractDocAdd"

    Public Sub FundingContractDocAdd(ByVal oCompany As Entities.FundingContract)

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingDocId", SqlDbType.int)
        params(3).Value = oCompany.FundingDocId

        params(4) = New SqlParameter("@DocumentNote", SqlDbType.VarChar, 50)
        params(4).Value = oCompany.DocumentNote


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTDOC_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub

#End Region

#Region "FundingContractDocDelete"
    Public Sub FundingContractDocDelete(ByVal oCompany As Entities.FundingContract)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTDOC_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region


#Region "FundingContractBatchAdd"

    Public Sub FundingContractBatchAdd(ByVal oCompany As Entities.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(25) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@BatchDate", SqlDbType.DateTime)
        params(4).Value = oCompany.BatchDate

        params(5) = New SqlParameter("@PrincipalAmtToFunCoy", SqlDbType.Decimal)
        params(5).Value = oCompany.PrincipalAmtToFunCoy

        params(6) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
        params(6).Value = oCompany.InterestRate

        params(7) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(7).Value = oCompany.InterestType

        params(8) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(8).Value = oCompany.FinalMaturityDate

        params(9) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(9).Value = oCompany.PaymentScheme

        params(10) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(10).Value = oCompany.ProvisionFeeAmount

        params(11) = New SqlParameter("@AdminAmount", SqlDbType.Decimal)
        params(11).Value = oCompany.AdminAmount

        params(12) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(12).Value = oCompany.CurrencyID

        params(13) = New SqlParameter("@ExchangeRate", SqlDbType.Decimal)
        params(13).Value = oCompany.ExchangeRate

        params(14) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(14).Value = oCompany.Tenor

        params(15) = New SqlParameter("@InstallmentPeriod", SqlDbType.Char, 1)
        params(15).Value = oCompany.InstallmentPeriod

        params(16) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 1)
        params(16).Value = oCompany.InstallmentScheme

        params(17) = New SqlParameter("@ProposeDate", SqlDbType.DateTime)
        params(17).Value = oCompany.ProposeDate

        params(18) = New SqlParameter("@AragingDate", SqlDbType.DateTime)
        params(18).Value = oCompany.AragingDate

        params(19) = New SqlParameter("@RealizedDate", SqlDbType.DateTime)
        params(19).Value = oCompany.RealizedDate

        params(20) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(20).Value = oCompany.InstallmentDueDate

        params(21) = New SqlParameter("@AccProposedNum", SqlDbType.Int)
        params(21).Value = oCompany.AccProposedNum

        params(22) = New SqlParameter("@AccRealizedNum", SqlDbType.Int)
        params(22).Value = oCompany.AccRealizedNum

        params(23) = New SqlParameter("@OSAmtToFunCoy", SqlDbType.Decimal)
        params(23).Value = oCompany.OSAmtToFunCoy

        params(24) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(24).Value = oCompany.AssetDocLocation

        params(25) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        params(25).Value = oCompany.BankAccountID

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCH_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

#Region "FundingContractBatchInsAdd"

    Public Sub FundingContractBatchInsAdd(ByVal oCompany As Entities.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(11) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(4).Value = oCompany.InsSecNo

        params(5) = New SqlParameter("@DueDate", SqlDbType.DateTime)
        params(5).Value = oCompany.DueDate

        params(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.PrincipalAmount

        params(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
        params(7).Value = oCompany.InterestAmount

        params(8) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
        params(8).Value = oCompany.PrincipalPaidAmount

        params(9) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
        params(9).Value = oCompany.InterestPaidAmount

        params(10) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
        params(10).Value = oCompany.OSPrincipalAmount

        params(11) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
        params(11).Value = oCompany.OSInterestAmount


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCHINS_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

#Region " FundingContractBatchEdit"
    Public Sub FundingContractBatchEdit(ByVal oCompany As Entities.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(25) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@BatchDate", SqlDbType.DateTime)
        params(4).Value = oCompany.BatchDate

        params(5) = New SqlParameter("@PrincipalAmtToFunCoy", SqlDbType.Decimal)
        params(5).Value = oCompany.PrincipalAmtToFunCoy

        params(6) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
        params(6).Value = oCompany.InterestRate

        params(7) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(7).Value = oCompany.InterestType

        params(8) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(8).Value = oCompany.FinalMaturityDate

        params(9) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(9).Value = oCompany.PaymentScheme

        params(10) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(10).Value = oCompany.ProvisionFeeAmount

        params(11) = New SqlParameter("@AdminAmount", SqlDbType.Decimal)
        params(11).Value = oCompany.AdminAmount

        params(12) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(12).Value = oCompany.CurrencyID

        params(13) = New SqlParameter("@ExchangeRate", SqlDbType.Decimal)
        params(13).Value = oCompany.ExchangeRate

        params(14) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(14).Value = oCompany.Tenor

        params(15) = New SqlParameter("@InstallmentPeriod", SqlDbType.Char, 1)
        params(15).Value = oCompany.InstallmentPeriod

        params(16) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 1)
        params(16).Value = oCompany.InstallmentScheme

        params(17) = New SqlParameter("@ProposeDate", SqlDbType.DateTime)
        params(17).Value = oCompany.ProposeDate

        params(18) = New SqlParameter("@AragingDate", SqlDbType.DateTime)
        params(18).Value = oCompany.AragingDate

        params(19) = New SqlParameter("@RealizedDate", SqlDbType.DateTime)
        params(19).Value = oCompany.RealizedDate

        params(20) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(20).Value = oCompany.InstallmentDueDate

        params(21) = New SqlParameter("@AccProposedNum", SqlDbType.Int)
        params(21).Value = oCompany.AccProposedNum

        params(22) = New SqlParameter("@AccRealizedNum", SqlDbType.Int)
        params(22).Value = oCompany.AccRealizedNum

        params(23) = New SqlParameter("@OSAmtToFunCoy", SqlDbType.Decimal)
        params(23).Value = oCompany.OSAmtToFunCoy

        params(24) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(24).Value = oCompany.AssetDocLocation

        params(25) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        params(25).Value = oCompany.BankAccountID
        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCH_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception("Update Failed!")
        End Try


    End Sub
#End Region

#Region "FundingUpdateAgreementSelected"

    Public Sub FundingUpdateAgreementSelected(ByVal oCompany As Entities.FundingContractBatch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@ValidateAmount", SqlDbType.Decimal)
        params(0).Value = oCompany.PlafondAmount

        params(1) = New SqlParameter("@SecurityType", SqlDbType.Char, 1)
        params(1).Value = oCompany.SecurityType

        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = oCompany.BranchId

        params(3) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(3).Value = oCompany.BankId

        params(4) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(4).Value = oCompany.FundingCoyId

        params(5) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(5).Value = oCompany.FundingContractNo

        params(6) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(6).Value = oCompany.FundingBatchNo

        params(7) = New SqlParameter("@FundingBatchDate", SqlDbType.DateTime)
        params(7).Value = oCompany.BatchDate



        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

#Region "FundingUpdateAgreementExecution"

    Public Sub FundingUpdateAgreementExecution(ByVal oCompany As Entities.FundingContractBatch)

        Dim params(3) As SqlParameter


        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENTEXECUTION, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingUpdateAgreementSecondExecution(ByVal oCompany As Entities.FundingContractBatch)

        Dim params(3) As SqlParameter


        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENTSECONDEXECUTION, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub


#End Region

#Region "fundingUncheckAgreement"
    Public Sub FundingUncheckAgreement(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, customclass.spName, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUncheckAgreement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region

#Region "FundingContractNegCovMnt"

#Region "Function FundingContractNegCovGet"
    'Create By : Parang, june 11st 2004
    Public Function FundingContractNegCovGet(ByVal CustomClass As Entities.FundingContract) As Entities.FundingContract
        Dim oParams(1) As SqlParameter
        oParams(0) = New SqlParameter("@fudingCoyID", SqlDbType.VarChar, 20)
        oParams(0).Value = CustomClass.FundingCoyId
        oParams(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        oParams(1).Value = CustomClass.FundingContractNo
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, SPFUNDINGCONTRACTNEGCOVMNT_GET, oParams).Tables(0)
        Catch ex As Exception

        End Try

        Return CustomClass
    End Function
#End Region

#Region "Sub FundingContractNegCovUpdate"
    'Create By : Parang, june 11st 2004
    Public Sub FundingContractNegCovUpdate(ByVal strcon As String, ByVal setField As String, ByVal WhereBy As String)
        Dim oParams(1) As SqlParameter
        oParams(0) = New SqlParameter("@cmdSet", SqlDbType.VarChar, 100)
        oParams(0).Value = setField
        oParams(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 100)
        oParams(1).Value = WhereBy
        Try
            SqlHelper.ExecuteNonQuery(strcon, CommandType.StoredProcedure, SPFUNDINGCONTRACTNEGCOVMNT_UPDATE, oParams)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region

#End Region

#Region " MISC"
    Public Function GetComboInputBank(ByVal strcon As String) As DataTable

        Dim customClass As New Entities.BankMaster

        Try
            customClass.listdata = SqlHelper.ExecuteDataset(strcon, CommandType.Text, spGETCOMBO_INPUTBANK).Tables(0)
        Catch ex As Exception

        End Try

        Return customClass.listdata
    End Function

#End Region
#Region "DrownDownReceived"
    Public Sub FundingDrowDownReceive(ByVal customclass As Entities.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(18) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId

            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo

            params(4) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(4).Value = customclass.ValueDate

            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(5).Value = customclass.ReferenceNo

            params(6) = New SqlParameter("@ReferenceNo2", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo2

            params(7) = New SqlParameter("@ReferenceNo3", SqlDbType.VarChar, 20)
            params(7).Value = customclass.ReferenceNo3

            params(8) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(8).Value = customclass.BranchId

            params(9) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(9).Value = customclass.BankAccountID

            params(10) = New SqlParameter("@BankAccountID2", SqlDbType.VarChar, 10)
            params(10).Value = customclass.BankAccountID2

            params(11) = New SqlParameter("@BankAccountID3", SqlDbType.VarChar, 10)
            params(11).Value = customclass.BankAccountID3

            params(12) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(12).Value = customclass.LoginId

            params(13) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(13).Value = customclass.BusinessDate

            params(14) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(14).Value = customclass.CompanyID

            params(15) = New SqlParameter("@DrowDownAmount", SqlDbType.Decimal)
            params(15).Value = customclass.PrincipalAmtToFunCoy

            params(16) = New SqlParameter("@BankAccountAmount1", SqlDbType.Decimal)
            params(16).Value = customclass.AmtDrawDown1

            params(17) = New SqlParameter("@BankAccountAmount2", SqlDbType.Decimal)
            params(17).Value = customclass.AmtDrawDown2

            params(18) = New SqlParameter("@BankAccountAmount3", SqlDbType.Decimal)
            params(18).Value = customclass.AmtDrawDown3


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPFUNDINGDROWDOWNRECEIVE, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingDrowDownReceive", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "InstallmentPaymentOut"



    Public Function GetGeneralEditView(ByVal customClass As Entities.FundingContractBatch) As Entities.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyId
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingBatchNo
        params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(3).Value = customClass.BusinessDate
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            Return customClass
        Catch exp As eloanExceptions
            Dim err As New eloanExceptions
            err.WriteLog("FundingCompany", "GetGeneralEditView", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("FundingCompany", "GetGeneralEditView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub PaymentOutInstallmentProcess(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(17) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(4).Value = customclass.PrincipalPaidAmount
            params(5) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.InterestPaidAmount
            params(6) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.PenaltyPaidAmount
            params(7) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(7).Value = customclass.ValueDate
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(8).Value = customclass.ReferenceNo
            params(9) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(9).Value = customclass.BusinessDate
            params(10) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(10).Value = customclass.BankAccountID
            params(11) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(11).Value = customclass.BranchId
            params(12) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(12).Value = customclass.LoginId
            params(13) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(13).Value = customclass.CompanyID
            params(14) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(14).Value = customclass.InsSecNo
            params(15) = New SqlParameter("@PPHPaid", SqlDbType.Decimal)
            params(15).Value = customclass.PPHPaid
            params(16) = New SqlParameter("@PrincipalMustPaid", SqlDbType.Decimal)
            params(16).Value = customclass.AmtDrawDown1
            params(17) = New SqlParameter("@InterestMustPaid", SqlDbType.Decimal)
            params(17).Value = customclass.AmtDrawDown2

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPaymentOutInstallmentProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PaymentOutInstallmentProcess", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "Prepayment"
    Public Function GetPrepaymentEditView(ByVal customClass As Entities.FundingContractBatch) As Entities.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyId
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingBatchNo
        params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(3).Value = customClass.BusinessDate
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchIDApplication
        params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(5).Value = customClass.ApplicationID
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            Return customClass
        Catch exp As eloanExceptions
            Dim err As New eloanExceptions
            err.WriteLog("FundingCompany", "GetPrepaymentEditView", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("FundingCompany", "GetPrepaymentEditView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub PrepaymentBPKBReplacing(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(7) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(4).Value = customclass.ApplicationID
            params(5) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(5).Value = customclass.BranchId
            params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(6).Value = customclass.BusinessDate
            params(7) = New SqlParameter("@IsBatchPrepayment", SqlDbType.Bit)
            params(7).Value = customclass.IsBatchPrepayment
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPrepaymentBPKBReplacingProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PrepaymentBPKBReplacing", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub PrepaymentPaymentOut(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(22) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(4).Value = customclass.ReferenceNo
            params(5) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(5).Value = customclass.BankAccountID
            params(6) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ApplicationID
            params(7) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(7).Value = customclass.BranchId
            params(8) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(8).Value = customclass.LoginId
            params(9) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(9).Value = customclass.CompanyID
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = customclass.BusinessDate
            params(11) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(11).Value = customclass.ValueDate
            params(12) = New SqlParameter("@AccruedInterest", SqlDbType.Decimal)
            params(12).Value = customclass.AccruedInterest
            params(13) = New SqlParameter("@PrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = customclass.PrepaymentAmount
            params(14) = New SqlParameter("@OSPrincipalUndue", SqlDbType.Decimal)
            params(14).Value = customclass.OSPrincipalAmount
            params(15) = New SqlParameter("@PrepaymentPaidAmount", SqlDbType.Decimal)
            params(15).Value = customclass.PrincipalPaidAmount
            params(16) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
            params(16).Value = customclass.PenaltyPaidAmount
            params(17) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
            params(17).Value = customclass.PrepaymentPenalty
            params(18) = New SqlParameter("@BranchIDApplication", SqlDbType.Decimal)
            params(18).Value = customclass.BranchIDApplication
            params(19) = New SqlParameter("@InterestForThisMonth", SqlDbType.Decimal)
            params(19).Value = customclass.InterestForThisMonth
            params(20) = New SqlParameter("@IsBatchPrepayment", SqlDbType.Bit)
            params(20).Value = customclass.IsBatchPrepayment
            params(21) = New SqlParameter("@PrincipalMustPaid", SqlDbType.Decimal)
            params(21).Value = customclass.AmtDrawDown1
            params(22) = New SqlParameter("@InterestMustPaid", SqlDbType.Decimal)
            params(22).Value = customclass.AmtDrawDown2

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPrepaymentPaymentOutProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PrepaymentPaymentOut", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "PaymentOutFees"
    Public Sub PaymentOutFees(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(14) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
            params(4).Value = customclass.AdminFeeFacility
            params(5) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
            params(5).Value = customclass.CommitmentFee
            params(6) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
            params(6).Value = customclass.ProvisionFeeAmount
            params(7) = New SqlParameter("@OtherFeeAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OtherFeeAmount
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(8).Value = customclass.ReferenceNo
            params(9) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(9).Value = customclass.BankAccountID
            params(10) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(10).Value = customclass.BranchId
            params(11) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(12).Value = customclass.CompanyID
            params(13) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(13).Value = customclass.ValueDate
            params(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(14).Value = customclass.BusinessDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPaymentOutFeesProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PaymentOutFees", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
    Public Sub AddFundingAgreementInstallment(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(17) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(3).Value = customclass.DueDate
            params(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(4).Value = customclass.PrincipalAmount
            params(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(5).Value = customclass.InterestAmount
            params(6) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.PrincipalPaidAmount
            params(7) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(7).Value = customclass.InterestPaidAmount
            params(8) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSPrincipalAmount
            params(9) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(9).Value = customclass.OSInterestAmount
            params(10) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingCoyId
            params(11) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingContractNo
            params(12) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(12).Value = customclass.FundingBatchNo
            params(13) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(13).Value = customclass.PrepaymentAmount

            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(15).Value = customclass.InterestRate
            params(16) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.LCGracePeriod
            params(17) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(17).Value = customclass.MaximumDateForDD

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingAddFundingAgreementInst, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "AddFundingAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub FundingRescheduleAgreementInstallment(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(16) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(3).Value = customclass.PrincipalAmount
            params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(4).Value = customclass.InterestAmount
            params(5) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.PrincipalPaidAmount
            params(6) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.InterestPaidAmount
            params(7) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OSPrincipalAmount
            params(8) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSInterestAmount
            params(9) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(9).Value = customclass.FundingCoyId
            params(10) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingContractNo
            params(11) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingBatchNo
            params(12) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(12).Value = customclass.PrepaymentAmount
            params(13) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(13).Value = customclass.InterestRate
            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(15).Value = customclass.LCGracePeriod
            params(16) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.MaximumDateForDD

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingRescheduleAgreementInstallment, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingRescheduleAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub FundingReschedulePrepaymentAgreementInstallment(ByVal customclass As Entities.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(16) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(3).Value = customclass.PrincipalAmount
            params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(4).Value = customclass.InterestAmount
            params(5) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.PrincipalPaidAmount
            params(6) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.InterestPaidAmount
            params(7) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OSPrincipalAmount
            params(8) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSInterestAmount
            params(9) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(9).Value = customclass.FundingCoyId
            params(10) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingContractNo
            params(11) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingBatchNo
            params(12) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(12).Value = customclass.PrepaymentAmount
            params(13) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(13).Value = customclass.InterestRate

            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(15).Value = customclass.LCGracePeriod
            params(16) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.MaximumDateForDD
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingReschedulePrepaymentAgreementInstallment, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingReschedulePrepaymentAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub FundingUpdateContractBatchInst(ByVal oCompany As Entities.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(11) {}
        Dim objCon As New SqlConnection(oCompany.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
            params(0).Value = oCompany.BankId

            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = oCompany.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = oCompany.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = oCompany.FundingBatchNo

            params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(4).Value = oCompany.InsSecNo

            params(5) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(5).Value = oCompany.PrincipalAmount

            params(6) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(6).Value = oCompany.InterestAmount

            params(7) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(7).Value = oCompany.PrincipalPaidAmount

            params(8) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(8).Value = oCompany.InterestPaidAmount

            params(9) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(9).Value = oCompany.OSPrincipalAmount

            params(10) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(10).Value = oCompany.OSInterestAmount

            params(11) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(11).Value = oCompany.InterestRate



            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingUpdateContractBatchInst, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUpdateContractBatchInst", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#Region "DueDateChangeProcess"
    Public Sub DueDateChangeSave(ByVal customClass As Entities.FundingContractBatch)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0
        Try
            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(3) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
            params(4) = New SqlParameter("@DueDate", SqlDbType.VarChar, 10)
            params(5) = New SqlParameter("@InsSeqNo", SqlDbType.Int)

            If customClass.ListData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customClass.ListData.Rows.Count - 1
                    i = i + 1
                    params(0).Value = customClass.FundingCoyId
                    params(1).Value = customClass.FundingContractNo
                    params(2).Value = customClass.FundingBatchNo
                    params(3).Value = customClass.FacilityKind
                    params(4).Value = customClass.ListData.Rows(intLoopOmset).Item("DueDate")
                    params(5).Value = CInt(customClass.ListData.Rows(intLoopOmset).Item("InSeqNo"))
                    SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spFundingChangeBatchDateProcess, params)
                Next
            End If
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("DChange", "DueDateChangeSave", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region
End Class
