﻿#Region "Code Disclaimer"
'Copyright Microsoft Corporation 2003
'We grant you a nonexclusive, royalty free right to use and modify sample code provided hereunder (“TCMResip POC”)
'and to reproduce and distribute the object code form of the Sample Code, provided that you agree:
'(i) to not use our name, logo, or trademarks to market your software product; 
'(ii) to include a valid copyright notice on your software product; and 
'(iii) to indemnify, hold harmless, and defend us and our suppliers from and against any claims or lawsuits, 
'     including attorneys’ fees, that arise or result from the use or distribution of your software product.
'All rights not expressly granted, are reserved.
#End Region



#Region "Imports"
Imports System.Configuration
Imports System.Reflection
Imports System.Runtime.Remoting
Imports System.Web
Imports Maxiloan
Imports Maxiloan.Framework.Reflection

#End Region

''' <summary>
''' This is the base class to be used for all implementations of component factories.
''' This base class encapulates the reading of the configuration file to determine whether a component should be created locally or remotely
''' </summary>
Public Class ComponentFactoryBase

#Region "Declares and constants"
    ''' <summary>
    ''' The key to the configuration file section
    ''' </summary>
    Private Shared REMOTE_CONFIG_SECTION As String = "RemotingConfiguration"
    ''' <summary>
    ''' Holds the values from the configuration file
    ''' </summary>
    Private Shared m_configSettings As RemotingConfigurationSettings
#End Region

#Region "Constructors"
    ''' <summary>
    ''' Shared constructor to insure that the following code is only run once
    ''' </summary>
    Shared Sub New()
        Dim remotingFilename As String

        Try

            'load the configuration file into memory
            m_configSettings = CType(ConfigurationManager.GetSection(REMOTE_CONFIG_SECTION), RemotingConfigurationSettings)

            'configure the channels
            remotingFilename = m_configSettings.RemotingConfigurationFilename
            If remotingFilename <> String.Empty Then
                If remotingFilename.ToLower = "web.config" Then
                    'RemotingConfiguration.Configure(HttpContext.Current.Server.MapPath("web.config"))
                    RemotingConfiguration.Configure(HttpContext.Current.Request.ServerVariables("APPL_PHYSICAL_PATH") & "web.config", False)
                Else
                    RemotingConfiguration.Configure(remotingFilename, False)
                End If
            End If

        Catch ex As Exception

            Throw
        End Try
    End Sub
#End Region

#Region "Public shared functions and procedures"

    ''' <summary>
    ''' Create a component.  This shared function reads the configuration file and determines where a file should be created locally or remotely
    ''' </summary>
    ''' <param name="componentKey">the key to read in the configuration file</param>
    ''' <returns>the object that was created</returns>
    Public Shared Function CreateComponent(ByVal componentKey As String) As Object
        Dim objectTypeSettings As TypeSettings

        Try

            If Not m_configSettings.ContainsType(componentKey) Then                
                Throw New Exception("Error")
            End If

            objectTypeSettings = m_configSettings.Types(componentKey)

            If objectTypeSettings.Mode = ModeTypes.Local Then

                Return ActivationFactory.Create(objectTypeSettings.Type)

            Else

                Dim typeInfo() As String = objectTypeSettings.Type.Split(",".ToCharArray)
                Dim assemblyInstance As [Assembly] = [Assembly].Load(typeInfo(1).Trim)
                Dim typeInstance As Type = assemblyInstance.GetType(typeInfo(0).Trim)
                Return Activator.GetObject(typeInstance, objectTypeSettings.ObjectUri)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

#End Region

End Class
