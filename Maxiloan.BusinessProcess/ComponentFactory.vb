﻿#Region "Imports"
Imports System.Runtime.Remoting
Imports System.Web
Imports System.Configuration
Imports Maxiloan.Interface
#End Region

Public Class ComponentFactory : Inherits ComponentFactoryBase

    Public Shared Function CreateAuthorize() As IAuthorize
        Try
            Return CType(CreateComponent("Authorize"), IAuthorize)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateLogin() As ILogin
        Try
            Return CType(CreateComponent("Login"), ILogin)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateUserManagementUserList() As IUserManagementUserList
        Return CType(CreateComponent("UserManagementUserList"), IUserManagementUserList)
    End Function

    Public Shared Function CreateUserManagementUserApplication() As IUserManagementUserApplication
        Return CType(CreateComponent("UserManagementUserApplication"), IUserManagementUserApplication)
    End Function

    Public Shared Function CreateUserManagementUserGroupDB() As IUserManagementUserGroupDB
        Return CType(CreateComponent("UserManagementUserGroupDB"), IUserManagementUserGroupDB)
    End Function

    Public Shared Function CreateUserManagementUserGroupMenu() As IUserManagementUserGroupMenu
        Return CType(CreateComponent("UserManagementUserGroupMenu"), IUserManagementUserGroupMenu)
    End Function

    Public Shared Function CreateUserManagementMasterForm() As IUserManagementMasterForm
        Return CType(CreateComponent("UserManagementMasterForm"), IUserManagementMasterForm)
    End Function

    Public Shared Function CreateUserManagementGroupUser() As IUserManagementGroupUser
        Return CType(CreateComponent("UserManagementGroupUser"), IUserManagementGroupUser)
    End Function

    Public Shared Function CreateFeature() As IFeature
        Return CType(CreateComponent("Feature"), IFeature)
    End Function

    Public Shared Function CreateSystemAlert() As ISystemAlert
        Try
            Return CType(CreateComponent("SystemAlert"), ISystemAlert)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInfoBox() As IInfoBox
        Try
            Return CType(CreateComponent("InfoBox"), IInfoBox)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateNews() As INews
        Try
            Return CType(CreateComponent("News"), INews)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Shared Function CreateBank() As IBank
        Return CType(CreateComponent("Bank"), IBank)
    End Function

    Public Shared Function CreateAdvanceRequest() As IAdvanceRequest
        Return CType(CreateComponent("AdvanceRequest"), IAdvanceRequest)
    End Function

    Public Shared Function CreateCashier() As ICashier
        Return CType(CreateComponent("Cashier"), ICashier)
    End Function

    Public Shared Function CreateCashierTransaction() As ICashierTransaction
        Return CType(CreateComponent("CashierTransaction"), ICashierTransaction)
    End Function

    Public Shared Function CreateCheckCashier() As ICheckCashier
        Return CType(CreateComponent("CheckCashier"), ICheckCashier)
    End Function

    Public Shared Function CreateBGMnt() As IBGMnt
        Try
            Return CType(CreateComponent("BGMnt"), IBGMnt)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAPDisbSelec() As IAPDisbSelec
        Try
            Return CType(CreateComponent("APDisbSelec"), IAPDisbSelec)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAPDisbApp() As IAPDisbApp
        Try
            Return CType(CreateComponent("APDisbApp"), IAPDisbApp)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAPDisbInq() As IAPDisbInq
        Try
            Return CType(CreateComponent("APDisbInq"), IAPDisbInq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateViewAPSupplier() As IViewAPSupplier
        Try
            Return CType(CreateComponent("ViewAPSupplier"), IViewAPSupplier)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInstallmentDueDate() As ICashMgtPrint
        Try
            Return CType(CreateComponent("InstallmentDueDate"), ICashMgtPrint)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateAccPay() As IAccPay
        Try
            Return CType(CreateComponent("AccPay"), IAccPay)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateBankPendingReq() As IEBankPendingReq
        Try
            Return CType(CreateComponent("BankPendingReq"), IEBankPendingReq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateKonfirmasiEbankExec() As IEKonfirmasiEbankExec
        Try
            Return CType(CreateComponent("KonfirmasiEbankExec"), IEKonfirmasiEbankExec)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateJournalScheme() As IJournalScheme
        Return CType(CreateComponent("JournalScheme"), IJournalScheme)
    End Function

    Public Shared Function CreatePaymentAllocation() As IPaymentAllocation
        Return CType(CreateComponent("PaymentAllocation"), IPaymentAllocation)
    End Function

    Public Shared Function CreatePDCReceive() As IPDCReceive
        Try
            Return CType(CreateComponent("PDCReceive"), IPDCReceive)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreatePDCMTrans() As IPDCMTrans
        Return CType(CreateComponent("PDCMTrans"), IPDCMTrans)
    End Function

    Public Shared Function CreatePDCDeposit() As IPDCDeposit
        Return CType(CreateComponent("PDCDeposit"), IPDCDeposit)
    End Function


    Public Shared Function CreatePDCChangeStatus() As IPDCStatus
        Return CType(CreateComponent("PDCChangeStatus"), IPDCStatus)
    End Function

    Public Shared Function CreatePDCRInquiry() As IPDCRInquiry
        Return CType(CreateComponent("PDCRInquiry"), IPDCRInquiry)
    End Function

    Public Shared Function CreatePDCClearReconcile() As IPDCClearRecon
        Return CType(CreateComponent("PDCClearReconcile"), IPDCClearRecon)
    End Function

    Public Shared Function CreatePDCMultiAgreement() As IPDCMultiAgreement
        Return CType(CreateComponent("PDCMultiAgreement"), IPDCMultiAgreement)
    End Function

    Public Shared Function CreateAmortization() As IAmortization
        Return CType(CreateComponent("Amortization"), IAmortization)
    End Function

    Public Shared Function CreateInstallmentReceive() As IInstallRcv
        Return CType(CreateComponent("InstallRCV"), IInstallRcv)
    End Function

    Public Shared Function CreateInvoiceOL() As IInvoiceOL
        Return CType(CreateComponent("InvoiceOL"), IInvoiceOL)
    End Function

    Public Shared Function CreatePaymentInfo() As IPaymentInfo
        Return CType(CreateComponent("PaymentInfo"), IPaymentInfo)
    End Function

    Public Shared Function CreatePaymentHistoryList() As IPaymentHistory
        Return CType(CreateComponent("PaymentHistoryList"), IPaymentHistory)
    End Function

    Public Shared Function CreateCashBankVoucher() As ICashBankVoucher
        Return CType(CreateComponent("CashBankVoucher"), ICashBankVoucher)
    End Function

    Public Shared Function CreatePrepayment() As IFullPrepay
        Return CType(CreateComponent("PrePayment"), IFullPrepay)
    End Function

    Public Shared Function CreateSuspendReceive() As ISuspendReceive
        Try
            Return CType(CreateComponent("SuspendReceive"), ISuspendReceive)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateSuspendReversal() As ISuspendReversal
        Try
            Return CType(CreateComponent("SuspendReversal"), ISuspendReversal)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateSuspendAllocation() As ISuspendAllocation
        Try
            Return CType(CreateComponent("SuspendAllocation"), ISuspendAllocation)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateSuspendInquiry() As ISuspendInquiry
        Try
            Return CType(CreateComponent("SuspendInquiry"), ISuspendInquiry)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateRefundInquiry() As IRefundInq
        Try
            Return CType(CreateComponent("RefundInquiry"), IRefundInq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateRefundRequest() As IRefundReq
        Try
            Return CType(CreateComponent("RefundRequest"), IRefundReq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateRefundRequestAdvance() As IRefundReqAdv
        Try
            Return CType(CreateComponent("RefundRequestAdvance"), IRefundReqAdv)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateOtherReceive() As IOtherReceive
        Try
            Return CType(CreateComponent("OtherReceive"), IOtherReceive)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateOtherDisburse() As IOtherDisburse
        Try
            Return CType(CreateComponent("OtherDisburse"), IOtherDisburse)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateOtherDisburseAgreementRelated() As IOtherDisburseAgreementRelated
        Try
            Return CType(CreateComponent("OtherDisburseAgreementRelated"), IOtherDisburseAgreementRelated)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateOtherReceiveAgreementRelated() As IOtherReceiveAgreementRelated
        Try
            Return CType(CreateComponent("OtherReceiveAgreementRelated"), IOtherReceiveAgreementRelated)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateTransferFundReconcile() As ITransferFundReconcile
        Try
            Return CType(CreateComponent("TransferFundReconcile"), ITransferFundReconcile)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreatePaymentRequest() As IPaymentRequest
        Try
            Return CType(CreateComponent("PaymentRequest"), IPaymentRequest)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Public Shared Function CreateTransferFundRequest() As ITransferFundRequest
    '    Try
    '        Return CType(CreateComponent("TransferFundRequest"), ITransferFundRequest)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function

    Public Shared Function CreatePaymentReversal() As IPaymentReversal
        Return CType(CreateComponent("PaymentReversal"), IPaymentReversal)
    End Function

    Public Shared Function CreatePDCBounce() As IPDCBounce
        Try
            Return CType(CreateComponent("PDCBounce"), IPDCBounce)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateLCWaived() As ILCWaived
        Try
            Return CType(CreateComponent("LCWaived"), ILCWaived)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateArAnalysisReport() As IArAnalysis
        Return CType(CreateComponent("ArAnalysisReport"), IArAnalysis)
    End Function

    Public Shared Function CreateRescheduling() As IRescheduling
        Try
            Return CType(CreateComponent("Rescheduling"), IRescheduling)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateFloating() As IFloating
        Try
            Return CType(CreateComponent("Floating"), IFloating)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAssetType() As IAssetType
        Return CType(CreateComponent("AssetType"), IAssetType)
    End Function

    Public Shared Function CreateZipCode() As IZipCode
        Return CType(CreateComponent("MasterZipCode"), IZipCode)
    End Function

    Public Shared Function CreateFiducia() As IFiducia
        Return CType(CreateComponent("Fiducia"), IFiducia)
    End Function

    Public Shared Function CreateApplication() As IApplication
        Return CType(CreateComponent("Application"), IApplication)
    End Function

    Public Shared Function CreateAppCreation() As IApplicationCreation
        Return CType(CreateComponent("ApplicationCreation"), IApplicationCreation)
    End Function
    Public Shared Function CreateProspect() As IProspect
        Return CType(CreateComponent("Prospect"), IProspect)
    End Function
    Public Shared Function CreateProspectAssignAppraiser() As IProspectAssignAppraiser
        Return CType(CreateComponent("ProspectAssignAppraiser"), IProspectAssignAppraiser)
    End Function
    Public Shared Function CreateProspectResultAppraiser() As IProspectResultAppraiser
        Return CType(CreateComponent("ProspectResultAppraiser"), IProspectResultAppraiser)
    End Function


    Public Shared Function ApplicationMaintenance() As IEditApplication
        Return CType(CreateComponent("ApplicationMaintenance"), IEditApplication)
    End Function

    Public Shared Function CreateEBankTransfer() As IEBankTranfer
        Try
            Return CType(CreateComponent("EBankTransfer"), IEBankTranfer)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#Region " RCA "
    Public Shared Function CreateRCA() As IRCA
        Return CType(CreateComponent("RCA"), IRCA)
    End Function
#End Region

#Region " Pending "
    Public Shared Function CreatePending() As IPending
        Return CType(CreateComponent("Pending"), IPending)
    End Function
#End Region

#Region "CreateNewAppInsurance"
    Public Shared Function CreateNewAppInsurance() As INewAppInsurance
        Return CType(CreateComponent("NewAppInsurance"), INewAppInsurance)
    End Function
#End Region

#Region "CreateNewAppInsuranceByCompany"
    Public Shared Function CreateNewAppInsuranceByCompany() As INewAppInsuranceByCompany
        Return CType(CreateComponent("NewAppInsuranceByCompany"), INewAppInsuranceByCompany)
    End Function
#End Region

#Region "CreateNewAppInsuranceByCust"
    Public Shared Function CreateNewAppInsuranceByCust() As INewAppInsuranceByCust
        Return CType(CreateComponent("NewAppInsuranceByCust"), INewAppInsuranceByCust)
    End Function

#End Region

#Region "CreateInsuranceApplication"
    Public Shared Function CreateInsuranceCalculation() As IInsuranceCalculation
        Return CType(CreateComponent("InsuranceCalculation"), IInsuranceCalculation)
    End Function
#End Region

#Region "CreateInsuranceCalculationResult"
    Public Shared Function CreateInsuranceCalculationResult() As IInsuranceCalculationResult
        Return CType(CreateComponent("InsuranceCalculationResult"), IInsuranceCalculationResult)
    End Function
#End Region

#Region " Customer "
    Public Shared Function CreateCustomer() As ICustomer
        Return CType(CreateComponent("Customer"), ICustomer)
    End Function
#End Region

#Region " AssetData "
    Public Shared Function CreateAssetData() As IAssetData
        Return CType(CreateComponent("AssetData"), IAssetData)
    End Function
#End Region

#Region " FinancialData "
    Public Shared Function CreateFinancialData() As IFinancialData
        Return CType(CreateComponent("FinancialData"), IFinancialData)
    End Function
#End Region

#Region " Customer Facility "
    Public Shared Function CreateCustomerFacility() As ICustomerFacility
        Return CType(CreateComponent("CustomerFacility"), ICustomerFacility)
    End Function
#End Region
    Public Shared Function CreateTransferAccount() As ITransferAccount
        Try
            Return CType(CreateComponent("TransferAccount"), ITransferAccount)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateTransferHOBranch() As ITransferHOBranch
        Try
            Return CType(CreateComponent("TransferHOBranch"), ITransferHOBranch)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateBankReconcile() As IBankReconcile
        Try
            Return CType(CreateComponent("BankReconcile"), IBankReconcile)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#Region "PPK"
    Public Shared Function CreatePPK() As IPPK
        Try
            Return CType(CreateComponent("PPK"), IPPK)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Pernyataan Setuju"
    Public Shared Function CreatePernyataanSetuju() As IPernyataanSetuju
        Try
            Return CType(CreateComponent("PernyataanSetuju"), IPernyataanSetuju)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Loan Activation List"
    Public Shared Function CreateLoanActivation() As ILoanActivationReport
        Try
            Return CType(CreateComponent("LoanActivation"), ILoanActivationReport)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Agent Fee Receipt"
    Public Shared Function CreateAgentFeeReceipt() As IPrint
        Try
            Return CType(CreateComponent("AgentFeeReceipt"), IPrint)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Credit Funding Receipt"
    Public Shared Function CreateCreditFundingReceipt() As IPrint
        Try
            Return CType(CreateComponent("CreditFundingReceipt"), IPrint)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Data Entry"
    Public Shared Function CreateCreditScoring() As ICreditScoring
        Try
            Return CType(CreateComponent("CreditScoring"), ICreditScoring)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreatePO() As IPO
        Try
            Return CType(CreateComponent("PO"), IPO)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateDO() As IDO
        Try
            Return CType(CreateComponent("DO"), IDO)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateInvoice() As IInvoice
        Try
            Return CType(CreateComponent("Invoice"), IInvoice)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Sales With Condition"

    Public Shared Function CreateSalesCondition() As ISalesWithCondition
        Try
            Return CType(CreateComponent("SalesCondition"), ISalesWithCondition)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "ActivityLog"
    Public Shared Function CreateActivityLog() As IActivityLog
        Try
            Return CType(CreateComponent("ActivityLog"), IActivityLog)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

    Public Shared Function CreateInventorySelling() As IInvSelling
        Try
            Return CType(CreateComponent("InventorySelling"), IInvSelling)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAssetReplacement() As IAssetReplacement
        Try
            Return CType(CreateComponent("AssetReplacement"), IAssetReplacement)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateSuratKuasaFiducia() As IPrint
        Try
            Return CType(CreateComponent("SuratKuasaFiducia"), IPrint)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInsEndorsment() As IInsEndorsment
        Try
            Return CType(CreateComponent("InsEndorsment"), IInsEndorsment)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#Region "InsRateCategory"
    Public Shared Function InsRateCategory() As IInsuranceAssetCategory
        Return CType(CreateComponent("InsRateCategory"), IInsuranceAssetCategory)
    End Function
#End Region

    Public Shared Function CreatePremiumToCustomer() As IPremiumToCustomer
        Return CType(CreateComponent("PremiumToCustomer"), IPremiumToCustomer)
    End Function


    Public Shared Function CreateInsuranceStandardPremium() As IInsuranceStandardPremium
        Return CType(CreateComponent("InsuranceStandardPremium"), IInsuranceStandardPremium)
    End Function

    Public Shared Function CreateInsuranceQuotaStatistic() As IInsQuotaStatistic
        Return CType(CreateComponent("InsuranceQuotaStatistic"), IInsQuotaStatistic)
    End Function

    Public Shared Function CreateInsuranceCompanySelection() As IInsCoAllocation
        Return CType(CreateComponent("InsuranceCompanySelection"), IInsCoAllocation)
    End Function

    Public Shared Function CreateInsDocSelection() As IInsDocSelection
        Return CType(CreateComponent("InsDocSelection"), IInsDocSelection)
    End Function

    Public Shared Function CreateInsCoAllocationDetailList() As IInsCoAllocationDetailList
        Return CType(CreateComponent("InsCoAllocationDetailList"), IInsCoAllocationDetailList)
    End Function

    Public Shared Function CreateRenewal() As IRenewal
        Return CType(CreateComponent("Renewal"), IRenewal)
    End Function

    Public Shared Function CreateSPPA() As ISPPA
        Return CType(CreateComponent("SPPA"), ISPPA)
    End Function

    Public Shared Function CreateInsurancePolicyReceive() As IPolicyReceive
        Return CType(CreateComponent("InsurancePolicyReceive"), IPolicyReceive)
    End Function

    Public Shared Function CreatePolicyDetail() As IPolicyDetail
        Return CType(CreateComponent("PolicyDetail"), IPolicyDetail)
    End Function

    Public Shared Function CreatePolicyNumber() As IInsurancePolicyNumber
        Return CType(CreateComponent("PolicyNumber"), IInsurancePolicyNumber)
    End Function

    Public Shared Function CreateInsuranceAssetDetail() As IInsuranceAssetDetail
        Return CType(CreateComponent("InsuranceAssetDetail"), IInsuranceAssetDetail)
    End Function

    Public Shared Function CreateInsuranceBillingSelect() As IInsuranceBillingSelect
        Return CType(CreateComponent("InsuranceBillingSelect"), IInsuranceBillingSelect)
    End Function

    Public Shared Function CreateInsuranceInq() As IInsuranceInq
        Return CType(CreateComponent("InsuranceInq"), IInsuranceInq)
    End Function

#Region "InsuranceClaim"
    Public Shared Function CreateInsuranceClaim() As IInsClaimReceive
        Return CType(CreateComponent("InsClaimReceive"), IInsClaimReceive)
    End Function
#End Region

    Public Shared Function CreateViewPV() As IViewPV
        Return CType(CreateComponent("ViewPV"), IViewPV)
    End Function

    Public Shared Function CreateAdvClaimReq() As IAdvClaimReq
        Return CType(CreateComponent("AdvClaimReq"), IAdvClaimReq)
    End Function


    Public Shared Function CreateInsTerminate() As IInsTerminate
        Return CType(CreateComponent("InsTerminate"), IInsTerminate)
    End Function

    Public Shared Function CreateInsInqRefund() As IInsInqRefund
        Return CType(CreateComponent("InsInqRefund"), IInsInqRefund)
    End Function

    Public Shared Function CreateInsNewCover() As IInsNewCover
        Return CType(CreateComponent("InsNewCover"), IInsNewCover)
    End Function

    Public Shared Function CreateInsRefundRpt() As IInsRefundRpt
        Return CType(CreateComponent("InsRefundRpt"), IInsRefundRpt)
    End Function

    Public Shared Function CreateInsClaimOutstanding() As IInsClaimOutstanding
        Return CType(CreateComponent("InsClaimOutstanding"), IInsClaimOutstanding)
    End Function

    Public Shared Function CreateInsClaimFinished() As IInsClaimFinished
        Return CType(CreateComponent("InsClaimFinished"), IInsClaimFinished)
    End Function

    Public Shared Function CreateInsEndorsmentReport() As IInsEndorsmentReport
        Return CType(CreateComponent("InsEndorsmentReport"), IInsEndorsmentReport)
    End Function

    Public Shared Function InsuranceDue() As IInsuranceDue
        Return CType(CreateComponent("InsuranceDue"), IInsuranceDue)
    End Function

#Region "CreditScoreComponent"
    Public Shared Function CreditScoreComponent() As ICredit
        Return CType(CreateComponent("CreditScore"), ICredit)
    End Function
#End Region

#Region "CreditScoringCard"
    Public Shared Function CreditScoringCard() As ICreditScoringMain
        Return CType(CreateComponent("CreditScoringCard"), ICreditScoringMain)
    End Function
#End Region

#Region "Product"
    Public Shared Function Product() As IProduct
        Return CType(CreateComponent("Product"), IProduct)
    End Function
#End Region

    Public Shared Function CreateInsCoSelection() As IInsCoSelection
        Return CType(CreateComponent("InsCoSelection"), IInsCoSelection)
    End Function

#Region "Supplier"
    Public Shared Function CreateSupplier() As ISupplier
        Return CType(CreateComponent("Supplier"), ISupplier)
    End Function
#End Region

#Region "SalesReport"
    Public Shared Function CreateSalesReport() As ISalesReport
        Return CType(CreateComponent("SalesReport"), ISalesReport)
    End Function
#End Region

#Region "NegativeCustomer"
    Public Shared Function CreateNegativeCustomer() As INegativeCustomer
        Return CType(CreateComponent("NegativeCustomer"), INegativeCustomer)
    End Function
#End Region

    Public Shared Function CreateIncentiveCard() As IIncentiveCard
        Return CType(CreateComponent("IncentiveCard"), IIncentiveCard)
    End Function


    Public Shared Function CreateAODirectSales() As IAODirectSales
        Return CType(CreateComponent("AODirectSales"), IAODirectSales)
    End Function

#Region "NewApplication"
    Public Shared Function MktNewApp() As IMktNewApp
        Return CType(CreateComponent("MktNewApp"), IMktNewApp)
    End Function
#End Region


    Public Shared Function CreateAOInDirectSales() As IAOIndirectSales
        Return CType(CreateComponent("AOInDirectSales"), IAOIndirectSales)
    End Function

#Region "SupervisorSales"
    Public Shared Function CreateAOSupervisorSales() As IAOSupervisorSales
        Return CType(CreateComponent("AOSupervisorSales"), IAOSupervisorSales)
    End Function
#End Region



    Public Shared Function CreateFundingCompany() As IFundingCompany
        Try
            Return CType(CreateComponent("FundingCompany"), IFundingCompany)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


#Region " Master "
    Public Shared Function CreateMaster() As IMaster
        Try
            Return CType(CreateComponent("Master"), IMaster)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region " NewMaster "
    Public Shared Function CreateNewMaster() As INewMaster
        Try
            Return CType(CreateComponent("NewMaster"), INewMaster)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region " EconomySector "
    Public Shared Function CreateEconomySector() As IEconomySector
        Return CType(CreateComponent("EconomySector"), IEconomySector)
    End Function
#End Region

#Region " IndustryType "
    Public Shared Function CreateIndustryType() As IIndustryType
        Return CType(CreateComponent("IndustryType"), IIndustryType)
    End Function
#End Region

#Region " MailType "
    Public Shared Function CreateMailType() As IMailType
        Return CType(CreateComponent("MailType"), IMailType)
    End Function
#End Region

#Region " BisnisUnit "
    Public Shared Function CreateBisnisUnit() As IBisnisUnit
        Return CType(CreateComponent("BisnisUnit"), IBisnisUnit)
    End Function
#End Region

#Region " Referal "
    Public Shared Function CreateReferal() As IReferal
        Return CType(CreateComponent("Referal"), IReferal)
    End Function
#End Region

	'#Region " D01 "
	'    Public Shared Function CreateD01() As ID01
	'        Return CType(CreateComponent("D01"), ID01)
	'    End Function
	'#End Region

	'#Region " F01 "
	'    Public Shared Function CreateF01() As IF01
	'        Return CType(CreateComponent("F01"), IF01)
	'    End Function
	'#End Region


#Region " SLIK "
	Public Shared Function CreateA01() As IA01
		Return CType(CreateComponent("A01"), IA01)
	End Function

	Public Shared Function CreateD01() As ID01
        Return CType(CreateComponent("D01"), ID01)
    End Function

    Public Shared Function CreateF01() As IF01
        Return CType(CreateComponent("F01"), IF01)
    End Function

    Public Shared Function CreateP01() As IP01
        Return CType(CreateComponent("P01"), IP01)
    End Function

    Public Shared Function CreateS01() As IS01
        Return CType(CreateComponent("S01"), IS01)
    End Function

    Public Shared Function S01() As IS01
		Return CType(CreateComponent("S01"), IS01)
	End Function
	Public Shared Function CreateK01() As IK01
		Return CType(CreateComponent("K01"), IK01)
	End Function
	Public Shared Function CreateD02() As ID02
		Return CType(CreateComponent("CreateD02"), ID02)
	End Function

	Public Shared Function CreateF06() As IF06
		Return CType(CreateComponent("CreateF06"), IF06)
	End Function

	Public Shared Function CreateM01() As IM01
		Return CType(CreateComponent("CreateM01"), IM01)
	End Function
#End Region


#Region " SIPP0010 "
	Public Shared Function CreateSIPP0010() As ISIPP0010
        Return CType(CreateComponent("SIPP0010"), ISIPP0010)
    End Function
#End Region

#Region " SIPP0030 "
    Public Shared Function CreateSIPP0030() As ISIPP0030
        Return CType(CreateComponent("SIPP0030"), ISIPP0030)
    End Function
#End Region

#Region " SIPP2300 "
    Public Shared Function CreateSIPP2300() As ISIPP2300
        Return CType(CreateComponent("SIPP2300"), ISIPP2300)
    End Function
#End Region

#Region " SIPP2550 "
    Public Shared Function CreateSIPP2550() As ISIPP2550
        Return CType(CreateComponent("SIPP2550"), ISIPP2550)
    End Function
#End Region

#Region " SIPP0025 "
    Public Shared Function CreateSIPP0025() As ISIPP0025
        Return CType(CreateComponent("SIPP0025"), ISIPP0025)
    End Function
#End Region

#Region " SIPP2100 "
    Public Shared Function CreateSIPP2100() As ISIPP2100
        Return CType(CreateComponent("SIPP2100"), ISIPP2100)
    End Function
#End Region

#Region " SIPP2200 "
    Public Shared Function CreateSIPP2200() As ISIPP2200
        Return CType(CreateComponent("SIPP2200"), ISIPP2200)
    End Function
#End Region

#Region " SIPP3010 "
    Public Shared Function CreateSIPP3010() As ISIPP3010
        Return CType(CreateComponent("SIPP3010"), ISIPP3010)
    End Function
#End Region

#Region " SIPP0041 "
    Public Shared Function CreateSIPP0041() As ISIPP0041
        Return CType(CreateComponent("SIPP0041"), ISIPP0041)
    End Function
#End Region

#Region " SIPP1100 "
    Public Shared Function CreateSIPP1100() As ISIPP1100
        Return CType(CreateComponent("SIPP1100"), ISIPP1100)
    End Function
#End Region

#Region " SIPP1200 "
    Public Shared Function CreateSIPP1200() As ISIPP1200
        Return CType(CreateComponent("SIPP1200"), ISIPP1200)
    End Function
#End Region

#Region " SIPP5310 "
    Public Shared Function CreateSIPP5310() As ISIPP5310
        Return CType(CreateComponent("SIPP5310"), ISIPP5310)
    End Function
#End Region

    '#Region "Latihan Maintenance"
    '    Public Shared Function CreateLatihanMnt() As ILatihan
    '        Return CType(CreateComponent("LatihanMnt"), ILatihan)
    '    End Function
    '#End Region

#Region "SIPP"
    Public Shared Function CreateSIPP0000() As ISIPP0000
        Return CType(CreateComponent("CreateSIPP0000"), ISIPP0000)
    End Function

    Public Shared Function CreateSIPP0020() As ISIPP0020
        Return CType(CreateComponent("CreateSIPP0020"), ISIPP0020)
    End Function

    Public Shared Function CreateSIPP0035() As ISIPP0035
        Return CType(CreateComponent("CreateSIPP0035"), ISIPP0035)
    End Function

    Public Shared Function CreateSIPP0046() As ISIPP0046
        Return CType(CreateComponent("CreateSIPP0046"), ISIPP0046)
    End Function

    Public Shared Function CreateSIPP1110() As ISIPP1110
        Return CType(CreateComponent("CreateSIPP1110"), ISIPP1110)
    End Function

    Public Shared Function CreateSIPP1300() As ISIPP1300
        Return CType(CreateComponent("CreateSIPP1300"), ISIPP1300)
    End Function

    Public Shared Function CreateSIPP2600() As ISIPP2600
        Return CType(CreateComponent("CreateSIPP2600"), ISIPP2600)
    End Function

    Public Shared Function CreateSIPP3020() As ISIPP3020
        Return CType(CreateComponent("CreateSIPP3020"), ISIPP3020)
    End Function

    Public Shared Function CreateSIPP0043() As ISIPP0043
        Return CType(CreateComponent("CreateSIPP0043"), ISIPP0043)
    End Function

    Public Shared Function CreateSIPP2490() As ISIPP2490
        Return CType(CreateComponent("CreateSIPP2490"), ISIPP2490)
    End Function

    Public Shared Function CreateSIPP2790() As ISIPP2790
        Return CType(CreateComponent("CreateSIPP2790"), ISIPP2790)
    End Function
#End Region


#Region "Area Maintenance"
	Public Shared Function CreateAreaMnt() As IArea
        Return CType(CreateComponent("AreaMnt"), IArea)
    End Function
#End Region

#Region "Company"
    Public Shared Function CreateCompany() As ICompany
        Try
            Return CType(CreateComponent("Company"), ICompany)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Branch"
    Public Shared Function CreateBranch() As IBranch
        Return CType(CreateComponent("Branch"), IBranch)
    End Function
#End Region


    Public Shared Function CreateEmployee() As IEmployee
        Return CType(CreateComponent("Employee"), IEmployee)
    End Function

#Region "EmployeePosition"
    Public Shared Function CreateEmployeePosition() As IEmployeePosition
        Return CType(CreateComponent("EmployeePosition"), IEmployeePosition)
    End Function
#End Region

#Region " Master TC"
    Public Shared Function CreateMasterTC() As IMasterTC
        Return CType(CreateComponent("MasterTC"), IMasterTC)
    End Function
#End Region


#Region " General Paging "
    Public Shared Function CreateGeneralPaging() As IGeneralPaging

        Return CType(CreateComponent("GeneralPaging"), IGeneralPaging)

    End Function
#End Region


    Public Shared Function CreateWriteOff() As IWriteOff
        Return CType(CreateComponent("WriteOff"), IWriteOff)
    End Function

    Public Shared Function CreateMailTransaction() As IMailTransaction
        Return CType(CreateComponent("MailTransaction"), IMailTransaction)
    End Function

    Public Shared Function CreateDataUserControl() As IDataUserControl
        Return CType(CreateComponent("DataUserControl"), IDataUserControl)
    End Function

    Public Shared Function CreateUcAgreementList() As IUCAgreementList
        Return CType(CreateComponent("UcAgreementList"), IUCAgreementList)
    End Function

    Public Shared Function CreateUcAgreementListExtend() As IAgreementList
        Return CType(CreateComponent("UcAgreementListExtend"), IAgreementList)
    End Function

    Public Shared Function CreateUCAssetList() As IUCAssetList
        Return CType(CreateComponent("UCAssetList"), IUCAssetList)
    End Function

    Public Shared Function CreateLookUpZipCode() As ILookUpZipCode

        Return CType(CreateComponent("LookUpZipCode"), ILookUpZipCode)

    End Function

    Public Shared Function CreateLookUpTransaction() As ILookUpTransaction
        Return CType(CreateComponent("LookUpTransaction"), ILookUpTransaction)
    End Function

    Public Shared Function CreateLookUpCustomer() As ILookUpCustomer
        Return CType(CreateComponent("LookUpCustomer"), ILookUpCustomer)
    End Function

    Public Shared Function CreateLookUpProductOffering() As ILookUpProductOffering
        Return CType(CreateComponent("LookUpProductOffering"), ILookUpProductOffering)
    End Function

    Public Shared Function CreateLookUpAsset() As ILookUpAsset
        Return CType(CreateComponent("LookUpAsset"), ILookUpAsset)
    End Function

    Public Shared Function CreateLookUpSupplier() As ILookUpSupplier
        Return CType(CreateComponent("LookUpSupplier"), ILookUpSupplier)
    End Function

    Public Shared Function CreateGeneralSetting() As IGeneralSetting
        Return CType(CreateComponent("GeneralSetting"), IGeneralSetting)
    End Function

    Public Shared Function CreateAcuanBungaIntern() As IAcuanBungaIntern
        Return CType(CreateComponent("AcuanBungaIntern"), IAcuanBungaIntern)
    End Function
#Region "AgreementCancelation"
    Public Shared Function CreateAgreementCancellation() As IAgreementCancellation
        Return CType(CreateComponent("AgreementCancellation"), IAgreementCancellation)
    End Function
#End Region

    Public Shared Function CreateApproval() As IApproval
        Return CType(CreateComponent("Approval"), IApproval)
    End Function
    Public Shared Function CreateApprovalScheme() As IApprovalScheme
        Return CType(CreateComponent("ApprovalScheme"), IApprovalScheme)
    End Function
    Public Shared Function CreateApprovalScreen() As IApprovalScreen
        Return CType(CreateComponent("ApprovalScreen"), IApprovalScreen)
    End Function



    Public Shared Function CreateChangeAddressARM() As IChangeAddressARM
        Return CType(CreateComponent("ChangeAddressARM"), IChangeAddressARM)
    End Function

#Region "TRFundPCReimburse"
    Public Shared Function CreateTransferReimbursePC() As ITransferForReimbursePC
        Return CType(CreateComponent("TransferReimbursePC"), ITransferForReimbursePC)
    End Function
#End Region

#Region "TransferForPaymentRequest"
    Public Shared Function CreateTransferPaymentRequest() As ITransferForPaymentRequest
        Return CType(CreateComponent("TransferPaymentRequest"), ITransferForPaymentRequest)
    End Function
#End Region

    Public Shared Function CreateChangeAddressInquiry() As IChangeAddressInq
        Return CType(CreateComponent("ChangeAddressInq"), IChangeAddressInq)
    End Function

    Public Shared Function CreateDocReceive() As IDoc
        Try
            Return CType(CreateComponent("DocReceive"), IDoc)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateExposure() As IExposure
        Try
            Return CType(CreateComponent("Exposure"), IExposure)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateReason() As IReason
        Try
            Return CType(CreateComponent("Reason"), IReason)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAgreementTransfer() As IAgreementTransfer
        Try
            Return CType(CreateComponent("AgreementTransfer"), IAgreementTransfer)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Shared Function CreateAgreementChangeWOP() As IAChangeWOP
        Try
            Return CType(CreateComponent("AgreementChangeWOP"), IAChangeWOP)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateBatchVoucher() As IBatchVoucher
        Try
            Return CType(CreateComponent("BatchVoucher"), IBatchVoucher)
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        End Try
    End Function


    Public Shared Function CreateClaimrequest() As IClaimReq
        Try
            Return CType(CreateComponent("Claimrequest"), IClaimReq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInsClaimAct() As IInsClaimAct
        Try
            Return CType(CreateComponent("InsClaimAct"), IInsClaimAct)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



#Region " LookUpBankBranch"
    Public Shared Function CreateLookUpBankBranch() As ILookUpBankBranch
        Return CType(CreateComponent("LookUpBankBranch"), ILookUpBankBranch)
    End Function
#End Region




    Public Shared Function CreateAgency() As IAgency
        Return CType(CreateComponent("Agency"), IAgency)
    End Function

    Public Shared Function CreateADASetting() As IADASetting
        Try
            Return CType(CreateComponent("ADASetting"), IADASetting)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInsuranceCompany() As iInsuranceCompany
        Try
            Return CType(CreateComponent("InsuranceCompany"), iInsuranceCompany)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInsuranceRateCard() As iInsuranceRateCard
        Try
            Return CType(CreateComponent("InsuranceRateCard"), iInsuranceRateCard)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreatePembayaranAngsuranFunding() As IPembayaranAngsuranFunding
        Try
            Return CType(CreateComponent("PembayaranAngsuranFunding"), IPembayaranAngsuranFunding)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateInsEndorsmentInquiry() As IInsEndorsInq
        Return CType(CreateComponent("InsEndorsmentInquiry"), IInsEndorsInq)
    End Function

    Public Shared Function CreateAgentFeeDisbursement() As IAgentFeeDisbursement
        Try
            Return CType(CreateComponent("AgentFeeDisbursement"), IAgentFeeDisbursement)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateBIReport() As IBIReport
        Return CType(CreateComponent("BIReport"), IBIReport)
    End Function

    Public Shared Function CreatePettyCash() As IPettyCash
        Return CType(CreateComponent("PettyCash"), IPettyCash)
    End Function

    Public Shared Function CreatePettyCashTrans() As IPettyCashTrans
        Return CType(CreateComponent("PettyCashTrans"), IPettyCashTrans)
    End Function



    Public Shared Function CreateStopAccrued() As IStopAccrued
        Try
            Return CType(CreateComponent("StopAccrued"), IStopAccrued)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateAgreementList() As IAgreementList
        Return CType(CreateComponent("AgreementList"), IAgreementList)
    End Function

    Public Shared Function CreateSTNKRenewal() As ISTNKRenewal
        Return CType(CreateComponent("STNKRenewal"), ISTNKRenewal)
    End Function

    Public Shared Function CreateAssetCheckList() As IAssetCheckList
        Return CType(CreateComponent("AssetCheckList"), IAssetCheckList)
    End Function

    Public Shared Function CreateChangeAddress() As IChangeAddress
        Return CType(CreateComponent("ChangeAddress"), IChangeAddress)
    End Function

    Public Shared Function CreateActionResult() As IActionResult
        Return CType(CreateComponent("ActionResult"), IActionResult)
    End Function
    Public Shared Function CreateCollGroup() As ICollGroup
        Return CType(CreateComponent("CollGroup"), ICollGroup)
    End Function

    Public Shared Function CreateCasesSetting() As ICasesSetting
        Return CType(CreateComponent("CasesSetting"), ICasesSetting)
    End Function

    Public Shared Function CreateCollector() As ICollector
        Return CType(CreateComponent("Collector"), ICollector)
    End Function

    Public Shared Function CreateCollectorProf() As ICollectorProf
        Return CType(CreateComponent("CollectorProf"), ICollectorProf)
    End Function

    Public Shared Function CreateCollZipCode() As ICollZipCode
        Return CType(CreateComponent("CollZipCode"), ICollZipCode)
    End Function
    Public Shared Function CreateDocumentDistribution() As IDocDistribution
        Return CType(CreateComponent("DocumentDistribution"), IDocDistribution)
    End Function

    Public Shared Function CreateInqDCR() As IDCR
        Return CType(CreateComponent("InqDCR"), IDCR)
    End Function

    Public Shared Function CreateCollActivity() As ICollActivity
        Return CType(CreateComponent("CollActivity"), ICollActivity)
    End Function
    Public Shared Function CreateSPVActivity() As ISPVActivity
        Return CType(CreateComponent("SPVActivity"), ISPVActivity)

    End Function

    Public Shared Function CreateInqZipCode() As IInqZipCode
        Return CType(CreateComponent("InqZipCode"), IInqZipCode)
    End Function

    Public Shared Function CreateAssetRepo() As IAssetRepo
        Return CType(CreateComponent("AssetRepo"), IAssetRepo)
    End Function

    Public Shared Function CreateInqAssetRepo() As IInqAssetRepo
        Return CType(CreateComponent("InqAssetRepo"), IInqAssetRepo)
    End Function

    Public Shared Function CreateViewCollection() As IViewCollection
        Return CType(CreateComponent("ViewCollection"), IViewCollection)
    End Function

    Public Shared Function CreateInqAssetInventory() As IInqAssetInventory
        Return CType(CreateComponent("InqAssetInventory"), IInqAssetInventory)
    End Function

    Public Shared Function CreateCollInvOnRequest() As ICollInvOnRequest
        Return CType(CreateComponent("CollInvOnRequest"), ICollInvOnRequest)
    End Function

    Public Shared Function CreateAssetRelease() As IAssetRelease
        Return CType(CreateComponent("AssetRelease"), IAssetRelease)
    End Function

    Public Shared Function CreateCCRequest() As ICCRequest
        Return CType(CreateComponent("CCRequest"), ICCRequest)
    End Function
    Public Shared Function CreateInvChgExpDate() As IInvChgExpDate
        Return CType(CreateComponent("InvChgExpDate"), IInvChgExpDate)
    End Function

    Public Shared Function CreateRptCollReceiptNotes() As IRptCollReceiptNotes
        Return CType(CreateComponent("RptCollReceiptNotes"), IRptCollReceiptNotes)
    End Function

    Public Shared Function CreateCollExpense() As ICollExpense
        Return CType(CreateComponent("CollExpense"), ICollExpense)
    End Function

    Public Shared Function CreateInventoryAppraisal() As IInventoryAppraisal
        Return CType(CreateComponent("InventoryAppraisal"), IInventoryAppraisal)
    End Function

    Public Shared Function CreateIncentiveBM() As IIncentiveBM
        Return CType(CreateComponent("IncentiveBM"), IIncentiveBM)
    End Function

    Public Shared Function CreateIncentiveBC() As IIncentiveBC
        Return CType(CreateComponent("IncentiveBC"), IIncentiveBC)
    End Function

    Public Shared Function CreateIncentiveRN() As IIncentiveRN
        Return CType(CreateComponent("IncentiveRN"), IIncentiveRN)
    End Function

    Public Shared Function CreateIncentiveBL() As IIncentiveBL
        Return CType(CreateComponent("IncentiveBL"), IIncentiveBL)
    End Function

    Public Shared Function DeskCollPerformance() As IDeskCollPerformance
        Return CType(CreateComponent("DeskCollPerformance"), IDeskCollPerformance)
    End Function

    Public Shared Function ReceiptNotesView() As IReceiptNotesView
        Return CType(CreateComponent("ReceiptNotesView"), IReceiptNotesView)
    End Function

    Public Shared Function RptBucketMovement() As IRptBucketMovement
        Return CType(CreateComponent("RptBucketMovement"), IRptBucketMovement)
    End Function

    Public Shared Function RptBillingCharges() As IRptBllingCharges
        Return CType(CreateComponent("RptBillingCharges"), IRptBllingCharges)
    End Function

    Public Shared Function RptReceiptNotes() As IRptReceiptNotes
        Return CType(CreateComponent("RptReceiptNotes"), IRptReceiptNotes)
    End Function

    Public Shared Function RptIncOverDueRating() As IRptIncOverDueRating
        Return CType(CreateComponent("RptIncOverDueRating"), IRptIncOverDueRating)
    End Function

    Public Shared Function IncentiveEXE() As IIncentiveEXE
        Return CType(CreateComponent("IncentiveEXE"), IIncentiveEXE)
    End Function

    Public Shared Function CollLetter() As ICollLetter
        Return CType(CreateComponent("CollLetter"), ICollLetter)
    End Function

    Public Shared Function CollLetterOnReq() As ICollLetterOnReq
        Return CType(CreateComponent("CollLetterOnReq"), ICollLetterOnReq)
    End Function

    Public Shared Function CreateAdminActivity() As IAdminActivity
        Return CType(CreateComponent("AdminActivity"), IAdminActivity)
    End Function

    Public Shared Function CreateCollectorScheme() As ICollectorScheme
        Return CType(CreateComponent("InqCollectorScheme"), ICollectorScheme)
    End Function

    Public Shared Function CreateDeskCollReminder() As IDeskCollReminderActivity
        Return CType(CreateComponent("DeskCollReminder"), IDeskCollReminderActivity)
    End Function

    Public Shared Function CreateDeskCollTask() As IDeskCollTask
        Return CType(CreateComponent("DeskCollTaskReport"), IDeskCollTask)
    End Function

    Public Shared Function CreateCLActivity() As ICLActivity
        Return CType(CreateComponent("CLActivity"), ICLActivity)
    End Function

    Public Shared Function CreateRptCollAct() As IRptCollAct
        Return CType(CreateComponent("RptCollAct"), IRptCollAct)
    End Function

    Public Shared Function CreateRptCollOverDue() As IRptCollOverDue
        Return CType(CreateComponent("RptCollOverDue"), IRptCollOverDue)
    End Function

    Public Shared Function CreateRptCollExecutor() As IRptCollExecutor
        Return CType(CreateComponent("RptCollExecutor"), IRptCollExecutor)
    End Function

    Public Shared Function CreateRALPrinting() As IRALPrinting
        Return CType(CreateComponent("RALPrinting"), IRALPrinting)
    End Function


    Public Shared Function CreateRALOnRequest() As IRALOnRequest
        Return CType(CreateComponent("RALOnRequest"), IRALOnRequest)
    End Function

    Public Shared Function CreateRALExtend() As IRALExtend
        Return CType(CreateComponent("RALExtend"), IRALExtend)
    End Function

    Public Shared Function CreateSPPrint() As ISPPrint
        Return CType(CreateComponent("SPPrint"), ISPPrint)
    End Function

    Public Shared Function CreateRALRelease() As IRALRelease
        Return CType(CreateComponent("RALRelease"), IRALRelease)
    End Function

    Public Shared Function CreateCollInvSelling() As ICollInvSelling
        Try
            Return CType(CreateComponent("CollInvSelling"), ICollInvSelling)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function CreateCollInvReturn() As ICollInvReturn
        Try
            Return CType(CreateComponent("CollInvReturn"), ICollInvReturn)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateCollPrintBPK() As ICollPrintBPK
        Try
            Return CType(CreateComponent("CollPrintBPK"), ICollPrintBPK)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreatePDCRequestPrint() As IPDCRequestPrint
        Return CType(CreateComponent("PDCRequestPrint"), IPDCRequestPrint)
    End Function

    Public Shared Function CreateRALChangeExec() As IRALChangeExec
        Return CType(CreateComponent("RALChangeExecutor"), IRALChangeExec)
    End Function

    Public Shared Function CreateRALHistory() As IRALHistory
        Try
            Return CType(CreateComponent("RALHistory"), IRALHistory)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function CreateRALInquiry() As IRALInquiry
        Return CType(CreateComponent("RALInquiry"), IRALInquiry)
    End Function

    Public Shared Function CreateRALWithoutEx() As IRALWithoutEx
        Try
            Return CType(CreateComponent("RalWithoutExecutor"), IRALWithoutEx)
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        End Try
    End Function

    Public Shared Function CreateReceiptNotesOnRequest() As IReceiptNotesOnRequest
        Return CType(CreateComponent("ReceiptNotesOnRequest"), IReceiptNotesOnRequest)
    End Function

    Public Shared Function CreateDatiII() As IDatiII
        Return CType(CreateComponent("DatiII"), IDatiII)
    End Function

    Public Shared Function CreatePropinsi() As IPropinsi
        Return CType(CreateComponent("Propinsi"), IPropinsi)
    End Function

    Public Shared Function CreateSupplierGroup() As ISupplierGroup
        Return CType(CreateComponent("SupplierGroup"), ISupplierGroup)
    End Function

    Public Shared Function CreateGroupSupplierAccount() As IGroupSupplierAccount
        Return CType(CreateComponent("GroupSupplierAccount"), IGroupSupplierAccount)
    End Function

    Public Shared Function CreateCustomerGroup() As ICustomerGroup
        Return CType(CreateComponent("CustomerGroup"), ICustomerGroup)
    End Function

    Public Shared Function CreateCustomerGroupPIC() As ICustomerGroupPIC
        Return CType(CreateComponent("CustomerGroupPIC"), ICustomerGroupPIC)
    End Function

    Public Shared Function CreateAssetMasterPrice() As IAssetMasterPrice
        Return CType(CreateComponent("AssetMasterPrice"), IAssetMasterPrice)
    End Function

    Public Shared Function CreateNegara() As INegara
        Return CType(CreateComponent("Negara"), INegara)
    End Function

    Public Shared Function CreateReportingService() As IReportingService
        Return CType(CreateComponent("ReportingService"), IReportingService)
    End Function

    Public Shared Function CreateRefundInsentif() As IRefundInsentif
        Return CType(CreateComponent("RefundInsentif"), IRefundInsentif)
    End Function

    Public Shared Function CreateApplicationDetailTransaction() As IApplicationDetailTransaction
        Return CType(CreateComponent("ApplicationDetailTransaction"), IApplicationDetailTransaction)
    End Function

    Public Shared Function CreateHasilSurvey() As IHasilSurvey
        Return CType(CreateComponent("HasilSurvey"), IHasilSurvey)
    End Function
    Public Shared Function CreateCreditAssesment() As ICreditAssesment
        Return CType(CreateComponent("CreditAssesment"), ICreditAssesment)
    End Function

    Public Shared Function CreateKonfirmasiDealer() As IKonfirmasiDealer
        Return CType(CreateComponent("KonfirmasiDealer"), IKonfirmasiDealer)
    End Function

    Public Shared Function CreateBankBranchMaster() As IBankBranchMaster
        Return CType(CreateComponent("BankBranchMaster"), IBankBranchMaster)
    End Function

    Public Shared Function CreateFundingCriteria() As IFundingCriteria
        Return CType(CreateComponent("FundingCriteria"), IFundingCriteria)
    End Function

    Public Shared Function CreateFundingCriteriaValue() As IFundingCriteriaValue
        Return CType(CreateComponent("FundingCriteriaValue"), IFundingCriteriaValue)
    End Function

    Public Shared Function CreateCustomerCases() As ICustomerCases
        Return CType(CreateComponent("CustomerCases"), ICustomerCases)
    End Function

    Public Shared Function CreateTrackingTVC() As ITrackingTVC
        Return CType(CreateComponent("TrackingTVC"), ITrackingTVC)
    End Function

    Public Shared Function CreateFundingContractCriteria() As IFundingContractCriteria
        Return CType(CreateComponent("FundingContractCriteria"), IFundingContractCriteria)
    End Function

    Public Shared Function CreateSuspendAllocationGeneral() As ISuspendAllocationGeneral
        Return CType(CreateComponent("SuspendAllocationGeneral"), ISuspendAllocationGeneral)
    End Function

    Public Shared Function CreateDChange() As IDChange
        Return CType(CreateComponent("DChange"), IDChange)
    End Function

    Public Shared Function CreateCreditScoreGradeMaster() As ICreditScoreGradeMaster
        Return CType(CreateComponent("CreditScoreGradeMaster"), ICreditScoreGradeMaster)
    End Function

    Public Shared Function CreateVendor() As IVendor
        Return CType(CreateComponent("Vendor"), IVendor)
    End Function

    Public Shared Function CreateAgreementMasterTransaction() As IAgreementMasterTransaction
        Return CType(CreateComponent("AgreementMasterTransaction"), IAgreementMasterTransaction)
    End Function

    Public Shared Function InsBlokir() As IInsBlokir
        Return CType(CreateComponent("InsBlokir"), IInsBlokir)
    End Function

    Public Shared Function AppDeviation() As IAppDeviation
        Return CType(CreateComponent("AppDeviation"), IAppDeviation)
    End Function

    Public Shared Function CreateCOA() As ICOA
        Return CType(CreateComponent("COA"), ICOA)
    End Function

    Public Shared Function CreateJournalVoucher() As IJournalVoucher
        Return CType(CreateComponent("JournalVoucher"), IJournalVoucher)
    End Function

    Public Shared Function CreateAuthorizedSigner() As IAuthorizedSigner
        Return CType(CreateComponent("AuthorizedSigner"), IAuthorizedSigner)
    End Function

    Public Shared Function CreatePersonalCustomerIdentitasHistory() As IPersonalCustomerIdentitasHistory
        Return CType(CreateComponent("PersonalCustomerIdentitasHistory"), IPersonalCustomerIdentitasHistory)
    End Function
    Public Shared Function CreateCurrency() As ICurrency
        Return CType(CreateComponent("Currency"), ICurrency)
    End Function

    Public Shared Function CreateLokasiAsset() As ILokasiAsset
        Return CType(CreateComponent("LokasiAsset"), ILokasiAsset)
    End Function
    'Public Shared Function Latihan() As ILatihan
    '    Return CType(CreateComponent("Latihan"), ILatihan)
    'End Function
    Public Shared Function Area() As IArea
        Return CType(CreateComponent("Area"), IArea)
    End Function


    Public Shared Function CreateTarifPph() As IRatePph
        Return CType(CreateComponent("RatePph"), IRatePph)
    End Function

    Public Shared Function CreateInsuranceProduct() As IInsCoProduct
        Return CType(CreateComponent("InsCoProduct"), IInsCoProduct)
    End Function

    Public Shared Function CreateInsuranceRateAdditional() As IInsCoRateAdditional
        Return CType(CreateComponent("InsCoRateAdditional"), IInsCoRateAdditional)
    End Function

    Public Shared Function CreateFactoringInvoice() As IFactoringInvoice
        Return CType(CreateComponent("FactoringInvoice"), IFactoringInvoice)
    End Function

    Public Shared Function CreateModalKerjaInvoice() As IModalKerjaInvoice
        Return CType(CreateComponent("ModalKerjaInvoice"), IModalKerjaInvoice)
    End Function

#Region "IMPLEMENTASI"
    Public Shared Function CreateImplementasi() As IImplementasi
        Return CType(CreateComponent("Implementasi"), IImplementasi)
    End Function
#End Region

#Region "Pembiayaan Mitra (Join Financing)"
    Public Shared Function CreateMitraMultifinance() As IMitraMultifinance
        Return CType(CreateComponent("MitraMultifinance"), IMitraMultifinance)
    End Function
    'Public Shared Function CreateMitraDrawdown() As IMitraDrawdown
    '    Return CType(CreateComponent("MitraMultifinance"), IMitraDrawdown)
    'End Function
    Public Shared Function CreateJFReference() As IJFReference
        Return CType(CreateComponent("JFReference"), IJFReference)
    End Function
    Public Shared Function CreateJFProduct() As IJFProduct
        Return CType(CreateComponent("JFProduct"), IJFProduct)
    End Function
    Public Shared Function CreateJFDocument() As IJFDocument
        Return CType(CreateComponent("JFDocument"), IJFDocument)
    End Function
    Public Shared Function CreateJFCriteria() As IJFCriteria
        Return CType(CreateComponent("JFCriteria"), IJFCriteria)
    End Function
#End Region
    Public Shared Function CreateInqEndPastDueDays() As IInqEndPastDueDays
        Return CType(CreateComponent("InqEndPastDueDays"), IInqEndPastDueDays)
    End Function
    Public Shared Function CreateBalanceSheet() As IBalanceSheet
        Return CType(CreateComponent("BalanceSheet"), IBalanceSheet)
    End Function

#Region " Installment Drawdown "
    Public Shared Function CreateInstallmentDrawdown() As IInstallmentDrawdown
        Return CType(CreateComponent("InstallmentDrawdown"), IInstallmentDrawdown)
    End Function

    Public Shared Function CreateDisburseFactoring() As IDisburseFactoring
        Return CType(CreateComponent("DisburseFactoring"), IDisburseFactoring)
    End Function

#End Region
#Region "Agunan Lain"
    Public Shared Function CreateAgunanLain() As IAgunanLain
        Return CType(CreateComponent("AgunanLain"), IAgunanLain)
    End Function

#End Region

#Region "JournalCrossPeriod"
    Public Shared Function CreateDataJurnalCrossPeriode() As IDataJurnalCrossPeriode
        Return CType(CreateComponent("DataJurnalCrossPeriode"), IDataJurnalCrossPeriode)
    End Function

#End Region

    Public Shared Function CreatePrepaidAllocation() As IPrepaidAllocation
        Return CType(CreateComponent("InstallRcv"), IPrepaidAllocation)
    End Function
#Region "AmortisasiJurnal"
    Public Shared Function CreateAmortisasiJurnal() As IAmortisasiJurnal
        Return CType(CreateComponent("AmortisasiJurnal"), IAmortisasiJurnal)
    End Function

#End Region
    Public Shared Function CreateCashAdvance() As ICashAdvance
        Return CType(CreateComponent("CashAdvance"), ICashAdvance)
    End Function

#Region "Simulasi Perhitungan Pembiayaan"
    Public Shared Function CreateSimulasiPerhitunganPembiayaan() As ISimulasiPerhitunganPembiayaan
        Return CType(CreateComponent("Simulasi"), ISimulasiPerhitunganPembiayaan)
        'Return CType(CreateComponent("CreateSimulasiPerhitunganPembiayaan"), ISimulasiPerhitunganPembiayaan)
    End Function
#End Region

#Region "Pembiayaan Mitra"
    Public Shared Function CreateLandingProcessApp() As ILendingProcess
        Return CType(CreateComponent("LendingProcess"), ILendingProcess)
    End Function
#End Region


    Public Shared Function CreateCetakKartuPiutang() As IImplementasi
        Return CType(CreateComponent("CetakKartuPiutang"), IImplementasi)
    End Function

    Public Shared Function CreateApplicationChanneling() As IImplementasi
        Return CType(CreateComponent("ApplicationChanneling"), IImplementasi)
    End Function
End Class
