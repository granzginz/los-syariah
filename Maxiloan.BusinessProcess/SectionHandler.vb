﻿


#Region "Imports"
Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Xml
#End Region

''' <summary>
''' A list of all the possible ways that a object can be invoked
''' </summary>
Public Enum ModeTypes
    Local = 1
    Remote = 2
End Enum

#Region "TypeSettings class"
'/// <summary>
'/// Base class for all providers settings within the UIP configuration settings in the config file.
'/// </summary>
Friend Class TypeSettings

#Region "Declares constants and variables"
    Private Const ATTR_NAME As String = "name"
    Private Const ATTR_TYPE As String = "type"
    Private Const ATTR_MODE As String = "mode"
    Private Const ATTR_URI As String = "objectUri"

    Private _name As String
    Private _type As String
    Private _mode As ModeTypes
    Private _uri As String
#End Region

#Region "Constructors"
    '/// <summary>
    '/// Default constructor
    '/// </summary>
    Public Sub New()
        'do nothing
    End Sub

    '/// <summary>
    '/// Creates an instance of the ObjectTypeSettings class imports the specified configNode
    '/// </summary>
    Public Sub New(ByVal configNode As XmlNode)

        Dim currentAttribute As XmlNode

        For Each currentAttribute In configNode.Attributes
            Select Case currentAttribute.Name
                Case ATTR_NAME
                    _name = currentAttribute.Value
                Case ATTR_TYPE
                    _type = currentAttribute.Value
                Case ATTR_MODE
                    _mode = CType([Enum].Parse(GetType(ModeTypes), currentAttribute.Value, True), ModeTypes)
                Case ATTR_URI
                    _uri = currentAttribute.Value
                Case Else
                    'do nothing
            End Select
        Next

        'do some quick validation
        If (Me.Mode = ModeTypes.Local) AndAlso (Me.Type Is Nothing) Then            
            Throw New Exception("Error")
        End If

        If (Me.Mode = ModeTypes.Remote) AndAlso (Me.ObjectUri Is Nothing) Then            
            Throw New Exception("Error")
        End If

    End Sub
#End Region

#Region "Properties"
    '/// <summary>
    '/// Gets/Sets the name key
    '/// </summary>
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    '/// <summary>
    '/// Gets/Sets the provider full qualified type name
    '/// </summary>
    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property

    '/// <summary>
    '/// Gets/Sets the mode full type
    '/// </summary>
    Public Property Mode() As ModeTypes
        Get
            Return _mode
        End Get
        Set(ByVal value As ModeTypes)
            _mode = value
        End Set
    End Property

    '/// <summary>
    '/// Gets/Sets the object uri
    '/// </summary>
    Public Property ObjectUri() As String
        Get
            Return _uri
        End Get
        Set(ByVal value As String)
            _uri = value
        End Set
    End Property

#End Region
End Class
#End Region

#Region "RemotingConfigurationSettings class"
''' <summary>
''' Base class for all providers settings within the Remoting Configuration Settings in the config file.' 
''' </summary>
Friend Class RemotingConfigurationSettings

#Region "Declares constants and variables"
    Private Const NODE_TYPES_XPATH As String = "types"
    Private Const ATTR_CONFIG_FILE As String = "remotingConfigFile"

    Private _remoteConfigFile As String
    Private _types As Hashtable
#End Region

#Region "Constructors"
    ''' <summary>
    ''' Default constructor
    ''' </summary>
    Public Sub New()
        _types = New Hashtable
    End Sub

    ''' <summary>
    ''' Creates an instance of the RemotingConfigurationSettings class imports the specified configNode
    ''' </summary>
    ''' <param name="configNode">The node containing the configuration information</param>
    Public Sub New(ByVal configNode As XmlNode)

        Dim node As XmlNode
        Dim typesNode As XmlNode

        _types = New Hashtable

        Dim objectType As TypeSettings
        typesNode = configNode.SelectSingleNode(NODE_TYPES_XPATH)

        For Each node In typesNode.ChildNodes
            objectType = New TypeSettings(node)
            _types.Add(objectType.Name, objectType)
        Next

        If Not typesNode.Attributes(ATTR_CONFIG_FILE) Is Nothing Then
            _remoteConfigFile = typesNode.Attributes(ATTR_CONFIG_FILE).Value
        End If
    End Sub

#End Region

#Region "Properties"

    ''' <summary>
    ''' Gets/Sets the type settings
    ''' </summary>
    ''' <param name="typeKey">The key to the type setting</param>
    ''' <returns>The type settings for that key</returns>
    Public Property Types(ByVal typeKey As String) As TypeSettings
        Get
            Return CType(_types.Item(typeKey), TypeSettings)
        End Get
        Set(ByVal value As TypeSettings)
            If _types.Contains(value.Name) Then
                _types.Item(value.Name) = value
            Else
                _types.Add(value.Name, value)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets whether the type exists or not
    ''' </summary>
    ''' <param name="typeKey">The key to the type setting</param>
    ''' <returns>True if the key exists</returns>
    Public ReadOnly Property ContainsType(ByVal typeKey As String) As Boolean
        Get
            Return _types.Contains(typeKey)
        End Get
    End Property

    ''' <summary>
    ''' Returns the file used to set up the remoting configuration information such as channels and formatters
    ''' </summary>
    ''' <returns>Returns the file used to set up the remoting configuration information such as channels and formatters</returns>
    Public Property RemotingConfigurationFilename() As String
        Get
            Return _remoteConfigFile
        End Get
        Set(ByVal value As String)
            _remoteConfigFile = value
        End Set
    End Property

#End Region
End Class
#End Region

''' <summary>
''' The section handler that reads the remote type information
''' </summary>
Public Class SectionHandler
    Implements IConfigurationSectionHandler

    Public Overridable Function Create(ByVal parent As Object, _
                                                   ByVal configContext As Object, _
                                                   ByVal section As XmlNode) As Object Implements IConfigurationSectionHandler.Create

        Dim settings As RemotingConfigurationSettings

        ' Exit if there are no configuration settings.
        If section Is Nothing Then
            settings = New RemotingConfigurationSettings
        Else
            settings = New RemotingConfigurationSettings(section)
        End If

        Return settings
    End Function
End Class
