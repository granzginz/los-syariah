﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_header.aspx.vb" Inherits="Maxiloan.Webform.am_header" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="Include/Header.css" type="text/css" />
    <link rel="Stylesheet" href="Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="header_div">
        <div class="logo_div">
            <a href="Webform.AppMgt/forms/am_inbox_004.aspx" target="content">
                <img src="Images/logo.png" alt="company_logo" style="padding-left: 9px;" />
            </a>
        </div>
        <div class="company_div">
            <span>
                <%= Session("sesCompanyName")%></span>
        </div>
        <div class="branch_div">
            <span>
                <%= Session("BranchName")%>
            </span>
        </div>
        <div class="date_div">
            <asp:Label ID="lblDate" runat="server" CssClass="header_right"></asp:Label>
        </div>
        <div class="loginname_div">
            <label>
                <%= Session("FullName")%></label>
        </div>
        <div class="logout_div">
            <div class="header_right">
                <asp:Button runat="server" ID="btnLogout" Text="Logout" CausesValidation="false"
                    PostBackUrl="javascript:parent.frames.location.href='am_logout.aspx'" UseSubmitBehavior="false"
                    target="_top" CssClass="small button"/>
            </div>
        </div>
    </div>
    <div class="listGrid"></div>
    </form>
</body>
</html>
