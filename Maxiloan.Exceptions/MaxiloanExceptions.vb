﻿
#Region "Import"

Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.IO
Imports System.Text
Imports System.Configuration
#End Region

Public Class MaxiloanExceptions
    Inherits Exception   
    Public Sub WriteLog(ByVal errNamaFile As String, ByVal errNamaFunction As String, ByVal errsource As String, ByVal errtarget As String, ByVal errmessage As String, ByVal errstacktrace As String)

        Dim iFileNo As Integer

        Try
            iFileNo = FreeFile()
            FileOpen(iFileNo, ConfigurationManager.AppSettings("errorLog"), OpenMode.Append)
            PrintLine(iFileNo, " ***** Error Logged  : ", Now)
            PrintLine(iFileNo, " Error in File of    : ", errNamaFile)
            PrintLine(iFileNo, " Error In Function   : ", errNamaFunction)
            PrintLine(iFileNo, " Source              : ", errsource)
            PrintLine(iFileNo, " Method              : ", errtarget)
            PrintLine(iFileNo, " Description         : ", errmessage)
            PrintLine(iFileNo, " Stack Trace         : ", errstacktrace)
            PrintLine(iFileNo, " End Of Error Logged   ")
            PrintLine(iFileNo, "")
            PrintLine(iFileNo, "")
        Catch ex As Exception
            Throw ex
        Finally
            FileClose(iFileNo)
        End Try

    End Sub
End Class

