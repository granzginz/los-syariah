
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class AOSupervisorSales : Inherits Maxiloan.SQLEngine.DataAccessBase
    'description : fungsi ini untuk menampilkan data supervisor ke datagrid
    Public Function AOSupervisorListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisor", params).Tables(0)
            data.TotalRecord = CInt(params(4).Value)
            Return data
        Catch exp As Exception
            WriteException("AOSupervisorSales", "AOSupervisorSales", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan budget yang ada dari supervisor yang dipilih
    Public Function AOSupervisorBudgetListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorBudget", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            WriteException("AOSupervisorSalesBudget", "AOSupervisorSalesBudget", exp.Message + exp.StackTrace)
        End Try

    End Function

    'fungsi ini untuk menampilkan semua budget dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOSupervisorBudgetView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorBudgetView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AOSupervisorSalesView", "AOSupervisorSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan budget dari supervisor yang dipilih untuk dicetak
    Public Function AOSupervisorBudgetPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorBudgetPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AOSupervisorSalesPrint", "AOSupervisorSalesPrint", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan forecast yang ada dari supervisor yang dipilih
    Public Function AOSupervisorForecastListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorForecast", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            WriteException("AOSupervisorSalesForecast", "AOSupervisorSalesForecast", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan semua Forecast dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOSupervisorForecastView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorForecastView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AOSupervisorSalesView", "AOSupervisorSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk melakukan print pada forecast yang diminta
    Public Function AOSupervisorForecastPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim data As New Parameter.AOSupervisorSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingSupervisorForecastPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AOSupervisorSalesPrint", "AOSupervisorSalesPrint", exp.Message + exp.StackTrace)
        End Try

    End Function

End Class
