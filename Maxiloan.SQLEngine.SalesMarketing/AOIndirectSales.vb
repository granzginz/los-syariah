
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class AOIndirectSales : Inherits Maxiloan.SQLEngine.DataAccessBase
    'description : fungsi ini untuk menampilkan data ao ke datagrid
    Public Function AOIndirectListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@Criteria", SqlDbType.VarChar, 100)
        params(4).Value = customclass.Criteria
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingInDirectSales", params).Tables(0)
            data.TotalRecord = CInt(params(5).Value)
            Return data
        Catch exp As Exception
            WriteException("AOInDirectSales", "AOInDirectSales", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan budget yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function AOBudgetListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectBudget", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            WriteException("AOInDirectSalesBudget", "AOInDirectSalesBudget", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function

    'fungsi ini untuk menampilkan semua budget dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOBudgetView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectBudgetView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AOInDirectSalesView", "AOInDirectSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function AOBudgetPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectBudgetPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AOInDirectSalesPrint", "AOInDirectSalesPrint", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan forecast yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function AOForecastListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectForecast", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            WriteException("AOIndirectSalesForecast", "AOIndirectSalesForecast", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan semua Forecast dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOForecastView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectForecastView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AOIndirectSalesView", "AOIndirectSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk melakukan print pada forecast yang diminta
    Public Function AOForecastPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim data As New Parameter.AOIndirectSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOIndirectForecastPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AOIndirectSalesPrint", "AOIndirectSalesPrint", exp.Message + exp.StackTrace)
        End Try

    End Function
End Class
