
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RptSales : Inherits DataAccessBase
#Region "PrivateConst"
    Private Const spViewDataAOSupervisor As String = "spSalesViewDataAOSupervisor"
    Private Const spViewDataAO As String = "spSalesViewDataAO"
    Private Const spViewDataArea As String = "spSalesViewDataArea"
    Private Const spViewDataCASupervisor As String = "spSalesViewDataCASupervisor"
    Private Const spViewDataCA As String = "spSalesViewDataCA"
    Private Const spViewDataBrand As String = "spSalesViewDataBrand"
    Private Const spViewDataSupplier As String = "spSalesViewDataSupplier"
    Private Const spViewPercentEffective As String = "spSalesViewPercentEffective"
    Private Const spViewPercentDP As String = "spSalesViewPercentDP"
    Private Const spViewamountFinance As String = "spSalesViewamountFinance"
    Private Const spAppGetProductBranch As String = "spProspectAppGetProductBranch"
    Private Const spAppGetAsset As String = "spProspectAppGetAsset"
    Private Const spViewInstallmentAmount As String = "spSalesViewInstallmentAmount"
    Private Const spViewDataProduct As String = "spSalesViewDataProduct"
    Private Const spInqSalesPerPeriod As String = "spInqSalesPerPeriod"
    Private Const spInqAgingStatus As String = "spInqAgingStatus"
    Private Const spListReport As String = "SpSalesReport"
    Private Const spViewDataProductWhere As String = "spSalesViewDataProductWhere"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Private Const PARAM_TOTAL_OD_ACCOUNT As String = "@TotalODAccount"
    Private Const PARAM_BALANCE_OD As String = "@BalanceOD"
    Private Const PARAM_BUCKET1 As String = "@bucket1"
    Private Const PARAM_BUCKET2 As String = "@bucket2"
    Private Const PARAM_BUCKET3 As String = "@bucket3"
    Private Const PARAM_BUCKET4 As String = "@bucket4"
    Private Const PARAM_BUCKET5 As String = "@bucket5"
    Private Const PARAM_BUCKET6 As String = "@bucket6"
    Private Const PARAM_BUCKET7 As String = "@bucket7"
    Private Const PARAM_BUCKET8 As String = "@bucket8"
    Private Const PARAM_BUCKET9 As String = "@bucket9"
    Private Const PARAM_BUCKET10 As String = "@bucket10"
    Private Const PARAM_ACCOUNT_BUCKET1 As String = "@accountbucket1"
    Private Const PARAM_ACCOUNT_BUCKET2 As String = "@accountbucket2"
    Private Const PARAM_ACCOUNT_BUCKET3 As String = "@accountbucket3"
    Private Const PARAM_ACCOUNT_BUCKET4 As String = "@accountbucket4"
    Private Const PARAM_ACCOUNT_BUCKET5 As String = "@accountbucket5"
    Private Const PARAM_ACCOUNT_BUCKET6 As String = "@accountbucket6"
    Private Const PARAM_ACCOUNT_BUCKET7 As String = "@accountbucket7"
    Private Const PARAM_ACCOUNT_BUCKET8 As String = "@accountbucket8"
    Private Const PARAM_ACCOUNT_BUCKET9 As String = "@accountbucket9"
    Private Const PARAM_ACCOUNT_BUCKET10 As String = "@accountbucket10"
    Private Const PARAM_AMOUNT_BUCKET1 As String = "@amountbucket1"
    Private Const PARAM_AMOUNT_BUCKET2 As String = "@amountbucket2"
    Private Const PARAM_AMOUNT_BUCKET3 As String = "@amountbucket3"
    Private Const PARAM_AMOUNT_BUCKET4 As String = "@amountbucket4"
    Private Const PARAM_AMOUNT_BUCKET5 As String = "@amountbucket5"
    Private Const PARAM_AMOUNT_BUCKET6 As String = "@amountbucket6"
    Private Const PARAM_AMOUNT_BUCKET7 As String = "@amountbucket7"
    Private Const PARAM_AMOUNT_BUCKET8 As String = "@amountbucket8"
    Private Const PARAM_AMOUNT_BUCKET9 As String = "@amountbucket9"
    Private Const PARAM_AMOUNT_BUCKET10 As String = "@amountbucket10"
#End Region


    Public Function GetAO(ByVal customclass As Parameter.Sales) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataAO, params).Tables(0)
    End Function

    Public Function GetAOSupervisor(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataAOSupervisor).Tables(0)
    End Function
    Public Function GetArea(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataArea).Tables(0)
    End Function
    Public Function GetCA(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataCA).Tables(0)
    End Function
    Public Function GetCASupervisor(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataCASupervisor).Tables(0)
    End Function
    Public Function GetBrand(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataBrand).Tables(0)
    End Function
    Public Function GetSupplier(ByVal customclass As Parameter.Sales) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataSupplier, params).Tables(0)
    End Function
    Public Function ViewRptDailySalesByAO(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReport, params)
        Return customclass
    End Function
    Public Function GetPercentDP(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPercentDP).Tables(0)
    End Function
    Public Function GetPercentEffective(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPercentEffective).Tables(0)
    End Function
    Public Function GetAmountFinance(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewamountFinance).Tables(0)
    End Function
    Public Function GetInstallmentAmount(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewInstallmentAmount).Tables(0)
    End Function
    Public Function GetProduct(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataProduct).Tables(0)
    End Function
    Public Function InqAgingStatus(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim params() As SqlParameter = New SqlParameter(36) {}
        With customclass

            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter(PARAM_TOTAL_OD_ACCOUNT, SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output
            params(6) = New SqlParameter(PARAM_BALANCE_OD, SqlDbType.Decimal)
            params(6).Direction = ParameterDirection.Output
            params(7) = New SqlParameter(PARAM_BUCKET1, SqlDbType.VarChar, 10)
            params(7).Direction = ParameterDirection.Output
            params(8) = New SqlParameter(PARAM_BUCKET2, SqlDbType.VarChar, 10)
            params(8).Direction = ParameterDirection.Output
            params(9) = New SqlParameter(PARAM_BUCKET3, SqlDbType.VarChar, 10)
            params(9).Direction = ParameterDirection.Output
            params(10) = New SqlParameter(PARAM_BUCKET4, SqlDbType.VarChar, 10)
            params(10).Direction = ParameterDirection.Output
            params(11) = New SqlParameter(PARAM_BUCKET5, SqlDbType.VarChar, 10)
            params(11).Direction = ParameterDirection.Output
            params(12) = New SqlParameter(PARAM_BUCKET6, SqlDbType.VarChar, 10)
            params(12).Direction = ParameterDirection.Output
            params(13) = New SqlParameter(PARAM_BUCKET7, SqlDbType.VarChar, 10)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter(PARAM_BUCKET8, SqlDbType.VarChar, 10)
            params(14).Direction = ParameterDirection.Output
            params(15) = New SqlParameter(PARAM_BUCKET9, SqlDbType.VarChar, 10)
            params(15).Direction = ParameterDirection.Output
            params(16) = New SqlParameter(PARAM_BUCKET10, SqlDbType.VarChar, 10)
            params(16).Direction = ParameterDirection.Output
            params(17) = New SqlParameter(PARAM_ACCOUNT_BUCKET1, SqlDbType.Int)
            params(17).Direction = ParameterDirection.Output
            params(18) = New SqlParameter(PARAM_ACCOUNT_BUCKET2, SqlDbType.Int)
            params(18).Direction = ParameterDirection.Output
            params(19) = New SqlParameter(PARAM_ACCOUNT_BUCKET3, SqlDbType.Int)
            params(19).Direction = ParameterDirection.Output
            params(20) = New SqlParameter(PARAM_ACCOUNT_BUCKET4, SqlDbType.Int)
            params(20).Direction = ParameterDirection.Output
            params(21) = New SqlParameter(PARAM_ACCOUNT_BUCKET5, SqlDbType.Int)
            params(21).Direction = ParameterDirection.Output
            params(22) = New SqlParameter(PARAM_ACCOUNT_BUCKET6, SqlDbType.Int)
            params(22).Direction = ParameterDirection.Output
            params(23) = New SqlParameter(PARAM_ACCOUNT_BUCKET7, SqlDbType.Int)
            params(23).Direction = ParameterDirection.Output
            params(24) = New SqlParameter(PARAM_ACCOUNT_BUCKET8, SqlDbType.Int)
            params(24).Direction = ParameterDirection.Output
            params(25) = New SqlParameter(PARAM_ACCOUNT_BUCKET9, SqlDbType.Int)
            params(25).Direction = ParameterDirection.Output
            params(26) = New SqlParameter(PARAM_ACCOUNT_BUCKET10, SqlDbType.Int)
            params(26).Direction = ParameterDirection.Output
            params(27) = New SqlParameter(PARAM_AMOUNT_BUCKET1, SqlDbType.Decimal)
            params(27).Direction = ParameterDirection.Output
            params(28) = New SqlParameter(PARAM_AMOUNT_BUCKET2, SqlDbType.Decimal)
            params(28).Direction = ParameterDirection.Output
            params(29) = New SqlParameter(PARAM_AMOUNT_BUCKET3, SqlDbType.Decimal)
            params(29).Direction = ParameterDirection.Output
            params(30) = New SqlParameter(PARAM_AMOUNT_BUCKET4, SqlDbType.Decimal)
            params(30).Direction = ParameterDirection.Output
            params(31) = New SqlParameter(PARAM_AMOUNT_BUCKET5, SqlDbType.Decimal)
            params(31).Direction = ParameterDirection.Output
            params(32) = New SqlParameter(PARAM_AMOUNT_BUCKET6, SqlDbType.Decimal)
            params(32).Direction = ParameterDirection.Output
            params(33) = New SqlParameter(PARAM_AMOUNT_BUCKET7, SqlDbType.Decimal)
            params(33).Direction = ParameterDirection.Output
            params(34) = New SqlParameter(PARAM_AMOUNT_BUCKET8, SqlDbType.Decimal)
            params(34).Direction = ParameterDirection.Output
            params(35) = New SqlParameter(PARAM_AMOUNT_BUCKET9, SqlDbType.Decimal)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter(PARAM_AMOUNT_BUCKET10, SqlDbType.Decimal)
            params(36).Direction = ParameterDirection.Output


        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqAgingStatus, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int32)
            ' objreader = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spInqAgingStatus, params)
            ' If objreader.Read Then
            With customclass
                .TotalODAccount = CInt(params(5).Value)
                .BalanceOD = CDbl(params(6).Value)
                .Bucket1 = CStr(params(7).Value)
                .Bucket2 = CStr(params(8).Value)
                .Bucket3 = CStr(params(9).Value)
                .Bucket4 = CStr(params(10).Value)
                .Bucket5 = CStr(params(11).Value)
                .Bucket6 = CStr(params(12).Value)
                .Bucket7 = CStr(params(13).Value)
                .Bucket8 = CStr(params(14).Value)
                .Bucket9 = CStr(params(15).Value)
                .Bucket10 = CStr(params(16).Value)
                .accountbucket1 = CInt(params(17).Value)
                .accountbucket2 = CInt(params(18).Value)
                .accountbucket3 = CInt(params(19).Value)
                .accountbucket4 = CInt(params(20).Value)
                .accountbucket5 = CInt(params(21).Value)
                .accountbucket6 = CInt(params(22).Value)
                .accountbucket7 = CInt(params(23).Value)
                .accountbucket8 = CInt(params(24).Value)
                .accountbucket9 = CInt(params(25).Value)
                .accountbucket10 = CInt(params(26).Value)
                .amountbucket1 = CDbl(params(27).Value)
                .amountbucket2 = CDbl(params(28).Value)
                .amountbucket3 = CDbl(params(29).Value)
                .amountbucket4 = CDbl(params(30).Value)
                .amountbucket5 = CDbl(params(31).Value)
                .amountbucket6 = CDbl(params(32).Value)
                .amountbucket7 = CDbl(params(33).Value)
                .amountbucket8 = CDbl(params(34).Value)
                .amountbucket9 = CDbl(params(35).Value)
                .amountbucket10 = CDbl(params(36).Value)
            End With
            ' End If
            '  objreader.Close()
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function InqSalesPerPeriod(ByVal customclass As Parameter.Sales) As Parameter.Sales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        With customclass

            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqSalesPerPeriod, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int32)

            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetProductWhere(ByVal customclass As Parameter.Sales) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataProductWhere, params).Tables(0)
    End Function
    Public Function GetProductBranch(ByVal customclass As Parameter.Sales) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAppGetProductBranch, params).Tables(0)
    End Function
    Public Function GetAssetCode(ByVal customclass As Parameter.Sales) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAppGetAsset).Tables(0)
    End Function

    Public Function DailySalesCMOCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customclass.sdate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customclass.edate

        customclass.Dataset = SqlHelper.ExecuteDataset(customclass.strConnection, _
        CommandType.StoredProcedure, "spSalesDailyCMOCross", params)

        Return customclass
    End Function

    Public Function DailySalesSupplierCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customclass.sdate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customclass.edate

        customclass.Dataset = SqlHelper.ExecuteDataset(customclass.strConnection, _
        CommandType.StoredProcedure, "spSalesDailySupplierCross", params)

        Return customclass
    End Function
End Class
