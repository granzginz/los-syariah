
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class StatementOfAO : Inherits DataAccessBase
#Region "PrivateConst"
    Private Const spMarketingAOSales As String = "spMarketingAOSales"
    Private Const spViewDataAO As String = "spSalesViewDataAO"
    Private Const spMarketingAOApproved As String = "spMarketingAOApproved"
    Private Const spMarketingAOFunding As String = "spMarketingAOFunding"
    Private Const spMarketingAOGrossYield As String = "spMarketingAOGrossYield"
    Private Const spMarketingAOBucket As String = "spMarketingAOBucket"
    Private Const spMarketingAOBucketTotal As String = "spMarketingAOBucketTotal"
    Private Const spMarketingAORepossess As String = "spMarketingAORepossess"
    Private Const spMarketingAOBPKB As String = "spMarketingAOBPKB"
    Private Const spMarketingAOBPKBPercent As String = "spMarketingAOBPKBPercent"
    Private Const spMarketingStatementAOForecast As String = "spMarketingStatementAOForecast"
    Private Const spMarketingStatementGetTarget As String = "spMarketingStatementGetTarget"
    Private Const spMarketingStatementBranchGetBranch As String = "spMarketingStatementBranchGetBranch"
    Private Const spMarketingStatementBranchForecast As String = "spMarketingStatementBranchForecast"
#End Region
    'fungsi ini untuk menampilkan forecast pada ao berdasar pada level ao
    Public Function MainReport(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingStatementAOForecast, params).Tables(0)
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spMarketingStatementGetTarget, params)
            If objread.Read Then
                With customclass
                    .Target = CType(objread("Target"), Integer)
                End With
            End If
            objread.Close()
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk menampilkan ranking pada ao berdasar pada level ao
    Public Function ReportView(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOSales, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SubReportApproved(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOApproved, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SubReportFunding(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOFunding, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SubReportGrossYield(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOGrossYield, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SubReportBucket(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOBucket, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SubReportBucketTotal(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOBucketTotal, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SubReportReppossess(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAORepossess, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function
    Public Function SubReportBPKB(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOBPKB, params).Tables(0)

            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function
    Public Function SubReportBPKBOverdue(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO

        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOBPKBPercent, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function
    'fungsi ini untuk mendapatkan data ao
    Public Function GetAO(ByVal customclass As Parameter.StatementOfAO) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataAO, params).Tables(0)
    End Function
    Public Function MainReportBranch(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 100)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter("@Year", SqlDbType.Int)
            params(1).Value = .Year

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingStatementBranchForecast, params).Tables(0)
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spMarketingStatementBranchGetBranch, params)
            If objread.Read Then
                With customclass
                    .BranchManagerName = CType(objread("branchmanagername"), String)
                    .BranchStatus = CType(objread("branchstatus"), String)
                    .AO = CType(objread("ao"), Integer)
                    .Productivity = CType(objread("ProductivityValue"), Integer)
                    .Supervisor = CType(objread("supervisor"), Integer)
                    .Admin = CType(objread("admin"), Integer)
                    .Collection = CType(objread("collection"), Integer)
                End With
            End If
            objread.Close()
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
