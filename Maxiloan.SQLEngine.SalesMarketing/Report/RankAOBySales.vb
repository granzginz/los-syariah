
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class RankAOBySales : Inherits DataAccessBase
#Region "PrivateConst"
    Private Const spMarketingAORank As String = "spMarketingAORank"
    
#End Region
    'fungsi ini untuk menampilkan ranking pada ao berdasar pada level ao
    Public Function AORankView(ByVal customclass As Parameter.RankAOBySales) As Parameter.RankAOBySales

        Dim params() As SqlParameter = New SqlParameter(0) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 200)
            params(0).Value = .WhereCond


        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAORank, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


End Class
