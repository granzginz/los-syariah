
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AOLevelReport : Inherits DataAccessBase
#Region "PrivateConst"
    Private Const spMarketingAOLevelEvaluation As String = "spMarketingAOLevelEvaluation"
    Private Const spViewDataAOSupervisor As String = "spSalesViewDataAOSupervisor"
    Private Const spViewDataAO As String = "spSalesViewDataAO"
#End Region
    'fungsi ini untuk menampilkan perubahan level pada ao berdasar supervisor tertentu
    Public Function AOLevelEvaluation(ByVal customclass As Parameter.AOLevelReport) As Parameter.AOLevelReport

        Dim params() As SqlParameter = New SqlParameter(0) {}
        With customclass

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.Char, 200)
            params(0).Value = .WhereCond


        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMarketingAOLevelEvaluation, params).Tables(0)
            Return customclass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk mendapatkan data ao
    Public Function GetAO(ByVal customclass As Parameter.AOLevelReport) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond

        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataAO, params).Tables(0)
    End Function

    Public Function GetAOSupervisor(ByVal customclass As Parameter.AOLevelReport) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataAOSupervisor).Tables(0)
    End Function
End Class
