﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CreditScoreGradeMaster : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    

    Private Const LIST_BY_ID As String = "spCreditScoreGradeMasterView"
    Private Const LIST_ADD As String = "spCreditScoreGradeMasterSaveAdd"
    Private Const LIST_UPDATE As String = "spCreditScoreGradeMasterSaveEdit"
    Private Const LIST_DELETE As String = "spCreditScoreGradeMasterDelete"
#End Region

    Public Function GetCreditScoreGradeMasterList(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As List(Of Parameter.CreditScoreGradeMaster)
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.CreditScoreGradeMaster
        Dim oReturnValueList As New List(Of Parameter.CreditScoreGradeMaster)
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.CreditScoreSchemeID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)            
            While reader.Read
                oReturnValue = New Parameter.CreditScoreGradeMaster
                oReturnValue.CreditScoreSchemeID = ocustomClass.CreditScoreSchemeID
                oReturnValue.CustType = reader("CustType").ToString
                oReturnValue.GradeID = CInt(reader("GradeID").ToString)
                oReturnValue.CustType = reader("CustType").ToString
                oReturnValue.GradeDescription = reader("GradeDescription").ToString
                oReturnValue.ScoreFrom = CInt(reader("ScoreFrom").ToString)
                oReturnValue.ScoreTo = CInt(reader("ScoreTo").ToString)
                oReturnValueList.Add(oReturnValue)
            End While
            reader.Close()
            Return oReturnValueList
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CreditScoreGradeMaster.GetCreditScoreGradeMasterList")
        End Try
    End Function

    Public Function CreditScoreGradeMasterSaveAdd(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As String
        Dim ErrMessage As String = ""
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.CreditScoreSchemeID
        params(1) = New SqlParameter("@CustType", SqlDbType.Char, 1)
        params(1).Value = ocustomClass.CustType
        params(2) = New SqlParameter("@GradeID", SqlDbType.Int)
        params(2).Value = ocustomClass.GradeID
        params(3) = New SqlParameter("@GradeDescription", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.GradeDescription
        params(4) = New SqlParameter("@ScoreFrom", SqlDbType.Int)
        params(4).Value = ocustomClass.ScoreFrom
        params(5) = New SqlParameter("@ScoreTo", SqlDbType.Int)
        params(5).Value = ocustomClass.ScoreTo

        params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(6).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CreditScoreGradeMaster.CreditScoreGradeMasterSaveAdd")
        End Try
    End Function

    Public Sub CreditScoreGradeMasterSaveEdit(ByVal ocustomClass As Parameter.CreditScoreGradeMaster)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.CreditScoreSchemeID
        params(1) = New SqlParameter("@CustType", SqlDbType.Char, 1)
        params(1).Value = ocustomClass.CustType
        params(2) = New SqlParameter("@GradeID", SqlDbType.Int)
        params(2).Value = ocustomClass.GradeID
        params(3) = New SqlParameter("@GradeDescription", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.GradeDescription
        params(4) = New SqlParameter("@ScoreFrom", SqlDbType.Int)
        params(4).Value = ocustomClass.ScoreFrom
        params(5) = New SqlParameter("@ScoreTo", SqlDbType.Int)
        params(5).Value = ocustomClass.ScoreTo
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CreditScoreGradeMaster.CreditScoreGradeMasterSaveEdit")
        End Try
    End Sub

    Public Function CreditScoreGradeMasterDelete(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.CreditScoreSchemeID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.CreditScoreGradeMaster.CreditScoreGradeMasterDelete")
        End Try
    End Function

End Class
