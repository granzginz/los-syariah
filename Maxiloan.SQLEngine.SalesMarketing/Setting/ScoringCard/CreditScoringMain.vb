

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CreditScoringMain : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spCreditScoringCardPaging"
    Private Const LIST_ADD As String = "spCreditScoringCardAdd"
    Private Const LIST_EDIT As String = "spCreditScoringCardEdit"
    Private Const LIST_VIEW As String = "spCreditScoringCardView"
    Private Const LIST_INSERT As String = "spCreditScoringCardSaveAdd"
    Private Const LIST_UPDATE As String = "spCreditScoringCardSaveEdit"
    Private Const LIST_DELETE As String = "spCreditScoringCardDelete"
    Private Const LIST_REPORT As String = "spCreditScoringCardReport"
    Private m_connection As SqlConnection

    ' Bagian Edit Range
    Private Const LIST_SELECT_RANGE As String = "spCreditScoringEditContent_RangePaging"
    Private Const LIST_EDIT_RANGE As String = "spCreditScoringEditContent_RangeEdit"
    Private Const LIST_UPDATE_RANGE As String = "spCreditScoringEditContent_RangeUpdate"
    Private Const LIST_ADD_RANGE As String = "spCreditScoringEditContent_RangeAdd"
    Private Const LIST_DELETE_RANGE As String = "spCreditScoringEditContent_RangeDelete"

    'Bagian Edit Table
    Private Const LIST_SELECT_TABLE As String = "spCreditScoringEditContent_TablePaging"
    Private Const LIST_EDIT_TABLE As String = "spCreditScoringEditContent_TableEdit"
    Private Const LIST_UPDATE_TABLE As String = "spCreditScoringEditContent_TableUpdate"
    Private Const LIST_ADD_TABLE As String = "spCreditScoringEditContent_TableAdd"
#End Region

#Region "New"
    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub
#End Region

    '=========================== Bagian Main==================================
#Region "GetCreditScoringMain"
    Public Function GetCreditScoringMain(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "GetCreditScoringMainReport"
    Public Function GetCreditScoringMainReport(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.CreditScoringMain

        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
#End Region

#Region "CreditScoringMainEdit"
    Public Function CreditScoringMainEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain        
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 2000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreShemeId
        params(2) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(2).Direction = ParameterDirection.Output
        params(3) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(3).Direction = ParameterDirection.Output
        params(4) = New SqlParameter("@PersonalApprovedScore", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@PersonalRejectScore", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@CompanyApprovedScore", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@CompanyRejectScore", SqlDbType.Int)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlParameter("@CustOffScore", SqlDbType.Int)
        params(8).Direction = ParameterDirection.Output
        'params(9) = New SqlParameter("@CutOffScoreAfterSurvey", SqlDbType.Int)
        'params(9).Direction = ParameterDirection.Output
        'params(8) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
        'params(8).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.CreditScoringMain
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(2).Value, Integer)
            oReturnValue.Description = CType(params(3).Value, String)

            If IsDBNull(params(4).Value) Then
                params(4).Value = "0"
            End If

            If IsDBNull(params(5).Value) Then
                params(5).Value = "0"
            End If

            If IsDBNull(params(6).Value) Then
                params(6).Value = "0"
            End If

            If IsDBNull(params(7).Value) Then
                params(7).Value = "0"
            End If

            If IsDBNull(params(8).Value) Then
                params(8).Value = "0"
            End If

            'If IsDBNull(params(9).Value) Then
            '    params(9).Value = "0"
            'End If

            oReturnValue.PersonalApprovedScore = CType(params(4).Value, Integer)
            oReturnValue.PersonalRejectScore = CType(params(5).Value, Integer)

            oReturnValue.CompanyApprovedScore = CType(params(6).Value, Integer)
            oReturnValue.CompanyRejectScore = CType(params(7).Value, Integer)

            oReturnValue.CutOffBefore = CType(params(8).Value, Integer)
            'oReturnValue.IsChecked = CChar(params(8).Value)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "CreditScoringMainAdd"
    Public Function CreditScoringMainAdd(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(1).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(1).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "CreditScoringMainSaveAdd"
    Public Function CreditScoringMainSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Dim dtPersonalCustomer As DataTable
        Dim dtPersonalAsset As DataTable
        Dim dtPersonalFinancial As DataTable
        Dim dtCompanyCustomer As DataTable
        Dim dtCompanyAsset As DataTable
        Dim dtCompanyFinancial As DataTable

        Dim counter_PersonalCustomer As Integer
        Dim counter_PersonalAsset As Integer
        Dim counter_PersonalFinancial As Integer
        Dim counter_CompanyCustomer As Integer
        Dim counter_CompanyAsset As Integer
        Dim counter_CompanyFinancial As Integer

        Dim description As String       

        With customClass
            dtPersonalCustomer = .dtPersonalCustomer
            dtPersonalAsset = .dtPersonalAsset
            dtPersonalFinancial = .dtPersonalFinancial
            dtCompanyCustomer = .dtCompanyCustomer
            dtCompanyAsset = .dtCompanyAsset
            dtCompanyFinancial = .dtCompanyFinancial
            description = .Description
        End With
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            Dim params(10) As SqlParameter
            params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(2) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
            params(3) = New SqlParameter("@Weight", SqlDbType.TinyInt, 1)
            params(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
            params(5) = New SqlParameter("@PersonalApprovedScore", SqlDbType.Decimal)
            params(6) = New SqlParameter("@PersonalRejectScore", SqlDbType.Decimal)
            params(7) = New SqlParameter("@CompanyApprovedScore", SqlDbType.Decimal)
            params(8) = New SqlParameter("@CompanyRejectScore", SqlDbType.Decimal)
            params(9) = New SqlParameter("@IsHeader", SqlDbType.Bit)
            params(10) = New SqlParameter("@Err", SqlDbType.VarChar, 10)
            params(10).Direction = ParameterDirection.Output

            params(0).Value = customClass.CreditScoreShemeId
            params(1).Value = customClass.Description
            params(2).Value = ""
            params(3).Value = 0
            params(4).Value = 0
            params(5).Value = customClass.PersonalApprovedScore
            params(6).Value = customClass.PersonalRejectScore
            params(7).Value = customClass.CompanyApprovedScore
            params(8).Value = customClass.CompanyRejectScore
            params(9).Value = customClass.CutOffBefore
            params(10).Value = CBool(1)

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            'PersonalCustomer
            For counter_PersonalCustomer = 0 To (dtPersonalCustomer.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("Weight"))
                params(4).Value = CBool(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("IsChecked"))
                params(5).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'PersonalAsset
            For counter_PersonalAsset = 0 To (dtPersonalAsset.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtPersonalAsset.Rows(counter_PersonalAsset).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtPersonalAsset.Rows(counter_PersonalAsset).Item("Weight"))
                params(4).Value = CBool(dtPersonalAsset.Rows(counter_PersonalAsset).Item("IsChecked"))
                params(5).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'PersonalFinancial
            For counter_PersonalFinancial = 0 To (dtPersonalFinancial.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("Weight"))
                params(4).Value = CBool(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("IsChecked"))
                params(5).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'CompanyCustomer
            For counter_CompanyCustomer = 0 To (dtCompanyCustomer.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("Weight"))
                params(4).Value = CBool(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("IsChecked"))
                params(5).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)

            Next

            'CompanyAsset
            For counter_CompanyAsset = 0 To (dtCompanyAsset.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtCompanyAsset.Rows(counter_CompanyAsset).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtCompanyAsset.Rows(counter_CompanyAsset).Item("Weight"))
                params(4).Value = CBool(dtCompanyAsset.Rows(counter_CompanyAsset).Item("IsChecked"))
                params(5).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)

                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'CompanyFinancial
            For counter_CompanyFinancial = 0 To (dtCompanyFinancial.Rows.Count - 1)
                params(0).Value = customClass.CreditScoreShemeId
                params(1).Value = customClass.Description
                params(2).Value = dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CreditScoreComponentID")
                params(3).Value = CInt(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("Weight"))
                params(4).Value = CBool(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("IsChecked"))
                params(5).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("PersonalApprovedScore"))
                params(6).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("PersonalRejectScore"))
                params(7).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CompanyApprovedScore"))
                params(8).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CompanyRejectScore"))
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                params(10).Value = CBool(0)
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception("Data Already Exists")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "CreditScoringMainDelete"
    Public Function CreditScoringMainDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CType(params(1).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "CreditScoringMainSaveEdit"
    Public Sub CreditScoringMainSaveEdit(ByVal customClass As Parameter.CreditScoringMain)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing


        Dim dtPersonalCustomer As DataTable
        Dim dtPersonalAsset As DataTable
        Dim dtPersonalFinancial As DataTable
        Dim dtCompanyCustomer As DataTable
        Dim dtCompanyAsset As DataTable
        Dim dtCompanyFinancial As DataTable
        Dim description As String

        Dim counter_PersonalCustomer As Integer
        Dim counter_PersonalAsset As Integer
        Dim counter_PersonalFinancial As Integer
        Dim counter_CompanyCustomer As Integer
        Dim counter_CompanyAsset As Integer
        Dim counter_CompanyFinancial As Integer

        With customClass
            dtPersonalCustomer = .dtPersonalCustomer
            dtPersonalAsset = .dtPersonalAsset
            dtPersonalFinancial = .dtPersonalFinancial
            dtCompanyCustomer = .dtCompanyCustomer
            dtCompanyAsset = .dtCompanyAsset
            dtCompanyFinancial = .dtCompanyFinancial
            description = .Description
        End With
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            Dim params(9) As SqlParameter
            params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
            params(1) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
            params(2) = New SqlParameter("@Weight", SqlDbType.TinyInt, 1)
            params(3) = New SqlParameter("@IsChecked", SqlDbType.Bit)
            params(4) = New SqlParameter("@PersonalApprovedScore", SqlDbType.Decimal)
            params(5) = New SqlParameter("@PersonalRejectScore", SqlDbType.Decimal)
            params(6) = New SqlParameter("@CompanyApprovedScore", SqlDbType.Decimal)
            params(7) = New SqlParameter("@CompanyRejectScore", SqlDbType.Decimal)
            params(8) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(9) = New SqlParameter("@CustOffScore", SqlDbType.Decimal)


            'PersonalCustomer
            For counter_PersonalCustomer = 0 To (dtPersonalCustomer.Rows.Count - 1)
                params(0).Value = dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CreditScoreSchemeID")
                params(1).Value = dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CreditScoreComponentID")
                params(2).Value = CInt(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("Weight"))
                params(3).Value = CBool(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("IsChecked"))
                params(4).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtPersonalCustomer.Rows(counter_PersonalCustomer).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'PersonalAsset
            For counter_PersonalAsset = 0 To (dtPersonalAsset.Rows.Count - 1)
                params(0).Value = dtPersonalAsset.Rows(counter_PersonalAsset).Item("CreditScoreSchemeID")
                params(1).Value = dtPersonalAsset.Rows(counter_PersonalAsset).Item("CreditScoreComponentID")
                params(2).Value = dtPersonalAsset.Rows(counter_PersonalAsset).Item("Weight")
                params(3).Value = dtPersonalAsset.Rows(counter_PersonalAsset).Item("IsChecked")
                params(4).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtPersonalAsset.Rows(counter_PersonalAsset).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'PersonalFinancial
            For counter_PersonalFinancial = 0 To (dtPersonalFinancial.Rows.Count - 1)
                params(0).Value = dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CreditScoreSchemeID")
                params(1).Value = dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CreditScoreComponentID")
                params(2).Value = dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("Weight")
                params(3).Value = dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("IsChecked")
                params(4).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtPersonalFinancial.Rows(counter_PersonalFinancial).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'CompanyCustomer
            For counter_CompanyCustomer = 0 To (dtCompanyCustomer.Rows.Count - 1)
                params(0).Value = dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CreditScoreSchemeID")
                params(1).Value = dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CreditScoreComponentID")
                params(2).Value = dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("Weight")
                params(3).Value = dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("IsChecked")
                params(4).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtCompanyCustomer.Rows(counter_CompanyCustomer).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'CompanyAsset
            For counter_CompanyAsset = 0 To (dtCompanyAsset.Rows.Count - 1)
                params(0).Value = dtCompanyAsset.Rows(counter_CompanyAsset).Item("CreditScoreSchemeID")
                params(1).Value = dtCompanyAsset.Rows(counter_CompanyAsset).Item("CreditScoreComponentID")
                params(2).Value = dtCompanyAsset.Rows(counter_CompanyAsset).Item("Weight")
                params(3).Value = dtCompanyAsset.Rows(counter_CompanyAsset).Item("IsChecked")
                params(4).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtCompanyAsset.Rows(counter_CompanyAsset).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next

            'CompanyFinancial
            For counter_CompanyFinancial = 0 To (dtCompanyFinancial.Rows.Count - 1)
                params(0).Value = dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CreditScoreSchemeID")
                params(1).Value = dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CreditScoreComponentID")
                params(2).Value = dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("Weight")
                params(3).Value = dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("IsChecked")
                params(4).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("PersonalApprovedScore"))
                params(5).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("PersonalRejectScore"))
                params(6).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CompanyApprovedScore"))
                params(7).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CompanyRejectScore"))
                params(8).Value = description
                params(9).Value = CDbl(dtCompanyFinancial.Rows(counter_CompanyFinancial).Item("CustOffScore"))
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.SPName, params)
            Next


            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
#End Region

    '=========================== Bagian View==================================
#Region "CreditScoringMainView"
    Public Function CreditScoringMainView(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 2000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreShemeId
        params(2) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(2).Direction = ParameterDirection.Output
        params(3) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(3).Direction = ParameterDirection.Output
        params(4) = New SqlParameter("@PersonalApprovedScore", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@PersonalRejectScore", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@CompanyApprovedScore", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@CompanyRejectScore", SqlDbType.Int)
        params(7).Direction = ParameterDirection.Output

        Try
            Dim oReturnValue As New Parameter.CreditScoringMain
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(2).Value, Integer)
            oReturnValue.Description = CType(params(3).Value, String)

            If IsDBNull(params(4).Value) Then
                params(4).Value = "0"
            End If

            If IsDBNull(params(5).Value) Then
                params(5).Value = "0"
            End If

            If IsDBNull(params(6).Value) Then
                params(6).Value = "0"
            End If

            If IsDBNull(params(7).Value) Then
                params(7).Value = "0"
            End If

            oReturnValue.PersonalApprovedScore = CType(params(4).Value, Integer)
            oReturnValue.PersonalRejectScore = CType(params(5).Value, Integer)
            oReturnValue.CompanyApprovedScore = CType(params(6).Value, Integer)
            oReturnValue.CompanyRejectScore = CType(params(7).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

    '=========================== Bagian Edit Content==================================

#Region "GetEditContentPaging"
    Public Function GetEditContentPaging(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "EditContentEdit"
    Public Function EditContentEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim reader As SqlDataReader
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeId", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentId", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ContentSeqNo", SqlDbType.Int)
        params(2).Value = customClass.ContentSeqNo

        Try
            Dim oReturnValue As New Parameter.CreditScoringMain
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            If reader.Read Then
                oReturnValue.ValueFrom = CInt(reader("ValueFrom"))
                oReturnValue.ValueTo = CInt(reader("ValueTo"))
                oReturnValue.ScoreValue = CInt(reader("ScoreValue"))
                oReturnValue.ScoreStatus = CStr(reader("ScoreStatus"))
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
#End Region

#Region "EditContentSaveAdd"
    Public Function EditContentSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim ErrMessage As String = ""
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeId", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentId", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ValueFrom", SqlDbType.Int)
        params(2).Value = customClass.ValueFrom
        params(3) = New SqlParameter("@ValueTo", SqlDbType.Int)
        params(3).Value = customClass.ValueTo
        params(4) = New SqlParameter("@ScoreValue", SqlDbType.Int)
        params(4).Value = customClass.ScoreValue
        params(5) = New SqlParameter("@ScoreStatus", SqlDbType.Char, 1)
        params(5).Value = customClass.ScoreStatus
        params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(6).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveAdd")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
#End Region

#Region "EditContentDelete"
    Public Function EditContentDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeId", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentId", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ContentSeqNo", SqlDbType.Int)
        params(2).Value = customClass.ContentSeqNo
        params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CType(params(3).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "EditContentSaveEdit"
    Public Sub EditContentSaveEdit(ByVal customClass As Parameter.CreditScoringMain)
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ContentSeqNo", SqlDbType.Int)
        params(2).Value = customClass.ContentSeqNo
        params(3) = New SqlParameter("@ValueFrom", SqlDbType.Int)
        params(3).Value = customClass.ValueFrom
        params(4) = New SqlParameter("@ValueTo", SqlDbType.Int)
        params(4).Value = customClass.ValueTo
        params(5) = New SqlParameter("@ScoreValue", SqlDbType.Int)
        params(5).Value = customClass.ScoreValue
        params(6) = New SqlParameter("@ScoreStatus", SqlDbType.Char, 1)
        params(6).Value = customClass.ScoreStatus

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveEdit")
        End Try
    End Sub
#End Region


#Region "GetEditContentPaging_Table"
    Public Function GetEditContentPaging_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@CreditScoreSchemeId", SqlDbType.Char, 10)
        params(4).Value = customClass.CreditScoreShemeId
        params(5) = New SqlParameter("@CreditScoreComponentId", SqlDbType.Char, 10)
        params(5).Value = customClass.CreditScoreComponentId
        params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(6).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "EditContentEdit_Table"
    Public Function EditContentEdit_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim reader As SqlDataReader
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeId", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentId", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ContentSeqNo", SqlDbType.Int)
        params(2).Value = customClass.ContentSeqNo

        Try
            Dim oReturnValue As New Parameter.CreditScoringMain
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            If reader.Read Then
                oReturnValue.ValueContent = CStr(reader("ValueContent"))
                oReturnValue.ValueDescription = CStr(reader("ValueDescription"))
                oReturnValue.ScoreValue = CInt(reader("ScoreValue"))
                oReturnValue.ScoreStatus = CStr(reader("ScoreStatus"))
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
#End Region

#Region "EditContentSaveEdit_Table"
    Public Sub EditContentSaveEdit_Table(ByVal customClass As Parameter.CreditScoringMain)
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = customClass.CreditScoreShemeId
        params(1) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
        params(1).Value = customClass.CreditScoreComponentId
        params(2) = New SqlParameter("@ContentSeqNo", SqlDbType.Int)
        params(2).Value = customClass.ContentSeqNo
        params(3) = New SqlParameter("@ValueContent", SqlDbType.VarChar, 10)
        params(3).Value = customClass.ValueContent
        params(4) = New SqlParameter("@ValueDescription", SqlDbType.VarChar, 100)
        params(4).Value = customClass.ValueDescription
        params(5) = New SqlParameter("@ScoreValue", SqlDbType.Int)
        params(5).Value = customClass.ScoreValue
        params(6) = New SqlParameter("@ScoreStatus", SqlDbType.Char, 1)
        params(6).Value = customClass.ScoreStatus

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveEdit")
        End Try
    End Sub
#End Region

#Region "Cut Off Score"
    Public Function CreditScoringCutOff(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function

    Public Function CreditScoringCutOffAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@ScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = customClass.ScoreSchemeID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@Package", SqlDbType.Char, 20)
        params(2).Value = customClass.Package
        params(3) = New SqlParameter("@CutOffScoreNewCustomer", SqlDbType.Int)
        params(3).Value = customClass.CutOffScoreNewCustomer
        params(4) = New SqlParameter("@CutOffScoreExistingCustomer", SqlDbType.Int)
        params(4).Value = customClass.CutOffScoreExistingCustomer
        params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(5).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CStr(params(5).Value)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.CutOffScoreAdd")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function

    Public Function CreditScoringCutOffEdit(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@ScoreSchemeID", SqlDbType.Char, 10)
        params(0).Value = customClass.ScoreSchemeID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@Package", SqlDbType.Char, 20)
        params(2).Value = customClass.Package
        params(3) = New SqlParameter("@CutOffScoreNewCustomer", SqlDbType.Int)
        params(3).Value = customClass.CutOffScoreNewCustomer
        params(4) = New SqlParameter("@CutOffScoreExistingCustomer", SqlDbType.Int)
        params(4).Value = customClass.CutOffScoreExistingCustomer
        params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(5).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CStr(params(5).Value)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.CutOffScoreEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function

    Public Function CreditScoringCutOffDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim oReturnValue As New Parameter.CreditScoringMain
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@Package", SqlDbType.Char, 20)
        params(1).Value = customClass.Package
        params(2) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CStr(params(2).Value)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.CutOffScoreDelete")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region
End Class



