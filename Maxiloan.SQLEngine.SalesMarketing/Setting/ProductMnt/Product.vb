

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Product : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spProductPaging"
    Private Const LIST_VIEW As String = "spProductView"

    Private Const LIST_EDIT As String = "spProductEdit" 
    Private Const LIST_INSERT As String = "spProductSaveAdd"
    Private Const LIST_UPDATE As String = "spProductSaveEdit"


    Private Const LIST_REPORT As String = "spProductReport"

    Private Const LIST_CB_ASSETTYPE As String = "spCbAssetType"
    Private Const LIST_CB_SCORESCHEMEMASTER As String = "spCbScoreSchemeMaster"
    Private Const LIST_CB_CREDITSCORESCHEMEMASTER As String = "spCbCreditScoreSchemeMaster"
    Private Const LIST_CB_JOURNALSCHEME As String = "spCbJournalScheme"
    Private Const LIST_CB_APPROVALTYPESCHEME As String = "spCbApprovalTypeScheme"
    Private Const LIST_CB_Term_n_Condition As String = "spCbTerm_n_Condition"
    Private Const LIST_CB_Term_n_Condition_Brc As String = "spCbTerm_n_Condition_Brc"

    Private Const LIST_SELECT_TC As String = "spProductTCPaging"
    Private Const LIST_EDIT_TC As String = "spProductTCEdit"
    Private Const LIST_INSERT_TC As String = "spProductTCSaveAdd"
    Private Const LIST_UPDATE_TC As String = "spProductTCSaveEdit"
    Private Const LIST_REPORT_TC As String = "spProductTCReport"
    Private Const LIST_DELETE_TC As String = "spProductTCDelete"

    Private Const LIST_SELECT_BRANCH As String = "spProductBranchHOPaging"
    Private Const LIST_EDIT_BRANCH As String = "spProductBranchHOEdit"
    Private Const LIST_INSERT_BRANCH As String = "spProductBranchHOSaveAdd"
    Private Const LIST_UPDATE_BRANCH As String = "spProductBranchHOSaveEdit"
    Private Const LIST_REPORT_BRANCH As String = "spProductBranchHOReport"

    Private Const LIST_SELECT_BRC As String = "spProductBranchPaging"
    Private Const LIST_EDIT_BRC As String = "spProductBranchEdit"
    Private Const LIST_UPDATE_BRC As String = "spProductBranchSaveEdit"
    Private Const LIST_REPORT_BRC As String = "spProductBranchReport"

    Private Const LIST_BRANCH As String = "spBranch"
    Private Const LIST_BRANCH_ALL As String = "spBranchAll"

    Private Const LIST_SELECT_BRC_TC As String = "spProductBranchTCPaging"
    Private Const LIST_EDIT_BRC_TC As String = "spProductBranchTCEdit"
    Private Const LIST_INSERT_BRC_TC As String = "spProductBranchTCSaveAdd"
    Private Const LIST_UPDATE_BRC_TC As String = "spProductBranchTCSaveEdit"
    Private Const LIST_REPORT_BRC_TC As String = "spProductBranchTCReport"
    Private Const LIST_DELETE_BRC_TC As String = "spProductBranchTCDelete"

    Private Const LIST_SELECT_OFF_BRC As String = "spProductOfferingBrcPaging"
    Private Const LIST_VIEW_OFF_BRC As String = "spProductOfferingBrcView"
    Private Const LIST_EDIT_OFF_BRC As String = "spProductOfferingBrcEdit"
    Private Const LIST_ADD_OFF_BRC As String = "spProductOfferingBrcAdd"
    Private Const LIST_INSERT_OFF_BRC As String = "spProductOfferingBrcSaveAdd"
    Private Const LIST_UPDATE_OFF_BRC As String = "spProductOfferingBrcSaveEdit"
    Private Const LIST_REPORT_OFF_BRC As String = "spProductOfferingBrcReport"
    Private Const LIST_DELETE_OFF_BRC As String = "spProductOfferingBrcDelete"
    Private Const spGetCboSkePemb As String = "spGetCboSkePemb"
    Private Const spGetCboJenPemb As String = "spGetCboJenPemb"
#End Region
#Region "helper"

#Region "Get_Combo_ScoreSchemeMaster"
    Public Function Get_Combo_ScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_SCORESCHEMEMASTER).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Get_Combo_CreditScoreSchemeMaster"
    Public Function Get_Combo_CreditScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_CREDITSCORESCHEMEMASTER).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Get_Combo_JournalScheme"
    Public Function Get_Combo_JournalScheme(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_JOURNALSCHEME).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Get_Combo_ApprovalTypeScheme"
    Public Function Get_Combo_ApprovalTypeScheme(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_APPROVALTYPESCHEME).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Get_Combo_Term_n_Condition"
    Public Function Get_Combo_Term_n_Condition(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_Term_n_Condition, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "Get_Combo_Term_n_Condition_Brc"
    Public Function Get_Combo_Term_n_Condition_Brc(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim oReturnValue As New Parameter.Product
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(1).Value = customClass.ProductId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_Term_n_Condition_Brc, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#Region "Get_Combo_AssetType"
    Public Function Get_Combo_AssetType(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_CB_ASSETTYPE).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#End Region


    Function bindParams(ByVal customClass As Parameter.Product, add As Boolean) As IList(Of SqlParameter)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        If (add = True) Then
            params.Add(New SqlParameter("@TotalBranch", SqlDbType.VarChar, 1000) With {.Value = customClass.TotalBranch})
            params.Add(New SqlParameter("@SaveProduct", SqlDbType.VarChar, 10) With {.Value = customClass.SaveProduct})
        End If


        params.Add(New SqlParameter("@Description", SqlDbType.VarChar, 100) With {.Value = customClass.Description})
        params.Add(New SqlParameter("@AssetTypeID", SqlDbType.Char, 10) With {.Value = customClass.AssetTypeID})
        params.Add(New SqlParameter("@ScoreSchemeID", SqlDbType.Char, 10) With {.Value = customClass.ScoreSchemeID})
        params.Add(New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10) With {.Value = customClass.CreditScoreSchemeID})
        params.Add(New SqlParameter("@JournalSchemeID", SqlDbType.Char, 10) With {.Value = customClass.JournalSchemeID})
        params.Add(New SqlParameter("@ApprovalSchemeID", SqlDbType.Char, 10) With {.Value = customClass.ApprovalSchemeID})
        params.Add(New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1) With {.Value = customClass.AssetUsedNew})

        params.Add(New SqlParameter("@IsSPAutomaticBehaviour", SqlDbType.Char, 1) With {.Value = customClass.IsSPAutomaticBehaviour})
        params.Add(New SqlParameter("@IsSP1AutomaticBehaviour", SqlDbType.Char, 1) With {.Value = customClass.IsSP1AutomaticBehaviour})
        params.Add(New SqlParameter("@IsSP2AutomaticBehaviour", SqlDbType.Char, 1) With {.Value = customClass.IsSP2AutomaticBehaviour})
        params.Add(New SqlParameter("@IsRecourse", SqlDbType.Bit) With {.Value = customClass.IsRecourse})

        params.Add(New SqlParameter("@KegiatanUsaha", SqlDbType.Char, 1) With {.Value = customClass.KegiatanUsaha})
        params.Add(New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 2) With {.Value = customClass.JenisPembiayaan})
        params.Add(New SqlParameter("@Paket", SqlDbType.Char, 3) With {.Value = customClass.paket})

        Return ProdParams(customClass, params)

    End Function

    Function ProdParams(ByVal customClass As Parameter.Product, params As IList(Of SqlParameter)) As IList(Of SqlParameter)
        params.Add(New SqlParameter("@ProductId", SqlDbType.Char, 10) With {.Value = customClass.ProductId})

        params.Add(New SqlParameter("@EffectiveRate", SqlDbType.Decimal) With {.Value = customClass.EffectiveRate})
        params.Add(New SqlParameter("@EffectiveRateBehaviour", SqlDbType.Char, 1) With {.Value = customClass.EffectiveRateBehaviour})
        params.Add(New SqlParameter("@GrossYieldRate", SqlDbType.Decimal) With {.Value = customClass.GrossYieldRate})
        params.Add(New SqlParameter("@GrossYieldRateBehaviour", SqlDbType.Char, 1) With {.Value = "D"})
        params.Add(New SqlParameter("@MinimumTenor", SqlDbType.SmallInt, 2) With {.Value = customClass.MinimumTenor})
        params.Add(New SqlParameter("@MaximumTenor", SqlDbType.SmallInt, 2) With {.Value = customClass.MaximumTenor})
        params.Add(New SqlParameter("@PenaltyPercentage", SqlDbType.Decimal) With {.Value = customClass.PenaltyPercentage})
        params.Add(New SqlParameter("@PenaltyPercentageBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PenaltyPercentageBehaviour})
        params.Add(New SqlParameter("@InsurancePenaltyPercentage", SqlDbType.Decimal) With {.Value = customClass.InsurancePenaltyPercentage})
        params.Add(New SqlParameter("@InsurancePenaltyPercentageBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InsurancePenaltyPercentageBehaviour})

        params.Add(New SqlParameter("@CancellationFee", SqlDbType.Decimal) With {.Value = customClass.CancellationFee})
        params.Add(New SqlParameter("@CancellationFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.CancellationFeeBehaviour})
        params.Add(New SqlParameter("@AdminFee", SqlDbType.Decimal) With {.Value = customClass.AdminFee})
        params.Add(New SqlParameter("@AdminFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.AdminFeeBehaviour})
        params.Add(New SqlParameter("@FiduciaFee", SqlDbType.Decimal) With {.Value = customClass.FiduciaFee})
        params.Add(New SqlParameter("@FiduciaFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.FiduciaFeeBehaviour})
        params.Add(New SqlParameter("@ProvisionFee", SqlDbType.Decimal) With {.Value = customClass.ProvisionFee})
        params.Add(New SqlParameter("@ProvisionFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.ProvisionFeeBehaviour})
        params.Add(New SqlParameter("@NotaryFee", SqlDbType.Decimal) With {.Value = customClass.NotaryFee})
        params.Add(New SqlParameter("@NotaryFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.NotaryFeeBehaviour})
        params.Add(New SqlParameter("@SurveyFee", SqlDbType.Decimal) With {.Value = customClass.SurveyFee})
        params.Add(New SqlParameter("@SurveyFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.SurveyFeeBehaviour})
        params.Add(New SqlParameter("@VisitFee", SqlDbType.Decimal) With {.Value = customClass.VisitFee})
        params.Add(New SqlParameter("@VisitFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.VisitFeeBehaviour})
        params.Add(New SqlParameter("@ReschedulingFee", SqlDbType.Decimal) With {.Value = customClass.ReschedulingFee})
        params.Add(New SqlParameter("@ReschedulingFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.ReschedulingFeeBehaviour})
        params.Add(New SqlParameter("@AgreementTransferFee", SqlDbType.Decimal) With {.Value = customClass.AgreementTransferFee})
        params.Add(New SqlParameter("@AgreementTransferFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.AgreementTransferFeeBehaviour})
        params.Add(New SqlParameter("@ChangeDueDateFee", SqlDbType.Decimal) With {.Value = customClass.ChangeDueDateFee})
        params.Add(New SqlParameter("@ChangeDueDateFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.ChangeDueDateFeeBehaviour})
        params.Add(New SqlParameter("@AssetReplacementFee", SqlDbType.Decimal) With {.Value = customClass.AssetReplacementFee})
        params.Add(New SqlParameter("@AssetReplacementFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.RepossesFeeBehaviour})
        params.Add(New SqlParameter("@RepossesFee", SqlDbType.Decimal) With {.Value = customClass.RepossesFee})
        params.Add(New SqlParameter("@RepossesFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.RepossesFeeBehaviour})
        params.Add(New SqlParameter("@LegalisirDocFee", SqlDbType.Decimal) With {.Value = customClass.LegalisirDocFee})
        params.Add(New SqlParameter("@LegalisirDocFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.LegalisirDocFeeBehaviour})


        params.Add(New SqlParameter("@PDCBounceFee", SqlDbType.Decimal) With {.Value = customClass.PDCBounceFee})
        params.Add(New SqlParameter("@PDCBounceFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PDCBounceFeeBehaviour})
        params.Add(New SqlParameter("@InsAdminFee", SqlDbType.Decimal) With {.Value = customClass.InsAdminFee})
        params.Add(New SqlParameter("@InsAdminFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InsAdminFeeBehaviour})
        params.Add(New SqlParameter("@InsStampDutyFee", SqlDbType.Decimal) With {.Value = customClass.InsStampDutyFee})
        params.Add(New SqlParameter("@InsStampDutyFeeBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InsStampDutyFeeBehaviour})
        params.Add(New SqlParameter("@DaysPOExpiration", SqlDbType.Int) With {.Value = customClass.DaysPOExpiration})
        params.Add(New SqlParameter("@DaysPOExpirationBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DaysPOExpirationBehaviour})
        params.Add(New SqlParameter("@InstallmentToleranceAmount", SqlDbType.Decimal) With {.Value = customClass.InstallmentToleranceAmount})
        params.Add(New SqlParameter("@InstallmentToleranceAmountBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InstallmentToleranceAmountBehaviour})
        params.Add(New SqlParameter("@DPPercentage", SqlDbType.Decimal) With {.Value = customClass.DPPercentage})
        params.Add(New SqlParameter("@DPPercentageBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DPPercentageBehaviour})

        params.Add(New SqlParameter("@RejectMinimumIncome", SqlDbType.Decimal) With {.Value = customClass.RejectMinimumIncome})
        params.Add(New SqlParameter("@RejectMinimumIncomeBehaviour", SqlDbType.Char, 1) With {.Value = "D"})
        params.Add(New SqlParameter("@WarningMinimumIncome", SqlDbType.Decimal) With {.Value = customClass.WarningMinimumIncome})
        params.Add(New SqlParameter("@WarningMinimumIncomeBehaviour", SqlDbType.Char, 1) With {.Value = "D"})
        params.Add(New SqlParameter("@IsSPAutomatic", SqlDbType.Bit) With {.Value = customClass.IsSPAutomatic})

        params.Add(New SqlParameter("@LengthSPProcess", SqlDbType.Int) With {.Value = customClass.LengthSPProcess})
        params.Add(New SqlParameter("@IsSP1Automatic", SqlDbType.Bit) With {.Value = customClass.IsSP1Automatic})
        params.Add(New SqlParameter("@LengthSP1Process", SqlDbType.Int) With {.Value = customClass.LengthSP1Process})
        params.Add(New SqlParameter("@IsSP2Automatic", SqlDbType.Bit) With {.Value = customClass.IsSP2Automatic})
        params.Add(New SqlParameter("@LengthSP2Process", SqlDbType.Int) With {.Value = customClass.LengthSP2Process})
        params.Add(New SqlParameter("@LengthMainDocProcess", SqlDbType.Int) With {.Value = customClass.LengthMainDocProcess})
        params.Add(New SqlParameter("@LengthMainDocProcessBehaviour", SqlDbType.Char, 1) With {.Value = customClass.LengthMainDocProcessBehaviour})

        params.Add(New SqlParameter("@LengthMainDocTaken", SqlDbType.Int) With {.Value = customClass.LengthMainDocTaken})
        params.Add(New SqlParameter("@LengthMainDocTakenBehaviour", SqlDbType.Char, 1) With {.Value = customClass.LengthMainDocTakenBehaviour})
        params.Add(New SqlParameter("@GracePeriodLateCharges", SqlDbType.Int) With {.Value = customClass.GracePeriodLateCharges})
        params.Add(New SqlParameter("@GracePeriodLateChargesBehaviour", SqlDbType.Char, 1) With {.Value = customClass.GracePeriodLateChargesBehaviour})
        params.Add(New SqlParameter("@PrepaymentPenaltyRate", SqlDbType.Decimal) With {.Value = customClass.PrepaymentPenaltyRate})
        params.Add(New SqlParameter("@PrepaymentPenaltyRateBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PrepaymentPenaltyRateBehaviour})
        params.Add(New SqlParameter("@DeskCollPhoneRemind", SqlDbType.Int) With {.Value = customClass.DeskCollPhoneRemind})
        params.Add(New SqlParameter("@DeskCollPhoneRemindBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DeskCollPhoneRemindBehaviour})


        params.Add(New SqlParameter("@DeskCollOD", SqlDbType.Int) With {.Value = customClass.DeskCollOD})
        params.Add(New SqlParameter("@DeskCollODBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DeskCollODBehaviour})
        params.Add(New SqlParameter("@PrevODToRemind", SqlDbType.Int) With {.Value = customClass.PrevODToRemind})
        params.Add(New SqlParameter("@PrevODToRemindBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PrevODToRemindBehaviour})
        params.Add(New SqlParameter("@PDCDayToRemind", SqlDbType.Int) With {.Value = customClass.PDCDayToRemind})



        params.Add(New SqlParameter("@PDCDayToRemindBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PDCDayToRemindBehaviour})
        params.Add(New SqlParameter("@DeskCollSMSRemind", SqlDbType.Int) With {.Value = customClass.DeskCollSMSRemind})
        params.Add(New SqlParameter("@DeskCollSMSRemindBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DeskCollSMSRemindBehaviour})

        ''meisan


        params.Add(New SqlParameter("@PengaturanPembayaran", SqlDbType.Char, 50) With {.Value = customClass.PengaturanBiaya})

        params.Add(New SqlParameter("@DeskCollSMSRemind2", SqlDbType.Int) With {.Value = customClass.DeskCollSMSRemind2})
        params.Add(New SqlParameter("@DeskCollSMSRemindBehaviour2", SqlDbType.Char, 1) With {.Value = customClass.DeskCollSMSRemindBehaviour2})

        params.Add(New SqlParameter("@DeskCollSMSRemind3", SqlDbType.Int) With {.Value = customClass.DeskCollSMSRemind3})
        params.Add(New SqlParameter("@DeskCollSMSRemindBehaviour3", SqlDbType.Char, 1) With {.Value = customClass.DeskCollSMSRemindBehaviour3})

        params.Add(New SqlParameter("@DCR", SqlDbType.Int) With {.Value = customClass.DCR})
        params.Add(New SqlParameter("@DCRBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DCRBehaviour})
        params.Add(New SqlParameter("@DaysBeforeDueToRN", SqlDbType.Int) With {.Value = customClass.DaysBeforeDueToRN})
        params.Add(New SqlParameter("@DaysBeforeDueToRNBehaviour", SqlDbType.Char, 1) With {.Value = customClass.DaysBeforeDueToRNBehaviour})
        params.Add(New SqlParameter("@ODToRAL", SqlDbType.Int) With {.Value = customClass.ODToRAL})
        params.Add(New SqlParameter("@ODToRALBehaviour", SqlDbType.Char, 1) With {.Value = customClass.ODToRALBehaviour})
        params.Add(New SqlParameter("@RALPeriod", SqlDbType.Int) With {.Value = customClass.RALPeriod})
        params.Add(New SqlParameter("@RALPeriodBehaviour", SqlDbType.Char, 1) With {.Value = customClass.RALPeriodBehaviour})
        params.Add(New SqlParameter("@MaxDaysRNPaid", SqlDbType.Int) With {.Value = customClass.MaxDaysRNPaid})
        params.Add(New SqlParameter("@MaxDaysRNPaidBehaviour", SqlDbType.Char, 1) With {.Value = customClass.MaxDaysRNPaidBehaviour})
        params.Add(New SqlParameter("@MaxPTPYDays", SqlDbType.Int) With {.Value = customClass.MaxPTPYDays})
        params.Add(New SqlParameter("@MaxPTPYDaysBehaviour", SqlDbType.Char, 1) With {.Value = customClass.MaxPTPYDaysBehaviour})
        params.Add(New SqlParameter("@PTPYBank", SqlDbType.Int) With {.Value = customClass.PTPYBank})
        params.Add(New SqlParameter("@PTPYBankBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PTPYBankBehaviour})


        params.Add(New SqlParameter("@PTPYCompany", SqlDbType.Int) With {.Value = customClass.PTPYCompany})
        params.Add(New SqlParameter("@PTPYCompanyBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PTPYCompanyBehaviour})
        params.Add(New SqlParameter("@PTPYSupplier", SqlDbType.Int) With {.Value = customClass.PTPYSupplier})
        params.Add(New SqlParameter("@PTPYSupplierBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PTPYSupplierBehaviour})
        params.Add(New SqlParameter("@RALExtension", SqlDbType.Int) With {.Value = customClass.RALExtension})
        params.Add(New SqlParameter("@RALExtensionBehaviour", SqlDbType.Char, 1) With {.Value = customClass.RALExtensionBehaviour})
        params.Add(New SqlParameter("@InventoryExpected", SqlDbType.Int) With {.Value = customClass.InventoryExpected})
        params.Add(New SqlParameter("@InventoryExpectedBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InventoryExpectedBehaviour})
        params.Add(New SqlParameter("@BillingCharges", SqlDbType.Decimal) With {.Value = customClass.BillingCharges})
        params.Add(New SqlParameter("@BillingChargesBehaviour", SqlDbType.Char, 1) With {.Value = customClass.BillingChargesBehaviour})
        params.Add(New SqlParameter("@LimitAPCash", SqlDbType.Decimal) With {.Value = customClass.LimitAPCash})
        params.Add(New SqlParameter("@LimitAPCashBehaviour", SqlDbType.Char, 1) With {.Value = customClass.LimitAPCashBehaviour})


        params.Add(New SqlParameter("@IsActive", SqlDbType.Bit) With {.Value = customClass.IsActive})
        params.Add(New SqlParameter("@PenaltyBasedOn", SqlDbType.Char, 2) With {.Value = customClass.PenaltyBasedOn})
        params.Add(New SqlParameter("@PenaltyBasedOnBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PenaltyBasedOnBehaviour})
        params.Add(New SqlParameter("@PrepaymentPenaltyFixed", SqlDbType.Decimal) With {.Value = customClass.PrepaymentPenaltyFixed})
        params.Add(New SqlParameter("@PrepaymentPenaltyFixedBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PrepaymentPenaltyFixedBehaviour})
        params.Add(New SqlParameter("@PenaltyRatePrevious", SqlDbType.Decimal) With {.Value = customClass.PenaltyRatePrevious})
        params.Add(New SqlParameter("@PenaltyRateEffectiveDate", SqlDbType.SmallDateTime) With {.Value = customClass.PenaltyRateEffectiveDate})
        params.Add(New SqlParameter("@PrepaymentPenaltyPrevious", SqlDbType.Decimal) With {.Value = customClass.PrepaymentPenaltyPrevious})
        params.Add(New SqlParameter("@PrepaymentPenaltyEffectiveDate", SqlDbType.SmallDateTime) With {.Value = customClass.PrepaymentPenaltyEffectiveDate})
        params.Add(New SqlParameter("@PrepaymentPenaltyFixedEffectiveDate", SqlDbType.SmallDateTime) With {.Value = customClass.PrepaymentPenaltyFixedEffectiveDate})
        params.Add(New SqlParameter("@PrepaymentPenaltyFixedPrevious", SqlDbType.Decimal) With {.Value = customClass.PrepaymentPenaltyFixedPrevious})
        params.Add(New SqlParameter("@PrioritasPembayaran", SqlDbType.Char, 4) With {.Value = customClass.PrioritasPembayaran})
    
        params.Add(New SqlParameter("@InsuranceDiscPercentage", SqlDbType.Decimal) With {.Value = customClass.InsuranceDiscPercentage})

        params.Add(New SqlParameter("@InsuranceDiscPercentageBehaviour", SqlDbType.Char, 1) With {.Value = customClass.InsuranceDiscPercentageBehaviour})
        params.Add(New SqlParameter("@IncomeInsRatioPercentage", SqlDbType.Decimal) With {.Value = customClass.IncomeInsRatioPercentage})
        params.Add(New SqlParameter("@IncomeInsRatioPercentageBehaviour", SqlDbType.Char, 1) With {.Value = customClass.IncomeInsRatioPercentageBehaviour})
        params.Add(New SqlParameter("@UmurKendaraanFrom", SqlDbType.SmallInt) With {.Value = customClass.UmurKendaraanFrom})
        params.Add(New SqlParameter("@UmurKendaraanTo", SqlDbType.SmallInt) With {.Value = customClass.UmurKendaraanTo})
        params.Add(New SqlParameter("@BiayaPolis", SqlDbType.Decimal) With {.Value = customClass.BiayaPolis})
        params.Add(New SqlParameter("@BiayaPolisBehaviour", SqlDbType.Char, 1) With {.Value = customClass.BiayaPolisBehaviour})

        params.Add(New SqlParameter("@SukuBungaFlat", SqlDbType.Decimal) With {.Value = customClass.SukuBungaFlat})
        params.Add(New SqlParameter("@SukuBungaFlatBehaviour", SqlDbType.Char, 1) With {.Value = customClass.SukuBungaFlatBehaviour})
        params.Add(New SqlParameter("@AngsuranPertama", SqlDbType.Char, 3) With {.Value = customClass.AngsuranPertama})
        params.Add(New SqlParameter("@OpsiUangMuka", SqlDbType.Char, 1) With {.Value = customClass.OpsiUangMuka})
        params.Add(New SqlParameter("@OpsiUangMukaAngsur", SqlDbType.SmallInt) With {.Value = customClass.OpsiUangMukaAngsur})

        params.Add(New SqlParameter("@PerpanjanganSKT", SqlDbType.SmallInt) With {.Value = customClass.PerpanjanganSKT})
        params.Add(New SqlParameter("@PerpanjanganSKTBehaviour", SqlDbType.Char, 1) With {.Value = customClass.PerpanjanganSKTBehaviour})


        params.Add(New SqlParameter("@CreditProtection", SqlDbType.SmallInt) With {.Value = customClass.CreditProtection})
        params.Add(New SqlParameter("@CreditProtectionBehaviour", SqlDbType.Char, 1) With {.Value = customClass.CreditProtectionBehaviour})

        params.Add(New SqlParameter("@InsJaminanKredit", SqlDbType.SmallInt) With {.Value = customClass.AsuransiKredit})
        params.Add(New SqlParameter("@InsJaminanKreditBehaviour", SqlDbType.Char, 1) With {.Value = customClass.AsuransiKreditBehaviour})
        params.Add(New SqlParameter("@SkemaPembiayaan", SqlDbType.Char, 3) With {.Value = customClass.SkemaPembiayaan})


        Return params
    End Function

    Private Sub readProductFromReader(oReturnValue As Parameter.Product, reader As IDataReader, Optional ho As Boolean = False)

        oReturnValue.IsRecourse = CBool(reader("IsRecourse"))
        oReturnValue.IsActive = CBool(reader("IsActive"))
        oReturnValue.Description = reader("Description").ToString
        oReturnValue.AssetTypeID = reader("AssetTypeID").ToString
        oReturnValue.AssetType_desc = reader("AssetType_desc").ToString
        oReturnValue.ScoreSchemeID = reader("ScoreSchemeID").ToString
        oReturnValue.ScoreSchemeMaster_desc = reader("ScoreScheme_desc").ToString
        oReturnValue.CreditScoreSchemeID = reader("CreditScoreSchemeID").ToString
        oReturnValue.CreditScoreSchemeMaster_desc = reader("CreditScoreScheme_desc").ToString
        oReturnValue.JournalSchemeID = reader("JournalSchemeID").ToString
        oReturnValue.JournalScheme_desc = reader("JournalScheme_desc").ToString
        oReturnValue.ApprovalSchemeID = reader("ApprovalSchemeID").ToString
        oReturnValue.ApprovalTypeScheme_desc = reader("ApprovalScheme_desc").ToString
        oReturnValue.AssetUsedNew = reader("AssetUsedNew").ToString
        oReturnValue.FinanceType = reader("FinanceType").ToString
        oReturnValue.EffectiveRate = CDec(reader("EffectiveRate"))
        oReturnValue.EffectiveRateBehaviour = reader("EffectiveRatePatern").ToString
        oReturnValue.GrossYieldRate = CDec(reader("GrossYieldRate"))
        oReturnValue.GrossYieldRateBehaviour = reader("GrossYieldRatePatern").ToString
        oReturnValue.MinimumTenor = CInt(reader("MinimumTenor"))
        oReturnValue.MaximumTenor = CInt(reader("MaximumTenor"))
        oReturnValue.PenaltyPercentage = CDec(reader("PenaltyPercentage"))
        oReturnValue.PenaltyPercentageBehaviour = reader("PenaltyPercentagePatern").ToString
        oReturnValue.InsurancePenaltyPercentage = CDec(reader("InsurancePenaltyPercentage"))
        oReturnValue.InsurancePenaltyPercentageBehaviour = reader("InsurancePenaltyPercentagePatern").ToString
        oReturnValue.CancellationFee = CDec(reader("CancellationFee"))
        oReturnValue.CancellationFeeBehaviour = reader("CancellationFeePatern").ToString
        oReturnValue.AdminFee = CDec(reader("AdminFee"))
        oReturnValue.AdminFeeBehaviour = reader("AdminFeePatern").ToString
        oReturnValue.FiduciaFee = CDec(reader("FiduciaFee"))
        oReturnValue.FiduciaFeeBehaviour = reader("FiduciaFeePatern").ToString
        oReturnValue.ProvisionFee = CDec(reader("ProvisionFee"))
        oReturnValue.ProvisionFeeBehaviour = reader("ProvisionFeePatern").ToString
        oReturnValue.NotaryFee = CDec(reader("NotaryFee"))
        oReturnValue.NotaryFeeBehaviour = reader("NotaryFeePatern").ToString
        oReturnValue.SurveyFee = CDec(reader("SurveyFee"))
        oReturnValue.SurveyFeeBehaviour = reader("SurveyFeePatern").ToString
        oReturnValue.VisitFee = CDec(reader("VisitFee"))
        oReturnValue.VisitFeeBehaviour = reader("VisitFeePatern").ToString
        oReturnValue.ReschedulingFee = CDec(reader("ReschedulingFee"))
        oReturnValue.ReschedulingFeeBehaviour = reader("ReschedulingFeePatern").ToString
        oReturnValue.AgreementTransferFee = CDec(reader("AgreementTransferFee"))
        oReturnValue.AgreementTransferFeeBehaviour = reader("AgreementTransferFeePatern").ToString
        oReturnValue.ChangeDueDateFee = CDec(reader("ChangeDueDateFee"))
        oReturnValue.ChangeDueDateFeeBehaviour = reader("ChangeDueDateFeePatern").ToString
        oReturnValue.AssetReplacementFee = CDec(reader("AssetReplacementFee"))
        oReturnValue.AssetReplacementFeeBehaviour = reader("AssetReplacementFeePatern").ToString
        oReturnValue.RepossesFee = CDec(reader("RepossesFee"))
        oReturnValue.RepossesFeeBehaviour = reader("RepossesFeePatern").ToString
        oReturnValue.LegalisirDocFee = CDec(reader("LegalisirDocFee"))
        oReturnValue.LegalisirDocFeeBehaviour = reader("LegalisirDocFeePatern").ToString
        oReturnValue.PDCBounceFee = CDec(reader("PDCBounceFee"))
        oReturnValue.PDCBounceFeeBehaviour = reader("PDCBounceFeePatern").ToString
        oReturnValue.InsAdminFee = CDec(reader("InsAdminFee"))
        oReturnValue.InsAdminFeeBehaviour = reader("InsAdminFeePatern").ToString
        oReturnValue.InsStampDutyFee = CDec(reader("InsStampDutyFee"))
        oReturnValue.InsStampDutyFeeBehaviour = reader("InsStampDutyFeePatern").ToString
        oReturnValue.DaysPOExpiration = CInt(reader("DaysPOExpiration"))
        oReturnValue.DaysPOExpirationBehaviour = reader("DaysPOExpirationPatern").ToString
        oReturnValue.InstallmentToleranceAmount = CDec(reader("InstallmentToleranceAmount"))
        oReturnValue.InstallmentToleranceAmountBehaviour = reader("InstallmentToleranceAmountPatern").ToString
        oReturnValue.DPPercentage = CDec(reader("DPPercentage"))
        oReturnValue.DPPercentageBehaviour = reader("DPPercentagePatern").ToString
        oReturnValue.RejectMinimumIncome = CDec(reader("RejectMinimumIncome"))
        oReturnValue.RejectMinimumIncomeBehaviour = reader("RejectMinimumIncomePatern").ToString
        oReturnValue.WarningMinimumIncome = CDec(reader("WarningMinimumIncome"))
        oReturnValue.WarningMinimumIncomeBehaviour = reader("WarningMinimumIncomePatern").ToString
        oReturnValue.IsSPAutomatic = CBool(reader("IsSPAutomatic"))
        oReturnValue.IsSPAutomaticBehaviour = reader("IsSPAutomaticPatern").ToString
        oReturnValue.LengthSPProcess = CInt(reader("LengthSPProcess"))
        oReturnValue.IsSP1Automatic = CBool(reader("IsSP1Automatic"))
        oReturnValue.IsSP1AutomaticBehaviour = reader("IsSP1AutomaticPatern").ToString
        oReturnValue.LengthSP1Process = CInt(reader("LengthSP1Process"))
        oReturnValue.IsSP2Automatic = CBool(reader("IsSP2Automatic"))
        oReturnValue.IsSP2AutomaticBehaviour = reader("IsSP2AutomaticPatern").ToString
        oReturnValue.LengthSP2Process = CInt(reader("LengthSP2Process"))
        oReturnValue.LengthMainDocProcess = CInt(reader("LengthMainDocProcess"))
        oReturnValue.LengthMainDocProcessBehaviour = reader("LengthMainDocProcessPatern").ToString
        oReturnValue.LengthMainDocTaken = CInt(reader("LengthMainDocTaken"))
        oReturnValue.LengthMainDocTakenBehaviour = reader("LengthMainDocTakenPatern").ToString
        oReturnValue.GracePeriodLateCharges = CInt(reader("GracePeriodLateCharges"))
        oReturnValue.GracePeriodLateChargesBehaviour = reader("GracePeriodLateChargesPatern").ToString
        oReturnValue.PrepaymentPenaltyRate = CDec(reader("PrepaymentPenaltyRate"))
        oReturnValue.PrepaymentPenaltyRateBehaviour = reader("PrepaymentPenaltyRatePatern").ToString
        oReturnValue.DeskCollPhoneRemind = CInt(reader("DeskCollPhoneRemind"))
        oReturnValue.DeskCollPhoneRemindBehaviour = reader("DeskCollPhoneRemindPatern").ToString
        oReturnValue.DeskCollOD = CInt(reader("DeskCollOD"))
        oReturnValue.DeskCollODBehaviour = reader("DeskCollODPatern").ToString
        oReturnValue.PrevODToRemind = CInt(reader("PrevODToRemind"))
        oReturnValue.PrevODToRemindBehaviour = reader("PrevODToRemindPatern").ToString
        oReturnValue.PDCDayToRemind = CInt(reader("PDCDayToRemind"))
        oReturnValue.PDCDayToRemindBehaviour = reader("PDCDayToRemindPatern").ToString
        oReturnValue.DeskCollSMSRemind = CInt(reader("DeskCollSMSRemind"))
        oReturnValue.DeskCollSMSRemindBehaviour = reader("DeskCollSMSRemindPatern").ToString

        ''MEISAN
        oReturnValue.DeskCollSMSRemind2 = CInt(IIf(IsNumeric(reader("DeskCollSMSRemind2")), reader("DeskCollSMSRemind2"), "0"))
        oReturnValue.DeskCollSMSRemindBehaviour2 = reader("DeskCollSMSRemindPatern2").ToString
        oReturnValue.DeskCollSMSRemind3 = CInt(IIf(IsNumeric(reader("DeskCollSMSRemind3")), reader("DeskCollSMSRemind3"), "0"))
        oReturnValue.DeskCollSMSRemindBehaviour3 = reader("DeskCollSMSRemindPatern3").ToString
        oReturnValue.DCR = CInt(reader("DCR"))
        oReturnValue.DCRBehaviour = reader("DCRPAtern").ToString
        oReturnValue.DaysBeforeDueToRN = CInt(reader("DaysBeforeDueToRN"))
        oReturnValue.DaysBeforeDueToRNBehaviour = reader("DaysBeforeDueToRNPatern").ToString
        oReturnValue.ODToRAL = CInt(reader("ODToRAL"))
        oReturnValue.ODToRALBehaviour = reader("ODToRALPatern").ToString
        oReturnValue.RALPeriod = CInt(reader("RALPeriod"))
        oReturnValue.RALPeriodBehaviour = reader("RALPeriodPatern").ToString
        oReturnValue.MaxDaysRNPaid = CInt(reader("MaxDaysRNPaid"))
        oReturnValue.MaxDaysRNPaidBehaviour = reader("MaxDaysRNPaidPatern").ToString
        oReturnValue.MaxPTPYDays = CInt(reader("MaxPTPYDays"))
        oReturnValue.MaxPTPYDaysBehaviour = reader("MaxPTPYDaysPatern").ToString
        oReturnValue.PTPYBank = CInt(reader("PTPYBank"))
        oReturnValue.PTPYBankBehaviour = reader("PTPYBankPatern").ToString
        oReturnValue.PTPYCompany = CInt(reader("PTPYCompany"))
        oReturnValue.PTPYCompanyBehaviour = reader("PTPYCompanyPatern").ToString
        oReturnValue.PTPYSupplier = CInt(reader("PTPYSupplier"))
        oReturnValue.PTPYSupplierBehaviour = reader("PTPYSupplierPatern").ToString
        oReturnValue.RALExtension = CInt(reader("RALExtension"))
        oReturnValue.RALExtensionBehaviour = reader("RALExtensionPatern").ToString
        oReturnValue.InventoryExpected = CInt(reader("InventoryExpected"))
        oReturnValue.InventoryExpectedBehaviour = reader("InventoryExpectedPatern").ToString
        oReturnValue.BillingCharges = CDec(reader("BillingCharges"))
        oReturnValue.BillingChargesBehaviour = reader("BillingChargesPatern").ToString
        oReturnValue.LimitAPCash = CInt(reader("LimitAPCash"))
        oReturnValue.LimitAPCashBehaviour = reader("LimitAPCashPatern").ToString
        oReturnValue.PenaltyBasedOn = reader("PenaltyBasedOn").ToString
        oReturnValue.PenaltyBasedOnBehaviour = reader("PenaltyBasedOnPatern").ToString
        oReturnValue.PrepaymentPenaltyFixed = CDec(reader("PrepaymentPenaltyFixed"))
        oReturnValue.PrepaymentPenaltyFixedBehaviour = reader("PrepaymentPenaltyFixedPatern").ToString
        oReturnValue.PenaltyRatePrevious = CDec(reader("PenaltyRatePrevious").ToString)
        oReturnValue.PenaltyRateEffectiveDate = CDate(reader("PenaltyRateEffectiveDate").ToString)
        oReturnValue.PrepaymentPenaltyPrevious = CDec(reader("PrepaymentPenaltyPrevious").ToString)
        oReturnValue.PrepaymentPenaltyEffectiveDate = CDate(reader("PrepaymentPenaltyEffectiveDate").ToString)
        oReturnValue.PrepaymentPenaltyFixedEffectiveDate = CDate(reader("PrepaymentPenaltyFixedEffectiveDate").ToString)
        oReturnValue.PrepaymentPenaltyFixedPrevious = CDec(reader("PrepaymentPenaltyFixedPrevious").ToString)
        oReturnValue.PrioritasPembayaran = reader("PrioritasPembayaran").ToString
        oReturnValue.PengaturanBiaya = reader("PengaturanPembayaran").ToString
        oReturnValue.InsuranceDiscPercentage = CDec(reader("InsuranceDiscPercentage").ToString)
        oReturnValue.InsuranceDiscPercentageBehaviour = reader("InsuranceDiscPercentagePatern").ToString
        oReturnValue.IncomeInsRatioPercentage = CDec(reader("IncomeInsRatioPercentage").ToString)
        oReturnValue.IncomeInsRatioPercentageBehaviour = reader("IncomeInsRatioPercentagePatern").ToString
        oReturnValue.UmurKendaraanFrom = CInt(reader("UmurKendaraanFrom").ToString)
        oReturnValue.UmurKendaraanTo = CInt(reader("UmurKendaraanTo").ToString)
        oReturnValue.BiayaPolis = CDec(reader("BiayaPolis").ToString)
        oReturnValue.BiayaPolisBehaviour = reader("BiayaPolisPatern").ToString
        oReturnValue.PerpanjanganSKT = reader("PerpanjanganSKT").ToString

        'oReturnValue.CreditProtection = CInt(reader("CreditProtection").ToString)
        'oReturnValue.CreditProtectionBehaviour = reader("CreditProtectionPatern").ToString
        'oReturnValue.AsuransiKredit = CInt(reader("InsJaminanKredit").ToString)
        'oReturnValue.AsuransiKreditBehaviour = reader("InsJaminanKreditPatern").ToString



        Dim _cID = reader.GetOrdinal("KegiatanUsahaDesc")
        Dim _desc = reader.GetOrdinal("JenisPembiayaanDesc")
        oReturnValue.KegiatanUsahaDesc = IIf(reader.IsDBNull(_cID), String.Empty, reader("KegiatanUsahaDesc").ToString)
        oReturnValue.JenisPembiayaanDesc = IIf(reader.IsDBNull(_desc), String.Empty, reader("JenisPembiayaanDesc").ToString)
        oReturnValue.KegiatanUsaha = reader("KegiatanUsaha").ToString
        oReturnValue.JenisPembiayaan = reader("JenisPembiayaan").ToString
        oReturnValue.AngsuranPertama = reader("AngsuranPertama").ToString
        oReturnValue.SukuBungaFlat = CDec(reader("SukuBungaFlat").ToString)
        oReturnValue.SukuBungaFlatBehaviour = reader("SukuBungaFlatPatern").ToString
        oReturnValue.OpsiUangMuka = reader("OpsiUangMuka").ToString
        oReturnValue.OpsiUangMukaAngsur = CInt(reader("OpsiUangMukaAngsur"))
        oReturnValue.paket = reader("PaketProduct").ToString
        oReturnValue.SkemaPembiayaan = reader("SkemaPembiayaan").ToString

    End Sub


    Private Sub readProductHoFromReader(oReturnValue As Parameter.Product, reader As IDataReader)

        readProductFromReader(oReturnValue, reader, True)

        oReturnValue.BranchFullName = reader("BranchFullName").ToString
        oReturnValue.EffectiveRate_HO = CDec(reader("EffectiveRate_HO"))
        oReturnValue.EffectiveRateBehaviour_HO = reader("EffectiveRatePatern_HO").ToString
        oReturnValue.GrossYieldRate_HO = CDec(reader("GrossYieldRate_HO"))
        oReturnValue.GrossYieldRateBehaviour_HO = reader("GrossYieldRatePatern_HO").ToString
        oReturnValue.MinimumTenor_HO = CInt(reader("MinimumTenor_HO"))
        oReturnValue.MaximumTenor_HO = CInt(reader("MaximumTenor_HO")) 
        oReturnValue.PenaltyPercentage_HO = CDec(reader("PenaltyPercentage_HO"))
        oReturnValue.PenaltyPercentageBehaviour_HO = reader("PenaltyPercentagePatern_HO").ToString
        oReturnValue.InsurancePenaltyPercentage_HO = CDec(reader("InsurancePenaltyPercentage_HO"))
        oReturnValue.InsurancePenaltyPercentageBehaviour_HO = reader("InsurancePenaltyPercentagePatern_HO").ToString
        oReturnValue.CancellationFee_HO = CDec(reader("CancellationFee_HO"))
        oReturnValue.CancellationFeeBehaviour_HO = reader("CancellationFeePatern_HO").ToString
        oReturnValue.AdminFee_HO = CDec(reader("AdminFee_HO"))
        oReturnValue.AdminFeeBehaviour_HO = reader("AdminFeePatern_HO").ToString
        oReturnValue.FiduciaFee_HO = CDec(reader("FiduciaFee_HO"))
        oReturnValue.FiduciaFeeBehaviour_HO = reader("FiduciaFeePatern_HO").ToString
        oReturnValue.ProvisionFeeHO = CDec(reader("ProvisionFeeHO"))
        oReturnValue.ProvisionFeeBehaviour_HO = reader("ProvisionFeePatern_HO").ToString
        oReturnValue.NotaryFee_HO = CDec(reader("NotaryFee_HO"))
        oReturnValue.NotaryFeeBehaviour_HO = reader("NotaryFeePatern_HO").ToString
        oReturnValue.SurveyFee_HO = CDec(reader("SurveyFee_HO"))
        oReturnValue.SurveyFeeBehaviour_HO = reader("SurveyFeePatern_HO").ToString
        oReturnValue.VisitFee_HO = CDec(reader("VisitFee_HO"))
        oReturnValue.VisitFeeBehaviour_HO = reader("VisitFeePatern_HO").ToString
        oReturnValue.ReschedulingFee_HO = CDec(reader("ReschedulingFee_HO"))
        oReturnValue.ReschedulingFeeBehaviour_HO = reader("ReschedulingFeePatern_HO").ToString
        oReturnValue.AgreementTransferFee_HO = CDec(reader("AgreementTransferFee_HO"))
        oReturnValue.AgreementTransferFeeBehaviour_HO = reader("AgreementTransferFeePatern_HO").ToString
        oReturnValue.ChangeDueDateFee_HO = CDec(reader("ChangeDueDateFee_HO"))
        oReturnValue.ChangeDueDateFeeBehaviour_HO = reader("ChangeDueDateFeePatern_HO").ToString
        oReturnValue.AssetReplacementFee_HO = CDec(reader("AssetReplacementFee_HO"))
        oReturnValue.AssetReplacementFeeBehaviour_HO = reader("AssetReplacementFeePatern_HO").ToString
        oReturnValue.RepossesFee_HO = CDec(reader("RepossesFee_HO"))
        oReturnValue.RepossesFeeBehaviour_HO = reader("RepossesFeePatern_HO").ToString
        oReturnValue.LegalisirDocFee_HO = CDec(reader("LegalisirDocFee_HO"))
        oReturnValue.LegalisirDocFeeBehaviour_HO = reader("LegalisirDocFeePatern_HO").ToString
        oReturnValue.PDCBounceFee_HO = CDec(reader("PDCBounceFee_HO"))
        oReturnValue.PDCBounceFeeBehaviour_HO = reader("PDCBounceFeePatern_HO").ToString
        oReturnValue.InsAdminFee_HO = CDec(reader("InsAdminFee_HO"))
        oReturnValue.InsAdminFeeBehaviour_HO = reader("InsAdminFeePatern_HO").ToString
        oReturnValue.InsStampDutyFee_HO = CDec(reader("InsStampDutyFee_HO"))
        oReturnValue.InsStampDutyFeeBehaviour_HO = reader("InsStampDutyFeePatern_HO").ToString
        oReturnValue.DaysPOExpiration_HO = CInt(reader("DaysPOExpiration_HO"))
        oReturnValue.DaysPOExpirationBehaviour_HO = reader("DaysPOExpirationPatern_HO").ToString
        oReturnValue.InstallmentToleranceAmount_HO = CDec(reader("InstallmentToleranceAmount_HO"))
        oReturnValue.InstallmentToleranceAmountBehaviour_HO = reader("InstallmentToleranceAmountPatern_HO").ToString
        oReturnValue.DPPercentage_HO = CDec(reader("DPPercentage_HO"))
        oReturnValue.DPPercentageBehaviour_HO = reader("DPPercentagePatern_HO").ToString
        oReturnValue.RejectMinimumIncome_HO = CDec(reader("RejectMinimumIncome_HO"))
        oReturnValue.RejectMinimumIncomeBehaviour_HO = reader("RejectMinimumIncomePatern_HO").ToString
        oReturnValue.WarningMinimumIncome_HO = CDec(reader("WarningMinimumIncome_HO"))
        oReturnValue.WarningMinimumIncomeBehaviour_HO = reader("WarningMinimumIncomePatern_HO").ToString
        oReturnValue.IsSPAutomatic_HO = CBool(reader("IsSPAutomatic_HO"))
        oReturnValue.IsSPAutomaticBehaviour_HO = reader("IsSPAutomaticPatern_HO").ToString
        oReturnValue.LengthSPProcess_HO = CInt(reader("LengthSPProcess_HO")) 
        oReturnValue.IsSP1Automatic_HO = CBool(reader("IsSP1Automatic_HO"))
        oReturnValue.IsSP1AutomaticBehaviour_HO = reader("IsSP1AutomaticPatern_HO").ToString
        oReturnValue.LengthSP1Process_HO = CInt(reader("LengthSP1Process_HO")) 
        oReturnValue.IsSP2Automatic_HO = CBool(reader("IsSP2Automatic_HO"))
        oReturnValue.IsSP2AutomaticBehaviour_HO = reader("IsSP2AutomaticPatern_HO").ToString
        oReturnValue.LengthSP2Process_HO = CInt(reader("LengthSP2Process_HO"))
         
        oReturnValue.LengthMainDocProcess_HO = CInt(reader("LengthMainDocProcess_HO"))
        oReturnValue.LengthMainDocProcessBehaviour_HO = reader("LengthMainDocProcessPatern_HO").ToString
        oReturnValue.LengthMainDocTaken_HO = CInt(reader("LengthMainDocTaken_HO"))
        oReturnValue.LengthMainDocTakenBehaviour_HO = reader("LengthMainDocTakenPatern_HO").ToString
         
        oReturnValue.GracePeriodLateCharges_HO = CInt(reader("GracePeriodLateCharges_HO"))
        oReturnValue.GracePeriodLateChargesBehaviour_HO = reader("GracePeriodLateChargesPatern_HO").ToString
         
        oReturnValue.PrepaymentPenaltyRate_HO = CDec(reader("PrepaymentPenaltyRate_HO"))
        oReturnValue.PrepaymentPenaltyRateBehaviour_HO = reader("PrepaymentPenaltyRatePatern_HO").ToString
        oReturnValue.DeskCollPhoneRemind_HO = CInt(reader("DeskCollPhoneRemind_HO"))
        oReturnValue.DeskCollPhoneRemindBehaviour_HO = reader("DeskCollPhoneRemindPatern_HO").ToString

       
        oReturnValue.DeskCollOD_HO = CInt(reader("DeskCollOD_HO"))
        oReturnValue.DeskCollODBehaviour_HO = reader("DeskCollODPatern_HO").ToString
        oReturnValue.PrevODToRemind_HO = CInt(reader("PrevODToRemind_HO"))
        oReturnValue.PrevODToRemindBehaviour_HO = reader("PrevODToRemindPatern_HO").ToString
        oReturnValue.PDCDayToRemind_HO = CInt(reader("PDCDayToRemind_HO"))
        oReturnValue.PDCDayToRemindBehaviour_HO = reader("PDCDayToRemindPatern_HO").ToString
        oReturnValue.DeskCollSMSRemind_HO = CInt(reader("DeskCollSMSRemind_HO"))
        oReturnValue.DeskCollSMSRemindBehaviour_HO = reader("DeskCollSMSRemindPatern_HO").ToString
         
        oReturnValue.DCR_HO = CInt(reader("DCR_HO"))
        oReturnValue.DCRBehaviour_HO = reader("DCRPatern_HO").ToString
         
        oReturnValue.DaysBeforeDueToRN_HO = CInt(reader("DaysBeforeDueToRN_HO"))
        oReturnValue.DaysBeforeDueToRNBehaviour_HO = reader("DaysBeforeDueToRNPatern_HO").ToString
         
        oReturnValue.ODToRAL_HO = CInt(reader("ODToRAL_HO"))
        oReturnValue.ODToRALBehaviour_HO = reader("ODToRALPatern_HO").ToString
        oReturnValue.RALPeriod_HO = CInt(reader("RALPeriod_HO"))
        oReturnValue.RALPeriodBehaviour_HO = reader("RALPeriodPatern_HO").ToString
        oReturnValue.MaxDaysRNPaid_HO = CInt(reader("MaxDaysRNPaid_HO"))
        oReturnValue.MaxDaysRNPaidBehaviour_HO = reader("MaxDaysRNPaidPatern_HO").ToString
        oReturnValue.MaxPTPYDays_HO = CInt(reader("MaxPTPYDays_HO"))
        oReturnValue.MaxPTPYDaysBehaviour_HO = reader("MaxPTPYDaysPatern_HO").ToString
        oReturnValue.PTPYBank_HO = CInt(reader("PTPYBank_HO"))
        oReturnValue.PTPYBankBehaviour_HO = reader("PTPYBankPatern_HO").ToString
        oReturnValue.PTPYCompany_HO = CInt(reader("PTPYCompany_HO"))
        oReturnValue.PTPYCompanyBehaviour_HO = reader("PTPYCompanyPatern_HO").ToString
        oReturnValue.PTPYSupplier_HO = CInt(reader("PTPYSupplier_HO"))
        oReturnValue.PTPYSupplierBehaviour_HO = reader("PTPYSupplierPatern_HO").ToString
        oReturnValue.RALExtension_HO = CInt(reader("RALExtension_HO"))
        oReturnValue.RALExtensionBehaviour_HO = reader("RALExtensionPatern_HO").ToString
        oReturnValue.InventoryExpected_HO = CInt(reader("InventoryExpected_HO"))
        oReturnValue.InventoryExpectedBehaviour_HO = reader("InventoryExpectedPatern_HO").ToString
        oReturnValue.BillingCharges_HO = CDec(reader("BillingCharges_HO"))
        oReturnValue.BillingChargesBehaviour_HO = reader("BillingChargesPatern_HO").ToString
        oReturnValue.LimitAPCash_HO = CInt(reader("LimitAPCash_HO"))
        oReturnValue.LimitAPCashBehaviour_HO = reader("LimitAPCashPatern_HO").ToString
  
        oReturnValue.PenaltyBasedOn_HO = reader("PenaltyBasedOn_HO").ToString
        oReturnValue.PenaltyBasedOnBehaviour_HO = reader("PenaltyBasedOnPatern_HO").ToString
        oReturnValue.PrepaymentPenaltyFixed_HO = CDec(reader("PrepaymentPenaltyFixed_HO"))
        oReturnValue.PrepaymentPenaltyFixedBehaviour_HO = reader("PrepaymentPenaltyFixedPatern_HO").ToString

        oReturnValue.PenaltyRatePrevious_HO = CDec(reader("PenaltyRatePrevious_HO").ToString)
        oReturnValue.PenaltyRateEffectiveDate_HO = CDate(reader("PenaltyRateEffectiveDate_HO").ToString)
        oReturnValue.PrepaymentPenaltyPrevious_HO = CDec(reader("PrepaymentPenaltyPrevious_HO").ToString)
        oReturnValue.PrepaymentPenaltyEffectiveDate_HO = CDate(reader("PrepaymentPenaltyEffectiveDate_HO").ToString)
        oReturnValue.PrepaymentPenaltyFixedEffectiveDate_HO = CDate(reader("PrepaymentPenaltyFixedEffectiveDate_HO").ToString)
        oReturnValue.PrepaymentPenaltyFixedPrevious_HO = CDec(reader("PrepaymentPenaltyFixedPrevious_HO").ToString)


        oReturnValue.InsuranceDiscPercentage_HO = CDec(reader("InsuranceDiscPercentage_HO").ToString)
        oReturnValue.InsuranceDiscPercentageBehaviour_HO = reader("InsuranceDiscPercentagePatern_HO").ToString
        oReturnValue.IncomeInsRatioPercentage_HO = CDec(reader("IncomeInsRatioPercentage_HO").ToString)
        oReturnValue.IncomeInsRatioPercentageBehaviour_HO = reader("IncomeInsRatioPercentagePatern_HO").ToString
        oReturnValue.UmurKendaraanFrom_HO = CInt(reader("UmurKendaraanFrom_HO").ToString)
        oReturnValue.UmurKendaraanTo_HO = CInt(reader("UmurKendaraanTo_HO").ToString) 
        oReturnValue.BiayaPolis_HO = CDec(reader("BiayaPolis_HO"))
        oReturnValue.BiayaPolisBehaviour_HO = reader("BiayaPolisPatern_HO").ToString

         
        oReturnValue.AngsuranPertama_HO = reader("AngsuranPertama_HO").ToString 
        oReturnValue.SukuBungaFlat_HO = CDec(reader("SukuBungaFlat_HO").ToString)

        oReturnValue.SukuBungaFlatBehaviour_HO = reader("SukuBungaFlatPatern_HO").ToString
        oReturnValue.OpsiUangMuka_HO = reader("OpsiUangMuka_HO").ToString
        oReturnValue.OpsiUangMukaAngsur_HO = CInt(reader("OpsiUangMukaAngsur_HO").ToString)

        ''meisan
        oReturnValue.PengaturanBiaya = reader("PengaturanPembayaran_HO").ToString

        oReturnValue.DeskCollSMSRemind2_HO = CInt(reader("DeskCollSMSRemind2_HO"))
        oReturnValue.DeskCollSMSRemindBehaviour2 = reader("DeskCollSMSRemindPatern2_HO").ToString

        oReturnValue.DeskCollSMSRemind3_HO = CInt(reader("DeskCollSMSRemind3_HO"))
        oReturnValue.DeskCollSMSRemindBehaviour3 = reader("DeskCollSMSRemindPatern3_HO").ToString

        oReturnValue.PrioritasPembayaran_HO = reader("PrioritasPembayaran_HO").ToString

        oReturnValue.PerpanjanganSKT_HO = reader("PerpanjanganSKT_HO").ToString
        oReturnValue.PerpanjanganSKTBehaviour_HO = reader("PerpanjanganSKTPatern_HO").ToString


        oReturnValue.CreditProtection_HO = reader("CreditProtection").ToString
        oReturnValue.CreditProtectionBehaviour_HO = reader("CreditProtectionPatern").ToString
        oReturnValue.AsuransiKredit_HO = reader("InsJaminanKredit").ToString
        oReturnValue.AsuransiKreditBehaviour_HO = reader("InsJaminanKreditPatern").ToString


    End Sub
#End Region



#Region " ProductPagingProductReport ProductEdit  ProductView  ProductSaveAdd  ProductSaveEdit"
    Public Function ProductPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
     
    Public Function ProductReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
    End Function


    Public Function ProductEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ProductId", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params)
            If reader.Read Then
                readProductFromReader(oReturnValue, reader)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductView(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ProductId", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_VIEW, params)
            If reader.Read Then
                readProductFromReader(oReturnValue, reader)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("DA Product View:" & ex.Message)
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function

    Public Function ProductSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ErrMessage As String = ""
        Dim params As IList(Of SqlParameter)
        Dim sp_INSERT As String = LIST_INSERT
        If customClass.SaveProduct = "Branch" Then
            params = New List(Of SqlParameter)
            params.Add(New SqlParameter("@TotalBranch", SqlDbType.VarChar, 1000) With {.Value = customClass.TotalBranch})
            params.Add(New SqlParameter("@ProductId", SqlDbType.Char, 10) With {.Value = customClass.ProductId})
            sp_INSERT = "spProductBranchSaveAdd"
        Else
            params = bindParams(customClass, True)
        End If
        Try
            Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 50) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, sp_INSERT, params.ToArray())
            ErrMessage = CType(prmErr.Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Return (ex.Message)
        End Try
    End Function

    Public Sub ProductSaveEdit(ByVal customClass As Parameter.Product)
        Dim ErrMessage As String = ""

        Dim params As IList(Of SqlParameter) = bindParams(customClass, False)

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params.ToArray())

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region



#Region "ProductBranchHOPaging  ProductBranchHOReport ProductBranchHOEdit ProductBranchHOSaveAdd ProductBranchHOSaveEdit"
    Public Function ProductBranchHOPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BRANCH, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function ProductBranchHOReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(1).Value = customClass.ProductId


        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT_BRANCH, params)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchHOEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.Branch_ID

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT_BRANCH, params)
            If reader.Read Then
                readProductHoFromReader(oReturnValue, reader)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductBranchHOSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@TotalBranch", SqlDbType.VarChar, 1000)
        params(1).Value = customClass.TotalBranch

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_INSERT_BRANCH, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ErrMessage
    End Function

    Public Sub ProductBranchHOSaveEdit(ByVal customClass As Parameter.Product)
        Dim ErrMessage As String = ""
        Dim params As IList(Of SqlParameter) = ProdParams(customClass, New List(Of SqlParameter))
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.Branch_ID})
        Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 50) With {.Direction = ParameterDirection.Output}
        params.Add(prmErr)
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE_BRANCH, params.ToArray())
            ErrMessage = CType(prmErr.Value, String)
            If ErrMessage <> "" Then
                Throw New Exception(ErrMessage)
            End If

        Catch ex As Exception

            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub CopyProduct(ByVal customClass As Parameter.CopyProduct)
        Dim ErrMessage As String = ""
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@ProductID", SqlDbType.Char, 10) With {.Value = customClass.ProductId})
        params.Add(New SqlParameter("@ReferenceProductID", SqlDbType.Char, 10) With {.Value = customClass.ReferenceProductID})
        params.Add(New SqlParameter("@Description", SqlDbType.VarChar, 100) With {.Value = customClass.Description})
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.BranID})
        Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 50) With {.Direction = ParameterDirection.Output}
        params.Add(prmErr)
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spCopyProduct", params.ToArray())
            ErrMessage = CType(prmErr.Value, String)
            If ErrMessage <> "" Then
                Throw New Exception(ErrMessage)
            End If

        Catch ex As Exception

            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region
       
      

#Region "ProductBranchPaging ProductBranchReport ProductBranchEdit ProductBranchSaveEdit"
    Public Function ProductBranchPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BRC, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductBranchReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 2000)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT_BRC, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductBranchEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.BranchId

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT_BRC, params)
            If reader.Read Then
                readProductHoFromReader(oReturnValue, reader)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#End Region

#Region "ProductBranchSaveEdit"
    Public Sub ProductBranchSaveEdit(ByVal customClass As Parameter.Product)
        Dim ErrMessage As String = ""

        Dim params As IList(Of SqlParameter) = ProdParams(customClass, New List(Of SqlParameter)) 
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.Branch_ID})
        Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 50) With {.Direction = ParameterDirection.Output}
        params.Add(prmErr)
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE_BRC, params.ToArray())
            ErrMessage = CType(prmErr.Value, String)
            If ErrMessage <> "" Then
                Throw New Exception(ErrMessage)
            End If

        Catch ex As Exception

            Throw New Exception(ex.Message)
        End Try
    End Sub

     

#End Region

#Region "Term & Condition"

#Region "ProductTCPaging"
    Public Function ProductTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_TC, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
    End Function
#End Region

#Region "ProductTCReport"
    Public Function ProductTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT_TC, params)

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")
        End Try
    End Function
#End Region

#Region "ProductTCEdit"
    Public Function ProductTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT_TC, params)
            If reader.Read Then
                oReturnValue.PriorTo = reader("PriorTo").ToString
                oReturnValue.IsMandatory = CBool(reader("IsMandatory").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function
#End Region

#Region "ProductTCSaveAdd"
    Public Function ProductTCSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
        params(2).Value = customClass.PriorTo
        params(3) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(3).Value = customClass.IsMandatory
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_INSERT_TC, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            objtrans.Commit()
            Return ""
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception("Data Already Exists")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "ProductTCDelete"
    Public Function ProductTCDelete(ByVal customClass As Parameter.Product) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(2).Value = customClass.BranchId
        params(3) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_TC, params)
            Return CType(params(3).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        End Try
    End Function
#End Region

#Region "ProductTCSaveEdit"
    Public Sub ProductTCSaveEdit(ByVal customClass As Parameter.Product)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
        params(2).Value = customClass.PriorTo
        params(3) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(3).Value = customClass.IsMandatory
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_UPDATE_TC, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception("Data Already Exists")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
#End Region



#Region "ProductBranchTCPaging"
    Public Function ProductBranchTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BRC_TC, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
    End Function
#End Region

#Region "ProductBranchTCReport"
    Public Function ProductBranchTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT_BRC_TC, params)

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")
        End Try
    End Function
#End Region

#Region "ProductBranchTCEdit"
    Public Function ProductBranchTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT_BRC_TC, params)
            If reader.Read Then
                oReturnValue.PriorTo = reader("PriorTo").ToString
                oReturnValue.IsMandatory = CBool(reader("IsMandatory").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function
#End Region

#Region "ProductTCSaveAdd"
    Public Function ProductBranchTCSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
        params(2).Value = customClass.PriorTo
        params(3) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(3).Value = customClass.IsMandatory
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_INSERT_BRC_TC, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            objtrans.Commit()
            Return ""
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception("Data Already Exists")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "ProductTCDelete"
    Public Function ProductBranchTCDelete(ByVal customClass As Parameter.Product) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(2).Value = customClass.BranchId
        params(3) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_BRC_TC, params)
            Return CType(params(3).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        End Try
    End Function
#End Region

#Region "ProductTCSaveEdit"
    Public Sub ProductBranchTCSaveEdit(ByVal customClass As Parameter.Product)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
        params(1).Value = customClass.MasterTCID
        params(2) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
        params(2).Value = customClass.PriorTo
        params(3) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(3).Value = customClass.IsMandatory
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            objtrans = conn.BeginTransaction

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_UPDATE_BRC_TC, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception("Data Already Exists")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
#End Region

#End Region


#Region "Product Offering "

#Region "ProductOfferingPaging"
    Public Function ProductOfferingPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_OFF_BRC, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
    End Function
#End Region

#Region "ProductOfferingReport"
    Public Function ProductOfferingReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT_OFF_BRC, params)

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")

        End Try
    End Function
#End Region

#Region "ProductOfferingEdit   ProductOfferingView"
    Public Function ProductOfferingEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
        params(2).Value = customClass.ProductOfferingID

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_EDIT_OFF_BRC, params)
            If reader.Read Then
                readProductFromReader(oReturnValue, reader)
                oReturnValue.StartDate = CDate(reader("StartDate"))
                oReturnValue.EndDate = CDate(reader("EndDate"))
            End If

            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
               

    Public Function ProductOfferingView(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
        params(1).Value = customClass.ProductOfferingID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(2).Value = customClass.BranchId

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_VIEW_OFF_BRC, params)
            If reader.Read Then

                readProductFromReader(oReturnValue, reader)
                
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function
#End Region

#Region "ProductOfferingSaveAdd"
    Public Function ProductOfferingSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ErrMessage As String = "" 
        Dim params = bindParams(customClass, False)
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.Branch_ID})
        params.Add(New SqlParameter("@ProductOfferingID", SqlDbType.VarChar, 10) With {.Value = customClass.ProductOfferingID})

        params.Add(New SqlParameter("@StartDate", SqlDbType.DateTime) With {.Value = customClass.StartDate})

        params.Add(New SqlParameter("@EndDate", SqlDbType.DateTime) With {.Value = customClass.EndDate})

        Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 50) With {.Direction = ParameterDirection.Output}


        params.Add(prmErr) 
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_INSERT_OFF_BRC, params.ToArray())

            ErrMessage = CType(prmErr.Value, String)
            If ErrMessage <> "" Then
                Throw New Exception(ErrMessage)
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message) 
        End Try
    End Function
        
#End Region

#Region "ProductOfferingSaveEdit"
    Public Sub ProductOfferingSaveEdit(ByVal customClass As Parameter.Product)
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(162) {}

        params(0) = New SqlParameter("@ProductId", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId

        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description

        params(2) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(2).Value = customClass.AssetTypeID

        params(3) = New SqlParameter("@ScoreSchemeID", SqlDbType.Char, 10)
        params(3).Value = customClass.ScoreSchemeID

        params(4) = New SqlParameter("@CreditScoreSchemeID", SqlDbType.Char, 10)
        params(4).Value = customClass.CreditScoreSchemeID

        params(5) = New SqlParameter("@JournalSchemeID", SqlDbType.Char, 10)
        params(5).Value = customClass.JournalSchemeID

        params(6) = New SqlParameter("@ApprovalSchemeID", SqlDbType.Char, 10)
        params(6).Value = customClass.ApprovalSchemeID

        params(7) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
        params(7).Value = customClass.AssetUsedNew

        params(8) = New SqlParameter("@FinanceType", SqlDbType.Char, 2)
        params(8).Value = customClass.FinanceType

        params(9) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
        params(9).Value = customClass.EffectiveRate

        params(10) = New SqlParameter("@EffectiveRateBehaviour", SqlDbType.Char, 1)
        params(10).Value = customClass.EffectiveRateBehaviour

        params(11) = New SqlParameter("@GrossYieldRate", SqlDbType.Decimal)
        params(11).Value = customClass.GrossYieldRate

        params(12) = New SqlParameter("@GrossYieldRateBehaviour", SqlDbType.Char, 1)
        params(12).Value = customClass.GrossYieldRateBehaviour

        params(13) = New SqlParameter("@MinimumTenor", SqlDbType.SmallInt, 2)
        params(13).Value = customClass.MinimumTenor

        params(14) = New SqlParameter("@MaximumTenor", SqlDbType.SmallInt, 2)
        params(14).Value = customClass.MaximumTenor

        params(15) = New SqlParameter("@PenaltyPercentage", SqlDbType.Decimal)
        params(15).Value = customClass.PenaltyPercentage

        params(16) = New SqlParameter("@PenaltyPercentageBehaviour", SqlDbType.Char, 1)
        params(16).Value = customClass.PenaltyPercentageBehaviour

        params(17) = New SqlParameter("@InsurancePenaltyPercentage", SqlDbType.Decimal)
        params(17).Value = customClass.InsurancePenaltyPercentage

        params(18) = New SqlParameter("@InsurancePenaltyPercentageBehaviour", SqlDbType.Char, 1)
        params(18).Value = customClass.InsurancePenaltyPercentageBehaviour

        params(19) = New SqlParameter("@CancellationFee", SqlDbType.Decimal)
        params(19).Value = customClass.CancellationFee

        params(20) = New SqlParameter("@CancellationFeeBehaviour", SqlDbType.Char, 1)
        params(20).Value = customClass.CancellationFeeBehaviour

        params(21) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
        params(21).Value = customClass.AdminFee

        params(22) = New SqlParameter("@AdminFeeBehaviour", SqlDbType.Char, 1)
        params(22).Value = customClass.AdminFeeBehaviour

        params(23) = New SqlParameter("@FiduciaFee", SqlDbType.Decimal)
        params(23).Value = customClass.FiduciaFee

        params(24) = New SqlParameter("@FiduciaFeeBehaviour", SqlDbType.Char, 1)
        params(24).Value = customClass.FiduciaFeeBehaviour

        params(25) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
        params(25).Value = customClass.ProvisionFee

        params(26) = New SqlParameter("@ProvisionFeeBehaviour", SqlDbType.Char, 1)
        params(26).Value = customClass.ProvisionFeeBehaviour

        params(27) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
        params(27).Value = customClass.NotaryFee

        params(28) = New SqlParameter("@NotaryFeeBehaviour", SqlDbType.Char, 1)
        params(28).Value = customClass.NotaryFeeBehaviour

        params(29) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
        params(29).Value = customClass.SurveyFee

        params(30) = New SqlParameter("@SurveyFeeBehaviour", SqlDbType.Char, 1)
        params(30).Value = customClass.SurveyFeeBehaviour

        params(31) = New SqlParameter("@VisitFee", SqlDbType.Decimal)
        params(31).Value = customClass.VisitFee

        params(32) = New SqlParameter("@VisitFeeBehaviour", SqlDbType.Char, 1)
        params(32).Value = customClass.VisitFeeBehaviour

        params(33) = New SqlParameter("@ReschedulingFee", SqlDbType.Decimal)
        params(33).Value = customClass.ReschedulingFee

        params(34) = New SqlParameter("@ReschedulingFeeBehaviour", SqlDbType.Char, 1)
        params(34).Value = customClass.ReschedulingFeeBehaviour

        params(35) = New SqlParameter("@AgreementTransferFee", SqlDbType.Decimal)
        params(35).Value = customClass.AgreementTransferFee

        params(36) = New SqlParameter("@AgreementTransferFeeBehaviour", SqlDbType.Char, 1)
        params(36).Value = customClass.AgreementTransferFeeBehaviour

        params(37) = New SqlParameter("@ChangeDueDateFee", SqlDbType.Decimal)
        params(37).Value = customClass.ChangeDueDateFee

        params(38) = New SqlParameter("@ChangeDueDateFeeBehaviour", SqlDbType.Char, 1)
        params(38).Value = customClass.ChangeDueDateFeeBehaviour

        params(39) = New SqlParameter("@AssetReplacementFee", SqlDbType.Decimal)
        params(39).Value = customClass.AssetReplacementFee

        params(40) = New SqlParameter("@AssetReplacementFeeBehaviour", SqlDbType.Char, 1)
        params(40).Value = customClass.RepossesFeeBehaviour

        params(41) = New SqlParameter("@RepossesFee", SqlDbType.Decimal)
        params(41).Value = customClass.RepossesFee

        params(42) = New SqlParameter("@RepossesFeeBehaviour", SqlDbType.Char, 1)
        params(42).Value = customClass.RepossesFeeBehaviour

        params(43) = New SqlParameter("@LegalisirDocFee", SqlDbType.Decimal)
        params(43).Value = customClass.LegalisirDocFee

        params(44) = New SqlParameter("@LegalisirDocFeeBehaviour", SqlDbType.Char, 1)
        params(44).Value = customClass.LegalisirDocFeeBehaviour

        params(45) = New SqlParameter("@PDCBounceFee", SqlDbType.Decimal)
        params(45).Value = customClass.PDCBounceFee

        params(46) = New SqlParameter("@PDCBounceFeeBehaviour", SqlDbType.Char, 1)
        params(46).Value = customClass.PDCBounceFeeBehaviour

        params(47) = New SqlParameter("@InsAdminFee", SqlDbType.Decimal)
        params(47).Value = customClass.InsAdminFee

        params(48) = New SqlParameter("@InsAdminFeeBehaviour", SqlDbType.Char, 1)
        params(48).Value = customClass.InsAdminFeeBehaviour

        params(49) = New SqlParameter("@InsStampDutyFee", SqlDbType.Decimal)
        params(49).Value = customClass.InsStampDutyFee

        params(50) = New SqlParameter("@InsStampDutyFeeBehaviour", SqlDbType.Char, 1)
        params(50).Value = customClass.InsStampDutyFeeBehaviour

        params(51) = New SqlParameter("@DaysPOExpiration", SqlDbType.Int)
        params(51).Value = customClass.DaysPOExpiration

        params(52) = New SqlParameter("@DaysPOExpirationBehaviour", SqlDbType.Char, 1)
        params(52).Value = customClass.DaysPOExpirationBehaviour

        params(53) = New SqlParameter("@InstallmentToleranceAmount", SqlDbType.Decimal)
        params(53).Value = customClass.InstallmentToleranceAmount

        params(54) = New SqlParameter("@InstallmentToleranceAmountBehaviour", SqlDbType.Char, 1)
        params(54).Value = customClass.InstallmentToleranceAmountBehaviour

        params(55) = New SqlParameter("@DPPercentage", SqlDbType.Decimal)
        params(55).Value = customClass.DPPercentage

        params(56) = New SqlParameter("@DPPercentageBehaviour", SqlDbType.Char, 1)
        params(56).Value = customClass.DPPercentageBehaviour

        params(57) = New SqlParameter("@RejectMinimumIncome", SqlDbType.Decimal)
        params(57).Value = customClass.RejectMinimumIncome

        params(58) = New SqlParameter("@RejectMinimumIncomeBehaviour", SqlDbType.Char, 1)
        params(58).Value = customClass.RejectMinimumIncomeBehaviour

        params(59) = New SqlParameter("@WarningMinimumIncome", SqlDbType.Decimal)
        params(59).Value = customClass.WarningMinimumIncome

        params(60) = New SqlParameter("@WarningMinimumIncomeBehaviour", SqlDbType.Char, 1)
        params(60).Value = customClass.WarningMinimumIncomeBehaviour

        params(61) = New SqlParameter("@IsSPAutomatic", SqlDbType.Bit)
        params(61).Value = customClass.IsSPAutomatic

        params(62) = New SqlParameter("@IsSPAutomaticBehaviour", SqlDbType.Char, 1)
        params(62).Value = customClass.IsSPAutomaticBehaviour

        params(63) = New SqlParameter("@LengthSPProcess", SqlDbType.Int)
        params(63).Value = customClass.LengthSPProcess

        params(64) = New SqlParameter("@IsSP1Automatic", SqlDbType.Bit)
        params(64).Value = customClass.IsSP1Automatic

        params(65) = New SqlParameter("@IsSP1AutomaticBehaviour", SqlDbType.Char, 1)
        params(65).Value = customClass.IsSP1AutomaticBehaviour

        params(66) = New SqlParameter("@LengthSP1Process", SqlDbType.Int)
        params(66).Value = customClass.LengthSP1Process

        params(67) = New SqlParameter("@IsSP2Automatic", SqlDbType.Bit)
        params(67).Value = customClass.IsSP2Automatic

        params(68) = New SqlParameter("@IsSP2AutomaticBehaviour", SqlDbType.Char, 1)
        params(68).Value = customClass.IsSP2AutomaticBehaviour

        params(69) = New SqlParameter("@LengthSP2Process", SqlDbType.Int)
        params(69).Value = customClass.LengthSP2Process

        params(70) = New SqlParameter("@LengthMainDocProcess", SqlDbType.Int)
        params(70).Value = customClass.LengthMainDocProcess

        params(71) = New SqlParameter("@LengthMainDocProcessBehaviour", SqlDbType.Char, 1)
        params(71).Value = customClass.LengthMainDocProcessBehaviour

        params(72) = New SqlParameter("@LengthMainDocTaken", SqlDbType.Int)
        params(72).Value = customClass.LengthMainDocTaken

        params(73) = New SqlParameter("@LengthMainDocTakenBehaviour", SqlDbType.Char, 1)
        params(73).Value = customClass.LengthMainDocTakenBehaviour

        params(74) = New SqlParameter("@GracePeriodLateCharges", SqlDbType.Int)
        params(74).Value = customClass.GracePeriodLateCharges

        params(75) = New SqlParameter("@GracePeriodLateChargesBehaviour", SqlDbType.Char, 1)
        params(75).Value = customClass.GracePeriodLateChargesBehaviour

        params(76) = New SqlParameter("@PrepaymentPenaltyRate", SqlDbType.Decimal)
        params(76).Value = customClass.PrepaymentPenaltyRate

        params(77) = New SqlParameter("@PrepaymentPenaltyRateBehaviour", SqlDbType.Char, 1)
        params(77).Value = customClass.PrepaymentPenaltyRateBehaviour

        params(78) = New SqlParameter("@DeskCollPhoneRemind", SqlDbType.Int)
        params(78).Value = customClass.DeskCollPhoneRemind

        params(79) = New SqlParameter("@DeskCollPhoneRemindBehaviour", SqlDbType.Char, 1)
        params(79).Value = customClass.DeskCollPhoneRemindBehaviour

        params(80) = New SqlParameter("@DeskCollOD", SqlDbType.Int)
        params(80).Value = customClass.DeskCollOD

        params(81) = New SqlParameter("@DeskCollODBehaviour", SqlDbType.Char, 1)
        params(81).Value = customClass.DeskCollODBehaviour

        params(82) = New SqlParameter("@PrevODToRemind", SqlDbType.Int)
        params(82).Value = customClass.PrevODToRemind

        params(83) = New SqlParameter("@PrevODToRemindBehaviour", SqlDbType.Char, 1)
        params(83).Value = customClass.PrevODToRemindBehaviour

        params(84) = New SqlParameter("@PDCDayToRemind", SqlDbType.Int)
        params(84).Value = customClass.PDCDayToRemind

        params(85) = New SqlParameter("@PDCDayToRemindBehaviour", SqlDbType.Char, 1)
        params(85).Value = customClass.PDCDayToRemindBehaviour

        params(86) = New SqlParameter("@DeskCollSMSRemind", SqlDbType.Int)
        params(86).Value = customClass.DeskCollSMSRemind

        params(87) = New SqlParameter("@DeskCollSMSRemindBehaviour", SqlDbType.Char, 1)
        params(87).Value = customClass.DeskCollSMSRemindBehaviour

        params(88) = New SqlParameter("@DCR", SqlDbType.Int)
        params(88).Value = customClass.DCR

        params(89) = New SqlParameter("@DCRBehaviour", SqlDbType.Char, 1)
        params(89).Value = customClass.DCRBehaviour

        params(90) = New SqlParameter("@DaysBeforeDueToRN", SqlDbType.Int)
        params(90).Value = customClass.DaysBeforeDueToRN

        params(91) = New SqlParameter("@DaysBeforeDueToRNBehaviour", SqlDbType.Char, 1)
        params(91).Value = customClass.DaysBeforeDueToRNBehaviour

        params(92) = New SqlParameter("@ODToRAL", SqlDbType.Int)
        params(92).Value = customClass.ODToRAL

        params(93) = New SqlParameter("@ODToRALBehaviour", SqlDbType.Char, 1)
        params(93).Value = customClass.ODToRALBehaviour

        params(94) = New SqlParameter("@RALPeriod", SqlDbType.Int)
        params(94).Value = customClass.RALPeriod

        params(95) = New SqlParameter("@RALPeriodBehaviour", SqlDbType.Char, 1)
        params(95).Value = customClass.RALPeriodBehaviour

        params(96) = New SqlParameter("@MaxDaysRNPaid", SqlDbType.Int)
        params(96).Value = customClass.MaxDaysRNPaid

        params(97) = New SqlParameter("@MaxDaysRNPaidBehaviour", SqlDbType.Char, 1)
        params(97).Value = customClass.MaxDaysRNPaidBehaviour

        params(98) = New SqlParameter("@MaxPTPYDays", SqlDbType.Int)
        params(98).Value = customClass.MaxPTPYDays

        params(99) = New SqlParameter("@MaxPTPYDaysBehaviour", SqlDbType.Char, 1)
        params(99).Value = customClass.MaxPTPYDaysBehaviour

        params(100) = New SqlParameter("@PTPYBank", SqlDbType.Int)
        params(100).Value = customClass.PTPYBank

        params(101) = New SqlParameter("@PTPYBankBehaviour", SqlDbType.Char, 1)
        params(101).Value = customClass.PTPYBankBehaviour

        params(102) = New SqlParameter("@PTPYCompany", SqlDbType.Int)
        params(102).Value = customClass.PTPYCompany

        params(103) = New SqlParameter("@PTPYCompanyBehaviour", SqlDbType.Char, 1)
        params(103).Value = customClass.PTPYCompanyBehaviour

        params(104) = New SqlParameter("@PTPYSupplier", SqlDbType.Int)
        params(104).Value = customClass.PTPYSupplier

        params(105) = New SqlParameter("@PTPYSupplierBehaviour", SqlDbType.Char, 1)
        params(105).Value = customClass.PTPYSupplierBehaviour

        params(106) = New SqlParameter("@RALExtension", SqlDbType.Int)
        params(106).Value = customClass.RALExtension

        params(107) = New SqlParameter("@RALExtensionBehaviour", SqlDbType.Char, 1)
        params(107).Value = customClass.RALExtensionBehaviour

        params(108) = New SqlParameter("@InventoryExpected", SqlDbType.Int)
        params(108).Value = customClass.InventoryExpected

        params(109) = New SqlParameter("@InventoryExpectedBehaviour", SqlDbType.Char, 1)
        params(109).Value = customClass.InventoryExpectedBehaviour

        params(110) = New SqlParameter("@BillingCharges", SqlDbType.Decimal)
        params(110).Value = customClass.BillingCharges

        params(111) = New SqlParameter("@BillingChargesBehaviour", SqlDbType.Char, 1)
        params(111).Value = customClass.BillingChargesBehaviour

        params(112) = New SqlParameter("@LimitAPCash", SqlDbType.Decimal)
        params(112).Value = customClass.LimitAPCash

        params(113) = New SqlParameter("@LimitAPCashBehaviour", SqlDbType.Char, 1)
        params(113).Value = customClass.LimitAPCashBehaviour

        params(114) = New SqlParameter("@IsRecourse", SqlDbType.Bit)
        params(114).Value = customClass.IsRecourse

        params(115) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
        params(115).Value = customClass.ProductOfferingID

        params(116) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(116).Value = customClass.Branch_ID

        params(117) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 8000)
        params(117).Value = customClass.SupplierID

        params(118) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 8000)
        params(118).Value = customClass.AssetCode

        params(119) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(119).Value = customClass.InstallmentAmount

        params(120) = New SqlParameter("@InstallmentAmountBehaviour", SqlDbType.Char, 1)
        params(120).Value = customClass.InstallmentAmountBehaviour

        params(121) = New SqlParameter("@ResidualValue", SqlDbType.Decimal)
        params(121).Value = customClass.ResidualValue

        params(122) = New SqlParameter("@ResidualValueBehaviour", SqlDbType.Char, 1)
        params(122).Value = customClass.ResidualValueBehaviour

        params(123) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(123).Value = customClass.StartDate

        params(124) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(124).Value = customClass.EndDate
        params(125) = New SqlParameter("@InsuranceRateCardID", SqlDbType.VarChar, 50)
        params(125).Value = customClass.InsuranceRateCardID.Trim

        params(126) = New SqlParameter("@AssetCategory", SqlDbType.Char, 10)
        params(126).Value = customClass.AssetCategory
        params(127) = New SqlParameter("@UmurKendaraanFrom", SqlDbType.SmallInt)
        params(127).Value = customClass.UmurKendaraanFrom
        params(128) = New SqlParameter("@UmurKendaraanTo", SqlDbType.SmallInt)
        params(128).Value = customClass.UmurKendaraanTo
        params(129) = New SqlParameter("@FirstInstallment", SqlDbType.Char)
        params(129).Value = customClass.FirstInstallment
        params(130) = New SqlParameter("@HargaOTRFrom", SqlDbType.Money)
        params(130).Value = customClass.HargaOTRFrom
        params(131) = New SqlParameter("@HargaOTRTo", SqlDbType.Money)
        params(131).Value = customClass.HargaOTRTo

        params(132) = New SqlParameter("@PenaltyBasedOn", SqlDbType.Char, 2)
        params(132).Value = customClass.PenaltyBasedOn

        params(133) = New SqlParameter("@PenaltyBasedOnBehaviour", SqlDbType.Char, 1)
        params(133).Value = customClass.PenaltyBasedOnBehaviour

        params(134) = New SqlParameter("@PrepaymentPenaltyFixed", SqlDbType.Decimal)
        params(134).Value = customClass.PrepaymentPenaltyFixed

        params(135) = New SqlParameter("@PrepaymentPenaltyFixedBehaviour", SqlDbType.Char, 1)
        params(135).Value = customClass.PrepaymentPenaltyFixedBehaviour

        params(136) = New SqlParameter("@PenaltyRatePrevious", SqlDbType.Decimal)
        params(136).Value = customClass.PenaltyRatePrevious

        params(137) = New SqlParameter("@PenaltyRateEffectiveDate", SqlDbType.SmallDateTime)
        params(137).Value = customClass.PenaltyRateEffectiveDate

        params(138) = New SqlParameter("@PrepaymentPenaltyPrevious", SqlDbType.Decimal)
        params(138).Value = customClass.PrepaymentPenaltyPrevious

        params(139) = New SqlParameter("@PrepaymentPenaltyEffectiveDate", SqlDbType.SmallDateTime)
        params(139).Value = customClass.PrepaymentPenaltyEffectiveDate

        params(140) = New SqlParameter("@PrepaymentPenaltyFixedEffectiveDate", SqlDbType.SmallDateTime)
        params(140).Value = customClass.PrepaymentPenaltyFixedEffectiveDate

        params(141) = New SqlParameter("@PrepaymentPenaltyFixedPrevious", SqlDbType.Decimal)
        params(141).Value = customClass.PrepaymentPenaltyFixedPrevious

        params(142) = New SqlParameter("@PrioritasPembayaran", SqlDbType.Char, 4)
        params(142).Value = customClass.PrioritasPembayaran

        params(143) = New SqlParameter("@InsuranceDiscPercentage", SqlDbType.Decimal)
        params(143).Value = customClass.InsuranceDiscPercentage

        params(144) = New SqlParameter("@InsuranceDiscPercentageBehaviour", SqlDbType.Char, 1)
        params(144).Value = customClass.InsuranceDiscPercentageBehaviour

        params(145) = New SqlParameter("@IncomeInsRatioPercentage", SqlDbType.Decimal)
        params(145).Value = customClass.IncomeInsRatioPercentage

        params(146) = New SqlParameter("@IncomeInsRatioPercentageBehaviour", SqlDbType.Char, 1)
        params(146).Value = customClass.IncomeInsRatioPercentageBehaviour

        params(147) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
        params(147).Value = customClass.BiayaPolis

        params(148) = New SqlParameter("@BiayaPolisBehaviour", SqlDbType.Char, 1)
        params(148).Value = customClass.BiayaPolisBehaviour

        params(149) = New SqlParameter("@Paket", SqlDbType.Char, 3)
        params(149).Value = customClass.paket

        params(150) = New SqlParameter("@PengaturanPembayaran", SqlDbType.Char, 4)
        params(150).Value = customClass.PengaturanBiaya

        params(151) = New SqlParameter("@PerpanjanganSKT", SqlDbType.Decimal)
        params(151).Value = customClass.PerpanjanganSKT

        params(152) = New SqlParameter("@PerpanjanganSKTBehaviour", SqlDbType.Char, 2)
        params(152).Value = customClass.PerpanjanganSKTBehaviour

        params(153) = New SqlParameter("@DeskCollSMSRemind2", SqlDbType.Decimal)
        params(153).Value = customClass.DeskCollPhoneRemind2

        params(154) = New SqlParameter("@DeskCollSMSRemind3", SqlDbType.Decimal)
        params(154).Value = customClass.DeskCollSMSRemind3

        params(155) = New SqlParameter("@DeskCollSMSRemindBehaviour2", SqlDbType.Char, 1)
        params(155).Value = customClass.DeskCollSMSRemindBehaviour2

        params(156) = New SqlParameter("@DeskCollSMSRemindBehaviour3", SqlDbType.Char, 1)
        params(156).Value = customClass.DeskCollSMSRemindBehaviour3

        params(157) = New SqlParameter("@AngsuranPertama", SqlDbType.Char, 3)
        params(157).Value = customClass.AngsuranPertama

        params(158) = New SqlParameter("@SukuBungaFlat", SqlDbType.Decimal)
        params(158).Value = customClass.SukuBungaFlat

        params(159) = New SqlParameter("@CreditProtection", SqlDbType.Decimal)
        params(159).Value = customClass.CreditProtection

        params(160) = New SqlParameter("@CreditProtectionBehaviour", SqlDbType.Char, 2)
        params(160).Value = customClass.CreditProtectionBehaviour

        params(161) = New SqlParameter("@InsJaminanKredit", SqlDbType.Decimal)
        params(161).Value = customClass.AsuransiKredit

        params(162) = New SqlParameter("@InsJaminanKreditBehaviour", SqlDbType.Char, 2)
        params(162).Value = customClass.AsuransiKreditBehaviour

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE_OFF_BRC, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveAdd")
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region

#Region "ProductOfferingAdd"
    Public Function ProductOfferingAdd(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.BranchId

        Try
            Dim oReturnValue As New Parameter.Product
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD_OFF_BRC, params)
            If reader.Read Then
                readProductFromReader(oReturnValue, reader)
              
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function
#End Region

#Region "ProductOfferingDelete"
    Public Function ProductOfferingDelete(ByVal customClass As Parameter.Product) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@ProductOfferingID", SqlDbType.VarChar, 10)
        params(1).Value = customClass.ProductOfferingID
        params(2) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(2).Value = customClass.BranchId
        params(3) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_OFF_BRC, params)
            Return CType(params(3).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
            Return Nothing
        End Try
    End Function
#End Region

#End Region
    Public Function ProductByKUJP(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.Branch_ID})
        params.Add(New SqlParameter("@KegiatanUsaha", SqlDbType.VarChar, 1) With {.Value = customClass.KegiatanUsaha})
        params.Add(New SqlParameter("@Akad", SqlDbType.VarChar, 2) With {.Value = customClass.Akad})
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spProductByKUJP", params.ToArray()).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
    End Function
    Public Function DropDownListProduct(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000) With {.Value = customClass.WhereCond})
       
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spDropDownListProduct", params.ToArray()).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try

    End Function
    Public Function LoadKegiatanUsaha(cnn As String) As IList(Of Parameter.KegiatanUsaha)

        Dim result = New List(Of Parameter.KegiatanUsaha)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, "select KegiatanUsahaId as Value  , Description  as Text from kegiatanusaha")


        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                'Throw New ArgumentNullException()
                Return Nothing
            End If
            If (reader.IsClosed) Then
                'Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.") 
                Return Nothing
            End If

            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text")


            While (reader.Read())
                Dim row = New Parameter.KegiatanUsaha(IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)), IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString()))
                loadJenisPembiayaan(cnn, row)
                result.Add(row)
            End While

        End If
        Return result
    End Function

    Private Sub loadJenisPembiayaan(cnn As String, kegiatanusaha As Parameter.KegiatanUsaha)

        Dim query = "select j.JenispembiayaanId  Value, Description Text from JenisPembiayaan j " + _
                                      " join kegiatanusahaJenisPembiayaan kj on kj.JenisPembiayaanID=j.JenisPembiayaanID where kj.KegiatanUsahaId =@kjId"

        Dim result = New List(Of Parameter.CommonValueText)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@kjId", kegiatanusaha.Value))

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query, params.ToArray)

        If (reader.HasRows) Then
            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text")
            While (reader.Read())
                kegiatanusaha.JenisPembiayaan.Add(New Parameter.CommonValueText(IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)), IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
            End While
        End If
    End Sub



#Region "GetBranch"
    Public Function GetBranch(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ProductID", SqlDbType.VarChar, 10)
        params(0).Value = customClass.ProductId
        params(1) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(1).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_BRANCH, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(1).Value, Int64)

        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
        Return oReturnValue
    End Function
#End Region

#Region "GetBranchAll"
    Public Function GetBranchAll(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(0).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_BRANCH_ALL, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(0).Value, Int64)

        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
        Return oReturnValue
    End Function
#End Region

#Region "GetCboSkePemb"
    Public Function GetCboSkePemb(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetCboSkePemb).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

#Region "GetCboJenPemb"
    Public Function GetCboJenPemb(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim oReturnValue As New Parameter.Product
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetCboJenPemb).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

End Class
