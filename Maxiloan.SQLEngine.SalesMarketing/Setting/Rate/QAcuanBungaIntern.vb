﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Public Class QAcuanBungaIntern : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const spListing As String = "spAcuanBungaInternListing"
    Private Const spSaveUpdate As String = "spAcuanBungaInternSaveUpdate"
    Private Const spDelete As String = "spAcuanBungaInternDelete"

    Private Const spListingUsage As String = "spGetAssetUsageWithAcuanBunga"
    Private Const spListingCategory As String = "spGetAssetCategoryWithAcuanBunga"
    Private Const spListingList As String = "spAssetMasterPagingWithAcuanBunga"

    Private Const spAssetUsageUpdate As String = "spAcuanBungaInternAssetUsageUpdate"
    Private Const spAssetCategoryUpdate As String = "spAcuanBungaInternAssetCategoryUpdate"
    Private Const spAssetListUpdate As String = "spAcuanBungaInternAssetListUpdate"

    Public Function GetListingUCL(ByVal oCustom As Parameter.PAcuanBungaIntern, ByVal Tipe As String) As DataTable
        Dim TempTable As New DataTable
        Dim CurrSp As String = ""
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@AcuanID", SqlDbType.VarChar)
        params(0).Value = oCustom.AcuanID

        If Tipe = "U" Then
            CurrSp = spListingUsage
        ElseIf Tipe = "C" Then
            CurrSp = spListingCategory
        ElseIf Tipe = "L" Then
            CurrSp = spListingList
        End If
        Try
            TempTable = SqlHelper.ExecuteDataset(oCustom.strConnection, CommandType.StoredProcedure, CurrSp, params).Tables(0)
            Return TempTable
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetListing(ByVal oCustom As Parameter.PAcuanBungaIntern) As Parameter.PAcuanBungaIntern
        Dim params(4) As SqlParameter
        Dim oReturnValue As New Parameter.PAcuanBungaIntern

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)


        params(0).Value = oCustom.CurrentPage
        params(1).Value = oCustom.PageSize
        params(2).Value = oCustom.WhereCond
        params(3).Value = oCustom.SortBy
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustom.strConnection, CommandType.StoredProcedure, spListing, params).Tables(0)
            oReturnValue.RecordCount = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function SaveUpdate(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
        Dim params(10) As SqlParameter
        Dim reader As SqlDataReader
        Dim result As String = ""
        Dim IntManfcYearFrom As Integer = 0
        Dim IntManfcYearTo As Integer = 0

        params(0) = New SqlParameter("@AcuanID", SqlDbType.VarChar)
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar)
        params(2) = New SqlParameter("@UsedNew", SqlDbType.VarChar)
        params(3) = New SqlParameter("@ManfcYearFrom", SqlDbType.Int)
        params(4) = New SqlParameter("@ManfcYearTo", SqlDbType.Int)
        params(5) = New SqlParameter("@RateType", SqlDbType.VarChar)
        params(6) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(7) = New SqlParameter("@Rate", SqlDbType.Decimal)
        params(8) = New SqlParameter("@JenisKredit", SqlDbType.VarChar)
        params(9) = New SqlParameter("@KategoriAsset", SqlDbType.VarChar)
        params(10) = New SqlParameter("@AddEdit", SqlDbType.VarChar)

        If oCustome.ManfcYearFrom.Length > 0 Then
            IntManfcYearFrom = CInt(oCustome.ManfcYearFrom)
        End If
        If oCustome.ManfcYearTo.Length > 0 Then
            IntManfcYearTo = CInt(oCustome.ManfcYearTo)
        End If

        params(0).Value = oCustome.AcuanID
        params(1).Value = oCustome.Description
        params(2).Value = oCustome.UsedNew
        params(3).Value = IntManfcYearFrom
        params(4).Value = IntManfcYearTo
        params(5).Value = oCustome.RateType
        params(6).Value = oCustome.Tenor
        params(7).Value = oCustome.Rate
        params(8).Value = oCustome.JenisKredit
        params(9).Value = oCustome.KategoriAsset
        params(10).Value = oCustome.AddEdit

        Try
            reader = SqlHelper.ExecuteReader(oCustome.strConnection, CommandType.StoredProcedure, spSaveUpdate, params)
            If reader.Read Then
                result = reader("status").ToString
            End If
            reader.Close()
            Return result
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Delete(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@AcuanId", SqlDbType.Char)
        params(0).Value = oCustome.AcuanID
        Try
            SqlHelper.ExecuteNonQuery(oCustome.strConnection, CommandType.StoredProcedure, spDelete, params)
            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function UpdateAssetAcuan(ByVal oCustome As Parameter.PAcuanBungaIntern, ByVal tipe As String) As String
        Dim params(1) As SqlParameter
        Dim sp As String = ""

        params(0) = New SqlParameter("@AcuanId", SqlDbType.Char)
        params(1) = New SqlParameter("@id", SqlDbType.VarChar)

        params(0).Value = oCustome.AcuanID
        params(1).Value = String.Join(",", oCustome.ArrUsage.ToArray)

        If tipe = "U" Then
            sp = spAssetUsageUpdate
        ElseIf tipe = "C" Then
            sp = spAssetCategoryUpdate
        ElseIf tipe = "L" Then
            sp = spAssetListUpdate
        End If

        Try

            SqlHelper.ExecuteNonQuery(oCustome.strConnection, CommandType.StoredProcedure, sp, params)
            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
