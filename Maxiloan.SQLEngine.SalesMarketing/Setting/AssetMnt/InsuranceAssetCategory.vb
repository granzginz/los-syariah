

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsuranceAssetCategory : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spInsAssetCatPaging"
    Private Const LIST_REPORT As String = "spInsAssetCatReport"
    Private Const LIST_ADD As String = "spInsAssetCatAdd"
    Private Const LIST_DELETE As String = "spInsAssetCatDelete"
    Private Const LIST_UPDATE As String = "spInsAssetCatUpdate"
#End Region

#Region " GetInsuranceAssetCategory "
    Public Function GetInsuranceAssetCategory(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oInsuranceAssetCategory.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oInsuranceAssetCategory.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oInsuranceAssetCategory.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oInsuranceAssetCategory.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.InsuranceAssetCategory

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oInsuranceAssetCategory.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try
    End Function
#End Region

#Region " GetInsuranceAssetCategoryReport "
    Public Function GetInsuranceAssetCategoryReport(ByVal oEntities As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = oEntities.WhereCond
        Try
            Dim oReturnValue As New Parameter.InsuranceAssetCategory

            oReturnValue.ListDataReport = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try
    End Function
#End Region

#Region " InsuranceAssetCategoryAdd "
    Public Function InsuranceAssetCategoryAdd(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@InsRateCategoryID", SqlDbType.Char, 2)
        params(0).Value = EncodeSQLStringValue(oInsuranceAssetCategory.InsRateCategoryID)
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(1).Value = EncodeSQLStringValue(oInsuranceAssetCategory.Description)
        params(2) = New SqlParameter("@AssetType", SqlDbType.VarChar, 10)
        params(2).Value = EncodeSQLStringValue(oInsuranceAssetCategory.AssetType)

        params(3) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.InsuranceAssetCategory

            SqlHelper.ExecuteNonQuery(oInsuranceAssetCategory.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            Return CType(params(3).Value, String)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.ZipCode.MasterZipCode.MasterZipCodeAdd")
        End Try
    End Function
#End Region

#Region " InsuranceAssetCategoryUpdate "
    Public Function InsuranceAssetCategoryUpdate(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String

        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@InsRateCategoryID", SqlDbType.Char, 2)
        params(0).Value = oInsuranceAssetCategory.InsRateCategoryID
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(1).Value = oInsuranceAssetCategory.Description
        params(2) = New SqlParameter("@AssetType", SqlDbType.VarChar, 10)
        params(2).Value = EncodeSQLStringValue(oInsuranceAssetCategory.AssetType)
        params(3) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(3).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.InsuranceAssetCategory
            SqlHelper.ExecuteNonQuery(oInsuranceAssetCategory.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
            Return CType(params(3).Value, String)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.Company.MasterCompany.MasterCompanyEdt")
        End Try

    End Function
#End Region

#Region " InsuranceAssetCategoryDelete"
    Public Function InsuranceAssetCategoryDelete(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String        
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@InsRateCategoryID", SqlDbType.Char, 2)
        params(0).Value = oInsuranceAssetCategory.InsRateCategoryID
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(1).Value = oInsuranceAssetCategory.Description
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(oInsuranceAssetCategory.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.setting.AssetMnt.InsuranceAssetCategoryDelete")
        End Try
    End Function
#End Region

End Class
