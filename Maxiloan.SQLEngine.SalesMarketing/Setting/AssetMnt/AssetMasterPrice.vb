﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetMasterPrice : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingAssetMasterPrice"
    Private Const LIST_SELECT_ASSETMASTER As String = "spAssetMasterPaging"
    Private Const LIST_SELECT_LIST_CUSTOMER As String = "spAssetMasterPriceListCustomer"
    Private Const LIST_SELECT_COMBO_TYPE As String = "spGetAssetType"
    Private Const LIST_SELECT_COMBO_ASSET_CODE As String = "spGetAssetCode"
    Private Const LIST_SELECT_ALL_ASSET_CODE As String = "spGetAllAssetCode"
    Private Const LIST_SELECT_ALL_ASSET_MASTER As String = "spAssetMasterView"
    Private Const LIST_BY_ID As String = "spAssetMasterPriceList"
    Private Const LIST_ADD As String = "spAssetMasterPriceSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetMasterPriceSaveEdit"
    Private Const LIST_DELETE As String = "spAssetMasterPriceDelete"
    Private Const LIST_REPORT As String = "spAssetMasterPriceReport"
#End Region

    Public Function GetAssetMasterPrice(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim oReturnValue As New Parameter.AssetMasterPrice
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetAssetMaster(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim oReturnValue As New Parameter.AssetMasterPrice
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ASSETMASTER, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetAssetMasterPriceReport(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim oReturnValue As New Parameter.AssetMasterPrice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetAssetMasterPriceList(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim reader As SqlDataReader
        Dim params(2) As SqlParameter
        Dim oReturnValue As New Parameter.AssetMasterPrice
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.AssetCode
        params(1) = New SqlParameter("@ManufacturingYear", SqlDbType.SmallInt)
        params(1).Value = IIf(ocustomClass.ManufacturingYear = "", Now.Year, ocustomClass.ManufacturingYear)
        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.BranchId
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.AssetCode = reader("AssetCode").ToString
                oReturnValue.ManufacturingYear = reader("ManufacturingYear").ToString
                oReturnValue.BranchId = reader("BranchId").ToString
                oReturnValue.Price = reader("Price").ToString
                oReturnValue.AssetDescription = reader("AssetDescription").ToString                
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function AssetMasterPriceSaveAdd(ByVal ocustomClass As Parameter.AssetMasterPrice) As String
        Dim ErrMessage As String = ""
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.EditAssetCode
        params(1) = New SqlParameter("@ManufacturingYear", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.EditManufacturingYear
        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.BranchId
        params(3) = New SqlParameter("@Price", SqlDbType.Decimal)
        params(3).Value = ocustomClass.Price
        params(4) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(4).Value = ocustomClass.LoginId
        params(5) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(5).Value = ocustomClass.BusinessDate
        params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(6).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub AssetMasterPriceSaveEdit(ByVal ocustomClass As Parameter.AssetMasterPrice)
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.AssetCode
        params(1) = New SqlParameter("@ManufacturingYear", SqlDbType.Int)
        params(1).Value = ocustomClass.ManufacturingYear
        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.BranchId
        params(3) = New SqlParameter("@Price", SqlDbType.Decimal)
        params(3).Value = ocustomClass.Price
        params(4) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(4).Value = ocustomClass.LoginId
        params(5) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(5).Value = ocustomClass.BusinessDate
        params(6) = New SqlParameter("@EditAssetCode", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.EditAssetCode
        params(7) = New SqlParameter("@EditManufacturingYear", SqlDbType.Int)
        params(7).Value = ocustomClass.EditManufacturingYear
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function AssetMasterPriceDelete(ByVal ocustomClass As Parameter.AssetMasterPrice) As String
        Dim Err As Integer
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.AssetCode
        params(1) = New SqlParameter("@ManufacturingYear", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.ManufacturingYear
        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.BranchId
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"            
        End Try
    End Function

    Public Function GetAssetTypeCombo(ByVal customclass As Parameter.AssetMasterPrice) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_COMBO_TYPE).Tables(0)
    End Function

    Public Function GetAssetCodeCombo(ByVal customclass As Parameter.AssetMasterPrice) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_COMBO_ASSET_CODE).Tables(0)
    End Function

    Public Function GetAllAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim oReturnValue As New Parameter.AssetMasterPrice
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar)
        params(0).Value = oCustomClass.AssetCode
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL_ASSET_CODE, params).Tables(0)            
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetAssetMasterByAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim oReturnValue As New Parameter.AssetMasterPrice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.AssetCode

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL_ASSET_MASTER, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
