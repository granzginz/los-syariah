
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AssetTypeCategory : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypeCategoryPaging"
    Private Const LIST_BY_ID As String = "spAssetTypeCategoryEdit"
    Private Const LIST_ADD As String = "spAssetTypeCategorySaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeCategorySaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeCategoryDelete"
    Private Const LIST_INSRATE As String = "spAssetTypeCategoryInsRate"
    Private Const LIST_INSRATEFORSPECIFICASSETTYPE As String = "sp_InsRate_AssetTypeId_For_SpecificAssetType"
    Private Const LIST_REPORT As String = "spAssetTypeCategoryReport"
#End Region

    Public Function GetAssetTypeCategory(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim oReturnValue As New Parameter.AssetTypeCategory
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategory")
        End Try
    End Function

    Public Function GetAssetTypeCategoryReport(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim oReturnValue As New Parameter.AssetTypeCategory
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(1).Value = customClass.WhereCond

        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategoryReport")
        End Try
    End Function

    Public Function GetAssetTypeCategoryEdit(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@CategoryID", SqlDbType.Char, 10)
        params(1).Value = customClass.CategoryID
        Try
            Dim oReturnValue As New Parameter.AssetTypeCategory
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.InsRate = reader("InsRateCategoryID").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategoryEdit")
        End Try
    End Function

    Public Function GetAssetTypeCategoryInsRate(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim oReturnValue As New Parameter.AssetTypeCategory
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSRATE).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategoryEdit")
        End Try
    End Function
    Public Function GetAssetTypeCategoryInsRateforSpecificAssetType(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim oReturnValue As New Parameter.AssetTypeCategory
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AssettypeId", SqlDbType.VarChar, 10)
        params(0).Value = customClass.Id
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSRATEFORSPECIFICASSETTYPE, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategoryInsRateforSpecificAssetType")
        End Try
    End Function
    Public Function AssetTypeCategorySaveAdd(ByVal customClass As Parameter.AssetTypeCategory) As String
        Dim ErrMessage As String = ""
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@CategoryID", SqlDbType.Char, 10)
        params(1).Value = customClass.CategoryID
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        params(3) = New SqlParameter("@InsRate", SqlDbType.Char, 2)
        params(3).Value = customClass.InsRate
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.AssetTypeCategorySaveAdd")
        End Try
    End Function

    Public Sub AssetTypeCategorySaveEdit(ByVal customClass As Parameter.AssetTypeCategory)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@CategoryID", SqlDbType.Char, 10)
        params(1).Value = customClass.CategoryID
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        params(3) = New SqlParameter("@InsRate", SqlDbType.Char, 2)
        params(3).Value = customClass.InsRate
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.AssetTypeCategorySaveEdit")
        End Try
    End Sub

    Public Function AssetTypeCategoryDelete(ByVal customClass As Parameter.AssetTypeCategory) As String
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@CategoryID", SqlDbType.Char, 10)
        params(1).Value = customClass.CategoryID
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.AssetTypeCategoryDelete")
        End Try
    End Function
End Class

