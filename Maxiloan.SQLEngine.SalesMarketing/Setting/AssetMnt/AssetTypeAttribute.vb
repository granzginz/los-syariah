

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AssetTypeAttribute : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypeAttributePaging"
    Private Const LIST_BY_ID As String = "spAssetTypeAttributeEdit"
    Private Const LIST_ADD As String = "spAssetTypeAttributeSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeAttributeSaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeAttributeDelete"
    Private Const LIST_REPORT As String = "spAssetTypeAttributeReport"
#End Region

    Public Function GetAssetTypeAttribute(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim oReturnValue As New Parameter.AssetTypeAttribute
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.GetAssetTypeAttribute")
        End Try
    End Function

    Public Function GetAssetTypeAttributeReport(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim oReturnValue As New Parameter.AssetTypeAttribute
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(1).Value = customClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")

        End Try
    End Function

    Public Function GetAssetTypeAttributeEdit(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
        params(1).Value = customClass.AttributeID
        Try
            Dim oReturnValue As New Parameter.AssetTypeAttribute
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Name = reader("Name").ToString
                oReturnValue.Length = reader("AttributeLength").ToString
                oReturnValue.Type = reader("AttributeType").ToString
                oReturnValue.Change = reader("CanBeChanged").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.GetAssetTypeAttributeEdit")

        End Try
    End Function

    Public Function AssetTypeAttributeSaveAdd(ByVal customClass As Parameter.AssetTypeAttribute) As String
        Dim ErrMessage As String = ""
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
        params(1).Value = customClass.AttributeID
        params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Name
        params(3) = New SqlParameter("@Type", SqlDbType.Char, 1)
        params(3).Value = customClass.Type
        params(4) = New SqlParameter("@Length", SqlDbType.SmallInt)
        params(4).Value = customClass.Length
        params(5) = New SqlParameter("@Change", SqlDbType.Bit)
        params(5).Value = customClass.Change
        params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(6).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.AssetTypeAttributeSaveAdd")
        End Try
    End Function

    Public Sub AssetTypeAttributeSaveEdit(ByVal customClass As Parameter.AssetTypeAttribute)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
        params(1).Value = customClass.AttributeID
        params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Name
        params(3) = New SqlParameter("@Type", SqlDbType.Char, 1)
        params(3).Value = customClass.Type
        params(4) = New SqlParameter("@Length", SqlDbType.SmallInt)
        params(4).Value = customClass.Length
        params(5) = New SqlParameter("@Change", SqlDbType.Bit)
        params(5).Value = customClass.Change
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.AssetTypeAttributeSaveEdit")

        End Try
    End Sub

    Public Function AssetTypeAttributeDelete(ByVal customClass As Parameter.AssetTypeAttribute) As String
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
        params(1).Value = customClass.AttributeID
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.AssetTypeAttributeDelete")
        End Try
    End Function
End Class

