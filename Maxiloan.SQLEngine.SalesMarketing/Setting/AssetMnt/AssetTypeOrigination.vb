
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetTypeOrigination : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypeOriginationPaging"
    Private Const LIST_BY_ID As String = "spAssetTypeOriginationEdit"
    Private Const LIST_ADD As String = "spAssetTypeOriginationSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeOriginationSaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeOriginationDelete"
    Private Const LIST_REPORT As String = "spAssetTypeOriginationReport"
#End Region

    Public Function GetAssetTypeOriginationReport(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim oReturnValue As New Parameter.AssetTypeOrigination
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(1).Value = customClass.WhereCond

        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.GetAssetTypeOriginationReport")
        End Try
    End Function
    Public Function GetAssetTypeOrigination(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim oReturnValue As New Parameter.AssetTypeOrigination
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.GetAssetTypeOrigination")
        End Try
    End Function

    Public Function GetAssetTypeOriginationEdit(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@OriginationID", SqlDbType.Char, 10)
        params(1).Value = customClass.OriginationID
        Try
            Dim oReturnValue As New Parameter.AssetTypeOrigination
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.GetAssetTypeOriginationEdit")
        End Try
    End Function

    Public Function AssetTypeOriginationSaveAdd(ByVal customClass As Parameter.AssetTypeOrigination) As String
        Dim ErrMessage As String = ""
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@OriginationID", SqlDbType.Char, 10)
        params(1).Value = customClass.OriginationID
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(3).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.AssetTypeOriginationSaveAdd")
        End Try
    End Function

    Public Sub AssetTypeOriginationSaveEdit(ByVal customClass As Parameter.AssetTypeOrigination)
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@OriginationID", SqlDbType.Char, 10)
        params(1).Value = customClass.OriginationID
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.AssetTypeOriginationSaveEdit")
        End Try
    End Sub

    Public Function AssetTypeOriginationDelete(ByVal customClass As Parameter.AssetTypeOrigination) As String
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@OriginationID", SqlDbType.Char, 10)
        params(1).Value = customClass.OriginationID
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeOrigination.AssetTypeOriginationDelete")
        End Try
    End Function
End Class

