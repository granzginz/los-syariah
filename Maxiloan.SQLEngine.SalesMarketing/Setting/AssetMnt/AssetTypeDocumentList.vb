
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AssetTypeDocumentList : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypeDocumentListPaging"
    Private Const LIST_BY_ID As String = "spAssetTypeDocumentListEdit"
    Private Const LIST_ADD As String = "spAssetTypeDocumentListSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeDocumentListSaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeDocumentListDelete"
    Private Const LIST_REPORT As String = "spAssetTypeDocumentListReport"
#End Region

    Public Function GetAssetTypeDocumentListReport(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim oReturnValue As New Parameter.AssetTypeDocumentList
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(1).Value = customClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.GetAssetTypeDocumentListReport")
        End Try
    End Function

    Public Function GetAssetTypeDocumentList(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim oReturnValue As New Parameter.AssetTypeDocumentList
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.GetAssetTypeDocumentList")

        End Try
    End Function

    Public Function GetAssetTypeDocumentListEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@DocumentListID", SqlDbType.Char, 10)
        params(1).Value = customClass.DocumentListID
        Try
            Dim oReturnValue As New Parameter.AssetTypeDocumentList
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Name = reader("AssetDocName").ToString
                oReturnValue.Value = reader("IsValueNeeded").ToString
                oReturnValue.NewAsset = reader("MandatoryForNewAsset").ToString
                oReturnValue.UsedAsset = reader("MandatoryForUsedAsset").ToString
                oReturnValue.MainDoc = reader("IsMainDoc").ToString
                oReturnValue.CustomerType = reader("CustomerType").ToString
                oReturnValue.Origination = reader("Origination").ToString
                oReturnValue.IsNoRequired = reader("IsNoRequired").ToString

                If oReturnValue.Value = "True" Then
                    oReturnValue.Value = "1"
                Else
                    oReturnValue.Value = "0"
                End If

                If oReturnValue.NewAsset = "True" Then
                    oReturnValue.NewAsset = "1"
                Else
                    oReturnValue.NewAsset = "0"
                End If

                If oReturnValue.UsedAsset = "True" Then
                    oReturnValue.UsedAsset = "1"
                Else
                    oReturnValue.UsedAsset = "0"
                End If

                If oReturnValue.MainDoc = "True" Then
                    oReturnValue.MainDoc = "1"
                Else
                    oReturnValue.MainDoc = "0"
                End If
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.GetAssetTypeDocumentListEdit")
        End Try
    End Function

    Public Function AssetTypeDocumentListSaveAdd(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim ErrMessage As String = ""
        Dim params(10) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@DocumentListID", SqlDbType.Char, 10)
        params(1).Value = customClass.DocumentListID
        params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Name
        params(3) = New SqlParameter("@IsValueNeeded", SqlDbType.Bit)
        params(3).Value = CType(customClass.Value, Boolean)
        params(4) = New SqlParameter("@MandatoryNew", SqlDbType.Bit)
        params(4).Value = CType(customClass.NewAsset, Boolean)
        params(5) = New SqlParameter("@MandatoryUsed", SqlDbType.Bit)
        params(5).Value = CType(customClass.UsedAsset, Boolean)
        params(6) = New SqlParameter("@IsMainDoc", SqlDbType.Bit)
        params(6).Value = CType(customClass.MainDoc, Boolean)
        params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        params(8).Value = customClass.CustomerType
        params(9) = New SqlParameter("@Origination", SqlDbType.Char, 3)
        params(9).Value = customClass.Origination
        params(10) = New SqlParameter("@IsNoRequired", SqlDbType.Bit)
        params(10).Value = CBool(customClass.IsNoRequired)
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(7).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.AssetTypeDocumentListSaveAdd")
        End Try
    End Function

    Public Function AssetTypeDocumentListSaveEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim ErrMessage As String = ""
        Dim params(10) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@DocumentListID", SqlDbType.Char, 10)
        params(1).Value = customClass.DocumentListID
        params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Name
        params(3) = New SqlParameter("@IsValueNeeded", SqlDbType.Bit)
        params(3).Value = CType(customClass.Value, Boolean)
        params(4) = New SqlParameter("@MandatoryNew", SqlDbType.Bit)
        params(4).Value = CType(customClass.NewAsset, Boolean)
        params(5) = New SqlParameter("@MandatoryUsed", SqlDbType.Bit)
        params(5).Value = CType(customClass.UsedAsset, Boolean)
        params(6) = New SqlParameter("@IsMainDoc", SqlDbType.Bit)
        params(6).Value = CType(customClass.MainDoc, Boolean)
        params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        params(8).Value = customClass.CustomerType
        params(9) = New SqlParameter("@Origination", SqlDbType.Char, 3)
        params(9).Value = customClass.Origination
        params(10) = New SqlParameter("@IsNoRequired", SqlDbType.Bit)
        params(10).Value = CBool(customClass.IsNoRequired)

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
            ErrMessage = CType(params(7).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.AssetTypeDocumentListSaveEdit")
        End Try
    End Function

    Public Function AssetTypeDocumentListDelete(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@DocumentListID", SqlDbType.Char, 10)
        params(1).Value = customClass.DocumentListID
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetTypeDocumentList.AssetTypeDocumentListDelete")
        End Try
    End Function
End Class

