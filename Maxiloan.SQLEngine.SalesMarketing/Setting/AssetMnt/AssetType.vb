
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AssetType : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypePaging"
    Private Const LIST_BY_ID As String = "spAssetTypeEdit"
    Private Const LIST_ADD As String = "spAssetTypeSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeSaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeDelete"
    Private Const LIST_REPORT As String = "spAssetTypeReport"
#End Region

    Public Function GetAssetType(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim oReturnValue As New Parameter.AssetType

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try
    End Function

    Public Function GetAssetTypeReport(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.AssetType
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")

        End Try
    End Function

    Public Function GetAssetTypeEdit(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        Try
            Dim oReturnValue As New Parameter.AssetType
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.Serial1 = reader("serialno1label").ToString
                oReturnValue.Serial2 = reader("serialno2label").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function

    Public Function AssetTypeSaveAdd(ByVal customClass As Parameter.AssetType) As String
        Dim ErrMessage As String = ""
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        params(2) = New SqlParameter("@serialno1label", SqlDbType.VarChar, 25)
        params(2).Value = customClass.Serial1
        params(3) = New SqlParameter("@serialno2label", SqlDbType.VarChar, 25)
        params(3).Value = customClass.Serial2
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveAdd")
        End Try
    End Function

    Public Function AssetTypeDelete(ByVal customClass As Parameter.AssetType) As String
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(1).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        End Try
    End Function

    Public Sub AssetTypeSaveEdit(ByVal customClass As Parameter.AssetType)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        params(2) = New SqlParameter("@serialno1label", SqlDbType.VarChar, 25)
        params(2).Value = customClass.Serial1
        params(3) = New SqlParameter("@serialno2label", SqlDbType.VarChar, 25)
        params(3).Value = customClass.Serial2
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveEdit")
        End Try
    End Sub

    Public Function GetMaxLevel(ByVal strAssetTypeID As String, ByVal strConnection As String) As Integer
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = strAssetTypeID
        params(1) = New SqlParameter("@MaxLevel", SqlDbType.Int)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(strConnection, CommandType.StoredProcedure, "spGetMaxLevel", params)
            Return CType(params(1).Value, Integer)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveEdit")
        End Try
    End Function
End Class
