
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetTypeScheme : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spAssetTypeSchemePaging"
    Private Const LIST_BY_ID As String = "spAssetTypeSchemeEdit"
    Private Const LIST_SAVEADD As String = "spAssetTypeSchemeSaveAdd"
    Private Const LIST_UPDATE As String = "spAssetTypeSchemeSaveEdit"
    Private Const LIST_DELETE As String = "spAssetTypeSchemeDelete"
    Private Const LIST_REPORT As String = "spAssetTypeSchemeReport"
    Private Const LIST_ADD As String = "spAssetTypeSchemeAdd"
    Private m_connection As SqlConnection
#End Region

    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub
    Public Function GetAssetTypeSchemeReport(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim oReturnValue As New Parameter.AssetTypeScheme
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(1).Value = customClass.WhereCond

        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.GetAssetTypeSchemeReport")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
    Public Function GetAssetTypeScheme(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim oReturnValue As New Parameter.AssetTypeScheme
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@WhereCond_total", SqlDbType.VarChar)
        params(3).Value = customClass.WhereCond_total
        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4).Value = customClass.SortBy
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@TotalRecords_total", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@AssetLevel", SqlDbType.Int)
        params(7).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            oReturnValue.TotalRecords_total = CType(params(6).Value, Int64)
            If IsDBNull(params(7).Value) Then
                oReturnValue.AssetLevel = "1"
            Else
                oReturnValue.AssetLevel = CType(params(7).Value, String)
            End If

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.GetAssetTypeScheme")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Function GetAssetTypeSchemeEdit(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AssetLevel", SqlDbType.SmallInt)
        params(1).Value = customClass.AssetLevel
        Try
            Dim oReturnValue As New Parameter.AssetTypeScheme
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.GetAssetTypeSchemeEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Function AssetTypeSchemeSaveAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim ErrMessage As String = ""
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AssetLevel", SqlDbType.SmallInt)
        params(1).Value = customClass.AssetLevel
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Description
        params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEADD, params)
            ErrMessage = CType(params(3).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.AssetTypeSchemeSaveAdd")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Sub AssetTypeSchemeSaveEdit(ByVal customClass As Parameter.AssetTypeScheme)
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AssetLevel", SqlDbType.SmallInt)
        params(1).Value = customClass.AssetLevel
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(2).Value = customClass.Description
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.AssetTypeSchemeSaveEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Sub

    Public Function AssetTypeSchemeDelete(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@AssetLevel", SqlDbType.SmallInt)
        params(1).Value = customClass.AssetLevel
        params(2) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(2).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.AssetTypeSchemeDelete")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Function AssetTypeSchemeAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            Return CType(params(1).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeScheme.AssetTypeSchemeDelete")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function
End Class

