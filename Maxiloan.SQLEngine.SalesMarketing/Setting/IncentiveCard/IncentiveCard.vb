
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class IncentiveCard : Inherits DataAccessBase
    Private Const spSupplierINCTVSupplierIncentiveCardHEdit As String = "spSupplierINCTVSupplierIncentiveCardHEdit"
    Private Const spSupplierINCTVSupplierIncentiveCardHAdd As String = "spSupplierINCTVSupplierIncentiveCardHAdd"
    Private Const spSupplierINCTVSupplierIncentiveCardHDelete As String = "spSupplierINCTVSupplierIncentiveCardHDelete"
    Private Const spSupplierINCTVGetComboSupplierIncentiveCard As String = "spSupplierINCTVGetComboSupplierIncentiveCard"
    Private Const spSupplierINCTVSupplierIncentiveCardDUnitADD As String = "spSupplierINCTVSupplierIncentiveCardDUnitADD"
    Private Const spSupplierINCTVSupplierIncentiveCardDUnitEDIT As String = "spSupplierINCTVSupplierIncentiveCardDUnitEDIT"
    Private Const spSupplierINCTVSupplierIncentiveCardDAFADD As String = "spSupplierINCTVSupplierIncentiveCardDAFADD"
    Private Const spSupplierINCTVSupplierIncentiveCardDAFEdit As String = "spSupplierINCTVSupplierIncentiveCardDAFEdit"
    Private Const spSupplierINCTVDailyDAdd As String = "spSupplierINCTVDailyDAdd"
    Private Const spSupplierINCTVDailyDEdit As String = "spSupplierINCTVDailyDEdit"
    Private Const spSupplierINCTVUpdateIncentiveRestAmount As String = "spSupplierINCTVUpdateIncentiveRestAmount"
    Private Const spSupplierINCTVUpdateIncentiveRestEmployeeAmount As String = "spSupplierINCTVUpdateIncentiveRestEmployeeAmount"
    Private Const spSupplierINCTVIncentiveRestExecution As String = "spSupplierINCTVIncentiveRestExecution"
    Private Const spSupplierINCTVEmployeeIncentiveRestExecution As String = "spSupplierINCTVEmployeeIncentiveRestExecution"

    Public Function GetViewByID(ByVal customClass As Parameter.IncentiveCard) As Parameter.IncentiveCard
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CardID", SqlDbType.Int)
        params(0).Value = customClass.CardID
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            Return customClass
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("IncentiveCard", "GetViewByID", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("IncentiveCard", "GetViewByID", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub SupplierINCTVHeaderAdd(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(6) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@CardName", SqlDbType.VarChar, 50)
            params(0).Value = customclass.CardName
            params(1) = New SqlParameter("@ValidFrom", SqlDbType.DateTime)
            params(1).Value = customclass.ValidFrom
            params(2) = New SqlParameter("@ValidUntil", SqlDbType.DateTime)
            params(2).Value = customclass.ValidUntil
            params(3) = New SqlParameter("@MinAFIncentiveUnit", SqlDbType.Decimal)
            params(3).Value = customclass.MinAFIncentiveUnit
            params(4) = New SqlParameter("@IntervalUnit", SqlDbType.Int)
            params(4).Value = customclass.IntervalUnit
            params(5) = New SqlParameter("@IntervalAF", SqlDbType.Int)
            params(5).Value = customclass.IntervalAF
            params(6) = New SqlParameter("@RefundPremi", SqlDbType.Decimal)
            params(6).Value = customclass.RefundPremi

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardHAdd, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVHeaderAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVHeaderEdit(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(7) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@CardName", SqlDbType.VarChar, 50)
            params(0).Value = customclass.CardName
            params(1) = New SqlParameter("@ValidFrom", SqlDbType.DateTime)
            params(1).Value = customclass.ValidFrom
            params(2) = New SqlParameter("@ValidUntil", SqlDbType.DateTime)
            params(2).Value = customclass.ValidUntil
            params(3) = New SqlParameter("@MinAFIncentiveUnit", SqlDbType.Decimal)
            params(3).Value = customclass.MinAFIncentiveUnit
            params(4) = New SqlParameter("@IntervalUnit", SqlDbType.Int)
            params(4).Value = customclass.IntervalUnit
            params(5) = New SqlParameter("@IntervalAF", SqlDbType.Int)
            params(5).Value = customclass.IntervalAF
            params(6) = New SqlParameter("@RefundPremi", SqlDbType.Decimal)
            params(6).Value = customclass.RefundPremi
            params(7) = New SqlParameter("@CardID", SqlDbType.Int)
            params(7).Value = customclass.cardID

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardHEdit, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVHeaderEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Function SupplierINCTVHeaderDelete(ByVal customClass As Parameter.IncentiveCard) As Parameter.IncentiveCard
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objCon As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        params(0) = New SqlParameter("@CardID", SqlDbType.Int)
        params(0).Value = customClass.CardID
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardHDelete, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVHeaderDelete", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
    Public Function GetComboSupplierIncentiveCard(ByVal customClass As Parameter.IncentiveCard) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierINCTVGetComboSupplierIncentiveCard).Tables(0)
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("IncentiveCard", "GetComboSupplierIncentiveCard", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("IncentiveCard", "GetComboSupplierIncentiveCard", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub SupplierINCTVDetailUnitAdd(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CardID", SqlDbType.Int)
            params(1) = New SqlParameter("@CategoryID", SqlDbType.VarChar, 21)
            params(2) = New SqlParameter("@UnitMin", SqlDbType.Int)
            params(3) = New SqlParameter("@UnitMax", SqlDbType.Int)
            params(4) = New SqlParameter("@IncentiveUnit", SqlDbType.Decimal)
            params(5) = New SqlParameter("@IntervalRow", SqlDbType.Int)

            If customclass.ListData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1
                    params(0).Value = customclass.CardID
                    params(1).Value = customclass.CategoryID
                    params(2).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("UnitMin"))
                    params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("UnitMax"))
                    params(4).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("IncentiveUnit"))
                    params(5).Value = intLoopOmset + 1
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardDUnitADD, params)
                Next
            End If
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDetailUnitAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVDetailUnitEdit(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CardID", SqlDbType.Int)
            params(1) = New SqlParameter("@CategoryID", SqlDbType.VarChar, 21)
            params(2) = New SqlParameter("@UnitMin", SqlDbType.Int)
            params(3) = New SqlParameter("@UnitMax", SqlDbType.Int)
            params(4) = New SqlParameter("@IncentiveUnit", SqlDbType.Decimal)
            params(5) = New SqlParameter("@IntervalRow", SqlDbType.Int)

            If customclass.ListData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1
                    params(0).Value = customclass.CardID
                    params(1).Value = customclass.CategoryID
                    params(2).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("UnitMin"))
                    params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("UnitMax"))
                    params(4).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("IncentiveUnit"))
                    params(5).Value = intLoopOmset + 1

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardDUnitEDIT, params)
                Next
            End If
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDetailUnitEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVDetailAFAdd(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CardID", SqlDbType.Int)
            params(1) = New SqlParameter("@UnitMin", SqlDbType.Decimal)
            params(2) = New SqlParameter("@UnitMax", SqlDbType.Decimal)
            params(3) = New SqlParameter("@RateIncentiveAF", SqlDbType.Float)
            params(4) = New SqlParameter("@IntervalRow", SqlDbType.Int)

            If customclass.ListData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1
                    params(0).Value = customclass.CardID
                    params(1).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("UnitMin"))
                    params(2).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("UnitMax"))
                    params(3).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("RateIncentiveAF"))
                    params(4).Value = intLoopOmset + 1
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardDAFADD, params)
                Next
            End If
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDetailAFAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVDetailAFEdit(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@CardID", SqlDbType.Int)
            params(1) = New SqlParameter("@UnitMin", SqlDbType.Decimal)
            params(2) = New SqlParameter("@UnitMax", SqlDbType.Decimal)
            params(3) = New SqlParameter("@RateIncentiveAF", SqlDbType.Decimal)
            params(4) = New SqlParameter("@IntervalRow", SqlDbType.Decimal)

            If customclass.ListData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1
                    params(0).Value = customclass.CardID
                    params(1).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("UnitMin"))
                    params(2).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("UnitMax"))
                    params(3).Value = CDec(customclass.ListData.Rows(intLoopOmset).Item("RateIncentiveAF"))
                    params(4).Value = intLoopOmset + 1
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVSupplierIncentiveCardDAFEdit, params)
                Next
            End If
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDetailAFEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVDailyDAdd(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@RefundToSupp", SqlDbType.Decimal)
            params(2).Value = customclass.RefundPremi           
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate
            params(4) = New SqlParameter("@RefundInterest", SqlDbType.Decimal)
            params(4).Value = customclass.RefundInterest

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVDailyDAdd, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDailyDAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVUpdateIncentiveRestAmount(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchID
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@IncentiveRest", SqlDbType.Decimal)
            params(2).Value = customclass.IncentiveRest


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVUpdateIncentiveRestAmount, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVUpdateIncentiveRestAmount", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVUpdateIncentiveRestEmployeeAmount(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchID
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@IncentiveRest", SqlDbType.Decimal)
            params(2).Value = customclass.IncentiveRest
            params(3) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.Char, 2)
            params(3).Value = customclass.SupplierEmployeePosition


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVUpdateIncentiveRestEmployeeAmount, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVUpdateIncentiveRestEmployeeAmount", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVIncentiveRestAmountExecution(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customclass.SupplierID
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVIncentiveRestExecution, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVIncentiveRestAmountExecution", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVEmployeeIncentiveRestAmountExecution(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@WhereSupplierID", SqlDbType.Char, 10)
            params(0).Value = customclass.SupplierID
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVEmployeeIncentiveRestExecution, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVEmployeeIncentiveRestAmountExecution", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub SupplierINCTVDailyDEdit(ByVal customclass As Parameter.IncentiveCard)
        Dim params() As SqlParameter = New SqlParameter(15) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@RefundToSupp", SqlDbType.Decimal)
            params(2).Value = customclass.RefundPremi
            params(3) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
            params(3).Value = customclass.SupplierID
            params(4) = New SqlParameter("@CompanyInsuranceAmount", SqlDbType.Decimal)
            params(4).Value = customclass.CompanyAmount
            params(5) = New SqlParameter("@GMInsuranceAmount", SqlDbType.Decimal)
            params(5).Value = customclass.GMAmount
            params(6) = New SqlParameter("@BMInsuranceAmount", SqlDbType.Decimal)
            params(6).Value = customclass.BMAmount
            params(7) = New SqlParameter("@ADHInsuranceAmount", SqlDbType.Decimal)
            params(7).Value = customclass.ADHAmount
            params(8) = New SqlParameter("@SPVInsuranceAmount", SqlDbType.Decimal)
            params(8).Value = customclass.SPVAmount
            params(9) = New SqlParameter("@SPInsuranceAmount", SqlDbType.Decimal)
            params(9).Value = customclass.SPAmount
            params(10) = New SqlParameter("@ADInsuranceAmount", SqlDbType.Decimal)
            params(10).Value = customclass.ADAmount
            params(11) = New SqlParameter("@SalesmanID", SqlDbType.VarChar, 10)
            params(11).Value = customclass.SalesmanID
            params(12) = New SqlParameter("@SalesSupervisorID", SqlDbType.VarChar, 10)
            params(12).Value = customclass.SalesSupervisorID
            params(13) = New SqlParameter("@SupplierAdminID", SqlDbType.VarChar, 10)
            params(13).Value = customclass.SupplierAdminID
            params(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(14).Value = customclass.BusinessDate
            params(15) = New SqlParameter("@RefundInterest", SqlDbType.Decimal)
            params(15).Value = customclass.RefundInterest

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVDailyDEdit, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("IncentiveCard", "SupplierINCTVDailyDAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
End Class
