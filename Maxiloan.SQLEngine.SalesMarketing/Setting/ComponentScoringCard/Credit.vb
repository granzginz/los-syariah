
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Credit : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spCreditPaging"
    Private Const LIST_BY_ID As String = "spCreditEdit"
    Private Const LIST_ADD As String = "spCreditSaveAdd"
    Private Const LIST_UPDATE As String = "spCreditSaveEdit"
    Private Const LIST_DELETE As String = "spCreditDelete"
    Private Const LIST_REPORT As String = "spCreditReport"
    Private m_connection As SqlConnection
#End Region

    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function GetCredit(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim oReturnValue As New Parameter.Credit
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function

    Public Function GetCreditReport(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = customClass.WhereCond

        Dim oReturnValue As New Parameter.Credit
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)

            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeReport")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Function CreditEdit(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        Try
            Dim oReturnValue As New Parameter.Credit
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.ScoringType = reader("ScoringType").ToString
                oReturnValue.GroupComponent = reader("GroupComponent").ToString
                oReturnValue.SeqComponent = CInt(reader("SeqComponent"))
                oReturnValue.CalculationType = reader("CalculationType").ToString
                oReturnValue.SQLCmd = reader("SQLCmd").ToString
                oReturnValue.DataSource = reader("DataSource").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function


    Public Function CreditSaveAdd(ByVal customClass As Parameter.Credit) As String
        Dim ErrMessage As String = ""
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        params(2) = New SqlParameter("@ScoringType", SqlDbType.Char, 1)
        params(2).Value = customClass.ScoringType
        params(3) = New SqlParameter("@GroupComponent", SqlDbType.VarChar, 1)
        params(3).Value = customClass.GroupComponent
        params(4) = New SqlParameter("@SeqComponent", SqlDbType.SmallInt, 2)
        params(4).Value = customClass.SeqComponent
        params(5) = New SqlParameter("@CalculationType", SqlDbType.Char, 1)
        params(5).Value = customClass.CalculationType
        'params(6) = New SqlParameter("@SQLCmd", SqlDbType.VarChar, 255)
        params(6) = New SqlParameter("@SQLCmd", SqlDbType.Text)
        params(6).Value = customClass.SQLCmd
        params(7) = New SqlParameter("@DataSource", SqlDbType.VarChar, 255)
        params(7).Value = customClass.DataSource
        params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(8).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            ErrMessage = CType(params(8).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveAdd")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Function

    Public Function CreditDelete(ByVal customClass As Parameter.Credit) As String
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
            Return CType(params(1).Value, String)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeDelete")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function

    Public Sub CreditSaveEdit(ByVal customClass As Parameter.Credit)
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        params(2) = New SqlParameter("@ScoringType", SqlDbType.Char, 1)
        params(2).Value = customClass.ScoringType
        params(3) = New SqlParameter("@GroupComponent", SqlDbType.VarChar, 1)
        params(3).Value = customClass.GroupComponent
        params(4) = New SqlParameter("@SeqComponent", SqlDbType.SmallInt, 2)
        params(4).Value = customClass.SeqComponent
        params(5) = New SqlParameter("@CalculationType", SqlDbType.Char, 1)
        params(5).Value = customClass.CalculationType
        'params(6) = New SqlParameter("@SQLCmd", SqlDbType.VarChar, 255)
        params(6) = New SqlParameter("@SQLCmd", SqlDbType.Text)
        params(6).Value = customClass.SQLCmd
        params(7) = New SqlParameter("@DataSource", SqlDbType.VarChar, 255)
        params(7).Value = customClass.DataSource
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, customClass.SPName, params)
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.AssetTypeSaveEdit")
        Finally
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
        End Try
    End Sub
End Class



