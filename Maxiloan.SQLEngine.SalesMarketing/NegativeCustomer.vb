

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class NegativeCustomer : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const spListNegativeCustomer As String = "spNegativeCustomerPaging"
    Private Const spAddNegativeCustomer As String = "spNegativeCustomerAdd"
    Private Const spViewNegativeCustomer As String = "spNegativeCustomerView"
    Private Const spEditNegativeCustomer As String = "spNegativeCustomerEdit"
    Private Const spRptNegativeCustomer As String = "spRptNegativeCustomer"
    Private Const spListIDType As String = "spListIDType"
    Private Const spListDataSource As String = "spListDataSource"
    Private Const spNegativeCustomerGetApprovalNo As String = "spNegativeCustomerGetApprovalNo"

#Region "ListNegativeCustomer"
    Public Function ListNegativeCustomer(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListNegative = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListNegativeCustomer, params).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)
        Return customclass
    End Function
#End Region

#Region "NegativeCustomerAdd"
    Public Function NegativeCustomerAdd(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim params() As SqlParameter = New SqlParameter(25) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()

        params(0) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 20)
        params(0).Value = customclass.CustomerName

        params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
        If customclass.IDType.Trim = "" Then
            params(1).Value = DBNull.Value
        Else
            params(1).Value = customclass.IDType
        End If

        params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 20)
        params(2).Value = customclass.IDNumber

        params(3) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
        params(3).Value = customclass.BirthPlace

        params(4) = New SqlParameter("@BirthDate", SqlDbType.Char, 8)
        If customclass.BirthDate = "" Then
            params(4).Value = DBNull.Value
        Else
            params(4).Value = customclass.BirthDate
        End If

        params(5) = New SqlParameter("@LegalAddress", SqlDbType.VarChar, 20)
        params(5).Value = customclass.Address

        params(6) = New SqlParameter("@LegalRT", SqlDbType.VarChar, 5)
        params(6).Value = customclass.RT

        params(7) = New SqlParameter("@LegalRW", SqlDbType.VarChar, 5)
        params(7).Value = customclass.RW

        params(8) = New SqlParameter("@LegalKelurahan", SqlDbType.VarChar, 20)
        params(8).Value = customclass.Kelurahan

        params(9) = New SqlParameter("@LegalKecamatan", SqlDbType.VarChar, 20)
        params(9).Value = customclass.Kecamatan

        params(10) = New SqlParameter("@LegalCity", SqlDbType.VarChar, 20)
        params(10).Value = customclass.City

        params(11) = New SqlParameter("@LegalZipCode", SqlDbType.VarChar, 5)
        params(11).Value = customclass.ZipCode

        params(12) = New SqlParameter("@LegalAreaPhone1", SqlDbType.VarChar, 5)
        params(12).Value = customclass.LegalAreaPhone1

        params(13) = New SqlParameter("@LegalPhone1", SqlDbType.VarChar, 10)
        params(13).Value = customclass.LegalPhone1

        params(14) = New SqlParameter("@LegalAreaPhone2", SqlDbType.VarChar, 5)
        params(14).Value = customclass.LegalAreaPhone2

        params(15) = New SqlParameter("@LegalPhone2", SqlDbType.VarChar, 10)
        params(15).Value = customclass.LegalPhone2

        params(16) = New SqlParameter("@LegalAreaFax", SqlDbType.VarChar, 5)
        params(16).Value = customclass.LegalAreaFax

        params(17) = New SqlParameter("@LegalfaxNo", SqlDbType.VarChar, 10)
        params(17).Value = customclass.LegalFax

        params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
        params(18).Value = customclass.MobilePhone

        params(19) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(19).Value = customclass.Email

        params(20) = New SqlParameter("@BadType", SqlDbType.Char, 1)
        params(20).Value = customclass.BadType

        params(21) = New SqlParameter("@DataSource", SqlDbType.Char, 10)
        params(21).Value = customclass.DataSource

        params(22) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
        params(22).Value = customclass.Notes

        params(23) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(23).Value = customclass.BranchId

        params(24) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(24).Value = customclass.BusinessDate

        params(25) = New SqlParameter("@hasil", SqlDbType.Int)
        params(25).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(oConnection, CommandType.StoredProcedure, spAddNegativeCustomer, params)
        customclass.Hasil = CInt(params(25).Value)
        Return customclass
    End Function
#End Region

#Region "NegativeCustomerView"
    Public Function NegativeCustomerView(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()

        params(0) = New SqlParameter("@NegativeCustomerID", SqlDbType.VarChar, 20)
        params(0).Value = customclass.NegativeCustomerID

        params(1) = New SqlParameter("@strKey", SqlDbType.VarChar, 5)
        params(1).Value = customclass.strKey

        customclass.ListNegative = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spViewNegativeCustomer, params).Tables(0)
        Return customclass
    End Function
#End Region

#Region "ListIDType"
    Public Function ListIDType(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        'Dim params() As SqlParameter = New SqlParameter(23) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        customclass.ListNegative = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListIDType).Tables(0)
        Return customclass
    End Function
#End Region

#Region "ListDataSource"
    Public Function ListDataSource(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        'Dim params() As SqlParameter = New SqlParameter(23) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        customclass.ListNegative = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListDataSource).Tables(0)
        Return customclass
    End Function
#End Region

#Region "NegativeCustomerEdit"
    Public Function NegativeCustomerEdit(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim params() As SqlParameter = New SqlParameter(25) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()

        params(0) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 20)
        params(0).Value = customclass.CustomerName

        params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
        If customclass.IDType.Trim = "" Then
            params(1).Value = DBNull.Value
        Else
            params(1).Value = customclass.IDType
        End If

        params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 20)
        params(2).Value = customclass.IDNumber

        params(3) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
        params(3).Value = customclass.BirthPlace

        params(4) = New SqlParameter("@BirthDate", SqlDbType.Char, 8)
        If customclass.BirthDate = "" Then
            params(4).Value = DBNull.Value
        Else
            params(4).Value = customclass.BirthDate
        End If

        params(5) = New SqlParameter("@LegalAddress", SqlDbType.VarChar, 20)
        params(5).Value = customclass.Address

        params(6) = New SqlParameter("@LegalRT", SqlDbType.VarChar, 5)
        params(6).Value = customclass.RT

        params(7) = New SqlParameter("@LegalRW", SqlDbType.VarChar, 5)
        params(7).Value = customclass.RW

        params(8) = New SqlParameter("@LegalKelurahan", SqlDbType.VarChar, 20)
        params(8).Value = customclass.Kelurahan

        params(9) = New SqlParameter("@LegalKecamatan", SqlDbType.VarChar, 20)
        params(9).Value = customclass.Kecamatan

        params(10) = New SqlParameter("@LegalCity", SqlDbType.VarChar, 20)
        params(10).Value = customclass.City

        params(11) = New SqlParameter("@LegalZipCode", SqlDbType.VarChar, 5)
        params(11).Value = customclass.ZipCode

        params(12) = New SqlParameter("@LegalAreaPhone1", SqlDbType.VarChar, 5)
        params(12).Value = customclass.LegalAreaPhone1

        params(13) = New SqlParameter("@LegalPhone1", SqlDbType.VarChar, 10)
        params(13).Value = customclass.LegalPhone1

        params(14) = New SqlParameter("@LegalAreaPhone2", SqlDbType.VarChar, 5)
        params(14).Value = customclass.LegalAreaPhone2

        params(15) = New SqlParameter("@LegalPhone2", SqlDbType.VarChar, 10)
        params(15).Value = customclass.LegalPhone2

        params(16) = New SqlParameter("@LegalAreaFax", SqlDbType.VarChar, 5)
        params(16).Value = customclass.LegalAreaFax

        params(17) = New SqlParameter("@LegalfaxNo", SqlDbType.VarChar, 10)
        params(17).Value = customclass.LegalFax

        params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
        params(18).Value = customclass.MobilePhone

        params(19) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(19).Value = customclass.Email

        params(20) = New SqlParameter("@BadType", SqlDbType.Char, 1)
        params(20).Value = customclass.BadType

        params(21) = New SqlParameter("@DataSource", SqlDbType.Char, 10)
        params(21).Value = customclass.DataSource

        params(22) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
        params(22).Value = customclass.Notes

        params(23) = New SqlParameter("@NegativeCustomerID", SqlDbType.VarChar, 20)
        params(23).Value = customclass.NegativeCustomerID

        params(24) = New SqlParameter("@IsActive", SqlDbType.Bit)
        params(24).Value = customclass.IsActive

        params(25) = New SqlParameter("@hasil", SqlDbType.Int)
        params(25).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(oConnection, CommandType.StoredProcedure, spEditNegativeCustomer, params)
        customclass.Hasil = CInt(params(25).Value)
        Return customclass
    End Function
#End Region

#Region "NegativeCustomerRpt"
    Public Function NegativeCustomerRpt(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        'Dim params() As SqlParameter = New SqlParameter(23) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        customclass.ListReport = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spRptNegativeCustomer)
        Return customclass
    End Function
#End Region

    Public Function GetApprovalNumberForHistory(ByVal data As Parameter.NegativeCustomer) As String
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim oConnection As New SqlConnection(data.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        params(0) = New SqlParameter("@NegativeCustomerId", SqlDbType.VarChar, 20)
        params(0).Value = data.NegativeCustomerID

        params(1) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
        params(1).Direction = ParameterDirection.Output

        SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spNegativeCustomerGetApprovalNo, params)
        Return params(1).Value.ToString

    End Function


    Public Function DoUploadNegativeCsv(cnn As String, BusinessDate As DateTime, BranchId As String, param As DataTable) As String

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
         
        Try
            params.Add(New SqlParameter("@NegativeCust", SqlDbType.Structured) With {.Value = param})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = BranchId})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 2048) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spNegativeCustomerCsv", params.ToArray)
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If

        Catch exp As Exception
            WriteException("Invoice", "InvoiceSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
        Return Nothing
    End Function
End Class
