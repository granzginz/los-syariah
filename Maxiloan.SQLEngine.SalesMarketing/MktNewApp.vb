
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MktNewApp : Inherits DataAccessBase
#Region " Private Const "
    Private Const MKT_NEW_APP_PAGING As String = "spMktNewApplicationPaging"
    Private Const MKT_NEW_APP_VIEW As String = "spMktNewApplicationView"
    Private Const MKT_NEW_APP_GET_CUSTOMER_ID As String = "spMktNewApplicationGetIDType"
    Private Const MKT_NEW_APP_GET_HOME_STATUS As String = "SELECT ID,Description from tblhomestatus ORDER BY Description"
    Private Const MKT_NEW_APP_GET_PROFESSION As String = "select id,description from tblprofession ORDER BY Description"
    Private Const MKT_NEW_APP_GET_ASSET_DESCRIPTION As String = "select AssetCode,Description from assetmaster where IsFinalLevel = 1 order by Description"
    Private Const MKT_NEW_APP_GET_ASSET_INSURANCE_C0 As String = "spGetInsuranceComBranchMarketing"
    Private Const MKT_NEW_APP_GET_HOME_INDUSTRY_TYPE As String = "select IndustryTypeId as Id, Description from industrytype ORDER BY Description"
    Private Const MKT_NEW_APP_GET_COMPANY_STATUS As String = "select Id,Description from tblcompanystatus ORDER BY Description"

    Private Const spBindCustomer1_002 As String = "spCustomer002Bind1"
    Private Const spBindCustomer2_002 As String = "spCustomer002Bind2"
    Private Const MKT_NEW_APP_SAVING_PERSONAL As String = "spMktNewApplicationSavePersonal"
    Private Const MKT_NEW_APP_SAVING_COMPANY As String = "spMktNewApplicationSaveCompany"
    Private Const MKT_NEW_APP_EDIT As String = "spMktNewApplicationEdit"
    Private Const spBindCustomerC1_002 As String = "spCustomer002BindCompany1"
    Private Const spBindCustomerC2_002 As String = "spCustomer002BindCompany2"
    Private Const MKT_NEW_APP_EDIT_COMPANY As String = "spMktNewApplicationEditCompany"
    Private m_connection As SqlConnection
#End Region
#Region " MktNewAppPaging"
    Public Function MktNewAppPaging(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, MKT_NEW_APP_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("MktNewApp", "MktNewAppPaging", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region
#Region " MktNewAppView"
    Public Function MktNewAppView(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter

        Try
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ProspectAppId

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, MKT_NEW_APP_VIEW, params)
            With oCustomClass
                If objread.Read Then
                    .ApplicationId = CType(objread("ApplicationId"), String)


                    '==========COMPANY DATA======
                    .CompanyName = CType(objread("Name"), String)
                    If IsDBNull(objread("IndustryTypeId")) Then
                        .IndustryType = "P"
                    Else
                        .IndustryType = CStr(objread("IndustryTypeId"))
                    End If



                    If IsDBNull(objread("CompanyStatus")) Then
                        .BusinessPlaceStatus = "KR"
                    Else
                        .BusinessPlaceStatus = CStr(objread("CompanyStatus"))
                    End If
                    .YearInBusiness = CType(objread("EstablishedSinceYear"), String)

                    '======PERSONAL DATA====
                    .Name = CType(objread("Name"), String)
                    If IsDBNull(objread("IDType")) Then
                        .IdType = "KTP"
                    Else
                        .IdType = CStr(objread("IDType"))
                    End If

                    .IdNumber = CType(objread("IDNumber"), String)
                    .BirthPlace = CType(objread("BirthPlace"), String)
                    If IsDBNull(objread("BirthDate")) Then
                        .BirthDate = CDate("01/01/1900")
                    Else
                        .BirthDate = CDate(objread("BirthDate"))
                    End If

                    .HomeStatus = CType(objread("HomeStatus"), String)

                    If IsDBNull(objread("Gender")) Then
                        .Gender = "M"
                    Else
                        .Gender = CType(objread("Gender"), String)
                    End If
                    '=========ADDRESS=====
                    .Address = CType(objread("Address"), String)
                    .RT = CType(objread("RT"), String)
                    .RW = CType(objread("RW"), String)
                    .Kelurahan = CType(objread("Kelurahan"), String)
                    .Kecamatan = CType(objread("Kecamatan"), String)
                    .City = CType(objread("City"), String)
                    .ZIPCode = CType(objread("ZipCode"), String)
                    .AreaPhone1 = CType(objread("AreaPhone1"), String)
                    .AreaPhone2 = CType(objread("AreaPhone2"), String)
                    .Phone1 = CType(objread("Phone1"), String)
                    .Phone2 = CType(objread("Phone2"), String)
                    .AreaFak = CType(objread("AreaFax"), String)
                    .FakNo = CType(objread("Fax"), String)
                    .mobilephone = CType(objread("MobilePhone"), String)
                    .Email = CType(objread("Email"), String)
                    '====WORK DATA =========
                    .Profession = CType(objread("ProfessionID"), String)
                    .LengthOfEmployment = CType(objread("EmploymentSinceYear"), Integer)
                    '======AGREEMENT DATA ========
                    oCustomClass.Product = CType(objread("ProductId"), String)
                    oCustomClass.ProductOfferingId = CType(objread("ProductOfferingID"), String)

                    .AssetDescription = CType(objread("AssetCode"), String)
                    .OTRPrice = CType(objread("OTRPrice"), Double)
                    .DownPayment = CType(objread("DPAmount"), Double)
                    .Tenor = CType(objread("Tenor"), Double)
                    .FlateRate = CType(objread("FlatRate"), Double)
                    .FirstIntallment = CType(objread("FirstInstallment"), String)
                    .InstallAmount = CType(objread("InstallmentAmount"), Double)
                    If IsDBNull(objread("DueDate")) Then
                        .Estimateduedate = CDate("01/01/1900")
                    Else
                        .Estimateduedate = CDate(objread("DueDate"))
                    End If
                    If IsDBNull(objread("MaskAssID")) Then
                        .InsuranceCo = "000"
                    Else
                        .InsuranceCo = CType(objread("MaskAssID"), String)
                    End If
                    .ExistingPolicyNo = CType(objread("ExistingPolicyNo"), String)
                    .AssetTypeID = CType(objread("AssetTypeID"), String)
                End If

            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("MktNewApp", "MktNewAppView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function MktNewAppViewCompany(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter

        Try
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ProspectAppId

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, MKT_NEW_APP_VIEW, params)
            With oCustomClass
                If objread.Read Then
                    .ApplicationId = CType(objread("ApplicationId"), String)


                    '==========COMPANY DATA======
                    .Name = CType(objread("Name"), String)
                    If IsDBNull(objread("IndustryTypeId")) Then
                        .IndustryType = "C"
                    Else
                        .IndustryType = CStr(objread("IndustryTypeId"))
                    End If

                    If IsDBNull(objread("CompanyStatus")) Then
                        .BusinessPlaceStatus = "KR"
                    Else
                        .BusinessPlaceStatus = CStr(objread("CompanyStatus"))
                    End If
                    .YearInBusiness = CType(objread("EstablishedSinceYear"), String)




                    '=========ADDRESS=====
                    .Address = CType(objread("Address"), String)
                    .RT = CType(objread("RT"), String)
                    .RW = CType(objread("RW"), String)
                    .Kelurahan = CType(objread("Kelurahan"), String)
                    .Kecamatan = CType(objread("Kecamatan"), String)
                    .City = CType(objread("City"), String)
                    .ZIPCode = CType(objread("ZipCode"), String)
                    .AreaPhone1 = CType(objread("AreaPhone1"), String)
                    .AreaPhone2 = CType(objread("AreaPhone2"), String)
                    .Phone1 = CType(objread("Phone1"), String)
                    .Phone2 = CType(objread("Phone2"), String)
                    .AreaFak = CType(objread("AreaFax"), String)
                    .FakNo = CType(objread("Fax"), String)
                    .mobilephone = CType(objread("MobilePhone"), String)
                    .Email = CType(objread("Email"), String)

                    '======AGREEMENT DATA ========
                    .Product = CType(objread("ProductID"), String)
                    .ProductOfferingId = CType(objread("ProductOfferingID"), String)
                    .AssetDescription = CType(objread("AssetCode"), String)
                    .OTRPrice = CType(objread("OTRPrice"), Double)
                    .DownPayment = CType(objread("DPAmount"), Double)
                    .Tenor = CType(objread("Tenor"), Double)
                    .FlateRate = CType(objread("FlatRate"), Double)
                    .FirstIntallment = CType(objread("FirstInstallment"), String)
                    .InstallAmount = CType(objread("InstallmentAmount"), Double)
                    If IsDBNull(objread("DueDate")) Then
                        .Estimateduedate = CDate("01/01/1900")
                    Else
                        .Estimateduedate = CDate(objread("DueDate"))
                    End If
                    If IsDBNull(objread("MaskAssID")) Then
                        .InsuranceCo = "000"
                    Else
                        .InsuranceCo = CType(objread("MaskAssID"), String)
                    End If
                    .ExistingPolicyNo = CType(objread("ExistingPolicyNo"), String)
                    .AssetTypeID = CType(objread("AssetTypeID"), String)
                End If

            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("MktNewApp", "MktNewAppViewCompany", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region " Get Personal Combo"
    Public Function GetPersonalIdType(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, MKT_NEW_APP_GET_CUSTOMER_ID).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("MktNewApp", "GetPersonalIdType", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.CustomerPersonalGetIDType")

        End Try
    End Function
    Public Function GetPersonalHomeStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Try
            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.Text, MKT_NEW_APP_GET_HOME_STATUS).Tables(0)
            Return ocustomclass

        Catch exp As Exception
            WriteException("MktNewApp", "GetPersonalHomeStatus", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetPersonalHomeStatus")
        End Try
    End Function
    Public Function GetProfession(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Try

            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.Text, MKT_NEW_APP_GET_PROFESSION).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("MktNewApp", "GetProfession", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetProfession")
        End Try
    End Function
    Public Function GetAssetDescription(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Try
            Dim AssetTypeID As String
            Dim MKT_NEW_APP_GET_ASSET_DESCRIPTION_With_WhereCond As String
            AssetTypeID = ocustomclass.AssetDescription
            MKT_NEW_APP_GET_ASSET_DESCRIPTION_With_WhereCond = "select AssetCode,Description from assetmaster where IsFinalLevel = 1 and AssetTypeID = '" & AssetTypeID.Trim & "' order by Description"
            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.Text, MKT_NEW_APP_GET_ASSET_DESCRIPTION_With_WhereCond).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("MktNewApp", "GetAssetDescription", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetAssetDescription")
        End Try
    End Function
    Public Function GetInsuranceCo(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@branchid", SqlDbType.Char, 3000)
            params(0).Value = ocustomclass.BranchId
            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, MKT_NEW_APP_GET_ASSET_INSURANCE_C0, params).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("MktNewApp", "GetInsuranceCo", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetInsuranceCo")
        End Try
    End Function
    Public Function GetIndustryType(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp

        Try
            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.Text, MKT_NEW_APP_GET_HOME_INDUSTRY_TYPE).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("MktNewApp", "GetIndustryType", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetInsuranceCo")
        End Try
    End Function
    Public Function GetCompanyStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp

        Try
            ocustomclass.ListData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.Text, MKT_NEW_APP_GET_COMPANY_STATUS).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("MktNewApp", "GetCompanyStatus", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Marketing.MktNewApp.GetInsuranceCo")
        End Try
    End Function


#End Region
#Region " Bind Personal"
    Public Function BindCustomer1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim oReturnValue As New Parameter.MktNewApp
        Dim params(3) As SqlParameter
        Dim oArr As Array = CType(oCustomer.Name.Split(CChar(" ")), Array)
        Try
            params(0) = New SqlParameter("@Birthdate", SqlDbType.VarChar, 10)
            params(0).Value = oCustomer.BirthDate.ToString("MM/dd/yyyy")
            params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(1).Value = oCustomer.IdType
            params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(2).Value = oCustomer.IdNumber
            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomer1_002, params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("MktNewApp", "BindCustomer1_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomer1_002")
        End Try
    End Function
    Public Function BindCustomer2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@Birthdate", SqlDbType.VarChar, 10)
            params(0).Value = oCustomer.BirthDate.ToString("MM/dd/yyyy")
            params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(1).Value = oCustomer.IdType
            params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(2).Value = oCustomer.IdNumber
            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = Replace(oCustomer.Name, "'", "''")
            oCustomer.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomer2_002, params).Tables(0)
            Return oCustomer
        Catch exp As Exception
            WriteException("Customer", "BindCustomer2_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomer2_002")
        End Try
    End Function

#End Region
#Region " Bind Company"
    Public Function BindCustomerC1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim oReturnValue As New Parameter.MktNewApp
        Dim params(0) As SqlParameter
        Try

            params(0) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(0).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomerC1_002, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomerC1_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomerC1_002")
        End Try
    End Function
    Public Function BindCustomerC2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim oReturnValue As New Parameter.MktNewApp
        Dim params(0) As SqlParameter
        Try

            params(0) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(0).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomerC2_002, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomerC2_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomerC2_002")
        End Try
    End Function
#End Region

#Region " MktNewAppSave"
    Public Sub MktNewAppSave(ByVal oCustomClass As Parameter.MktNewApp)
        Dim objtrans As SqlTransaction = Nothing
        Dim params(40) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(1).Value = oCustomClass.CustomerType
            params(2) = New SqlParameter("@ProspectAppDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.ProspectAppDate

            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = oCustomClass.Name
            params(4) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.IdType
            params(5) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.IdNumber
            params(6) = New SqlParameter("@Gender", SqlDbType.Char, 1)
            params(6).Value = oCustomClass.Gender
            params(7) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
            params(7).Value = oCustomClass.BirthPlace
            params(8) = New SqlParameter("@BirthDate", SqlDbType.DateTime)
            params(8).Value = oCustomClass.BirthDate
            params(9) = New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10)
            params(9).Value = oCustomClass.HomeStatus

            params(10) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(10).Value = oCustomClass.Address
            params(11) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(11).Value = oCustomClass.RT
            params(12) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(12).Value = oCustomClass.RW
            params(13) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(13).Value = oCustomClass.Kelurahan
            params(14) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(14).Value = oCustomClass.Kecamatan
            params(15) = New SqlParameter("@City", SqlDbType.Char, 30)
            params(15).Value = oCustomClass.City
            params(16) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(16).Value = oCustomClass.ZIPCode
            params(17) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(17).Value = oCustomClass.AreaPhone1
            params(18) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            params(18).Value = oCustomClass.Phone1
            params(19) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(19).Value = oCustomClass.AreaPhone2
            params(20) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            params(20).Value = oCustomClass.Phone2
            params(21) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(21).Value = oCustomClass.AreaFak
            params(22) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            params(22).Value = oCustomClass.FakNo
            params(23) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(23).Value = oCustomClass.mobilephone
            params(24) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(24).Value = oCustomClass.Email

            params(25) = New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10)
            params(25).Value = oCustomClass.Profession
            params(26) = New SqlParameter("@EmploymentSinceYear", SqlDbType.Int)
            params(26).Value = oCustomClass.LengthOfEmployment

            params(27) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(27).Value = oCustomClass.Product
            params(28) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(28).Value = oCustomClass.ProductOfferingId
            params(29) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.AssetDescription
            params(30) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(30).Value = oCustomClass.OTRPrice
            params(31) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(31).Value = oCustomClass.DownPayment
            params(32) = New SqlParameter("@Tenor", SqlDbType.Int)
            params(32).Value = oCustomClass.Tenor
            params(33) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(33).Value = oCustomClass.InstallAmount
            params(34) = New SqlParameter("@flatRate", SqlDbType.Decimal)
            params(34).Value = oCustomClass.FlateRate
            params(35) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(35).Value = oCustomClass.FirstIntallment
            params(36) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(36).Value = oCustomClass.Estimateduedate
            params(37) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(37).Value = oCustomClass.InsuranceCo
            params(38) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
            params(38).Value = oCustomClass.InsuranceCoBranch
            params(39) = New SqlParameter("@ExistingPolicyNo", SqlDbType.VarChar, 50)
            params(39).Value = oCustomClass.ExistingPolicyNo

            params(40) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(40).Direction = ParameterDirection.Output



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, MKT_NEW_APP_SAVING_PERSONAL, params)
            oCustomClass.ProspectAppId = CType(params(40).Value, String)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("MktNewApp", "MktNewAppSave", exp.Message + exp.StackTrace)

        End Try
    End Sub


#End Region
#Region " MktNewAppSaveCompany"
    Public Sub MktNewAppSaveCompany(ByVal oCustomClass As Parameter.MktNewApp)
        Dim objtrans As SqlTransaction = Nothing
        Dim params(35) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(1).Value = oCustomClass.CustomerType
            params(2) = New SqlParameter("@ProspectAppDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.ProspectAppDate

            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = oCustomClass.Name
            params(4) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(4).Value = oCustomClass.IndustryType
            params(5) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(5).Value = oCustomClass.BusinessPlaceStatus
            params(6) = New SqlParameter("@EstablishedSinceYear", SqlDbType.Int)
            params(6).Value = oCustomClass.YearInBusiness

            params(7) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(7).Value = oCustomClass.Address
            params(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(8).Value = oCustomClass.RT
            params(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(9).Value = oCustomClass.RW
            params(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(10).Value = oCustomClass.Kelurahan
            params(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(11).Value = oCustomClass.Kecamatan
            params(12) = New SqlParameter("@City", SqlDbType.Char, 30)
            params(12).Value = oCustomClass.City
            params(13) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(13).Value = oCustomClass.ZIPCode
            params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(14).Value = oCustomClass.AreaPhone1
            params(15) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            params(15).Value = oCustomClass.Phone1
            params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(16).Value = oCustomClass.AreaPhone2
            params(17) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            params(17).Value = oCustomClass.Phone2
            params(18) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(18).Value = oCustomClass.AreaFak
            params(19) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            params(19).Value = oCustomClass.FakNo
            params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(20).Value = oCustomClass.mobilephone
            params(21) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(21).Value = oCustomClass.Email


            params(22) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(22).Value = oCustomClass.Product
            params(23) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(23).Value = oCustomClass.ProductOfferingId
            params(24) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(24).Value = oCustomClass.AssetDescription
            params(25) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(25).Value = oCustomClass.OTRPrice
            params(26) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(26).Value = oCustomClass.DownPayment
            params(27) = New SqlParameter("@Tenor", SqlDbType.Int)
            params(27).Value = oCustomClass.Tenor
            params(28) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(28).Value = oCustomClass.InstallAmount
            params(29) = New SqlParameter("@flatRate", SqlDbType.Decimal)
            params(29).Value = oCustomClass.FlateRate
            params(30) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(30).Value = oCustomClass.FirstIntallment
            params(31) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(31).Value = oCustomClass.Estimateduedate
            params(32) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(32).Value = oCustomClass.InsuranceCo
            params(33) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
            params(33).Value = oCustomClass.InsuranceCoBranch
            params(34) = New SqlParameter("@ExistingPolicyNo", SqlDbType.VarChar, 50)
            params(34).Value = oCustomClass.ExistingPolicyNo
            params(35) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(35).Direction = ParameterDirection.Output





            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, MKT_NEW_APP_SAVING_COMPANY, params)
            oCustomClass.ProspectAppId = CType(params(35).Value, String)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            'WriteException("MktNewApp", "MktNewAppSave", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("MktNewApp", "MktNewAppSaveCompany", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)

            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        End Try
    End Sub
#End Region
#Region " MktNewAppEdit"
    Public Sub MktNewAppEdit(ByVal oCustomClass As Parameter.MktNewApp)
        Dim objtrans As SqlTransaction = Nothing
        Dim params(40) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(1).Value = oCustomClass.CustomerType
            params(2) = New SqlParameter("@ProspectAppDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.ProspectAppDate
            params(3) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(3).Value = oCustomClass.ProspectAppId

            params(4) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(4).Value = oCustomClass.Name
            params(5) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(5).Value = oCustomClass.IdType
            params(6) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 20)
            params(6).Value = oCustomClass.IdNumber
            params(7) = New SqlParameter("@Gender", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.Gender
            params(8) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
            params(8).Value = oCustomClass.BirthPlace
            params(9) = New SqlParameter("@BirthDate", SqlDbType.DateTime)
            params(9).Value = oCustomClass.BirthDate
            params(10) = New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10)
            params(10).Value = oCustomClass.HomeStatus

            params(11) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(11).Value = oCustomClass.Address
            params(12) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(12).Value = oCustomClass.RT
            params(13) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(13).Value = oCustomClass.RW
            params(14) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(14).Value = oCustomClass.Kelurahan
            params(15) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(15).Value = oCustomClass.Kecamatan
            params(16) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(16).Value = oCustomClass.City
            params(17) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(17).Value = oCustomClass.ZIPCode
            params(18) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(18).Value = oCustomClass.AreaPhone1
            params(19) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            params(19).Value = oCustomClass.Phone1
            params(20) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 4)
            params(20).Value = oCustomClass.AreaPhone2
            params(21) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            params(21).Value = oCustomClass.Phone2
            params(22) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(22).Value = oCustomClass.AreaFak
            params(23) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            params(23).Value = oCustomClass.FakNo
            params(24) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(24).Value = oCustomClass.mobilephone
            params(25) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(25).Value = oCustomClass.Email

            params(26) = New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10)
            params(26).Value = oCustomClass.Profession
            params(27) = New SqlParameter("@EmploymentSinceYear", SqlDbType.Int)
            params(27).Value = oCustomClass.LengthOfEmployment

            params(28) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(28).Value = oCustomClass.Product
            params(29) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(29).Value = oCustomClass.ProductOfferingId



            params(30) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(30).Value = oCustomClass.AssetDescription
            params(31) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(31).Value = oCustomClass.OTRPrice
            params(32) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(32).Value = oCustomClass.DownPayment
            params(33) = New SqlParameter("@Tenor", SqlDbType.Int)
            params(33).Value = oCustomClass.Tenor
            params(34) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(34).Value = oCustomClass.InstallAmount
            params(35) = New SqlParameter("@flatRate", SqlDbType.Decimal)
            params(35).Value = oCustomClass.FlateRate
            params(36) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(36).Value = oCustomClass.FirstIntallment
            params(37) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(37).Value = oCustomClass.Estimateduedate
            params(38) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(38).Value = oCustomClass.InsuranceCo
            params(39) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
            params(39).Value = oCustomClass.InsuranceCoBranch
            params(40) = New SqlParameter("@ExistingPolicyNo", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.ExistingPolicyNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, MKT_NEW_APP_EDIT, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("MktNewApp", "MktNewAppEdit", exp.Message + exp.StackTrace)

        End Try
    End Sub


#End Region
#Region " MKTNewAPpEditCompany"
    Public Sub MktNewAppEditCompany(ByVal oCustomClass As Parameter.MktNewApp)
        Dim objtrans As SqlTransaction = Nothing
        Dim params(35) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(1).Value = oCustomClass.CustomerType
            params(2) = New SqlParameter("@ProspectAppDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.ProspectAppDate
            params(3) = New SqlParameter("@ProspectAppId", SqlDbType.Char, 20)
            params(3).Value = oCustomClass.ProspectAppId

            params(4) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(4).Value = oCustomClass.Name
            params(5) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(5).Value = oCustomClass.IndustryType
            params(6) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(6).Value = oCustomClass.BusinessPlaceStatus
            params(7) = New SqlParameter("@EstablishedSinceYear", SqlDbType.Int)
            params(7).Value = oCustomClass.YearInBusiness

            params(8) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(8).Value = oCustomClass.Address
            params(9) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(9).Value = oCustomClass.RT
            params(10) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(10).Value = oCustomClass.RW
            params(11) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(11).Value = oCustomClass.Kelurahan
            params(12) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(12).Value = oCustomClass.Kecamatan
            params(13) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(13).Value = oCustomClass.City
            params(14) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(14).Value = oCustomClass.ZIPCode
            params(15) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(15).Value = oCustomClass.AreaPhone1
            params(16) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            params(16).Value = oCustomClass.Phone1
            params(17) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 4)
            params(17).Value = oCustomClass.AreaPhone2
            params(18) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            params(18).Value = oCustomClass.Phone2
            params(19) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(19).Value = oCustomClass.AreaFak
            params(20) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            params(20).Value = oCustomClass.FakNo
            params(21) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(21).Value = oCustomClass.mobilephone
            params(22) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(22).Value = oCustomClass.Email



            params(23) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(23).Value = oCustomClass.Product
            params(24) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(24).Value = oCustomClass.ProductOfferingId
            params(25) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.AssetDescription
            params(26) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(26).Value = oCustomClass.OTRPrice
            params(27) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(27).Value = oCustomClass.DownPayment
            params(28) = New SqlParameter("@Tenor", SqlDbType.Int)
            params(28).Value = oCustomClass.Tenor
            params(29) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.InstallAmount
            params(30) = New SqlParameter("@flatRate", SqlDbType.Decimal)
            params(30).Value = oCustomClass.FlateRate
            params(31) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(31).Value = oCustomClass.FirstIntallment
            params(32) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(32).Value = oCustomClass.Estimateduedate
            params(33) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(33).Value = oCustomClass.InsuranceCo
            params(34) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
            params(34).Value = oCustomClass.InsuranceCoBranch
            params(35) = New SqlParameter("@ExistingPolicyNo", SqlDbType.VarChar, 50)
            params(35).Value = oCustomClass.ExistingPolicyNo



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, MKT_NEW_APP_EDIT_COMPANY, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("MktNewApp", "MktNewAppEdit", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region

End Class
