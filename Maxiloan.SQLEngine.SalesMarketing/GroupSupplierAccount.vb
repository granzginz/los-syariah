﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class GroupSupplierAccount : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spPagingGroupSupplierAccount"
    Private Const LIST_BY_ID As String = "spGroupSupplierAccountList"    
    Private Const LIST_ADD As String = "spGroupSupplierAccountSaveAdd"
    Private Const LIST_UPDATE As String = "spGroupSupplierAccountSaveEdit"
    Private Const LIST_DELETE As String = "spGroupSupplierAccountDelete"
    Private Const LIST_REPORT As String = "spGroupSupplierAccountReport"
#End Region

    Public Function GetGroupSupplierAccount(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim oReturnValue As New Parameter.GroupSupplierAccount
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GroupSupplierAccount.GetGroupSupplierAccount")
        End Try
    End Function

    Public Function GetGroupSupplierAccountReport(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim oReturnValue As New Parameter.GroupSupplierAccount
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GroupSupplierAccount.GetGroupSupplierAccountReport")
        End Try
    End Function

    Public Function GetGroupSupplierAccountList(ByVal ocustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.GroupSupplierAccount
        params(0) = New SqlParameter("@Id", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.ID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.ID = ocustomClass.ID
                oReturnValue.SupplierGroupID = reader("SupplierGroupID").ToString
                oReturnValue.GroupSupplierAccountBankID = reader("GroupSupplierAccountBankID").ToString
                oReturnValue.GroupSupplierAccountBankName = reader("GroupSupplierAccountBankName").ToString                
                oReturnValue.SandiCabang = reader("SandiCabang").ToString
                oReturnValue.SandiCabang = reader("SandiBank").ToString
                oReturnValue.GroupSupplierAccountBankBranchID = Convert.ToInt32(reader("GroupSupplierAccountBankBranchID").ToString)
                oReturnValue.GroupSupplierAccountBankBranch = reader("GroupSupplierAccountBankBranch").ToString
                oReturnValue.GroupSupplierAccountNo = reader("GroupSupplierAccountNo").ToString
                oReturnValue.GroupSupplierAccountName = reader("GroupSupplierAccountName").ToString
                oReturnValue.DefaultAccount = CBool(reader("DefaultAccount").ToString)
                oReturnValue.ForPayment = reader("ForPayment").ToString
                oReturnValue.EffectiveDate = CDate(reader("EffectiveDate").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GroupSupplierAccount.GetGroupSupplierAccountList")
        End Try
    End Function

    Public Function GroupSupplierAccountSaveAdd(ByVal ocustomClass As Parameter.GroupSupplierAccount) As String
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter



        params(0) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SupplierGroupID
        params(1) = New SqlParameter("@GroupSupplierAccountBankID", SqlDbType.Char, 5)
        params(1).Value = ocustomClass.GroupSupplierAccountBankID
        params(2) = New SqlParameter("@GroupSupplierAccountBankBranchID", SqlDbType.Int, 4)
        params(2).Value = ocustomClass.GroupSupplierAccountBankBranchID
        params(3) = New SqlParameter("@GroupSupplierAccountBankBranch", SqlDbType.VarChar, 30)
        params(3).Value = ocustomClass.GroupSupplierAccountBankBranch
        params(4) = New SqlParameter("@GroupSupplierAccountNo", SqlDbType.VarChar, 25)
        params(4).Value = ocustomClass.GroupSupplierAccountNo
        params(5) = New SqlParameter("@GroupSupplierAccountName", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.GroupSupplierAccountName
        params(6) = New SqlParameter("@DefaultAccount", SqlDbType.Bit)
        params(6).Value = ocustomClass.DefaultAccount
        params(7) = New SqlParameter("@ForPayment", SqlDbType.VarChar, 50)
        params(7).Value = ocustomClass.ForPayment
        params(8) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
        params(8).Value = ocustomClass.EffectiveDate
        params(9) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(9).Value = ocustomClass.LoginId
        params(10) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(10).Value = ocustomClass.BusinessDate
        params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(11).Direction = ParameterDirection.Output


        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(11).Value, String)

            Return ErrMessage
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub GroupSupplierAccountSaveEdit(ByVal ocustomClass As Parameter.GroupSupplierAccount)
        Dim params(11) As SqlParameter

        params(0) = New SqlParameter("@ID", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.ID
        params(1) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.SupplierGroupID
        params(2) = New SqlParameter("@GroupSupplierAccountBankID", SqlDbType.Char, 5)
        params(2).Value = ocustomClass.GroupSupplierAccountBankID
        params(3) = New SqlParameter("@GroupSupplierAccountBankBranchID", SqlDbType.Int, 4)
        params(3).Value = ocustomClass.GroupSupplierAccountBankBranchID
        params(4) = New SqlParameter("@GroupSupplierAccountBankBranch", SqlDbType.VarChar, 30)
        params(4).Value = ocustomClass.GroupSupplierAccountBankBranch
        params(5) = New SqlParameter("@GroupSupplierAccountNo", SqlDbType.VarChar, 25)
        params(5).Value = ocustomClass.GroupSupplierAccountNo
        params(6) = New SqlParameter("@GroupSupplierAccountName", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.GroupSupplierAccountName
        params(7) = New SqlParameter("@DefaultAccount", SqlDbType.Bit)
        params(7).Value = ocustomClass.DefaultAccount
        params(8) = New SqlParameter("@ForPayment", SqlDbType.VarChar, 50)
        params(8).Value = ocustomClass.ForPayment
        params(9) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
        params(9).Value = ocustomClass.EffectiveDate
        params(10) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(10).Value = ocustomClass.LoginId
        params(11) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(11).Value = ocustomClass.BusinessDate        



        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function GroupSupplierAccountDelete(ByVal ocustomClass As Parameter.GroupSupplierAccount) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.GroupSupplierAccount.GroupSupplierAccountDelete")
        End Try
    End Function

End Class
