
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class Supplier : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const Supplier"
    'Stored Procedure name    
    Private Const spSupplierPaging As String = "spSupplierPaging"
    Private Const spSupplierEdit As String = "spSupplierEdit"
    Private Const spSupplierSaveAdd As String = "spSupplierSaveAdd"
    Private Const spSupplierSaveEdit As String = "spSupplierSaveEdit"
    Private Const spSupplierSaveAddPrivate As String = "spSupplierSaveAddPrivate"
    Private Const spSupplierDelete As String = "spSupplierDelete"
    Private Const spSupplierBranch As String = "spSupplierBranch"
    Private Const spSupplierSaveBranch As String = "spSupplierSaveBranch"
    Private Const spSupplierIdGenerate As String = "spGetNoSupplierID"
    Private Const spLisstAssetType As String = "spListAssetType"
    Private Const spListDataSource As String = "spListDataSource"
    Private Const spGenGetSupplierID As String = "spSupplierGetSupplierID"
    Private Const spGetNoPKS As String = "spSupplierGetNoPKS"
    Private m_connection As SqlConnection
#End Region
#Region " Private Const SupplierBranch"
    'Stored Procedure name    
    Private Const spSupplierBranchPaging As String = "spSupplierBranchPaging"
    Private Const spSupplierBranchEdit As String = "spSupplierBranchEdit"
    Private Const spSupplierBranchAdd As String = "spSupplierBranchAdd"
    Private Const spSupplierBranchSaveAdd As String = "spSupplierBranchSaveAdd"
    Private Const spSupplierBranchSaveEdit As String = "spSupplierBranchSaveEdit"
    Private Const spSupplierBranchDelete As String = "spSupplierBranchDelete"
    Private Const spSupplierBranchIncentiveCard As String = "spSupplierBranchIncentiveCard"
    Private Const spSupplierBranchEditAO As String = "spSupplierBranchEditAO"
    Private Const spSupplierViewBudgetForecast As String = "spSupplierViewBudgetForecast"
    Private Const spSupplierView As String = "spSupplierView"
    Private Const spSupplierINCTVGetSupplierEmployeeBankAccount As String = "spSupplierINCTVGetSupplierEmployeeBankAccount"
    Private Const spGetSupplierAccountbySupplierID As String = "spGetSupplierAccountbySupplierID"
    Private Const spSupplierBranchHeaderSaveEdit As String = "spSupplierBranchHeaderSaveEdit"
    Private Const spSupplierBranchRefundSaveEdit As String = "spSupplierBranchRefundSaveEdit"

#End Region
#Region " Private Const SupplierAccount"
    'Stored Procedure name    
    Private Const spSupplierAccountPaging As String = "spSupplierAccountPaging"
    Private Const spSupplierAccountListBySupplierID As String = "spSupplierAccountListBySupplierID"
    Private Const spSupplierAccountSaveAdd As String = "spSupplierAccountSaveAdd"
    Private Const spSupplierAccountSaveEdit As String = "spSupplierAccountSaveEdit"
    Private Const spSupplierAccountDelete As String = "spSupplierAccountDelete"
    Private Const spSupplierAccountBankName As String = "spSupplierAccountBankName"
#End Region
#Region " Private Const SupplierEmployee"
    'Stored Procedure name    
    Private Const spSupplierEmployeePaging As String = "spSupplierEmployeePaging"
    Private Const SPDaftarMasterTransaksiPaging As String = "SPDaftarMasterTransaksiPaging"

    Private Const spSupplierEmployeeEdit As String = "spSupplierEmployeeEdit"
    Private Const spSupplierEmployeeSaveAdd As String = "spSupplierEmployeeSaveAdd"
    Private Const spSupplierEmployeeSaveEdit As String = "spSupplierEmployeeSaveEdit"
    Private Const spSupplierEmployeeDelete As String = "spSupplierEmployeeDelete"
    Private Const spSupplierEmployeeDetailTransaksiDelete As String = "spSupplierEmployeeDetailTransaksiDelete"

    Private Const spSupplierEmployeeBankName As String = "spSupplierEmployeeBankName"
    Private Const spSupplierEmployeeDetailTransaksiSaveAdd As String = "spSupplierEmployeeDetailTransaksiSaveAdd"
    Private Const spSupplierEmployeeDetailTransaksiSaveEdit As String = "spSupplierEmployeeDetailTransaksiSaveEdit"
    Private Const spSupplierEmployeePostition As String = "spGetTblEmployeeSupplier"
    Private Const spGetSupplierBranchRefund As String = "spGetSupplierBranchRefund"

#End Region
#Region " Private Const SupplierSignature"
    'Stored Procedure name    
    Private Const spSupplierSignaturePaging As String = "spSupplierSignaturePaging"
    Private Const spSupplierSignatureEdit As String = "spSupplierSignatureEdit"
    Private Const spSupplierSignatureSaveAdd As String = "spSupplierSignatureSaveAdd"
    Private Const spSupplierSignatureSaveEdit As String = "spSupplierSignatureSaveEdit"
    Private Const spSupplierSignatureDelete As String = "spSupplierSignatureDelete"
    Private Const spSupplierSignatureBankName As String = "spSupplierSignatureBankName"
#End Region
#Region " Private Const SupplierOwner"
    Private Const spSupplierOwnerPaging As String = "spSupplierOwnerPaging"
    Private Const spSupplierOwnerEdit As String = "spSupplierOwnerEdit"
    Private Const spSupplierOwnerAdd As String = "spSupplierOwnerAdd"
    Private Const spSupplierOwnerSaveAdd As String = "spSupplierOwnerSaveAdd"
    Private Const spSupplierOwnerSaveEdit As String = "spSupplierOwnerSaveEdit"
    Private Const spSupplierOwnerDelete As String = "spSupplierOwnerDelete"
    Private Const spSupplierOwnerIDType As String = "spSupplierOwnerIDType"
    Private Const spSupplierOwnerView As String = "spSupplierOwnerView"
#End Region
#Region "Private Const SupplierBudget"
    Private Const spSupplierBAdd As String = "spSupplierBudgetAdd"
    Private Const spCheckDupSupplierB As String = "spCheckDupSupplierBudget"
    Private Const spSupplierBEdit As String = "spSupplierBudgetEdit"
#End Region
#Region "Private Const SupplierForecast"
    Private Const spSupplierFAdd As String = "spSupplierForecastAdd"
    Private Const spCheckDupSupplierF As String = "spCheckDupSupplierForecast"
    Private Const spSupplierFEdit As String = "spSupplierForecastEdit"
#End Region
#Region "Private Const SupplierLevelStatus"
    Private Const spSupplierLevelStatus As String = "spSupplierLevelStatus"
#End Region

#Region "Private Const SupplierRelasi"
    Private Const spGetSupplierEmployeeSPV As String = "spSupplierEmployeeSPV"
    Private Const spGetSupplierEmployeeSales As String = "spSupplierEmployeeSales"
    Private Const spSaveSupplierEmployeeRelasi As String = "spSaveSupplierEmployeeRelasi"
    Private Const spGetSupplierEmployeeSPVBySales As String = "spSupplierEmployeeSPVBySales"
#End Region

#Region "SupplierRelasi"
    Public Function GetSupplierEmployeeSPV(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oRetrun As New Parameter.Supplier
        Try
            Dim param As SqlParameter
            param = New SqlParameter("@SupplierID", SqlDbType.Char)
            param.Value = customClass.SupplierID
            oRetrun.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetSupplierEmployeeSPV, param).Tables(0)
            Return oRetrun
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierEmployeeSPV")
        End Try
    End Function
    Public Function GetSupplierEmployeeSales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oRetrun As New Parameter.Supplier
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@SupplierID", SqlDbType.Char)
            param(0).Value = customClass.SupplierID

            param(1) = New SqlParameter("@superisorID", SqlDbType.Char)
            param(1).Value = customClass.SupervisorID

            oRetrun.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetSupplierEmployeeSales, param).Tables(0)
            Return oRetrun
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierEmployeeSales")

        End Try
    End Function
    Public Function SaveSupplierEmployeeRelasi(ByVal customClass As Parameter.Supplier, ByVal tblEmployee As DataTable) As String
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@supplierID", SqlDbType.Char)
        params(0).Value = customClass.SupplierID.Trim
        params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char)
        params(1).Value = customClass.SupervisorID.Trim
        params(2) = New SqlParameter("@employeeID", SqlDbType.Structured)
        params(2).Value = tblEmployee


        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSaveSupplierEmployeeRelasi, params)

            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function GetSupplierEmployeeSPVBySales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturn As New Parameter.Supplier
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@supplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            params(1) = New SqlParameter("@SalesID", SqlDbType.Char, 10)
            params(1).Value = customClass.SupplierEmployeeID.Trim
            params(2) = New SqlParameter("@SPVId", SqlDbType.Char, 10)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spGetSupplierEmployeeSPVBySales, params)

            If Not IsDBNull(params(2).Value) Then
                oReturn.SupervisorID = params(2).Value
            Else
                oReturn.SupervisorID = 0
            End If

            
            Return oReturn
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

#Region "Supplier"
    Public Function GetSupplier(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplier")
        End Try
    End Function
    Public Function SupplierSaveAddPrivate(ByVal oSupplier As Parameter.Supplier, ByVal oAddress As Parameter.Address) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim ErrMessage As String = ""
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        'Dim params(24) As SqlParameter
        Try
            params.Add(New SqlParameter("@SupplierID", SqlDbType.Char, 10) With {.Value = oSupplier.SupplierID})
            'params(1) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 1)
            'params(1).Value = oSupplier.SupplierGroupID
            params.Add(New SqlParameter("@SupplierName", SqlDbType.VarChar, 50) With {.Value = oSupplier.SupplierName})
            'params(3) = New SqlParameter("@SupplierShortName", SqlDbType.Char, 10)
            'params(3).Value = oSupplier.SupplierShortName
            'params(4) = New SqlParameter("@SupplierInitialName", SqlDbType.Char, 5)
            'params(4).Value = oSupplier.SupplierInitialName
            params.Add(New SqlParameter("@NPWP", SqlDbType.VarChar, 30) With {.Value = oSupplier.NPWP})
            params.Add(New SqlParameter("@SupplierAddress", SqlDbType.VarChar, 100) With {.Value = oAddress.Address})
            params.Add(New SqlParameter("@SupplierRT", SqlDbType.Char, 3) With {.Value = oAddress.RT})
            params.Add(New SqlParameter("@SupplierRW", SqlDbType.Char, 3) With {.Value = oAddress.RW})
            params.Add(New SqlParameter("@SupplierKelurahan", SqlDbType.VarChar, 30) With {.Value = oAddress.Kelurahan})
            params.Add(New SqlParameter("@SupplierKecamatan", SqlDbType.VarChar, 30) With {.Value = oAddress.Kecamatan})
            params.Add(New SqlParameter("@SupplierCity", SqlDbType.VarChar, 30) With {.Value = oAddress.City})
            params.Add(New SqlParameter("@SupplierZipCode", SqlDbType.Char, 5) With {.Value = oAddress.ZipCode})
            params.Add(New SqlParameter("@SupplierAreaPhone1", SqlDbType.Char, 4) With {.Value = oAddress.AreaPhone1})
            params.Add(New SqlParameter("@SupplierPhone1", SqlDbType.Char, 10) With {.Value = oAddress.Phone1})
            params.Add(New SqlParameter("@SupplierAreaPhone2", SqlDbType.Char, 4) With {.Value = oAddress.AreaPhone2})
            params.Add(New SqlParameter("@SupplierPhone2", SqlDbType.Char, 10) With {.Value = oAddress.Phone2})
            params.Add(New SqlParameter("@SupplierAreaFax", SqlDbType.Char, 4) With {.Value = oAddress.AreaFax})
            params.Add(New SqlParameter("@SupplierFax", SqlDbType.Char, 10) With {.Value = oAddress.Fax})
            params.Add(New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50) With {.Value = oSupplier.CPName})
            params.Add(New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50) With {.Value = oSupplier.CPJobTitle})
            params.Add(New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30) With {.Value = oSupplier.CPEmail})
            params.Add(New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20) With {.Value = oSupplier.CPHP})
            params.Add(New SqlParameter("@PF", SqlDbType.Bit) With {.Value = oSupplier.PF})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oSupplier.BusinessDate})
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oSupplier.BranchId})
            params.Add(New SqlParameter("@isPerseorangan", SqlDbType.Bit) With {.Value = oSupplier.isPerseorangan})

            Dim rSupplierID = New SqlParameter("@RSupplierID", SqlDbType.Char, 10) With {.Direction = ParameterDirection.Output}
            params.Add(rSupplierID)
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierSaveAddPrivate, params.ToArray())
            oReturnValue.SupplierID = CType(rSupplierID.Value, String)

            Return oReturnValue
        Catch ex As Exception
            ' Throw New Exception("Error On DataAccess.Marketing.Supplier.ApplicationSaveAddPrivate")
            Throw New Exception(ex.Message)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function SupplierSaveAdd(ByVal oSupplier As Parameter.Supplier, _
      ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim ErrMessage As String = ""
        Dim params(55) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SupplierName", SqlDbType.VarChar, 50)
            params(1).Value = oSupplier.SupplierName
            params(2) = New SqlParameter("@NPWP", SqlDbType.VarChar, 30)
            params(2).Value = oSupplier.NPWP
            params(3) = New SqlParameter("@TDP", SqlDbType.VarChar, 30)
            params(3).Value = oSupplier.TDP
            params(4) = New SqlParameter("@SIUP", SqlDbType.VarChar, 30)
            params(4).Value = oSupplier.SIUP
            params(5) = New SqlParameter("@SupplierAddress", SqlDbType.VarChar, 100)
            params(5).Value = oAddress.Address
            params(6) = New SqlParameter("@SupplierRT", SqlDbType.Char, 3)
            params(6).Value = oAddress.RT
            params(7) = New SqlParameter("@SupplierRW", SqlDbType.Char, 3)
            params(7).Value = oAddress.RW
            params(8) = New SqlParameter("@SupplierKelurahan", SqlDbType.VarChar, 30)
            params(8).Value = oAddress.Kelurahan
            params(9) = New SqlParameter("@SupplierKecamatan", SqlDbType.VarChar, 30)
            params(9).Value = oAddress.Kecamatan
            params(10) = New SqlParameter("@SupplierCity", SqlDbType.VarChar, 30)
            params(10).Value = oAddress.City
            params(11) = New SqlParameter("@SupplierZipCode", SqlDbType.Char, 5)
            params(11).Value = oAddress.ZipCode
            params(12) = New SqlParameter("@SupplierAreaPhone1", SqlDbType.Char, 4)
            params(12).Value = oAddress.AreaPhone1
            params(13) = New SqlParameter("@SupplierPhone1", SqlDbType.Char, 10)
            params(13).Value = oAddress.Phone1
            params(14) = New SqlParameter("@SupplierAreaPhone2", SqlDbType.Char, 4)
            params(14).Value = oAddress.AreaPhone2
            params(15) = New SqlParameter("@SupplierPhone2", SqlDbType.Char, 10)
            params(15).Value = oAddress.Phone2
            params(16) = New SqlParameter("@SupplierAreaFax", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaFax
            params(17) = New SqlParameter("@SupplierFax", SqlDbType.Char, 10)
            params(17).Value = oAddress.Fax
            params(18) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(18).Value = oSupplier.CPName
            params(19) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
            params(19).Value = oSupplier.CPJobTitle
            params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(20).Value = oSupplier.CPEmail
            params(21) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(21).Value = oSupplier.CPHP
            params(22) = New SqlParameter("@SupplierStartDate", SqlDbType.VarChar, 8)
            params(22).Value = oSupplier.SupplierStartDate
            params(23) = New SqlParameter("@SupplierBadStatus", SqlDbType.Char, 1)
            params(23).Value = oSupplier.SupplierBadStatus
            params(24) = New SqlParameter("@SupplierLevelStatus", SqlDbType.Char, 1)
            params(24).Value = oSupplier.SupplierLevelStatus
            params(25) = New SqlParameter("@IsAutomotive", SqlDbType.Char, 1)
            params(25).Value = oSupplier.IsAutomotive
            params(26) = New SqlParameter("@IsUrusBPKB", SqlDbType.Char, 1)
            params(26).Value = oSupplier.IsUrusBPKB
            params(27) = New SqlParameter("@SupplierCategory", SqlDbType.Char, 2)
            params(27).Value = oSupplier.SupplierCategory
            params(28) = New SqlParameter("@SupplierAssetStatus", SqlDbType.Char, 1)
            params(28).Value = oSupplier.SupplierAssetStatus
            params(29) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(29).Value = oSupplier.BusinessDate
            params(30) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(30).Value = oSupplier.BranchId
            params(31) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(31).Direction = ParameterDirection.Output
            params(32) = New SqlParameter("@SupplierIncentiveIDNew", SqlDbType.Int)
            If oSupplier.CardIDNew = 0 Then
                params(32).Value = DBNull.Value
            Else
                params(32).Value = oSupplier.CardIDNew
            End If
            params(33) = New SqlParameter("@SupplierIncentiveIDUsed", SqlDbType.Int)
            If oSupplier.CardIDUsed = 0 Then
                params(33).Value = DBNull.Value
            Else
                params(33).Value = oSupplier.CardIDUsed
            End If
            params(34) = New SqlParameter("@SupplierIncentiveIDRO", SqlDbType.Int)
            If oSupplier.CardIDRO = 0 Then
                params(34).Value = DBNull.Value
            Else
                params(34).Value = oSupplier.CardIDRO
            End If

            params(35) = New SqlParameter("@NoPKS", SqlDbType.VarChar, 50)
            params(35).Value = oSupplier.NoPKS
            params(36) = New SqlParameter("@PenerapanTVC", SqlDbType.Bit)
            params(36).Value = oSupplier.PenerapanTVC
            params(37) = New SqlParameter("@PF", SqlDbType.Bit)
            params(37).Value = oSupplier.PF
            params(38) = New SqlParameter("@isPerseorangan", SqlDbType.Bit)
            params(38).Value = oSupplier.isPerseorangan
            params(39) = New SqlParameter("@PKSDate", SqlDbType.DateTime)
            params(39).Value = oSupplier.PKSDate
            params(40) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 20)
            params(40).Value = oSupplier.SupplierGroupID
            params(41) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
            params(41).Value = oSupplier.Produk
            params(42) = New SqlParameter("@SKBP", SqlDbType.Bit)
            params(42).Value = oSupplier.SKBP
            params(43) = New SqlParameter("@NPWPSupplierAddress", SqlDbType.VarChar, 100)
            params(43).Value = oAddressNPWP.Address
            params(44) = New SqlParameter("@NPWPSupplierRT", SqlDbType.Char, 3)
            params(44).Value = oAddressNPWP.RT
            params(45) = New SqlParameter("@NPWPSupplierRW", SqlDbType.Char, 3)
            params(45).Value = oAddressNPWP.RW
            params(46) = New SqlParameter("@NPWPSupplierKelurahan", SqlDbType.VarChar, 30)
            params(46).Value = oAddressNPWP.Kelurahan
            params(47) = New SqlParameter("@NPWPSupplierKecamatan", SqlDbType.VarChar, 30)
            params(47).Value = oAddressNPWP.Kecamatan
            params(48) = New SqlParameter("@NPWPSupplierCity", SqlDbType.VarChar, 30)
            params(48).Value = oAddressNPWP.City
            params(49) = New SqlParameter("@NPWPSupplierZipCode", SqlDbType.Char, 5)
            params(49).Value = oAddressNPWP.ZipCode
            params(50) = New SqlParameter("@NPWPSupplierAreaPhone1", SqlDbType.Char, 4)
            params(50).Value = oAddressNPWP.AreaPhone1
            params(51) = New SqlParameter("@NPWPSupplierPhone1", SqlDbType.Char, 10)
            params(51).Value = oAddressNPWP.Phone1
            params(52) = New SqlParameter("@NPWPSupplierAreaPhone2", SqlDbType.Char, 4)
            params(52).Value = oAddressNPWP.AreaPhone2
            params(53) = New SqlParameter("@NPWPSupplierPhone2", SqlDbType.Char, 10)
            params(53).Value = oAddressNPWP.Phone2
            params(54) = New SqlParameter("@NPWPSupplierAreaFax", SqlDbType.Char, 4)
            params(54).Value = oAddressNPWP.AreaFax
            params(55) = New SqlParameter("@NPWPSupplierFax", SqlDbType.Char, 10)
            params(55).Value = oAddressNPWP.Fax
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierSaveAdd, params)
            ErrMessage = CType(params(31).Value, String)
            If ErrMessage <> "" Then
                oReturnValue.Output = ErrMessage
                Return oReturnValue
                Exit Function
            End If
            oReturnValue.Output = ErrMessage
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.ApplicationSaveAdd")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function GetBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranch).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetBranch")
        End Try
    End Function
    Public Sub SupplierSaveBranch(ByVal oSupplier As Parameter.Supplier, ByVal oDataBranch As DataTable)
        Dim oReturnValue As New Parameter.Supplier
        Dim m_connection As New SqlConnection(oSupplier.strConnection)
        m_connection.Open()
        Dim transaction As SqlTransaction = m_connection.BeginTransaction
        Dim ErrMessage As String = ""
        Dim intLoop As Integer
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime, 10)
            params(1).Value = oSupplier.BusinessDate
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            For intLoop = 0 To oDataBranch.Rows.Count - 1
                params(2).Value = oDataBranch.Rows(intLoop).Item(0)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierSaveBranch, params)
            Next

            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSaveBranch")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub
    Public Function SupplierEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierEdit, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierEdit")
        End Try
    End Function
    Public Function DeleteSupplier(ByVal customClass As Parameter.Supplier) As String
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierDelete, params)
            Return ""
        Catch ex As Exception
            Return ex.Message
            Throw New Exception("Error On DataAccess.Marketing.Supplier.DeleteSupplier")
        End Try
    End Function
    Public Sub SupplierSaveEdit(ByVal oSupplier As Parameter.Supplier, _
      ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address)
        Dim params(52) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SupplierName", SqlDbType.VarChar, 50)
            params(1).Value = oSupplier.SupplierName
            params(2) = New SqlParameter("@NPWP", SqlDbType.VarChar, 30)
            params(2).Value = oSupplier.NPWP
            params(3) = New SqlParameter("@TDP", SqlDbType.VarChar, 30)
            params(3).Value = oSupplier.TDP
            params(4) = New SqlParameter("@SIUP", SqlDbType.VarChar, 30)
            params(4).Value = oSupplier.SIUP
            params(5) = New SqlParameter("@SupplierAddress", SqlDbType.VarChar, 100)
            params(5).Value = oAddress.Address
            params(6) = New SqlParameter("@SupplierRT", SqlDbType.Char, 3)
            params(6).Value = oAddress.RT
            params(7) = New SqlParameter("@SupplierRW", SqlDbType.Char, 3)
            params(7).Value = oAddress.RW
            params(8) = New SqlParameter("@SupplierKelurahan", SqlDbType.VarChar, 30)
            params(8).Value = oAddress.Kelurahan
            params(9) = New SqlParameter("@SupplierKecamatan", SqlDbType.VarChar, 30)
            params(9).Value = oAddress.Kecamatan
            params(10) = New SqlParameter("@SupplierCity", SqlDbType.VarChar, 30)
            params(10).Value = oAddress.City
            params(11) = New SqlParameter("@SupplierZipCode", SqlDbType.Char, 5)
            params(11).Value = oAddress.ZipCode
            params(12) = New SqlParameter("@SupplierAreaPhone1", SqlDbType.Char, 4)
            params(12).Value = oAddress.AreaPhone1
            params(13) = New SqlParameter("@SupplierPhone1", SqlDbType.Char, 10)
            params(13).Value = oAddress.Phone1
            params(14) = New SqlParameter("@SupplierAreaPhone2", SqlDbType.Char, 4)
            params(14).Value = oAddress.AreaPhone2
            params(15) = New SqlParameter("@SupplierPhone2", SqlDbType.Char, 10)
            params(15).Value = oAddress.Phone2
            params(16) = New SqlParameter("@SupplierAreaFax", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaFax
            params(17) = New SqlParameter("@SupplierFax", SqlDbType.Char, 10)
            params(17).Value = oAddress.Fax
            params(18) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(18).Value = oSupplier.CPName
            params(19) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
            params(19).Value = oSupplier.CPJobTitle
            params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(20).Value = oSupplier.CPEmail
            params(21) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(21).Value = oSupplier.CPHP
            params(22) = New SqlParameter("@SupplierStartDate", SqlDbType.VarChar, 8)
            params(22).Value = oSupplier.SupplierStartDate
            params(23) = New SqlParameter("@SupplierBadStatus", SqlDbType.Char, 1)
            params(23).Value = oSupplier.SupplierBadStatus
            params(24) = New SqlParameter("@SupplierLevelStatus", SqlDbType.Char, 1)
            params(24).Value = oSupplier.SupplierLevelStatus
            params(25) = New SqlParameter("@IsAutomotive", SqlDbType.Char, 1)
            params(25).Value = oSupplier.IsAutomotive
            params(26) = New SqlParameter("@IsUrusBPKB", SqlDbType.Char, 1)
            params(26).Value = oSupplier.IsUrusBPKB
            params(27) = New SqlParameter("@SupplierCategory", SqlDbType.Char, 2)
            params(27).Value = oSupplier.SupplierCategory
            params(28) = New SqlParameter("@SupplierAssetStatus", SqlDbType.Char, 1)
            params(28).Value = oSupplier.SupplierAssetStatus
            params(29) = New SqlParameter("@SupplierIncentiveIDNew", SqlDbType.Int)
            If oSupplier.CardIDNew = 0 Then
                params(29).Value = DBNull.Value
            Else
                params(29).Value = oSupplier.CardIDNew
            End If
            params(30) = New SqlParameter("@SupplierIncentiveIDUsed", SqlDbType.Int)
            If oSupplier.CardIDUsed = 0 Then
                params(30).Value = DBNull.Value
            Else
                params(30).Value = oSupplier.CardIDUsed
            End If
            params(31) = New SqlParameter("@SupplierIncentiveIDRO", SqlDbType.Int)
            If oSupplier.CardIDRO = 0 Then
                params(31).Value = DBNull.Value
            Else
                params(31).Value = oSupplier.CardIDRO
            End If
            params(32) = New SqlParameter("@NoPKS", SqlDbType.VarChar, 50)
            params(32).Value = oSupplier.NoPKS
            params(33) = New SqlParameter("@PenerapanTVC", SqlDbType.Bit)
            params(33).Value = oSupplier.PenerapanTVC
            params(34) = New SqlParameter("@PF", SqlDbType.Bit)
            params(34).Value = oSupplier.PF
            params(35) = New SqlParameter("@isPerseorangan", SqlDbType.Bit)
            params(35).Value = oSupplier.isPerseorangan
            params(36) = New SqlParameter("@PKSDate", SqlDbType.DateTime)
            params(36).Value = oSupplier.PKSDate
            params(37) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 20)
            params(37).Value = oSupplier.SupplierGroupID
            params(38) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
            params(38).Value = oSupplier.Produk
            params(39) = New SqlParameter("@SKBP", SqlDbType.Bit)
            params(39).Value = oSupplier.SKBP
            params(40) = New SqlParameter("@NPWPSupplierAddress", SqlDbType.VarChar, 100)
            params(40).Value = oAddressNPWP.Address
            params(41) = New SqlParameter("@NPWPSupplierRT", SqlDbType.Char, 3)
            params(41).Value = oAddressNPWP.RT
            params(42) = New SqlParameter("@NPWPSupplierRW", SqlDbType.Char, 3)
            params(42).Value = oAddressNPWP.RW
            params(43) = New SqlParameter("@NPWPSupplierKelurahan", SqlDbType.VarChar, 30)
            params(43).Value = oAddressNPWP.Kelurahan
            params(44) = New SqlParameter("@NPWPSupplierKecamatan", SqlDbType.VarChar, 30)
            params(44).Value = oAddressNPWP.Kecamatan
            params(45) = New SqlParameter("@NPWPSupplierCity", SqlDbType.VarChar, 30)
            params(45).Value = oAddressNPWP.City
            params(46) = New SqlParameter("@NPWPSupplierZipCode", SqlDbType.Char, 5)
            params(46).Value = oAddressNPWP.ZipCode
            params(47) = New SqlParameter("@NPWPSupplierAreaPhone1", SqlDbType.Char, 4)
            params(47).Value = oAddressNPWP.AreaPhone1
            params(48) = New SqlParameter("@NPWPSupplierPhone1", SqlDbType.Char, 10)
            params(48).Value = oAddressNPWP.Phone1
            params(49) = New SqlParameter("@NPWPSupplierAreaPhone2", SqlDbType.Char, 4)
            params(49).Value = oAddressNPWP.AreaPhone2
            params(50) = New SqlParameter("@NPWPSupplierPhone2", SqlDbType.Char, 10)
            params(50).Value = oAddressNPWP.Phone2
            params(51) = New SqlParameter("@NPWPSupplierAreaFax", SqlDbType.Char, 4)
            params(51).Value = oAddressNPWP.AreaFax
            params(52) = New SqlParameter("@NPWPSupplierFax", SqlDbType.Char, 10)
            params(52).Value = oAddressNPWP.Fax

            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierSaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSaveEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub
    Public Function GetSupplierID(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = OSupplier.BusinessDate
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oSupplier.BranchId
        params(2) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spGenGetSupplierID, params)
            oReturnValue.SupplierID = CType(params(2).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierID")
        End Try
    End Function
    Public Function GetNoPKS(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = oSupplier.BusinessDate
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oSupplier.BranchId
        params(2) = New SqlParameter("@NoPKS", SqlDbType.Char, 20)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spGetNoPKS, params)
            oReturnValue.NoPKS = CType(params(2).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetNoPKS")
        End Try
    End Function
#End Region

#Region "SupplierBranch"
    Public Function GetSupplierBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranch")
        End Try
    End Function
    Public Function GetSupplierBranchAdd(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchAdd, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchAdd")
        End Try
    End Function
    Public Function GetSupplierBranchIncentiveCard(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchIncentiveCard).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchIncentiveCard")
        End Try
    End Function
    Public Function GetSupplierBranchEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(1) As SqlParameter
        Dim params1(0) As SqlParameter
        Dim params2(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchEdit, params).Tables(0)

            params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params1(0).Value = customClass.BranchId
            oReturnValue.ListData2 = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchEditAO, params1).Tables(0)
            If customClass.WhereCond Is Nothing Then
            Else
                params2(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                params2(0).Value = customClass.SupplierID
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params2(1).Value = customClass.WhereCond.Trim
                oReturnValue.ListData3 = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierINCTVGetSupplierEmployeeBankAccount, params2).Tables(0)
            End If


            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchEdit")
        End Try
    End Function
    Public Sub SupplierBranchSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim params(25) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = oSupplier.BranchId
            params(2) = New SqlParameter("@AOID", SqlDbType.Char, 10)
            params(2).Value = oSupplier.AOID
            params(3) = New SqlParameter("@NewVehicleIC", SqlDbType.Char, 10)
            params(3).Value = oSupplier.NewVehicleIC
            params(4) = New SqlParameter("@UsedVehicleIC", SqlDbType.Char, 10)
            params(4).Value = oSupplier.UsedVehicleIC
            params(5) = New SqlParameter("@ROIC", SqlDbType.Char, 10)
            params(5).Value = oSupplier.ROIC
            params(6) = New SqlParameter("@IncentiveDistributionMethod", SqlDbType.Char, 1)
            params(6).Value = oSupplier.IncentiveDistributionMethod
            params(7) = New SqlParameter("@CompanyPercentage", SqlDbType.Decimal)
            params(7).Value = oSupplier.CompanyPercentage
            params(8) = New SqlParameter("@GMPercentage", SqlDbType.Decimal)
            params(8).Value = oSupplier.GMPercentage
            params(9) = New SqlParameter("@BMPercentage", SqlDbType.Decimal)
            params(9).Value = oSupplier.BMPercentage
            params(10) = New SqlParameter("@ADHPercentage", SqlDbType.Decimal)
            params(10).Value = oSupplier.ADHPercentage
            params(11) = New SqlParameter("@SPVPercentage", SqlDbType.Decimal)
            params(11).Value = oSupplier.SPVPercentage
            params(12) = New SqlParameter("@SPPercentage", SqlDbType.Decimal)
            params(12).Value = oSupplier.SPPercentage
            params(13) = New SqlParameter("@ADPercentage", SqlDbType.Decimal)
            params(13).Value = oSupplier.ADPercentage
            params(14) = New SqlParameter("@GMAmount", SqlDbType.Decimal)
            params(14).Value = oSupplier.GMAmount
            params(15) = New SqlParameter("@BMAmount", SqlDbType.Decimal)
            params(15).Value = oSupplier.BMAmount
            params(16) = New SqlParameter("@ADHAmount", SqlDbType.Decimal)
            params(16).Value = oSupplier.ADHAmount
            params(17) = New SqlParameter("@SPVAmount", SqlDbType.Decimal)
            params(17).Value = oSupplier.SPVAmount
            params(18) = New SqlParameter("@SPAmount", SqlDbType.Decimal)
            params(18).Value = oSupplier.SPAmount
            params(19) = New SqlParameter("@ADAmount", SqlDbType.Decimal)
            params(19).Value = oSupplier.ADAmount
            params(20) = New SqlParameter("@EmployeeUnitWayICPayment", SqlDbType.Char, 1)
            params(20).Value = oSupplier.EmployeeUnitWayICPayment
            params(21) = New SqlParameter("@EmployeeAFWayICPayment", SqlDbType.Char, 1)
            params(21).Value = oSupplier.EmployeeAFWayICPayment
            params(22) = New SqlParameter("@EmployeeRAWayICPayment", SqlDbType.Char, 1)
            params(22).Value = oSupplier.EmployeeRAWayICPayment
            params(23) = New SqlParameter("@CompanyUnitWayICPayment", SqlDbType.Char, 1)
            params(23).Value = oSupplier.CompanyUnitWayICPayment
            params(24) = New SqlParameter("@CompanyAFWayICPayment", SqlDbType.Char, 1)
            params(24).Value = oSupplier.CompanyAFWayICPayment
            params(25) = New SqlParameter("@CompanyRAWayICPayment", SqlDbType.Char, 1)
            params(25).Value = oSupplier.CompanyRAWayICPayment
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierBranchSaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierBranchSaveEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub

    Public Sub SupplierBranchRefundHeaderSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim params(13) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oSupplier.BranchId
            params(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(1).Value = oSupplier.SupplierID
            params(2) = New SqlParameter("@CMO", SqlDbType.Char, 10)
            params(2).Value = oSupplier.CMO
            params(3) = New SqlParameter("@RefundInterestPercent", SqlDbType.Decimal, 18, 2)
            params(3).Value = oSupplier.RefundInterestPercent
            params(4) = New SqlParameter("@RefundInterestAmount", SqlDbType.Decimal, 18, 2)
            params(4).Value = oSupplier.RefundInterestAmount
            params(5) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal, 18, 2)
            params(5).Value = oSupplier.RefundPremiPercent
            params(6) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal, 18, 2)
            params(6).Value = oSupplier.RefundPremiAmount
            params(7) = New SqlParameter("@RefundInterestBy", SqlDbType.Char, 20)
            params(7).Value = oSupplier.RefundBungaBy
            params(8) = New SqlParameter("@RefundInterestMultiply", SqlDbType.Char, 20)
            params(8).Value = oSupplier.RefundBungaMultiplier
            params(9) = New SqlParameter("@RefundPremiBy", SqlDbType.Char, 20)
            params(9).Value = oSupplier.RefundPremiBy
            params(10) = New SqlParameter("@RefundInterestAlokasi", SqlDbType.Char, 20)
            params(10).Value = oSupplier.RefundInterestAlokasi
            params(11) = New SqlParameter("@RefundPremiAlokasi", SqlDbType.Char, 20)
            params(11).Value = oSupplier.RefundPremiAlokasi

            params(12) = New SqlParameter("@RewardUnit", SqlDbType.Decimal, 18, 2)
            params(12).Value = oSupplier.RewardUnit

            params(13) = New SqlParameter("@BukBonus", SqlDbType.Decimal, 18, 2)
            params(13).Value = oSupplier.BukBonus

            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierBranchHeaderSaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierBranchRefundHeaderSaveEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub

    Public Sub SupplierBranchRefundSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim params(5) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oSupplier.BranchId
            params(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(1).Value = oSupplier.SupplierID
            params(2) = New SqlParameter("@AlokasiType", SqlDbType.Char, 3)
            params(2).Value = oSupplier.AlokasiType
            params(3) = New SqlParameter("@SupplierEmployeePositionId", SqlDbType.Char, 2)
            params(3).Value = oSupplier.SupplierEmployeePositionId
            params(4) = New SqlParameter("@Persentase", SqlDbType.Decimal, 18, 2)
            params(4).Value = oSupplier.NilaiPersentase
            params(5) = New SqlParameter("@Amount", SqlDbType.Decimal, 18, 2)
            params(5).Value = oSupplier.NilaiAmount

            'params(6) = New SqlParameter("@IsPersentaseAmount", SqlDbType.Char, 2)
            'params(6).Value = oSupplier.RefundPercentageAmount

            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierBranchRefundSaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierBranchRefundSaveEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub
    Public Function GetSupplierBranchDelete(ByVal customClass As Parameter.Supplier) As String
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierBranchDelete, params)
            Return ""
        Catch ex As Exception
            Return "Unable to delete!"
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchEdit")
        End Try
    End Function
#End Region
#Region "SupplierOwner"
    Public Function GetSupplierOwner(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierOwnerPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierOwner")
        End Try
    End Function
    Public Function GetSupplierOwnerIDType(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierOwnerIDType).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierOwnerIDType")
        End Try
    End Function
    Public Function SupplierOwnerSaveAdd(ByVal oSupplier As Parameter.Supplier, _
     ByVal oAddress As Parameter.Address) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim ErrMessage As String = ""
        Dim params(24) As SqlParameter
        Dim spName As String
        If oSupplier.AddEdit = "Add" Then
            spName = spSupplierOwnerSaveAdd
        Else
            spName = spSupplierOwnerSaveEdit
        End If
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SupplierOwnerID", SqlDbType.Char, 10)
            params(1).Value = oSupplier.ID
            params(2) = New SqlParameter("@SupplierOwnerName", SqlDbType.VarChar, 50)
            params(2).Value = oSupplier.Name
            params(3) = New SqlParameter("@SupplierOwnerTitle", SqlDbType.VarChar, 50)
            params(3).Value = oSupplier.Title
            params(4) = New SqlParameter("@SupplierOwnerBirthPlace", SqlDbType.VarChar, 50)
            params(4).Value = oSupplier.BirthPlace
            params(5) = New SqlParameter("@SupplierOwnerBirthDate", SqlDbType.VarChar, 8)
            params(5).Value = oSupplier.BirthDate
            params(6) = New SqlParameter("@SupplierOwnerNPWP", SqlDbType.VarChar, 20)
            params(6).Value = oSupplier.NPWP
            params(7) = New SqlParameter("@SupplierOwnerIDType", SqlDbType.VarChar, 10)
            params(7).Value = oSupplier.IDType
            params(8) = New SqlParameter("@SupplierOwnerIDNumber", SqlDbType.VarChar, 20)
            params(8).Value = oSupplier.IDNumber
            params(9) = New SqlParameter("@SupplierOwnerAddress", SqlDbType.VarChar, 100)
            params(9).Value = oAddress.Address
            params(10) = New SqlParameter("@SupplierOwnerRT", SqlDbType.Char, 3)
            params(10).Value = oAddress.RT
            params(11) = New SqlParameter("@SupplierOwnerRW", SqlDbType.Char, 3)
            params(11).Value = oAddress.RW
            params(12) = New SqlParameter("@SupplierOwnerKelurahan", SqlDbType.VarChar, 30)
            params(12).Value = oAddress.Kelurahan
            params(13) = New SqlParameter("@SupplierOwnerKecamatan", SqlDbType.VarChar, 30)
            params(13).Value = oAddress.Kecamatan
            params(14) = New SqlParameter("@SupplierOwnerCity", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.City
            params(15) = New SqlParameter("@SupplierOwnerZipCode", SqlDbType.Char, 5)
            params(15).Value = oAddress.ZipCode
            params(16) = New SqlParameter("@SupplierOwnerAreaPhone1", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone1
            params(17) = New SqlParameter("@SupplierOwnerPhone1", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone1
            params(18) = New SqlParameter("@SupplierOwnerAreaPhone2", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaPhone2
            params(19) = New SqlParameter("@SupplierOwnerPhone2", SqlDbType.Char, 10)
            params(19).Value = oAddress.Phone2
            params(20) = New SqlParameter("@SupplierOwnerAreaFax", SqlDbType.Char, 4)
            params(20).Value = oAddress.AreaFax
            params(21) = New SqlParameter("@SupplierOwnerFax", SqlDbType.Char, 10)
            params(21).Value = oAddress.Fax
            params(22) = New SqlParameter("@SupplierOwnerEmail", SqlDbType.VarChar, 30)
            params(22).Value = oSupplier.Email
            params(23) = New SqlParameter("@SupplierOwnerHP", SqlDbType.VarChar, 20)
            params(23).Value = oSupplier.HP
            params(24) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(24).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spName, params)
            ErrMessage = CType(params(24).Value, String)
            If ErrMessage <> "" Then
                oReturnValue.Output = ErrMessage
                Return oReturnValue
                Exit Function
            End If
            oReturnValue.Output = ErrMessage
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.ApplicationSaveAdd")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function GetSupplierOwnerEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        params(1) = New SqlParameter("@SupplierOwnerID", SqlDbType.Char, 10)
        params(1).Value = customClass.ID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierOwnerEdit, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierOwnerEdit")
        End Try
    End Function
    Public Function GetSupplierOwnerDetele(ByVal customClass As Parameter.Supplier) As String
        Dim oReturnValue As New Parameter.Supplier
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        params(1) = New SqlParameter("@SupplierOwnerID", SqlDbType.Char, 10)
        params(1).Value = customClass.ID
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 20)
        params(2).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierOwnerDelete, params).Tables(0)
            Return CType(params(2).Value, String)
        Catch ex As Exception
        End Try
    End Function
    Public Function GetSupplierOwnerView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        params(1) = New SqlParameter("@SupplierOwnerID", SqlDbType.Char, 10)
        params(1).Value = customClass.ID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierOwnerView, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierOwnerView")
        End Try
    End Function
    'Public Function GetSupplierOwnerID(ByVal customClass As Parameter.Supplier) As String
    '    Dim Result As String
    '    Dim params() As SqlParameter = New SqlParameter(1) {}
    '    params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
    '    params(0).Value = customClass.SupplierID
    '    params(1) = New SqlParameter("@SupplierOwnerID", SqlDbType.Char, 10)
    '    params(1).Direction = ParameterDirection.Output
    '    Try
    '        SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spGetSupplierOwnerID", params)
    '        Result = CType(params(1).Value, String)
    '        If Result <> "" Then
    '            Return Result
    '        End If
    '        Return ""
    '    Catch ex As Exception
    '    End Try
    'End Function
#End Region
#Region "Report"
    Public Function GetSupplierReport(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Try
            If customClass.Title = "Supplier" Then
                Dim params(0) As SqlParameter
                params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
                params(0).Value = customClass.WhereCond
                oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spSupplierReport", params).Tables(0)
            Else
                Dim spName As String
                spName = "sp" + customClass.Title + "Report"
                Dim params(1) As SqlParameter
                params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
                params(0).Value = customClass.WhereCond
                params(1) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
                params(1).Value = customClass.WhereCond2
                oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            End If
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierReport")
        End Try
    End Function
#End Region
#Region "SupplierAccount"
    Public Function GetSupplierAccount(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierAccountPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierAccount")
        End Try
    End Function
    Public Function SupplierAccountSave(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
        Dim oReturn As New Parameter.Supplier
        Dim params(10) As SqlParameter
        Dim spName As String

        If oSupplier.AddEdit = "Add" Then
            spName = spSupplierAccountSaveAdd
        Else
            spName = spSupplierAccountSaveEdit
        End If
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SupplierAccID", SqlDbType.Char, 10)
            params(1).Value = oSupplier.ID
            params(2) = New SqlParameter("@SupplierBankID", SqlDbType.Char, 5)
            params(2).Value = oSupplier.BankID
            params(3) = New SqlParameter("@SupplierBankBranch", SqlDbType.VarChar, 30)
            params(3).Value = oSupplier.BankBranch
            params(4) = New SqlParameter("@SupplierAccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oSupplier.AccountNo
            params(5) = New SqlParameter("@SupplierAccountName", SqlDbType.VarChar, 50)
            params(5).Value = oSupplier.AccountName
            params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output
            params(7) = New SqlParameter("@SupplierBankBranchId", SqlDbType.VarChar, 50)
            params(7).Value = oSupplier.BankBranchId
            params(8) = New SqlParameter("@DefaultAccount", SqlDbType.Bit)
            params(8).Value = oSupplier.DefaultAccount
            params(9) = New SqlParameter("@UntukBayar", SqlDbType.VarChar, 50)
            params(9).Value = oSupplier.UntukBayar
            params(10) = New SqlParameter("@TanggalEfektif", SqlDbType.Date)
            params(10).Value = oSupplier.TanggalEfektif

            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spName, params)
            oReturn.Output = CType(params(6).Value, String)
            Return oReturn
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function SupplierAccountDelete(ByVal customClass As Parameter.Supplier) As String
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            params(1) = New SqlParameter("@SupplierAccID", SqlDbType.Char, 10)
            params(1).Value = customClass.ID
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierAccountDelete, params)
            Return ""
        Catch ex As Exception
            Return "Unable to delete!"
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierAccountDelete")
        End Try
    End Function
#End Region
#Region "SupplierSignature"
    Public Function GetSupplierSignature(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierSignaturePaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierSignature")
        End Try
    End Function
    Public Sub SupplierSignatureSaveAdd(ByVal oSupplier As Parameter.Supplier)
        Dim oReturn As New Parameter.Supplier
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            'params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            'params(1).Value = oSupplier.Name
            params(1) = New SqlParameter("@JobTitle", SqlDbType.VarChar, 50)
            params(1).Value = oSupplier.JobTitle
            params(2) = New SqlParameter("@SignatureURL", SqlDbType.VarChar, 100)
            params(2).Value = oSupplier.SignatureURL
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierSignatureSaveAdd, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSignatureSaveAdd")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub
    Public Sub SupplierSignatureSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim oReturn As New Parameter.Supplier
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SequenceNo", SqlDbType.SmallInt)
            params(1).Value = oSupplier.SequenceNo
            'params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            'params(2).Value = oSupplier.Name
            params(2) = New SqlParameter("@JobTitle", SqlDbType.VarChar, 50)
            params(2).Value = oSupplier.JobTitle
            params(3) = New SqlParameter("@SignatureURL", SqlDbType.VarChar, 100)
            params(3).Value = oSupplier.SignatureURL
            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spSupplierSignatureSaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSignatureSaveEdit")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Sub
    Public Function SupplierSignatureDelete(ByVal customClass As Parameter.Supplier) As String
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID
            params(1) = New SqlParameter("@SequenceNo", SqlDbType.SmallInt)
            params(1).Value = customClass.SequenceNo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierSignatureDelete, params)
            Return ""
        Catch ex As Exception
            Return "Unable to delete!"
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSignatureDelete")
        End Try
    End Function
    Public Function SupplierSignaturePath(ByVal customClass As Parameter.Supplier) As String
        Dim params(0) As SqlParameter
        Dim oData As New DataTable
        Dim Path As String
        Try
            params(0) = New SqlParameter("@GSValue", SqlDbType.VarChar, 500)
            params(0).Direction = ParameterDirection.Output
            oData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spSupplierSignaturePath", params).Tables(0)
            Path = oData.Rows(0).Item(0).ToString.Trim
            Return Path
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSignaturePath")
        End Try
    End Function
    Public Function SupplierSignatureGetSequenceNo(ByVal SupplierId As String, ByVal strConnection As String) As String
        Dim params(1) As SqlParameter
        Dim SequenceNo As String
        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
            params(0).Value = SupplierId
            params(1) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            params(1).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(strConnection, CommandType.StoredProcedure, "spSupplierSignatureGetSequenceNo", params)
            SequenceNo = CType(params(1).Value, String)
            Return SequenceNo
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierSignatureGetSequenceNo")
        End Try
    End Function
#End Region
#Region "SupplierEmployee"
    Public Function GetSupplierEmployee(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            If customClass.FormID = "SeMasterTransaksi" Then
                oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SPDaftarMasterTransaksiPaging, params).Tables(0)
            Else
                oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierEmployeePaging, params).Tables(0)
            End If
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierEmployee")
        End Try
    End Function
    Public Function SupplierEmployeeSave(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
        Dim oReturn As New Parameter.Supplier

        Dim params(14) As SqlParameter
        Dim paramsx(6) As SqlParameter
        Dim spName As String
        If oSupplier.AddEdit = "Add" Then
            spName = spSupplierEmployeeSaveAdd
        Else
            spName = spSupplierEmployeeSaveEdit
        End If
        If oSupplier.FormID = "SeMasterTransaksi" Then
            If oSupplier.AddEdit = "Add" Then
                paramsx(3) = New SqlParameter("@TransID", SqlDbType.Char, 3)
                paramsx(3).Value = 0
                spName = spSupplierEmployeeDetailTransaksiSaveAdd
            Else
                spName = spSupplierEmployeeDetailTransaksiSaveEdit
                paramsx(3) = New SqlParameter("@TransID", SqlDbType.Char, 3)
                paramsx(3).Value = oSupplier.TransID.Trim
            End If
            Try
                paramsx(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                paramsx(0).Value = oSupplier.SupplierID.Trim
                paramsx(1) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
                paramsx(1).Value = oSupplier.ID.Trim
                paramsx(2) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
                paramsx(2).Direction = ParameterDirection.Output


                paramsx(4) = New SqlParameter("@TransIDNew", SqlDbType.Char, 3)
                paramsx(4).Value = oSupplier.TransIDNew.Trim
                paramsx(5) = New SqlParameter("@NilaiAlokasi", SqlDbType.Money)
                paramsx(5).Value = oSupplier.NilaiAlokasi.ToString
                paramsx(6) = New SqlParameter("@PersenAlokasi", SqlDbType.Decimal)
                paramsx(6).Value = oSupplier.PersenAlokasi.ToString
                SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spName, paramsx)
                oReturn.Output = CType(paramsx(2).Value, String)
                Return oReturn
            Catch ex As Exception
                Throw New Exception(ex.Message)

            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try
        Else
            Try
                params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                params(0).Value = oSupplier.SupplierID.Trim
                params(1) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
                params(1).Value = oSupplier.ID.Trim
                params(2) = New SqlParameter("@SupplierEmployeeName", SqlDbType.VarChar, 50)
                params(2).Value = oSupplier.EmployeeName.Trim
                params(3) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.VarChar, 50)
                params(3).Value = oSupplier.EmployeePosition.Trim
                params(4) = New SqlParameter("@SupplierEmployeeBankID", SqlDbType.Char, 5)
                params(4).Value = oSupplier.BankID.Trim
                params(5) = New SqlParameter("@SupplierEmployeeBankBranch", SqlDbType.VarChar, 50)
                params(5).Value = oSupplier.BankBranch.Trim
                params(6) = New SqlParameter("@SupplierEmployeeAccountNo", SqlDbType.Char, 20)
                params(6).Value = oSupplier.AccountNo.Trim
                params(7) = New SqlParameter("@SupplierEmployeeAccountName", SqlDbType.VarChar, 50)
                params(7).Value = oSupplier.AccountName.Trim
                params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
                params(8).Direction = ParameterDirection.Output
                params(9) = New SqlParameter("@IsActive", SqlDbType.Bit)
                params(9).Value = oSupplier.IsActive
                params(10) = New SqlParameter("@SupplierEmployeeBankBranchID", SqlDbType.VarChar)
                params(10).Value = oSupplier.BankBranchId.Trim

                params(11) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
                params(11).Value = oSupplier.NoKTP.Trim
                params(12) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
                params(12).Value = oSupplier.NoNPWP.Trim
                params(13) = New SqlParameter("@SupplierSaleStatus", SqlDbType.Char, 50)
                params(13).Value = oSupplier.SupplierSaleStatus.Trim
                params(14) = New SqlParameter("@AlamatNPWP", SqlDbType.VarChar, 250)
                params(14).Value = oSupplier.AlamatNPWP

                SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, spName, params)
                oReturn.Output = CType(params(8).Value, String)

                Return oReturn
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try
        End If
    End Function
    Public Function SupplierEmployeeDelete(ByVal customClass As Parameter.Supplier) As String
        Dim params(1) As SqlParameter
        Dim paramsx(2) As SqlParameter

        If customClass.FormID = "SeMasterTransaksi" Then
            Try
                paramsx(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                paramsx(0).Value = customClass.SupplierID
                paramsx(1) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
                paramsx(1).Value = customClass.ID
                paramsx(2) = New SqlParameter("@TransID", SqlDbType.Char, 3)
                paramsx(2).Value = customClass.TransID
                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierEmployeeDetailTransaksiDelete, paramsx)
                Return ""
            Catch ex As Exception
                Return "Unable to delete!"
                Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierEmployeeDelete")
            End Try
        Else
            Try
                params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                params(0).Value = customClass.SupplierID
                params(1) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
                params(1).Value = customClass.ID
                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spSupplierEmployeeDelete, params)
                Return ""
            Catch ex As Exception
                Return "Unable to delete!"
                Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierEmployeeDelete")
            End Try
        End If
    End Function
    Public Function GetSupplierEmployeePosition(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = ""
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierEmployeePostition, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierEmployee")
        End Try
    End Function

    Public Function GetSupplierBranchRefund(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customClass.WhereCond
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetSupplierBranchRefund, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchRefund")
        End Try
    End Function


    'Public Function GetSupplierBranchRefund(ByVal CustomClass As Parameter.Supplier) As Parameter.Supplier
    '    Dim oReturnValue As New Parameter.Supplier
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, spGetSupplierBranchRefund).Tables(0)
    '        Return oReturnValue
    '    Catch ex As Exception
    '        Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierBranchRefund")
    '    End Try
    'End Function
#End Region

#Region "Supplier Budget"
    Public Function AddSupplierBudget(ByVal customclass As Parameter.Supplier) As Parameter.Supplier
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        objCon.Open()
        Dim transaction As SqlTransaction = objCon.BeginTransaction

        Try
            '        @BranchID char(3),
            '@SupplierID  varchar(10),
            '@BudgetYear smallint,
            '@BudgetMonth smallint,
            '@AssetUsedNew char(1),
            '@Unit int,
            '@Amount dec(17,2),
            '@strerror varchar(100) output
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
            params(2) = New SqlParameter("@BudgetYear", SqlDbType.SmallInt)
            params(3) = New SqlParameter("@BudgetMonth", SqlDbType.SmallInt)
            params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
            params(5) = New SqlParameter("@Unit", SqlDbType.Int)
            params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

            If CheckDupSupplier(customclass) Then
                customclass.strError = "Data Already Exist"
            Else

                If customclass.ListData.Rows.Count > 0 Then
                    For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1

                        params(0).Value = customclass.BranchId
                        params(1).Value = customclass.SupplierID
                        params(2).Value = customclass.BudgetYear
                        params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("No"))
                        params(4).Value = "N"
                        params(5).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("Unit")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("Unit")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("Unit"))
                        params(6).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("Amount")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("Amount")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("Amount"))
                        params(7).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierBAdd, params)
                    Next

                    For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1

                        params(0).Value = customclass.BranchId
                        params(1).Value = customclass.SupplierID
                        params(2).Value = customclass.BudgetYear
                        params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("No"))
                        params(4).Value = "U"
                        params(5).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("UnitUsed")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("UnitUsed")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("UnitUsed"))
                        params(6).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("AmountUsed")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("AmountUsed")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("AmountUsed"))
                        params(7).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierBAdd, params)
                    Next
                End If
                transaction.Commit()
                customclass.strError = CStr(params(7).Value)
            End If
            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception("Failed On Add Supplier Budget")

        Finally

            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function

    Public Function CheckDupSupplier(ByVal customclass As Parameter.Supplier) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = customclass.SupplierID

        params(2) = New SqlParameter("@BudgetYear", SqlDbType.SmallInt)
        params(2).Value = customclass.BudgetYear

        params(3) = New SqlParameter("@ISChange", SqlDbType.Bit)
        params(3).Direction = ParameterDirection.Output

        params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCheckDupSupplierB, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            Throw New Exception("Data Already Exist")
        End Try
    End Function


    Public Function EditSupplierBudget(ByVal customclass As Parameter.Supplier) As Parameter.Supplier
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = customclass.SupplierID

        params(2) = New SqlParameter("@BudgetYear", SqlDbType.SmallInt)
        params(2).Value = customclass.BudgetYear

        params(3) = New SqlParameter("@BudgetMonth", SqlDbType.SmallInt)
        params(3).Value = customclass.BudgetMonth

        params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
        params(4).Value = customclass.Status

        params(5) = New SqlParameter("@Unit", SqlDbType.Int)
        params(5).Value = customclass.Unit

        params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(6).Value = customclass.Amount

        params(7) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSupplierBEdit, params)
            customclass.strError = CStr(params(7).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception("Failed on Update Supplier Budget")
        End Try
    End Function
#End Region

#Region "Supplier Forecast"
    Public Function AddSupplierForecast(ByVal customclass As Parameter.Supplier) As Parameter.Supplier
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        objCon.Open()
        Dim transaction As SqlTransaction = objCon.BeginTransaction

        Try
            '        @BranchID char(3),
            '@SupplierID  varchar(10),
            '@BudgetYear smallint,
            '@BudgetMonth smallint,
            '@AssetUsedNew char(1),
            '@Unit int,
            '@Amount dec(17,2),
            '@strerror varchar(100) output
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
            params(2) = New SqlParameter("@ForecastYear", SqlDbType.SmallInt)
            params(3) = New SqlParameter("@ForecastMonth", SqlDbType.SmallInt)
            params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
            params(5) = New SqlParameter("@Unit", SqlDbType.Int)
            params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

            If CheckDupSupplierF(customclass) Then
                customclass.strError = "Data Already Exist"
            Else

                If customclass.ListData.Rows.Count > 0 Then
                    For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1

                        params(0).Value = customclass.BranchId
                        params(1).Value = customclass.SupplierID
                        params(2).Value = customclass.ForecastYear
                        params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("No"))
                        params(4).Value = "N"
                        params(5).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("Unit")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("Unit")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("Unit"))
                        params(6).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("Amount")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("Amount")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("Amount"))
                        params(7).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierFAdd, params)
                    Next

                    For intLoopOmset = 0 To customclass.ListData.Rows.Count - 1

                        params(0).Value = customclass.BranchId
                        params(1).Value = customclass.SupplierID
                        params(2).Value = customclass.ForecastYear
                        params(3).Value = CInt(customclass.ListData.Rows(intLoopOmset).Item("No"))
                        params(4).Value = "U"
                        params(5).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("UnitUsed")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("UnitUsed")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("UnitUsed"))
                        params(6).Value = IIf(IsDBNull(customclass.ListData.Rows(intLoopOmset).Item("AmountUsed")) Or CStr(customclass.ListData.Rows(intLoopOmset).Item("AmountUsed")) = "", "0", customclass.ListData.Rows(intLoopOmset).Item("AmountUsed"))
                        params(7).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierFAdd, params)
                    Next
                End If
                transaction.Commit()
                customclass.strError = CStr(params(7).Value)
            End If

            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception("Failed On Add Supplier Forecast")

        Finally

            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function

    Public Function CheckDupSupplierF(ByVal customclass As Parameter.Supplier) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = customclass.SupplierID

        params(2) = New SqlParameter("@ForecastYear", SqlDbType.SmallInt)
        params(2).Value = customclass.ForecastYear

        params(3) = New SqlParameter("@ISChange", SqlDbType.Bit)
        params(3).Direction = ParameterDirection.Output

        params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCheckDupSupplierF, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            Throw New Exception("Data Already Exist")
        End Try
    End Function


    Public Function EditSupplierForecast(ByVal customclass As Parameter.Supplier) As Parameter.Supplier
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = customclass.SupplierID

        params(2) = New SqlParameter("@ForecastYear", SqlDbType.SmallInt)
        params(2).Value = customclass.ForecastYear

        params(3) = New SqlParameter("@ForecastMonth", SqlDbType.SmallInt)
        params(3).Value = customclass.ForecastMonth

        params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
        params(4).Value = customclass.Status

        params(5) = New SqlParameter("@Unit", SqlDbType.Int)
        params(5).Value = customclass.Unit

        params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(6).Value = customclass.Amount

        params(7) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSupplierFEdit, params)

            customclass.strError = CStr(params(7).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception("Failed on Update Supplier Forecast")
        End Try
    End Function
#End Region
#Region " SupplierView "
    Public Function SupplierView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(0) As SqlParameter

        Try

            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = customClass.SupplierID

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierView, params).Tables(0)



            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierViewBudgetForecast")
        End Try
    End Function

    Public Function SupplierViewBudgetForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(2) As SqlParameter

        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(1).Value = customClass.SupplierID
            params(2) = New SqlParameter("@Year", SqlDbType.Int)
            params(2).Value = customClass.Year

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spSupplierViewBudgetForecast, params).Tables(0)



            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierViewBudgetForecast")
        End Try
    End Function

#End Region

#Region "SupplierLevelStatus"
    Public Function SupplierLevelStatus(ByVal strconnection As String) As DataTable
        Dim oReturnValue As DataTable
        Try
            oReturnValue = SqlHelper.ExecuteDataset(strconnection, CommandType.StoredProcedure, spSupplierLevelStatus).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.SupplierLevelStatus")
        End Try
    End Function
#End Region

#Region "GetSupplier Account bySupplierID"
    Public Function GetSupplierAccountbySupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Dim params(1) As SqlParameter

        Try

            params(0) = New SqlParameter("@SupplierName", SqlDbType.VarChar, 50)
            params(0).Value = customClass.SupplierName
            params(1) = New SqlParameter("@BankCode", SqlDbType.Char, 20)
            params(1).Value = customClass.BankCode

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spGetSupplierAccountbySupplierID, params).Tables(0)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region

    Public Function GetProduk(ByVal CustomClass As Parameter.Supplier) As Parameter.Supplier
        Dim oReturnValue As New Parameter.Supplier
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, spLisstAssetType).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.GetSector")
        End Try
    End Function
    Public Function SupplierAccountSave2(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
        Dim oReturn As New Parameter.Supplier
        Dim params(10) As SqlParameter

        Try
            params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(0).Value = oSupplier.SupplierID
            params(1) = New SqlParameter("@SupplierAccID", SqlDbType.Char, 10)
            params(1).Value = oSupplier.ID
            params(2) = New SqlParameter("@SupplierBankID", SqlDbType.Char, 5)
            params(2).Value = oSupplier.BankID
            params(3) = New SqlParameter("@SupplierBankBranch", SqlDbType.VarChar, 30)
            params(3).Value = oSupplier.BankBranch
            params(4) = New SqlParameter("@SupplierAccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oSupplier.AccountNo
            params(5) = New SqlParameter("@SupplierAccountName", SqlDbType.VarChar, 50)
            params(5).Value = oSupplier.AccountName
            params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output
            params(7) = New SqlParameter("@SupplierBankBranchId", SqlDbType.VarChar, 50)
            params(7).Value = oSupplier.BankBranchId
            params(8) = New SqlParameter("@DefaultAccount", SqlDbType.Bit)
            params(8).Value = oSupplier.DefaultAccount
            params(9) = New SqlParameter("@UntukBayar", SqlDbType.VarChar, 50)
            params(9).Value = oSupplier.UntukBayar
            params(10) = New SqlParameter("@TanggalEfektif", SqlDbType.Date)
            params(10).Value = oSupplier.TanggalEfektif

            SqlHelper.ExecuteNonQuery(oSupplier.strConnection, CommandType.StoredProcedure, "spSupplierAccountSave", params)
            oReturn.Output = CType(params(6).Value, String)
            Return oReturn
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
End Class
