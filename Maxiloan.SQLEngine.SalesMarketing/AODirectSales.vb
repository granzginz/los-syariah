
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AODirectSales : Inherits Maxiloan.SQLEngine.DataAccessBase
    'description : fungsi ini untuk menampilkan data ao ke datagrid
    Public Function AOListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@Criteria", SqlDbType.VarChar, 100)
        params(4).Value = customclass.Criteria
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingDirectSalesBudget", params).Tables(0)
            data.TotalRecord = CInt(params(5).Value)
            Return data
        Catch exp As Exception
            WriteException("AODirectSales", "AODirectSales", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk menampilkan budget yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function AOBudgetListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOBudget", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            Return Nothing
            WriteException("AODirectSalesBudget", "AODirectSalesBudget", exp.Message + exp.StackTrace)
        End Try
    End Function
    'sub ini untuk menambah budget untuk tahun yang baru
    Public Sub AOBudgetAdd(ByVal customclass As Parameter.AODirectSales)
        Dim i As Integer
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try

            If customclass.ListData.Rows.Count > 0 Then
                If objcon.State = ConnectionState.Closed Then objcon.Open()
                objtrans = objcon.BeginTransaction

                For i = 0 To customclass.ListData.Rows.Count - 1
                    Dim params() As SqlParameter = New SqlParameter(6) {}
                    params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params(0).Value = customclass.ListData.Rows(i).Item("BranchID")
                    params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
                    params(1).Value = customclass.ListData.Rows(i).Item("AOID")
                    params(2) = New SqlParameter("@Year", SqlDbType.Int)
                    params(2).Value = customclass.ListData.Rows(i).Item("BudgetYear")
                    params(3) = New SqlParameter("@Month", SqlDbType.Int)
                    params(3).Value = customclass.ListData.Rows(i).Item("BudgetMonth")
                    params(4) = New SqlParameter("@AssetStatus", SqlDbType.Char, 1)
                    params(4).Value = customclass.ListData.Rows(i).Item("AssetUsedNew")
                    params(5) = New SqlParameter("@Unit", SqlDbType.Int)
                    params(5).Value = customclass.ListData.Rows(i).Item("Unit")
                    params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
                    params(6).Value = customclass.ListData.Rows(i).Item("Amount")


                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spMarketingAOBudgetAdd", params)

                Next
                objtrans.Commit()
            End If

        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()

        End Try






    End Sub
    'sub ini untuk melakukan edit pada unit dan amount budget yang dipilih 
    Public Sub AOBudgetEdit(ByVal customclass As Parameter.AODirectSales)
        Dim data As New Parameter.AODirectSales

        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@AssetStatus", SqlDbType.Char, 1)
        params(4).Value = customclass.AssetStatus
        params(5) = New SqlParameter("@Unit", SqlDbType.Int)
        params(5).Value = customclass.Unit
        params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(6).Value = customclass.Amount

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOBudgetEdit", params)


        Catch exp As Exception
            WriteException("AODirectSalesEdit", "AODirectSalesEdit", exp.Message + exp.StackTrace)
        End Try

    End Sub
    'fungsi ini untuk menampilkan semua budget dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOBudgetView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOBudgetView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AODirectSalesView", "AODirectSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function AOBudgetPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOBudgetPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AODirectSalesPrint", "AODirectSalesPrint", exp.Message + exp.StackTrace)
            Return data
        End Try

    End Function
    'fungsi ini untuk menampilkan forecast yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function AOForecastListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(4).Value = customclass.EmployeeID
        params(5) = New SqlParameter("@Year", SqlDbType.Int)
        params(5).Value = customclass.Year
        params(6) = New SqlParameter("@Month", SqlDbType.Int)
        params(6).Value = customclass.Month
        params(7) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(7).Value = customclass.AssetStatus
        params(8) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(8).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOForecast", params).Tables(0)
            data.TotalRecord = CInt(params(8).Value)
            Return data
        Catch exp As Exception
            WriteException("AODirectSalesForecast", "AODirectSalesForecast", exp.Message + exp.StackTrace)
        End Try

    End Function
    'sub ini untuk menambah Forecast untuk tahun yang baru
    Public Sub AOForecastAdd(ByVal customclass As Parameter.AODirectSales)
        Dim i As Integer
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try

            If customclass.ListData.Rows.Count > 0 Then
                If objcon.State = ConnectionState.Closed Then objcon.Open()
                objtrans = objcon.BeginTransaction
                For i = 0 To customclass.ListData.Rows.Count - 1
                    Dim params() As SqlParameter = New SqlParameter(6) {}
                    params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params(0).Value = customclass.ListData.Rows(i).Item("BranchID")
                    params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
                    params(1).Value = customclass.ListData.Rows(i).Item("AOID")
                    params(2) = New SqlParameter("@Year", SqlDbType.Int)
                    params(2).Value = customclass.ListData.Rows(i).Item("ForecastYear")
                    params(3) = New SqlParameter("@Month", SqlDbType.Int)
                    params(3).Value = customclass.ListData.Rows(i).Item("ForecastMonth")
                    params(4) = New SqlParameter("@AssetStatus", SqlDbType.Char, 1)
                    params(4).Value = customclass.ListData.Rows(i).Item("AssetUsedNew")
                    params(5) = New SqlParameter("@Unit", SqlDbType.Int)
                    params(5).Value = customclass.ListData.Rows(i).Item("Unit")
                    params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
                    params(6).Value = customclass.ListData.Rows(i).Item("Amount")


                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spMarketingAOForecastAdd", params)

                Next
                objtrans.Commit()
            End If

        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()

        End Try






    End Sub
    'sub ini untuk melakukan edit pada unit dan amount Forecast yang dipilih 
    Public Sub AOForecastEdit(ByVal customclass As Parameter.AODirectSales)
        Dim data As New Parameter.AODirectSales

        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@AssetStatus", SqlDbType.Char, 1)
        params(4).Value = customclass.AssetStatus
        params(5) = New SqlParameter("@Unit", SqlDbType.Int)
        params(5).Value = customclass.Unit
        params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(6).Value = customclass.Amount

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOForecastEdit", params)


        Catch exp As Exception
            WriteException("AODirectSalesForecastEdit", "AODirectSalesForecastEdit", exp.Message + exp.StackTrace)
        End Try

    End Sub
    'fungsi ini untuk menampilkan semua Forecast dalam 12 bulan berdasar tahun yang diinginkan
    Public Function AOForecastView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOForecastView", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("AODirectSalesView", "AODirectSalesView", exp.Message + exp.StackTrace)
        End Try

    End Function
    'fungsi ini untuk melakukan print pada forecast yang diminta
    Public Function AOForecastPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@AOID", SqlDbType.Char, 10)
        params(1).Value = customclass.EmployeeID
        params(2) = New SqlParameter("@Year", SqlDbType.Int)
        params(2).Value = customclass.Year
        params(3) = New SqlParameter("@Month", SqlDbType.Int)
        params(3).Value = customclass.Month
        params(4) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(4).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spMarketingAOForecastPrint", params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("AODirectSalesPrint", "AODirectSalesPrint", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function AOBudgetList(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim data As New Parameter.AODirectSales
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000) With {.Value = customclass.WhereCond})
        params.Add(New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000) With {.Value = customclass.WhereCond2})
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAOBudgetList", params.ToArray()).Tables(0)
            Return data
        Catch exp As Exception
            WriteException("spAOBudgetList", "spAOBudgetList", exp.Message + exp.StackTrace)
            Return data
        End Try

    End Function

    Public Sub AOBudgetSaveEdit(ByVal customclass As Parameter.AODirectSales)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId})
        params.Add(New SqlParameter("@AOID", SqlDbType.Char, 10) With {.Value = customclass.EmployeeID})
        params.Add(New SqlParameter("@BudgetYear", SqlDbType.Int) With {.Value = customclass.Year})
        params.Add(New SqlParameter("@BudgetMonth", SqlDbType.Int) With {.Value = customclass.Month})
        params.Add(New SqlParameter("@NewAssetUnit", SqlDbType.Int) With {.Value = customclass.NewAssetUnit})
        params.Add(New SqlParameter("@NewAssetAmount", SqlDbType.Decimal) With {.Value = customclass.NewAssetAmount})
        params.Add(New SqlParameter("@UsedAssetUnit", SqlDbType.Int) With {.Value = customclass.UsedAssetUnit})
        params.Add(New SqlParameter("@UsedAssetAmount", SqlDbType.Decimal) With {.Value = customclass.UsedAssetAmount})
         Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spAOBudgetSaveEdit", params.ToArray())


        Catch exp As Exception
            WriteException("AODirectSalesForecastEdit", "AODirectSalesForecastEdit", exp.Message + exp.StackTrace)
        End Try
    End Sub
End Class
