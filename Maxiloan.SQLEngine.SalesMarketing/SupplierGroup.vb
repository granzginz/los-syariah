﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SupplierGroup : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT_JOB_ALL As String = "spJobPositionListAll"
    Private Const LIST_SELECT As String = "spPagingSupplierGroup"
    Private Const LIST_BY_ID As String = "spSupplierGroupList"
    Private Const LIST_ADD As String = "spSupplierGroupSaveAdd"
    Private Const LIST_UPDATE As String = "spSupplierGroupSaveEdit"
    Private Const LIST_DELETE As String = "spSupplierGroupDelete"
    Private Const LIST_REPORT As String = "spSupplierGroupReport"
#End Region

    Public Function GetSupplierGroup(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim oReturnValue As New Parameter.SupplierGroup
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SupplierGroup.GetSupplierGroup")
        End Try
    End Function

    Public Function GetSupplierGroupReport(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim oReturnValue As New Parameter.SupplierGroup
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SupplierGroup.GetSupplierGroupReport")
        End Try
    End Function

    Public Function GetSupplierGroupList(ByVal ocustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SupplierGroup
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SupplierGroupID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.SupplierGroupID = reader("SupplierGroupID").ToString
                oReturnValue.SupplierGroupName = reader("SupplierGroupName").ToString
                oReturnValue.SupplierGroupPlafond = CDec(reader("SupplierGroupPlafond").ToString)
                oReturnValue.NPWP = reader("NPWP").ToString
                oReturnValue.TDP = reader("TDP").ToString
                oReturnValue.SIUP = reader("SIUP").ToString
                oReturnValue.Alamat = reader("Alamat").ToString
                oReturnValue.RT = reader("RT").ToString
                oReturnValue.RW = reader("RW").ToString
                oReturnValue.Kelurahan = reader("Kelurahan").ToString
                oReturnValue.Kecamatan = reader("Kecamatan").ToString
                oReturnValue.Kota = reader("Kota").ToString
                oReturnValue.KodePos = reader("KodePos").ToString
                oReturnValue.AreaPhone1 = reader("AreaPhone1").ToString
                oReturnValue.Phone1 = reader("Phone1").ToString
                oReturnValue.AreaPhone2 = reader("AreaPhone2").ToString
                oReturnValue.Phone2 = reader("Phone2").ToString
                oReturnValue.AreaFax = reader("AreaFax").ToString
                oReturnValue.Fax = reader("Fax").ToString
                oReturnValue.ContactPerson = reader("ContactPerson").ToString
                oReturnValue.JabatanID = reader("JabatanID").ToString
                oReturnValue.Email = reader("Email").ToString
                oReturnValue.NoHP = reader("NoHP").ToString
                oReturnValue.Notes = reader("Notes").ToString

            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SupplierGroup.GetSupplierGroupList")
        End Try
    End Function

    Public Function SupplierGroupSaveAdd(ByVal ocustomClass As Parameter.SupplierGroup) As String

        If (IsSupplierGroupNameDup(ocustomClass.strConnection, ocustomClass.SupplierGroupName)) Then
            Throw New Exception(String.Format("Nama Supplier Group {0} sudah terdaftar", ocustomClass.SupplierGroupName))
        End If

        Dim ErrMessage As String = ""
        Dim params(25) As SqlParameter


        params(0) = New SqlParameter("@SupplierGroupName", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.SupplierGroupName
        params(1) = New SqlParameter("@SupplierGroupPlafond", SqlDbType.Decimal)
        params(1).Value = ocustomClass.SupplierGroupPlafond
        params(2) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.NPWP
        params(3) = New SqlParameter("@TDP", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.TDP
        params(4) = New SqlParameter("@SIUP", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.SIUP
        params(5) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.Alamat
        params(6) = New SqlParameter("@RT", SqlDbType.Char, 4)
        params(6).Value = ocustomClass.RT
        params(7) = New SqlParameter("@RW", SqlDbType.Char, 4)
        params(7).Value = ocustomClass.RW
        params(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
        params(8).Value = ocustomClass.Kelurahan
        params(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
        params(9).Value = ocustomClass.Kecamatan
        params(10) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
        params(10).Value = ocustomClass.Kota
        params(11) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(11).Value = ocustomClass.KodePos
        params(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(12).Value = ocustomClass.AreaPhone1
        params(13) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(13).Value = ocustomClass.Phone1
        params(14) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(14).Value = ocustomClass.AreaPhone2
        params(15) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(15).Value = ocustomClass.Phone2
        params(16) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(16).Value = ocustomClass.AreaFax
        params(17) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(17).Value = ocustomClass.Fax
        params(18) = New SqlParameter("@ContactPerson", SqlDbType.VarChar, 50)
        params(18).Value = ocustomClass.ContactPerson
        params(19) = New SqlParameter("@JabatanID", SqlDbType.VarChar, 10)
        params(19).Value = ocustomClass.JabatanID
        params(20) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
        params(20).Value = ocustomClass.Email
        params(21) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(21).Value = ocustomClass.NoHP
        params(22) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(22).Value = ocustomClass.Notes
        params(23) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(23).Value = ocustomClass.BranchId
        params(24) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(24).Value = ocustomClass.BusinessDate
        params(25) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(25).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(25).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SupplierGroup.SupplierGroupSaveAdd")
        End Try
    End Function

    Function IsSupplierGroupNameDup(ByVal cnn As String, SupplierGroupName As String) As Boolean
        Dim query = "select top 1 1 from SupplierGroup  where  SupplierGroupName=@SupplierGroupName "
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(cnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@SupplierGroupName", SupplierGroupName)}))
        Return ret > 0
    End Function

    Public Sub SupplierGroupSaveEdit(ByVal ocustomClass As Parameter.SupplierGroup)

        Dim params(23) As SqlParameter
        params(0) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SupplierGroupID
        params(1) = New SqlParameter("@SupplierGroupName", SqlDbType.Char, 50)
        params(1).Value = ocustomClass.SupplierGroupName
        params(2) = New SqlParameter("@SupplierGroupPlafond", SqlDbType.Decimal)
        params(2).Value = ocustomClass.SupplierGroupPlafond
        params(3) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.NPWP
        params(4) = New SqlParameter("@TDP", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.TDP
        params(5) = New SqlParameter("@SIUP", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.SIUP
        params(6) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.Alamat
        params(7) = New SqlParameter("@RT", SqlDbType.Char, 4)
        params(7).Value = ocustomClass.RT
        params(8) = New SqlParameter("@RW", SqlDbType.Char, 4)
        params(8).Value = ocustomClass.RW
        params(9) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
        params(9).Value = ocustomClass.Kelurahan
        params(10) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
        params(10).Value = ocustomClass.Kecamatan
        params(11) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
        params(11).Value = ocustomClass.Kota
        params(12) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(12).Value = ocustomClass.KodePos
        params(13) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(13).Value = ocustomClass.AreaPhone1
        params(14) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(14).Value = ocustomClass.Phone1
        params(15) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(15).Value = ocustomClass.AreaPhone2
        params(16) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(16).Value = ocustomClass.Phone2
        params(17) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(17).Value = ocustomClass.AreaFax
        params(18) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(18).Value = ocustomClass.Fax
        params(19) = New SqlParameter("@ContactPerson", SqlDbType.VarChar, 50)
        params(19).Value = ocustomClass.ContactPerson
        params(20) = New SqlParameter("@JabatanID", SqlDbType.VarChar, 10)
        params(20).Value = ocustomClass.JabatanID
        params(21) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
        params(21).Value = ocustomClass.Email
        params(22) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(22).Value = ocustomClass.NoHP
        params(23) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(23).Value = ocustomClass.Notes
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SupplierGroup.SupplierGroupSaveEdit")
        End Try
    End Sub

    Public Function SupplierGroupDelete(ByVal ocustomClass As Parameter.SupplierGroup) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SupplierGroupID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.SupplierGroup.SupplierGroupDelete")
        End Try
    End Function

    Public Function GetJobPositionCombo(ByVal customclass As Parameter.SupplierGroup) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_JOB_ALL).Tables(0)
    End Function
End Class
