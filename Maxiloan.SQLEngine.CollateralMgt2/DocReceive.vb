
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.DBNull
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class DocReceive : Inherits DataAccessBase

    Private Const spDocRec As String = "spDocReceiveList"
    Private Const spFLoc As String = "spGetFLoc"
    Private Const spDocList As String = "spDocRecList"
    Private Const spDocAssetBNIListPaging As String = "spDocAssetBNIListPaging"
    Private Const SPDocAssetBNIList As String = "SPDocAssetBNIList"
    Private Const SPInqDocAssetBNIList As String = "SPInqDocAssetBNIList"
    Private Const spDocRecSave As String = "spDocReceiveSave"
    Private Const spDocReceiveJanjiSave As String = "spDocReceiveJanjiSave"
    Private Const spDocRecSaveH As String = "spDocReceiveSaveH"
    Private Const spDocPledgeRecSave As String = "spDocPledgeRecSave"
    Private Const spDocBorrow As String = "spDocBorrowSaveH"
    Private Const spDocBorowPledging As String = "spDocBorowPledgingSaveH"
    Private Const spDocRelease As String = "spDocReleaseSave"
    Private Const spChangeLoc As String = "spDocChangeLocSave"
    Private Const spRackBranch As String = "spGetRackBranch"
    Private Const spWait As String = "spDocWaitInqPaging"
    Private Const spRackEdit As String = "spDocRackEdit"
    Private Const spRackAdd As String = "spDocRackAdd"
    Private Const spRackDelete As String = "spDocRackDelete"

    Private Const spFillEdit As String = "spDocFillEdit"
    Private Const spFillAdd As String = "spDocFillAdd"
    Private Const spFillDelete As String = "spDocFillDelete"
    Private Const spNotExistsMainDoc As String = "spReportNotExistsMainDoc"
    Private Const spInfo As String = "spDocReleaseInfo"
    Private Const spGetBPKBPosition As String = "spGetBPKBPosition"
    Private Const SPSaveDocAssetBNI As String = "SPSaveDocAssetBNI"
    Private Const spGetBranchName As String = "spGetBranch"
    Private Const spSummaryBPKBReport As String = "spReportRekapitulasi"
    Private Const spAgreementListAssetDocWithDrawal As String = "spAgreementListAssetDocWithDrawal"
    Private Const spAssetDocPledgeProcess As String = "spAssetDocPledgeProcess"
    Private Const spGetSPAssetDocument As String = "spGetSPAssetDocument"
    Private Const spSavePrintSPAssetDoc As String = "spSavePrintSPAssetDoc"
    Private Const spSavePrintSPPADoc As String = "spSavePrintSPPAD"
    Private Const spDocSaveEditH As String = "spDocSaveEditH"
    Private Const spDocSaveEditD As String = "spDocSaveEditD"
    Private Const spSPPADList As String = "spSelectSPPAD"



#Region "ListDocReceive"
    Public Function ListDocReceive(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationId

            params(1) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(2).Value = customclass.AssetSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spDocRec, params)
            If objread.Read Then
                With customclass
                    .ApplicationId = CType(objread("applicationid"), String)
                    .AgreementNo = CType(objread("agreementno"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .Customername = CType(objread("customername"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .Supplierid = CType(objread("supplierid"), String)
                    .SupplierName = CType(objread("SupplierName"), String)
                    .assetDesc = CType(objread("Assetdesc"), String)
                    .KondisiAset = CType(objread("KondisiAset"), String)
                    .AssetDocStatus = CType(objread("Assetdocstatus"), String)
                    .seriallabel1 = CType(objread("serialno1label"), String)
                    .seriallabel2 = CType(objread("serialno2label"), String)
                    .EngineNo = CType(objread("content2"), String)
                    .LicensePlate = CType(objread("licenseplate"), String)
                    .ChasisNo = CType(objread("content1"), String)
                    .RackLoc = CType(objread("rackdesc"), String)
                    .RackLocID = CType(objread("assetdocrack"), String)
                    .FLocID = CType(objread("assetdocfilling"), String)
                    .FillingLoc = CType(objread("fillingdesc"), String)
                    .Itaxdate = CType(objread("intaxdate"), String)
                    If .Itaxdate = "1" Then
                        .taxdate = CType(objread("taxdate"), Date)
                    End If
                    .CrossDefault = CType(objread("crossdefault"), String)
                    .FundingCoName = CType(objread("FundingCoName"), String)
                    .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    .BorrowBy = CType(objread("BorrowerName"), String)
                    .BorrowDate = CType(objread("BorrowDate"), Date)
                    .RackDesc = CType(objread("rackdesc"), String)
                    .FillingDesc = CType(objread("fillingdesc"), String)
                    .NamaBPKBSama = CBool(objread("NamaBPKBSama"))
                    .DocInFunding = CBool(objread("DocInFunding"))
                    .Color = CType(objread("Color"), String)
                    .BranchOnStatus = CType(objread("BranchOnStatus"), String)
                    .AssetUsage = CType(objread("AssetUsage"), String)
                    .AssetUsageDesc = CType(objread("AssetUsageDesc"), String)
                    '.IsIzinTrayek = CInt(objread("IsIzinTrayek"))
                    .IsIzinTrayek = CBool(objread("IsIzinTrayek"))
                    .AssetTypeID = CType(objread("assettypeid"), String)
                End With
            End If

            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "ListDocReceive", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ListDocAssetBNI"
    Public Function ListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId



            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPDocAssetBNIList, params)
            If objread.Read Then
                With customclass
                    .NoPolisi = CType(objread("NoPolisi"), String)
                    .AgreementNo = CType(objread("AgreementNo"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .NoRangka = CType(objread("NoRangka"), String)
                    .NamaDiBPKB = CType(objread("NamaDiBPKB"), String)
                    .TglTerimaBPKB = CType(objread("TglTerimaBPKB"), String)
                    .NoMesin = CType(objread("NoMesin"), String)
                    .NoBPKB = CType(objread("NoBPKB"), String)
                    .Nasabah = CType(objread("Nasabah"), String)
                    .FillingId = CType(objread("FillingId"), String)
                    .RAKId = CType(objread("RAKId"), String)


                    'If .Itaxdate = "1" Then
                    '    .taxdate = CType(objread("taxdate"), Date)
                    'End If
                    '.CrossDefault = CType(objread("crossdefault"), String)
                    '.FundingCoName = CType(objread("FundingCoName"), String)
                    '.FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    '.BorrowBy = CType(objread("BorrowerName"), String)
                    '.BorrowDate = CType(objread("BorrowDate"), Date)
                    '.RackDesc = CType(objread("rackdesc"), String)
                    '.FillingDesc = CType(objread("fillingdesc"), String)
                    '.NamaBPKBSama = CBool(objread("NamaBPKBSama"))
                    '.DocInFunding = CBool(objread("DocInFunding"))
                    '.Color = CType(objread("Color"), String)
                    '.BranchOnStatus = CType(objread("BranchOnStatus"), String)
                    '.AssetUsage = CType(objread("AssetUsage"), String)
                    '.AssetUsageDesc = CType(objread("AssetUsageDesc"), String)
                    ''.IsIzinTrayek = CInt(objread("IsIzinTrayek"))
                    '.IsIzinTrayek = CBool(objread("IsIzinTrayek"))
                    '.AssetTypeID = CType(objread("assettypeid"), String)
                End With
            End If

            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "ListDocAssetBNI", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "InqListDocAssetBNI"
    Public Function InqListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId



            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPInqDocAssetBNIList, params)
            If objread.Read Then
                With customclass
                    .NoPolisi = CType(objread("NoPolisi"), String)
                    .AgreementNo = CType(objread("AgreementNo"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .NoRangka = CType(objread("NoRangka"), String)
                    .NamaDiBPKB = CType(objread("NamaDiBPKB"), String)
                    .TglTerimaBPKB = CType(objread("TglTerimaBPKB"), String)
                    .NoMesin = CType(objread("NoMesin"), String)
                    .NoBPKB = CType(objread("NoBPKB"), String)
                    .CatatanClose = CType(objread("CatatanClose"), String)
                    .TglClose = CType(objread("TglClose"), String)
                    .Nasabah = CType(objread("Nasabah"), String)
                    .FillingId = CType(objread("FillingId"), String)
                    .RAKId = CType(objread("RAKId"), String)

                End With
            End If

            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "InqListDocAssetBNI", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetFLoc"

    Public Function GetFLoc(ByVal customclass As Parameter.DocRec) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(0).Value = customclass.WhereCond

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFLoc, params).Tables(0)
        Catch exp As Exception
            WriteException("DocReceive", "GetFLoc", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "GetRack"

    Public Function GetRack(ByVal strConnection As String, ByVal strBranch As String, Optional isFund As Boolean = False) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))


        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 200)
        params(0).Value = strBranch

        params(1) = New SqlParameter("@isFund", SqlDbType.Bit)
        params(1).Value = isFund

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spGetRack", params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DocReceive", "GetRack", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "DocListPaging"

    Public Function DocListPaging(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.listDoc = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDocList, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "DocListPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "DocListPagingAssetBNI"

    Public Function DocListPagingAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.Listdoc = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDocAssetBNIListPaging, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "DocListPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "SPPAD List"
    Public Function SPPADList(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.listDoc = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSPPADList, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("SPPADList", "SPPADList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "SaveDocReceive"

    Public Function SaveDocReceive(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        'Dim params() As SqlParameter
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim params2 As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            Dim parameters As IList(Of SqlParameter)
            If customclass.listData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.listData.Rows.Count - 1
                    parameters = New List(Of SqlParameter)


                    parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId})
                    parameters.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationId})
                    parameters.Add(New SqlParameter("@StatusDate", SqlDbType.DateTime) With {.Value = customclass.BusinessDate})
                    parameters.Add(New SqlParameter("@DocReceiveddate", SqlDbType.DateTime) With {.Value = customclass.ReceiveDate})
                    parameters.Add(New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50) With {.Value = customclass.listData.Rows(intLoopOmset).Item("DocNo")})
                    parameters.Add(New SqlParameter("@Receivedfrom", SqlDbType.VarChar, 50) With {.Value = customclass.ReceiveFrom})
                    parameters.Add(New SqlParameter("@AssetDocStatus", SqlDbType.Char, 1) With {.Value = customclass.listData.Rows(intLoopOmset).Item("AssetDocStatus")})
                    parameters.Add(New SqlParameter("@AssetDocID", SqlDbType.Char, 10) With {.Value = customclass.listData.Rows(intLoopOmset).Item("AssetDocID")})
                    parameters.Add(New SqlParameter("@AssetseqNo", SqlDbType.Int) With {.Value = customclass.AssetSeqNo})
                    parameters.Add(New SqlParameter("@DocumentDate", SqlDbType.DateTime) With {.Value = customclass.listData.Rows(intLoopOmset).Item("DocDate")})
                    parameters.Add(New SqlParameter("@strerror", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output})
                    If (customclass.DocPinjamDariBank = "GF") Then
                        parameters.Add(New SqlParameter("@ExpectedReturnedDate ", SqlDbType.DateTime) With {.Value = customclass.TanggalKembali})
                    End If
                    parameters.Add(New SqlParameter("@AssetDocRack", SqlDbType.VarChar, 20) With {.Value = customclass.RackLoc})
                    parameters.Add(New SqlParameter("@AssetDocFiling", SqlDbType.VarChar, 20) With {.Value = customclass.FillingLoc})

                    parameters.Add(New SqlParameter("@PromiseDate", SqlDbType.DateTime) With {.Value = customclass.listData.Rows(intLoopOmset).Item("PromiseDate")})
                    parameters.Add(New SqlParameter("@FollowUpAction", SqlDbType.VarChar, 20) With {.Value = customclass.listData.Rows(intLoopOmset).Item("FollowUpAction")})
                    parameters.Add(New SqlParameter("@BranchOnStatus", SqlDbType.Char, 3) With {.Value = customclass.BranchOnStatus})

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocRecSave, parameters.ToArray)
                Next
            End If

            params2.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customclass.BranchId}) 
            params2.Add( New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)With {.Value = customclass.ApplicationId}) 
            params2.Add( New SqlParameter("@AssetDocFilling", SqlDbType.VarChar, 10)With {.Value = customclass.FillingLoc}) 
            params2.Add(New SqlParameter("@AssetDocRack", SqlDbType.VarChar, 10) With {.Value = customclass.RackLoc})

            params2.Add(New SqlParameter("@Businessdate", SqlDbType.DateTime) With {.Value = customclass.BusinessDate})
            params2.Add(New SqlParameter("@AssetseqNo", SqlDbType.Int) With {.Value = customclass.AssetSeqNo})
            Dim outp = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params2.Add(outp)
             
            If (customclass.IsTbo) Then
                'UPDATE HEADER TERIMA DOKUMENT TBO
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spDocReceiveTboSaveH", params2.ToArray)
            Else
                params2.Add(New SqlParameter("@isMainDoc", SqlDbType.Bit) With {.Value = CByte(customclass.isMainDoc)})
                params2.Add(New SqlParameter("@LicensePlate", SqlDbType.VarChar, 50) With {.Value = customclass.LicensePlate})
                params2.Add(New SqlParameter("@TaxDate", SqlDbType.DateTime) With {.Value = customclass.taxdate})
                params2.Add(New SqlParameter("@AssetDocID", SqlDbType.VarChar, 8000) With {.Value = customclass.strGroupId})
                params2.Add(New SqlParameter("@OwnerAsset", SqlDbType.VarChar, 50) With {.Value = customclass.OwnerAsset})
                params2.Add(New SqlParameter("@OwnerAddress", SqlDbType.VarChar, 100) With {.Value = customclass.OwnerAddress})
                params2.Add(New SqlParameter("@OwnerKelurahan", SqlDbType.VarChar, 30) With {.Value = customclass.OwnerKelurahan})
                params2.Add(New SqlParameter("@OwnerKecamatan", SqlDbType.VarChar, 30) With {.Value = customclass.OwnerKecamatan})
                params2.Add(New SqlParameter("@OwnerCity", SqlDbType.VarChar, 30) With {.Value = customclass.OwnerCity})
                params2.Add(New SqlParameter("@OwnerRT", SqlDbType.Char, 3) With {.Value = customclass.OwnerRT})
                params2.Add(New SqlParameter("@OwnerRW", SqlDbType.Char, 3) With {.Value = customclass.OwnerRW})
                params2.Add(New SqlParameter("@Notes", SqlDbType.VarChar, 255) With {.Value = customclass.Notes})
                params2.Add(New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50) With {.Value = customclass.ChasisNo})
                params2.Add(New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50) With {.Value = customclass.EngineNo})

                If (customclass.DocPinjamDariBank = "GF" Or customclass.DocPinjamDariBank = "FG") Then
                    'PINJAM DARI BANK
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spDocOnCreditorSave", params2.ToArray)
                Else
                    params2.Add(New SqlParameter("@BranchOnStatus", SqlDbType.Char, 3) With {.Value = customclass.BranchOnStatus})
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocRecSaveH, params2.ToArray)
                End If
            End If


            transaction.Commit()
            customclass.strError = CStr(outp.Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "SaveDocReceive", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Receive")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function SaveDocReceiveJanji(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        'Dim params() As SqlParameter
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            Dim parameters As IList(Of SqlParameter)
            If customclass.listData.Rows.Count > 0 Then
                For intLoopOmset = 0 To customclass.listData.Rows.Count - 1
                    parameters = New List(Of SqlParameter)

                    parameters.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationId})
                    parameters.Add(New SqlParameter("@AssetDocID", SqlDbType.Char, 10) With {.Value = customclass.listData.Rows(intLoopOmset).Item("AssetDocID")})
                    parameters.Add(New SqlParameter("@AssetseqNo", SqlDbType.Int) With {.Value = customclass.AssetSeqNo})
                    parameters.Add(New SqlParameter("@PromiseDate", SqlDbType.DateTime) With {.Value = customclass.listData.Rows(intLoopOmset).Item("PromiseDate")})
                    parameters.Add(New SqlParameter("@FollowUpAction", SqlDbType.VarChar, 20) With {.Value = customclass.listData.Rows(intLoopOmset).Item("FollowUpAction")})
                    Dim outp = New SqlParameter("@strerror", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
                    parameters.Add(outp)

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocReceiveJanjiSave, parameters.ToArray)
                Next
            End If

            transaction.Commit()
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "SaveDocReceiveJanji", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Janji Document ")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region
#Region "GetAssetRegistration"
    Public Function GetAssetRegistration(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim objread As SqlDataReader

        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationId
            params(1) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spGetAssetRegistration", params)
            If objread.Read Then
                With customclass
                    .OwnerAsset = CType(objread("OwnerAsset"), String)
                    .OwnerAddress = CType(objread("OwnerAddress"), String)
                    .OwnerRT = CType(objread("OwnerRT"), String)
                    .OwnerRW = CType(objread("OwnerRW"), String)
                    .OwnerKelurahan = CType(objread("OwnerKelurahan"), String)
                    .OwnerKecamatan = CType(objread("OwnerKecamatan"), String)
                    .OwnerZipCode = CType(objread("OwnerZipCode"), String)
                    .OwnerCity = CType(objread("OwnerCity"), String)
                    .taxdate = CType(objread("TaxDate"), Date)
                    .Notes = CType(objread("Notes"), String)
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "GetAssetRegistration", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Getting Asset Registration")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region
#Region "GetTaxDate"
    Public Function GetTaxDate(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim objread As SqlDataReader

        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationId
            params(1) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spGetTaxDate", params)
            If objread.Read Then
                With customclass
                    .taxdate = CType(objread("TaxDate"), Date)
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "GetTaxDate", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Getting Tax Date From Agreement Asset")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region
#Region "GetBranchName"
    Public Function GetBranchName(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim objread As SqlDataReader

        Try
            params(0) = New SqlParameter("@Branchid", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spGetBranchName, params)
            If objread.Read Then
                With customclass
                    .BranchName = CType(objread("Name"), String)
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "GetBranchName", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Getting BranchName  From Agreement Asset")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region
#Region "DocBorrow"
    Public Function SaveDocBorrow(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(10) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationId

            params(2) = New SqlParameter("@Borrowername", SqlDbType.VarChar, 50)
            params(2).Value = customclass.BorrowBy

            params(3) = New SqlParameter("@BorrowDate", SqlDbType.DateTime)
            params(3).Value = customclass.BorrowDate

            params(4) = New SqlParameter("@ExpectedReturnedDate", SqlDbType.DateTime)
            params(4).Value = customclass.ReturnDate

            params(5) = New SqlParameter("@isMainDoc", SqlDbType.Bit)
            params(5).Value = CByte(customclass.isMainDoc)

            params(6) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 8000)
            params(6).Value = customclass.strGroupId

            params(7) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
            params(7).Value = customclass.BusinessDate

            params(8) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(9).Value = customclass.AssetSeqNo

            params(10) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(10).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocBorrow, params)

            transaction.Commit()
            customclass.strError = CStr(params(10).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "SaveDocBorrow", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Borrow")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region

#Region "DocBorrowPledging(Dijaminkan)"
    Public Function SaveDocBorrowPledging(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(10) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationId

            params(2) = New SqlParameter("@Borrowername", SqlDbType.VarChar, 50)
            params(2).Value = customclass.BorrowBy

            params(3) = New SqlParameter("@BorrowDate", SqlDbType.DateTime)
            params(3).Value = customclass.BorrowDate

            params(4) = New SqlParameter("@ExpectedReturnedDate", SqlDbType.DateTime)
            params(4).Value = customclass.ReturnDate

            params(5) = New SqlParameter("@isMainDoc", SqlDbType.Bit)
            params(5).Value = CByte(customclass.isMainDoc)

            params(6) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 8000)
            params(6).Value = customclass.strGroupId

            params(7) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
            params(7).Value = customclass.BusinessDate

            params(8) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(9).Value = customclass.AssetSeqNo

            params(10) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(10).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocBorowPledging, params)

            transaction.Commit()
            customclass.strError = CStr(params(10).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "SaveDocBorrowPledging", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Borrow")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region
#Region "DocRelease"
#Region "SaveDocRelease"

    Public Function SaveDocRelease(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationId

            params(2) = New SqlParameter("@ReleaseDate", SqlDbType.DateTime)
            params(2).Value = customclass.ReleaseDate

            params(3) = New SqlParameter("@isMainDoc", SqlDbType.Bit)
            params(3).Value = CByte(customclass.isMainDoc)

            params(4) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 8000)
            params(4).Value = customclass.strGroupId

            params(5) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
            params(5).Value = customclass.BusinessDate

            params(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(6).Value = customclass.Notes

            params(7) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(7).Value = customclass.AssetSeqNo

            params(8) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocRelease, params)

            transaction.Commit()
            customclass.strError = CStr(params(8).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "SaveDocRelease", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Release")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function

#End Region
#Region "ListInfo"

    Public Function ListInfo(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.listDoc = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInfo, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "ListInfo", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "PrintBPKBSerahTerima"

    Public Function PrintBPKBSerahTerima(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationId

            customclass.listReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spDocSerahTerimaBPKB", params)
            Return customclass
        Catch exp As Exception
            WriteException("BPKBSerahTerima", "BPKBSerahTerima", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#End Region
#Region "Change Location"
    Public Function SaveDocChangeLoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationId

            params(2) = New SqlParameter("@AssetDocRack", SqlDbType.VarChar, 10)
            params(2).Value = customclass.RackLoc

            params(3) = New SqlParameter("@AssetDocFilling", SqlDbType.VarChar, 10)
            params(3).Value = customclass.FillingLoc

            params(4) = New SqlParameter("@assetseqno", SqlDbType.Int)
            params(4).Value = customclass.AssetSeqNo

            params(5) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
            params(5).Value = customclass.BusinessDate

            params(6) = New SqlParameter("@BranchInTransit", SqlDbType.Char, 3)
            params(6).Value = customclass.BranchInTransit

            params(7) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(7).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spChangeLoc, params)
            customclass.strError = CStr(params(7).Value)
            objtrans.Commit()
            Return customclass
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("DocReceive", "SaveDocChangeLoc", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Location")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Function
#End Region
#Region "Inquiry"
    Public Function GetRackBranch(ByVal customclass As Parameter.DocRec) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(0).Value = customclass.WhereCond

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRackBranch, params).Tables(0)
        Catch exp As Exception
            WriteException("DocReceive", "GetRackBranch", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetAssetHistory(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationId
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetAssetHistory", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "GetAssetHistory", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ListInquiry(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spWait, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "ListInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, businessdate As DateTime, aging As DateTime, where As String) As Parameter.DocRec
        Dim params = New List(Of SqlParameter)
        Dim customclass As New Parameter.DocRec
        Try
            params.Add(New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int) With {.Value = currentPage})
            params.Add(New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int) With {.Value = PageSize})
            params.Add(New SqlParameter("@branchid", SqlDbType.VarChar, 3) With {.Value = branchid})
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = businessdate})
            params.Add(New SqlParameter("@WhereStr", SqlDbType.VarChar, 200) With {.Value = where})
            If (aging.Year > 1900) Then
                params.Add(New SqlParameter("@aging", SqlDbType.DateTime) With {.Value = aging})
            End If

            customclass.listData = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spDocTboInqPaging", params.ToArray()).Tables(0)
            customclass.TotalRecord = 0
            If (customclass.listData.Rows.Count > 0) Then
                customclass.TotalRecord = customclass.listData.Rows(0)("totalRow")
            End If 
        Catch exp As Exception
            WriteException("DocReceive", "ListInquiry", exp.Message + exp.StackTrace)
        End Try

        Return customclass
    End Function


    Public Function GetBPKBPosition(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim objread As SqlDataReader
        Dim oReturnvalue As New Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@sesBranchID", SqlDbType.Char, 3)
            params(1).Value = customclass.sesBranchID
            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationId
            params(3) = New SqlParameter("@BPKBBranchID", SqlDbType.Char, 3)
            params(3).Direction = ParameterDirection.Output
            params(4) = New SqlParameter("@BPKBBranchName", SqlDbType.Char, 50)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spGetBPKBPosition, params)

            oReturnvalue.BranchId = CType(params(3).Value, String)
            oReturnvalue.BranchName = CType(params(4).Value, String)
            Return oReturnvalue
        Catch exp As Exception
            WriteException("DocReceive", "GetBPKBPosition", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SaveDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim objread As SqlDataReader
        Dim oReturnvalue As New Parameter.DocAssetBNI
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try

            params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo
            params(1) = New SqlParameter("@TglClose", SqlDbType.Date)
            params(1).Value = customclass.TglClose
            params(2) = New SqlParameter("@CatatanClose", SqlDbType.Char, 100)
            params(2).Value = customclass.CatatanClose


            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, SPSaveDocAssetBNI, params)

            'oReturnvalue.BranchId = CType(params(3).Value, String)
            'oReturnvalue.BranchName = CType(params(4).Value, String)
            Return oReturnvalue
        Catch exp As Exception
            WriteException("DocReceive", "SaveDocAssetBNI", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "Setting"
#Region "AddRack"
    Public Function AddRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@RackID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.RackLocID

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params(2).Value = customclass.RackLoc

            params(3) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(3).Value = CByte(customclass.IsActive)

            params(4) = New SqlParameter("@IsFunding", SqlDbType.Bit)
            params(4).Value = CByte(customclass.IsFunding)

            params(5) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output

            If customclass.IsAdd = "1" Then
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRackAdd, params)
            ElseIf customclass.IsAdd = "2" Then
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRackEdit, params)
            End If

            transaction.Commit()
            customclass.strError = CStr(params(4).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "AddRack", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Rack Location")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region
#Region "DeleteRack"
    Public Function DeleteRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@RackID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.RackLocID

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRackDelete, params)

            transaction.Commit()
            customclass.strError = CStr(params(2).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "DeleteRack", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Delete Rack Location")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region

#Region "AddFilling"
    Public Function AddFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@RackID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.RackLocID

            params(1) = New SqlParameter("@FillingID", SqlDbType.VarChar, 10)
            params(1).Value = customclass.FLocID

            params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params(2).Value = customclass.FillingLoc

            params(3) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(3).Value = CByte(customclass.IsActive)

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            If customclass.IsAdd = "1" Then
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFillAdd, params)
            ElseIf customclass.IsAdd = "2" Then
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFillEdit, params)
            End If

            transaction.Commit()
            customclass.strError = CStr(params(4).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "AddFill", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Filling Location")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region
#Region "DeleteFill"
    Public Function DeleteFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@RackID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.RackLocID

            params(1) = New SqlParameter("@FillingID", SqlDbType.VarChar, 10)
            params(1).Value = customclass.FLocID

            params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFillDelete, params)

            transaction.Commit()
            customclass.strError = CStr(params(2).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "AddFill", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Delete Filling Location")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
#End Region
#End Region
#Region "Reports"
    Public Function NotExistsMainDocReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oReturnValue As New Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@AgingDate", SqlDbType.DateTime)
            params(1).Value = customclass.ValueDate

            oReturnValue.listReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spNotExistsMainDoc, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Report", "ReportNotExistsMainDoc", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function SummaryBPKBReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oReturnValue As New Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@Period", SqlDbType.DateTime)
            params(1).Value = customclass.Period

            oReturnValue.listReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSummaryBPKBReport, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Report", "ReportRekapitulasi", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function CreateSPAssetDocument(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oReturnValue As New Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
            params(1).Value = customclass.SortBy
            oReturnValue.listReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetSPAssetDocument, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Report", "ReportRekapitulasi", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function SavePrintSPAssetDoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.listData
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter("@SPNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("SPNo")

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintSPAssetDoc, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            ex.Message.ToString()
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
    Public Function SavePrintSPPADoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.listData
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter("@SPNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("SPNo")

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintSPPADoc, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            ex.Message.ToString()
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function AgreementListADWithDrawal(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oReturnValue As New Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@Branch", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@PODateFrom", SqlDbType.Char, 10)
        params(1).Value = customclass.PODateFrom

        params(2) = New SqlParameter("@PODateTo", SqlDbType.Char, 10)
        params(2).Value = customclass.PODateTo

        Try
            oReturnValue.listReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAgreementListAssetDocWithDrawal, params)
            Return oReturnValue

        Catch exp As Exception
            WriteException("AssetDocument", "AssetDocumentReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ListPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPemeriksaanBPKBList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListReportPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPemeriksaanBPKBPrint", params)
        Return customclass
    End Function

    Public Function ListBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBlokirBPKBList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListReportBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBlokirBPKBPrint", params)
        Return customclass
    End Function

    Public Function GetSPReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
        Return customclass
    End Function

    Public Function ListBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBukaBlokirBPKBList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListReportBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBukaBlokirBPKBPrint", params)
        Return customclass
    End Function

    Public Function ListPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPinjamNamaBPKBList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListReportPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPinjamNamaBPKBPrint", params)
        Return customclass
    End Function

#End Region
#Region "DocPledge"
    Public Function DocPledgeProcess(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Dim IntLoop As Integer

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            For IntLoop = 0 To customclass.totalAgreementPledge
                params(0) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
                params(0).Value = customclass.ArrApplicationId(IntLoop)

                params(1) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
                params(1).Value = customclass.BusinessDate

                params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
                params(2).Direction = ParameterDirection.Output

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAssetDocPledgeProcess, params)
            Next
            transaction.Commit()
            customclass.strError = CStr(params(2).Value)
            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "DocPledgeProcess", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Borrow")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
    Public Function GetDocPledgePaging(ByVal customClass As Parameter.DocRec) As Parameter.DocRec

        Dim oReturnValue As New Parameter.GeneralPaging
        Dim spName As String

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 8000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customClass.listData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            customClass.TotalRecords = CType(params(4).Value, Int64)
            Return customClass
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("DocReceive", "GetDocPledgePaging", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("DocReceive", "GetDocPledgePaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "AdditionalProcess"
    Public Function SaveEdit(ByVal customclass As Parameter.DocRec, ByVal oData1 As DataTable) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction
        Dim params2() As SqlParameter = New SqlParameter(16) {}

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params2(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params2(0).Value = customclass.BranchId

            params2(1) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params2(1).Value = customclass.ApplicationId

            params2(2) = New SqlParameter("@ChasisNo", SqlDbType.VarChar, 50)
            params2(2).Value = customclass.ChasisNo

            params2(3) = New SqlParameter("@EngineNo", SqlDbType.VarChar, 50)
            params2(3).Value = customclass.EngineNo

            params2(4) = New SqlParameter("@AssetDocStatus", SqlDbType.Char, 1)
            params2(4).Value = customclass.AssetDocStatus

            params2(5) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params2(5).Value = customclass.AssetSeqNo

            params2(6) = New SqlParameter("@TaxDate", SqlDbType.DateTime)
            params2(6).Value = customclass.taxdate

            params2(7) = New SqlParameter("@OwnerAsset", SqlDbType.VarChar, 50)
            params2(7).Value = customclass.OwnerAsset

            params2(8) = New SqlParameter("@OwnerAddress", SqlDbType.VarChar, 100)
            params2(8).Value = customclass.OwnerAddress

            params2(9) = New SqlParameter("@OwnerKelurahan", SqlDbType.VarChar, 30)
            params2(9).Value = customclass.OwnerKelurahan

            params2(10) = New SqlParameter("@OwnerKecamatan", SqlDbType.VarChar, 30)
            params2(10).Value = customclass.OwnerKecamatan

            params2(11) = New SqlParameter("@OwnerCity", SqlDbType.VarChar, 30)
            params2(11).Value = customclass.OwnerCity

            params2(12) = New SqlParameter("@OwnerRT", SqlDbType.Char, 3)
            params2(12).Value = customclass.OwnerRT

            params2(13) = New SqlParameter("@OwnerRW", SqlDbType.Char, 3)
            params2(13).Value = customclass.OwnerRW

            params2(14) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
            params2(14).Value = customclass.Notes

            params2(15) = New SqlParameter("@LicensePlate", SqlDbType.VarChar, 50)
            params2(15).Value = customclass.LicensePlate

            params2(16) = New SqlParameter("@Color", SqlDbType.VarChar, 50)
            params2(16).Value = customclass.Color
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocSaveEditH, params2)

            If customclass.AssetDocStatus <> "W" Then
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(2) = New SqlParameter("@DocNo", SqlDbType.VarChar, 50)
                params(3) = New SqlParameter("@DocDate", SqlDbType.DateTime)
                params(4) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
                params(5) = New SqlParameter("@AssetDocStatus", SqlDbType.Char, 1)
                params(6) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 20)
                params(7) = New SqlParameter("@AssetHistorySeq", SqlDbType.Int)
                params(8) = New SqlParameter("@ContentName", SqlDbType.VarChar, 50)

                If oData1.Rows.Count > 0 Then
                    For intLoopOmset = 0 To oData1.Rows.Count - 1
                        params(0).Value = customclass.BranchId
                        params(1).Value = customclass.ApplicationId
                        params(2).Value = oData1.Rows(intLoopOmset).Item("DocNo")
                        params(3).Value = CDate(oData1.Rows(intLoopOmset).Item("DocDate"))
                        params(4).Value = CDate(oData1.Rows(intLoopOmset).Item("Date"))
                        params(5).Value = oData1.Rows(intLoopOmset).Item("AssetDocStatus")
                        params(6).Value = oData1.Rows(intLoopOmset).Item("AssetDocID")
                        params(7).Value = oData1.Rows(intLoopOmset).Item("AssetHistorySeq")
                        params(8).Value = oData1.Rows(intLoopOmset).Item("ContentName")

                        If oData1.Rows(intLoopOmset).Item("AssetDocStatus") <> "W" Then
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocSaveEditD, params)
                        End If
                        If oData1.Rows(intLoopOmset).Item("AssetDocStatus") = "W" Then
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocSaveEditD, params)
                        End If
                    Next
                End If
            End If

            transaction.Commit()
            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocumentEdit", "SaveDocumentEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region
#Region "ProcessReportPrepaymentRequest"

    Public Function ProcessReportPrepaymentRequest(ByVal customClass As Parameter.DocRec) As Parameter.DocRec
        'Dim oReturnValue As New Entities.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationId
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingCoyID
        params(3) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
        params(3).Value = customClass.FundingContractNo
        params(4) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
        params(4).Value = customClass.FundingBatchNo
        params(5) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(5).Value = customClass.WhereCond
        params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(6).Value = customClass.BusinessDate
        params(7) = New SqlParameter("@DebetDate", SqlDbType.DateTime)
        params(7).Value = customClass.BorrowDate

        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception

            WriteException("DocReceive", "ProcessReportPrepaymentRequest", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "DocPledgeRecSave"

    Public Function DocPledgeRecSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@AssetDocFilling", SqlDbType.VarChar, 10)
            params(0).Value = customclass.FillingLoc

            params(1) = New SqlParameter("@AssetDocRack", SqlDbType.VarChar, 10)
            params(1).Value = customclass.RackLoc

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            params(3) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingCoyID

            params(4) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(4).Value = customclass.FundingContractNo

            params(5) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(5).Value = customclass.FundingBatchNo

            params(6) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.WhereCond

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDocPledgeRecSave, params)

            transaction.Commit()

            Return customclass

        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "DocPledgeRecSave", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Save Document Pledge Receive")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region

    Public Function getAssetDocumentStock(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@businessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, _
            CommandType.StoredProcedure, "spGetAssetDocumentStock", params)
            Return customclass
        Catch exp As Exception
            WriteException("DocReceive", "getAssetDocumentStock", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function PemeriksaanBPKBSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 5000)
            params(0).Value = customclass.WhereCond

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPemeriksaanBPKBSave", params)

            transaction.Commit()
            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "PemeriksaanBPKBSave", exp.Message + exp.StackTrace)
            Throw New Exception("PemeriksaanBPKBSave")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
    Public Function HasilCekBPKBSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationId
            params(1) = New SqlParameter("@HasilCekBPKB", SqlDbType.Bit)
            params(1).Value = CBool(customclass.HasilCekBPKB)
            params(2) = New SqlParameter("@HasilCekNote", SqlDbType.VarChar, 255)
            params(2).Value = customclass.HasilCekNote

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spHasilCekBPKBSave", params)

            transaction.Commit()
            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            WriteException("DocReceive", "HasilCekBPKBSave", exp.Message + exp.StackTrace)
            Throw New Exception("HasilCekBPKBSave")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

    End Function
    Public Function ProcessReportBBNRequest(ByVal customClass As Parameter.DocRec) As Parameter.DocRec
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationId
        params(1) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingCoyID
        params(2) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingContractNo
        params(3) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
        params(3).Value = customClass.FundingBatchNo

        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception

            WriteException("DocReceive", "ProcessReportBBNRequest", exp.Message + exp.StackTrace)
        End Try
    End Function


End Class
