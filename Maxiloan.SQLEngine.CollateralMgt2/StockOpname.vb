﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class StockOpname : Inherits Maxiloan.SQLEngine.DataAccessBase


    Public Function DoCreateStockOpname(cnn As String, rows As Parameter.StockOpDoc) As String
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@branchid", SqlDbType.Char, 3) With {.Value = rows.Branchid})
            params.Add(New SqlParameter("@OpnameDate", SqlDbType.DateTime) With {.Value = rows.OpnameDate})

            Dim stockOpnameNo = New SqlParameter("@StockOpnameNo", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            params.Add(stockOpnameNo)

            Dim prmErr = New SqlParameter("@errMsg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spStockOpnameCreate", params.ToArray)

            If (CType(prmErr.Value,String) <> "") Then
                Throw New Exception(prmErr.Value)
            End If
            Return CType(stockOpnameNo.Value, String)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return ""
    End Function

    Function opnamePageResult(cnn As String, params As List(Of SqlParameter), spName As String) As Parameter.StockOpDoc
        Dim oReturnValue As New Parameter.StockOpDoc
         
        Dim totParam = New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
        params.Add(totParam)
        Try
            oReturnValue.DataSetResult = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, spName, params.ToArray)
            oReturnValue.TotalRecords = CType(totParam.Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetResultOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        'Dim oReturnValue As New Parameter.StockOpDoc
        Dim params = New List(Of SqlParameter) 
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = currentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = pageSize})
        params.Add(New SqlParameter("@StockOpnameNo ", SqlDbType.VarChar, 20) With {.Value = opNameNo})

        Return opnamePageResult(cnn, params, "spGetResultOpnamePage")

        'Dim totParam = New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
        'params.Add(totParam)

        'Try
        '    oReturnValue.DataSetResult = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spGetResultOpnamePage", params.ToArray)
        '    oReturnValue.TotalRecords = CType(totParam.Value, Int64)
        '    Return oReturnValue
        'Catch exp As Exception
        '    Throw New Exception(exp.Message)
        'End Try
    End Function


    Public Function GetProcesResultOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String, isOnhand As Boolean) As Parameter.StockOpDoc

        Dim params = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = currentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = pageSize})
        params.Add(New SqlParameter("@StockOpnameNo ", SqlDbType.VarChar, 20) With {.Value = opNameNo})
        params.Add(New SqlParameter("@IsMacthing", SqlDbType.Bit) With {.Value = 0})

        Return opnamePageResult(cnn, params, IIf(isOnhand, "spGetResultOpnamePage", "spGetResultOpnameActPage"))

    End Function

    Public Function ProsesStockOpname(cnn As String, StockOpnameNo As String, ActStockOpnameNo As String, prosesDate As DateTime) As String
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@StockOpnameNo", SqlDbType.Char, 20) With {.Value = StockOpnameNo})
            params.Add(New SqlParameter("@StockOpnameActNo", SqlDbType.Char, 20) With {.Value = ActStockOpnameNo})
            params.Add(New SqlParameter("@prosesDate", SqlDbType.DateTime) With {.Value = prosesDate})

           
            Dim prmErr = New SqlParameter("@errMsg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spStockOpnameProses", params.ToArray)

            If (CType(prmErr.Value, String) <> "") Then
                Throw New Exception(prmErr.Value)
            End If

            Return "OK"
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return ""
    End Function 

    Public Function getOpnameNo(cnn As String, prm As DateTime, str As String) As String
        'Dim Str = "SELECT StockOpnameNo FROM  StockOpnameActualBPKBheader where OpnameDate =@OpnameDate"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, Str, New SqlParameter("@OpnameDate", SqlDbType.DateTime) With {.Value = prm})
        If (reader.HasRows) Then
            If (reader Is Nothing) Then Return ""
            If (reader.IsClosed) Then Return ""
            Dim _cID = reader.GetOrdinal("StockOpnameNo")

            While (reader.Read())
                Return IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID))
            End While
        End If
        Return ""
    End Function

    Public Function GetResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        Dim params = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = currentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = pageSize})
        params.Add(New SqlParameter("@StockOpnameNo ", SqlDbType.VarChar, 20) With {.Value = opNameNo})

        Return opnamePageResult(cnn, params, "spGetResultActualOpnamePage")

    End Function

    Public Function DocInformation(ByVal oCustomclass As Parameter.DocRec) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = oCustomclass.ApplicationId

            Return SqlHelper.ExecuteDataset(oCustomclass.strConnection, CommandType.StoredProcedure, "spDocInformation", params).Tables(0)
        Catch exp As Exception
            WriteException("StockOpname", "DocInformation", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SaveStockOpnameActual(ByVal oCustomclass As Parameter.StockOpDoc)
        Dim paramsHeader() As SqlParameter = New SqlParameter(3) {}
        Dim paramsDetail() As SqlParameter = New SqlParameter(21) {}
        Dim objcon As New SqlConnection(oCustomclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            paramsHeader(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            paramsHeader(0).Value = oCustomclass.BranchId

            paramsHeader(1) = New SqlParameter("@AssetDocStatus", SqlDbType.Char, 1)
            paramsHeader(1).Value = "O"

            paramsHeader(2) = New SqlParameter("@StockOpnameNo", SqlDbType.VarChar, 20)
            paramsHeader(2).Direction = ParameterDirection.Output

            paramsHeader(3) = New SqlParameter("@errMsg", SqlDbType.VarChar, 2000)
            paramsHeader(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spStockOpnameActualSaveH", paramsHeader)

            paramsDetail(0) = New SqlParameter("@StockOpnameNo", SqlDbType.VarChar, 25)
            paramsDetail(1) = New SqlParameter("@BranchOnStatus", SqlDbType.Char, 3)
            paramsDetail(2) = New SqlParameter("@SeqNo", SqlDbType.SmallInt)
            paramsDetail(3) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            paramsDetail(4) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            paramsDetail(5) = New SqlParameter("@AssetTypeId", SqlDbType.Char, 10)
            paramsDetail(6) = New SqlParameter("@AssetDocID", SqlDbType.Char, 10)
            paramsDetail(7) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
            paramsDetail(8) = New SqlParameter("@DocumentDate", SqlDbType.DateTime)
            paramsDetail(9) = New SqlParameter("@LicensePlate", SqlDbType.VarChar, 50)
            paramsDetail(10) = New SqlParameter("@AssetDocStatus", SqlDbType.Char, 1)
            paramsDetail(11) = New SqlParameter("@AssetDocRack", SqlDbType.Char, 10)
            paramsDetail(12) = New SqlParameter("@AssetDocFilling", SqlDbType.Char, 10)
            paramsDetail(13) = New SqlParameter("@OwnerAsset", SqlDbType.VarChar, 50)
            paramsDetail(14) = New SqlParameter("@OwnerAddress", SqlDbType.VarChar, 100)
            paramsDetail(15) = New SqlParameter("@OwnerKelurahan", SqlDbType.VarChar, 30)
            paramsDetail(16) = New SqlParameter("@OwnerKecamatan", SqlDbType.VarChar, 30)
            paramsDetail(17) = New SqlParameter("@OwnerCity", SqlDbType.VarChar, 30)
            paramsDetail(18) = New SqlParameter("@OwnerZipCode", SqlDbType.Char, 5)
            paramsDetail(19) = New SqlParameter("@IsMacthing", SqlDbType.Bit)
            paramsDetail(20) = New SqlParameter("@Descriptions", SqlDbType.VarChar, 100)
            paramsDetail(21) = New SqlParameter("@errMsg", SqlDbType.VarChar, 2000)

            For i = 0 To oCustomclass.DocInformation.Rows.Count - 1
                paramsDetail(0).Value = paramsHeader(2).Value
                paramsDetail(1).Value = oCustomclass.DocInformation.Rows(i).Item("BranchOnStatus")
                paramsDetail(2).Value = i + 1
                paramsDetail(3).Value = oCustomclass.DocInformation.Rows(i).Item("BranchId")
                paramsDetail(4).Value = oCustomclass.DocInformation.Rows(i).Item("ApplicationId")
                paramsDetail(5).Value = oCustomclass.DocInformation.Rows(i).Item("AssetTypeId")
                paramsDetail(6).Value = oCustomclass.DocInformation.Rows(i).Item("AssetDocID")
                paramsDetail(7).Value = oCustomclass.DocInformation.Rows(i).Item("DocumentNo")
                paramsDetail(8).Value = oCustomclass.DocInformation.Rows(i).Item("DocumentDate")
                paramsDetail(9).Value = oCustomclass.DocInformation.Rows(i).Item("LicensePlate")
                paramsDetail(10).Value = oCustomclass.DocInformation.Rows(i).Item("AssetDocStatus")
                paramsDetail(11).Value = oCustomclass.DocInformation.Rows(i).Item("AssetDocRack")
                paramsDetail(12).Value = oCustomclass.DocInformation.Rows(i).Item("AssetDocFilling")
                paramsDetail(13).Value = oCustomclass.DocInformation.Rows(i).Item("OwnerAsset")
                paramsDetail(14).Value = oCustomclass.DocInformation.Rows(i).Item("OwnerAddress")
                paramsDetail(15).Value = ""
                paramsDetail(16).Value = ""
                paramsDetail(17).Value = oCustomclass.DocInformation.Rows(i).Item("OwnerCity")
                paramsDetail(18).Value = oCustomclass.DocInformation.Rows(i).Item("OwnerZipCode")
                paramsDetail(19).Value = 0
                paramsDetail(20).Value = "Stock Opname Actual"
                paramsDetail(21).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spStockOpnameActualSaveD", paramsDetail)

            Next
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("StockOpname", "StockOpnameActual", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
End Class
