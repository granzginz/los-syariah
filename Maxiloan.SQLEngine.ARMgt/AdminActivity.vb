
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class AdminActivity : Inherits DataAccessBase
    Private Const spListAdmActivity As String = "spAdminActivityPaging"
    Private Const spAdmActivityDataAgreement As String = "spDeskCollReminderActivityView"
    Private Const spAdmActivityAction As String = "spListResultAction"
    Private Const spAdmActivitySave As String = "spAdminActivitySave"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"
    Protected Const PARAM_CONTACT As String = "@Contact"
    Protected Const PARAM_ISREQUEST As String = "@isRequest"
    Protected Const PARAM_PLANDATE As String = "@PlanDate"
    Protected Const PARAM_ACTIVITYID As String = "@ActivityID"
    Protected Const PARAM_PTPYDATE As String = "@PTPYDate"
    Protected Const PARAM_RESULTID As String = "@ResultID"
    Protected Const PARAM_PLANACTIVITYID As String = "@PlanActivityID"
    Protected Const PARAM_NOTES As String = "@Notes"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_HASIL As String = "@hasil"

    Public Function ListAdminActivity(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAdmActivity, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AdminActivity", "ListAdminActivity", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AdminActivityDatAgreement(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAdmActivityDataAgreement, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AdminActivity", "AdminActivityDatAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AdminActivityAction(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 10)
            params(0).Value = customclass.strKey

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAdmActivityAction, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AdminActivity", "AdminActivityAction", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AdminActivitySave(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim params() As SqlParameter = New SqlParameter(13) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_CONTACT, SqlDbType.Char, 50)
            params(2).Value = customclass.Contact

            params(3) = New SqlParameter(PARAM_ISREQUEST, SqlDbType.Bit)
            params(3).Value = customclass.isRequest

            params(4) = New SqlParameter(PARAM_PLANDATE, SqlDbType.Char, 8)
            If customclass.PlanDate = "" Then
                params(4).Value = DBNull.Value
            Else
                params(4).Value = customclass.PlanDate
            End If

            params(5) = New SqlParameter(PARAM_PTPYDATE, SqlDbType.Char, 8)
            If customclass.PTPYDate = "" Then
                params(5).Value = DBNull.Value
            Else
                params(5).Value = customclass.PTPYDate
            End If

            params(6) = New SqlParameter(PARAM_RESULTID, SqlDbType.Char, 10)
            params(6).Value = customclass.ResultID

            params(7) = New SqlParameter(PARAM_ACTIVITYID, SqlDbType.Char, 10)
            params(7).Value = customclass.ActivityID

            params(8) = New SqlParameter(PARAM_PLANACTIVITYID, SqlDbType.Char, 10)
            params(8).Value = customclass.PlanActivityID

            'params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.Char, 100)
            params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.Char, 500)
            params(9).Value = customclass.Notes

            params(10) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(10).Value = customclass.BusinessDate

            params(11) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(11).Value = customclass.CGID

            params(12) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(12).Value = customclass.LoginId

            params(13) = New SqlParameter(PARAM_HASIL, SqlDbType.Char, 50)
            params(13).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spAdmActivitySave, params)
            customclass.Hasil = CInt(params(13).Value)
            objtrans.Commit()
            Return customclass
        Catch exp As Exception
            customclass.Hasil = 0
            objtrans.Rollback()
            Throw New Exception(exp.Message)
            Return customclass
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Function
End Class
