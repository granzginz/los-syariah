

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class DeskCollReminderActivity : Inherits DataAccessBase
    Private Const spDeskCollRemindActList As String = "spDeskCollRemiderActivityPaging"
    Private Const spDeskCollRemindActView As String = "spDeskCollReminderActivityView"
    Private Const spListResultAction As String = "spListResultAction"
    Private Const spSaveDeskCollActivity As String = "spSaveDeskCollReminderActivity"
    Private Const spSaveDeskCollODActivity As String = "spSaveDeskCollOverDueActivity"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"
    Protected Const PARAM_RESULTID As String = "@ResultID"
    Protected Const PARAM_PLANDATE As String = "@PlanDate"
    Protected Const PARAM_PTPYDATE As String = "@PTPYdate"
    Protected Const PARAM_PLANACTIVITYID As String = "@PlanACtivityID"
    Protected Const PARAM_ACTIONID As String = "@ActionID"
    Protected Const PARAM_ISREQUESTASSIGN As String = "@isRequestAssign"
    Protected Const PARAM_SUCCESS As String = "@Succes"
    Protected Const PARAM_DESKCOLLID As String = "@DeskCollID"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_NOTES As String = "@Notes"

    Public Function ListDeskCollReminder(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            'params(4) = New SqlParameter("@Collector", SqlDbType.VarChar, 1000)
            'params(4).Value = customclass.Collector

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output



            customclass.ListDeskColl = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDeskCollRemindActList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("DeskCollReminderActivity", "ListDeskCollReminder", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListDeskCollReminderView(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID
            customclass.ListDeskColl = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDeskCollRemindActView, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DeskCollReminderActivity", "ListDeskCollReminder", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListResultAction(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 10)
            params(0).Value = customclass.strKey
            customclass.ListDeskColl = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListResultAction, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DeskCollReminderActivity", "ListResultAction", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function SaveDeskCollActivity(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim str, spName As String

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_RESULTID, SqlDbType.Char, 10)
            params(2).Value = customclass.ResultID


            params(3) = New SqlParameter(PARAM_PLANDATE, SqlDbType.Char, 20)
            If customclass.PlanDate = "" Then
                params(3).Value = DBNull.Value
            Else
                params(3).Value = customclass.PlanDate
            End If


            params(4) = New SqlParameter(PARAM_PTPYDATE, SqlDbType.Char, 20)
            If customclass.PTPYDate = "" Then
                params(4).Value = DBNull.Value
            Else
                params(4).Value = customclass.PTPYDate
            End If


            params(5) = New SqlParameter(PARAM_PLANACTIVITYID, SqlDbType.Char, 10)
            If customclass.PlanActivityID = "" Then
                params(5).Value = DBNull.Value
            Else
                params(5).Value = customclass.PlanActivityID
            End If


            params(6) = New SqlParameter(PARAM_ACTIONID, SqlDbType.Char, 10)
            params(6).Value = customclass.ActionID

            params(7) = New SqlParameter(PARAM_ISREQUESTASSIGN, SqlDbType.Bit)
            params(7).Value = customclass.isRequestAssign

            params(8) = New SqlParameter(PARAM_SUCCESS, SqlDbType.Char, 50)
            params(8).Value = customclass.Contact

            params(9) = New SqlParameter(PARAM_DESKCOLLID, SqlDbType.Char, 10)
            params(9).Value = customclass.DeskCollID

            params(10) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(10).Value = customclass.BusinessDate

            params(11) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 500)
            params(11).Value = customclass.Notes

            params(12) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(12).Direction = ParameterDirection.Output

            str = customclass.strKey
            If str = "RI" Then
                spName = spSaveDeskCollActivity
            Else
                spName = spSaveDeskCollODActivity
            End If
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spName, params)
            customclass.hasil = CInt(params(12).Value)
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            customclass.hasil = 0
            oSaveTransaction.Rollback()
            WriteException("DeskCollReminderActivity", "ListResultAction", exp.Message + exp.StackTrace)
            Return customclass
        End Try
    End Function


    Public Function DownloadSMSReminderList(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 100)
            params(0).Value = customclass.WhereCond
            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spDownloadSMSReminderList", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DeskCollReminderActivity", "DownloadSMSReminderList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DownloadSMSReminderPaging(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spDownloadSMSReminderPaging", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("DeskCollReminderActivity", "DownloadSMSReminderPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
