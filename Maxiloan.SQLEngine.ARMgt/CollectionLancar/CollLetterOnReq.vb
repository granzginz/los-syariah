
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class CollLetterOnReq : Inherits DataAccessBase
#Region " Private Const "
    Private Const COLLLETTER_ON_REQUEST_PAGING As String = "spCollLetterOnReqPaging"
    Private Const COLLLETTER_ON_REQUEST_VIEW As String = "spCollLetterOnReqView"
    Private Const COLLETTER_ON_REQUEST_VIEW_LETTER As String = "spCollLetterOnReqViewLETTER"
    Private Const COLLETTER_ON_REQUEST_COMBO As String = "spCollLetterOnReqCombo"
    Private Const COLLETTER_ON_REQUEST_COMBO_VIEW As String = "spCollLetterOnReqComboView"
    Private Const COLLLETTER_ON_REQUEST_TEMPLATE_FIELD As String = "Select TagName From CollLetterTemplateField"
    Private Const COLL_LETTER_ON_REQ_SAVING As String = "spCollLetterOnReqSavingTextToPrint"
    Private Const COLL_LETTER_ON_REQ_VIEW_TEXT_TO_PRINT As String = "spCollLetterOnReqViewTextToPrint"
    Private Const COLLLETTER_ON_REQ_REPORTS As String = "spCollLetterOnRequestReports"
#End Region
#Region "CollLetterOnReqPaging"
    Public Function CollLetterOnReqPaging(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, COLLLETTER_ON_REQUEST_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("CollLetterOnReq", "CollLetterOnReqPaging", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region
#Region "CollLetterOnReqView"
    Public Function CollLetterOnReqView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter

        Try
            params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(0).Value = ocustomclass.Applicationid
            params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(1).Value = ocustomclass.AgreementNo
            objread = SqlHelper.ExecuteReader(ocustomclass.strConnection, CommandType.StoredProcedure, COLLETTER_ON_REQUEST_VIEW_LETTER, params)
            With ocustomclass
                If objread.Read Then
                    .AgreementNo = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .Asset = CType(objread("Asset"), String)
                    .InstallmentNo = CType(objread("InstallmentNo"), String)
                    .InstallmentDate = CType(objread("InstallmentDate"), String)
                    .InstallMentAmount = CType(objread("InstallmentAmount"), Double)
                    .OsBalance = CType(objread("OsBalance"), Double)
                End If
                objread.Close()
            End With
            Return ocustomclass

        Catch exp As Exception
            WriteException("CollLetterOnReq", "CollLetterOnReqView", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "LetterNameOnReqCombo"
    Public Function LetterNameOnReqCombo(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, COLLETTER_ON_REQUEST_COMBO).Tables(0)

        Catch exp As Exception
            WriteException("CollLetterOnReq", "LetterNameOnReqCombo", exp.Message + exp.StackTrace)


        End Try

    End Function
#End Region
#Region "CollLetterTemplateField"
    Public Function CollLetterTemplateField(ByVal oCustomClass As Parameter.CollLetterOnReq) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.Text, COLLLETTER_ON_REQUEST_TEMPLATE_FIELD).Tables(0)
        Catch ex As Exception

        End Try
    End Function

#End Region
#Region "LetterNameOnReqComboView"
    Public Function LetterNameOnReqComboView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim params(0) As SqlParameter
        Dim objread As SqlDataReader

        Try
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = ocustomclass.LetterId

            objread = SqlHelper.ExecuteReader(ocustomclass.strConnection, CommandType.StoredProcedure, COLLETTER_ON_REQUEST_COMBO_VIEW, params)
            With ocustomclass
                If objread.Read Then
                    .LetterName = CType(objread("LetterName"), String)
                    .LetterTemplate = CType(objread("Template"), String)
                End If
            End With
            Return ocustomclass
        Catch exp As Exception
            WriteException("CollLetterOnReq", "LetterNameOnReqComboView", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
#Region "CollLetterTemplateFieldName"
    Public Function CollLetterTemplateFieldName(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(0).Value = ocustomclass.Applicationid
            params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(1).Value = ocustomclass.AgreementNo

            Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, COLLLETTER_ON_REQUEST_VIEW, params).Tables(0)
        Catch ex As Exception

        End Try
    End Function



#End Region
#Region "CollLetterOnReqSave"
    Public Sub CollLetterOnReqSaving(ByVal oCustomClass As Parameter.CollLetterOnReq)
        Dim params(1) As SqlParameter

        Try
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.LetterId
            params(1) = New SqlParameter("@TextToPrint", SqlDbType.Text)
            params(1).Value = oCustomClass.TextToPrint

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, COLL_LETTER_ON_REQ_SAVING, params)


        Catch exp As Exception
            WriteException("CollLetterOnReq", "CollLetterOnReqSaving", exp.Message + exp.StackTrace)
        End Try

    End Sub
#End Region
    Public Function CollLetterOnReqViewTextToPrint(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim objread As SqlDataReader
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = ocustomclass.LetterId

            objread = SqlHelper.ExecuteReader(ocustomclass.strConnection, CommandType.StoredProcedure, COLL_LETTER_ON_REQ_VIEW_TEXT_TO_PRINT, params)
            With ocustomclass
                If objread.Read Then
                    .TextToPrint = CType(objread("TextToPrint"), String)
                End If
                objread.Close()
            End With
            Return ocustomclass

        Catch ex As Exception

        End Try
    End Function
    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.LetterId
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, COLLLETTER_ON_REQ_REPORTS, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("CollLetter", "CollLetterReports", exp.Message + exp.StackTrace)
        End Try

    End Function


End Class
