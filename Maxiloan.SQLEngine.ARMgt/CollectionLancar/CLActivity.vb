
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CLActivity : Inherits DataAccessBase
    Private Const spListCLActivity As String = "spCLActivityPaging"
    Private Const spCLActivityListCollector As String = "spCLActivityListCollector"
    Private Const spCLActivityDataAgreement As String = "spDeskCollReminderActivityView"
    Private Const spCLActivityActionResult As String = "spListResultAction"
    Private Const spCLActivitySave As String = "spCLActivityResultSave"
    Private Const spGetResult As String = "spViewDataResult"
    Private Const spCLActivityListMobCollector As String = "spMobileCLActivityPaging"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_COLLECTORID As String = "@CollectorID"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"
    Protected Const PARAM_CONTACT As String = "@Contact"
    Protected Const PARAM_ISREQUEST As String = "@isRequest"
    Protected Const PARAM_PLANDATE As String = "@PlanDate"
    Protected Const PARAM_PTPYDATE As String = "@PTPYDate"
    Protected Const PARAM_RESULTID As String = "@ResultID"
    Protected Const PARAM_ACTIVITYID As String = "@ActivityID"
    Protected Const PARAM_PLANACTIVITYID As String = "@PlanActivityID"
    Protected Const PARAM_NOTES As String = "@Notes"
    Protected Const PARAM_HASIL As String = "@hasil"
    Private Const spDCRSave As String = "spDRCPlan"

#Region "CLActivity"

    Public Function ListCLActivity(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("WhereCond2", SqlDbType.VarChar, 1000)
            params(5).Value = IIf(customclass.WhereCond2 Is Nothing, "", customclass.WhereCond2)

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCLActivity, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("CLActivity", "ListCLActivity", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListCLActivityRemidial(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("WhereCond2", SqlDbType.VarChar, 1000)
            params(5).Value = IIf(customclass.WhereCond2 Is Nothing, "", customclass.WhereCond2)

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCLActivityPagingRemidial", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("CLActivity", "ListCLActivity", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function CLActivityListCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(1).Value = customclass.LoginId

            params(2) = New SqlParameter(PARAM_STRKEY, SqlDbType.VarChar, 10)
            params(2).Value = customclass.strKey

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCLActivityListCollector, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CLActivity", "CLActivityListCollector", exp.Message)
        End Try
    End Function

    Public Function CLActivityDataAgreement(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCLActivityDataAgreement, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CLActivity", "CLActivityDataAgreement", exp.Message)
        End Try
    End Function

    Public Function CLActivityActionResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_STRKEY, SqlDbType.VarChar, 10)
            params(0).Value = customclass.strKey

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCLActivityActionResult, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CLActivity", "CLActivityActionResult", exp.Message)
        End Try
    End Function

    Public Function CLActivitySave(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(13) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_CONTACT, SqlDbType.Char, 50)
            params(2).Value = customclass.Contact

            params(3) = New SqlParameter(PARAM_ISREQUEST, SqlDbType.Bit)
            params(3).Value = customclass.isRequest



            params(4) = New SqlParameter(PARAM_PLANDATE, SqlDbType.Char, 20)
            If customclass.PlanDate = "" Then
                params(4).Value = DBNull.Value
            Else
                params(4).Value = customclass.PlanDate
            End If

            params(5) = New SqlParameter(PARAM_PTPYDATE, SqlDbType.Char, 8)
            If customclass.PTPYDate = "" Then
                params(5).Value = DBNull.Value
            Else
                params(5).Value = customclass.PTPYDate
            End If

            params(6) = New SqlParameter(PARAM_RESULTID, SqlDbType.Char, 10)
            params(6).Value = customclass.ResultID

            params(7) = New SqlParameter(PARAM_ACTIVITYID, SqlDbType.Char, 10)
            params(7).Value = customclass.ActivityID

            params(8) = New SqlParameter(PARAM_PLANACTIVITYID, SqlDbType.Char, 10)
            If customclass.PlanActivityID = "" Then
                params(8).Value = DBNull.Value
            Else
                params(8).Value = customclass.PlanActivityID
            End If


            params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.Char, 500)
            params(9).Value = customclass.Notes

            params(10) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(10).Value = customclass.BusinessDate

            params(11) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(11).Value = customclass.CGID

            params(12) = New SqlParameter(PARAM_COLLECTORID, SqlDbType.Char, 12)
            params(12).Value = customclass.CollectorID

            params(13) = New SqlParameter(PARAM_HASIL, SqlDbType.Char, 50)
            params(13).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCLActivitySave, params)
            customclass.Hasil = CInt(params(13).Value)
            objtrans.Commit()
            Return customclass

        Catch exp As Exception
            WriteException("CLActivity", "CLActivitySave", exp.Message)
            objtrans.Rollback()
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Function

    Public Function getResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_ACTIVITYID, SqlDbType.Char, 10)
            params(0).Value = customclass.ActivityID

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetResult, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CLActivity", "CLActivitySave", exp.Message)
        End Try
    End Function

#End Region

#Region "DCR"
    Public Function DCRSave(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction

        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction

        '           @branchid  Varchar(3),
        '@Applicationid varchar(20),
        '@PlanDate datetime,
        '@PlanActivity varchar(20),
        '@Businessdate datetime

        params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
        params(0).Value = customclass.AgreementNo

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter(PARAM_PLANDATE, SqlDbType.Char, 20)
        If customclass.PlanDate = "" Then
            params(2).Value = DBNull.Value
        Else
            params(2).Value = customclass.PlanDate
        End If

        params(3) = New SqlParameter(PARAM_PLANACTIVITYID, SqlDbType.Char, 10)
        If customclass.PlanActivityID = "" Then
            params(3).Value = DBNull.Value
        Else
            params(3).Value = customclass.PlanActivityID
        End If

        params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(4).Value = customclass.BusinessDate
        Try
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spDCRSave, params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("DCR", "DCR", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function
#End Region

#Region "GetDCRList"



    Public Function GetDCRList(ByVal customClass As Parameter.CLActivity) As Parameter.CLActivity

        Dim oReturnValue As New Parameter.CLActivity
        Dim spName As String

        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@WhereCondForCA", SqlDbType.VarChar, 1000)
        params(3).Value = customClass.WhereCondForCA
        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4).Value = customClass.SortBy
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customClass.BusinessDate.ToShortDateString
        params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(6).Value, Int64)
            Return oReturnValue
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("CLActivity", "GetDCRList", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("CLActivity", "GetDCRList", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetMobilePaymentList"
    Public Function CLActivityListMobCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("WhereCond2", SqlDbType.VarChar, 1000)
            params(5).Value = IIf(customclass.WhereCond2 Is Nothing, "", customclass.WhereCond2)

            customclass.ListCLActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCLActivityListMobCollector, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CLActivity", "CLActivityListMobCollector", exp.Message)
        End Try
    End Function
#End Region

End Class
