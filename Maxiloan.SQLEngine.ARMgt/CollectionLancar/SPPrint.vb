
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SPPrint : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spListSPPrinting As String = "spSPPrintingPaging"
    Private Const spListReportSPPrint As String = "spRptSPPrint"
    Private Const spSPPrintProcess As String = "spPrintSPProcess"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"

    ''SSPRINT
    Private Const spListSSPrinting As String = "spSSPrintingPaging"
    Private Const spListReportSSPrint As String = "spRptSSPrint"
    Private Const spSSPrintProcess As String = "spPrintSSProcess"


    Public Function ListCGSPPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 10)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("SPPrint", "ListCGSPPrint", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListSPPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            If customclass.produk = "FACT" Then
                customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSPPrintingPagingFACT", params).Tables(0)
                customclass.TotalRecord = CInt(params(4).Value)
            ElseIf customclass.produk = "MDKJ" Then
                customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSPPrintingPagingMDKJ", params).Tables(0)
                customclass.TotalRecord = CInt(params(4).Value)
            Else
                customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSPPrinting, params).Tables(0)
                customclass.TotalRecord = CInt(params(4).Value)
            End If
            'customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSPPrinting, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SPPrint", "ListSPPrinting", exp.Message + exp.StackTrace)
        End Try
    End Function
 
    Public Function ListSSPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListSPPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSSPrinting, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SSPrint", "ListSSPrinting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListReportSPPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            params(2) = New SqlParameter("@author1", SqlDbType.VarChar, 10)
            params(2).Value = customclass.author1
            params(3) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 30)
            params(3).Value = customclass.ApplicationID
            params(4) = New SqlParameter("@Rubrik1", SqlDbType.VarChar, 30)
            params(4).Value = customclass.Rubrik1


            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReportSPPrint, params)
            Return customclass
        Catch exp As Exception
            WriteException("SPPrint", "ListReportSPPrint", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SPPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_USRUPD, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSPPrintProcess, params)
            customclass.hasil = CInt(params(3).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SPPrint", "SPPrintProcess", exp.Message + exp.StackTrace)
        End Try
    End Function


    ''SSPRINT
    Public Function ListReportSSPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReportSSPrint, params)
            Return customclass
        Catch exp As Exception
            WriteException("SSPrint", "ListReportSSPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function SSPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_USRUPD, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSSPrintProcess, params)
            customclass.hasil = CInt(params(3).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SSPrint", "SSPrintProcess", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListRubrikPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            customclass.ListRubrikPrint = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRubrikUnit", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("SPPrint", "ListRubrikPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
