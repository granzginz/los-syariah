
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class SPVActivity : Inherits DataAccessBase

#Region "Const"
    Private Const SPVAct_List As String = "spSPVActList"
    Private Const SPVActChoosen_List As String = "spSPVActListChoosen"
    Private Const SPVAct_Fixed As String = "spSPVActFixed"
    Private Const SPVAct_RAL As String = "spSPVActRAL"
    Private Const SPVAct_RedDot As String = "spSPVActRedDot"
    Private Const SPVAct_Cases As String = "spSPVActCases"

    Private Const SPCheckCollectorType As String = "spCheckCollectorType"
    Private Const SP_COMBO_COLLECTOR As String = "spComboCollector"
    Private Const QUERY_COMBO_CASES As String = "select casesID as ID, penyebab as Name from cases order by CasesID"

#End Region

    Public Function SPVActivity_List(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim params(4) As SqlParameter
        Try
            With SPVAct
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            SPVAct.listdata = SqlHelper.ExecuteDataset(SPVAct.strConnection, CommandType.StoredProcedure, SPVAct_List, params).Tables(0)
            SPVAct.TotalRecord = CType(params(4).Value, Int16)
            Return SPVAct
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivity_List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SPVActivityChoosen_List(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim params(3) As SqlParameter
        Try
            With SPVAct
                params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
                params(0).Value = .WhereAgreementNo
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(3).Direction = ParameterDirection.Output
            End With
            SPVAct.listdata = SqlHelper.ExecuteDataset(SPVAct.strConnection, CommandType.StoredProcedure, SPVActChoosen_List, params).Tables(0)
            SPVAct.TotalRecord = CType(params(3).Value, Int16)
            Return SPVAct
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityChoosen_List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetComboCases(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity

        Try
            SPVAct.listdata = SqlHelper.ExecuteDataset(SPVAct.strConnection, CommandType.Text, QUERY_COMBO_CASES).Tables(0)
            Return SPVAct
        Catch exp As Exception
            WriteException("SPVActivity", "GetComboCases", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetComboCollector(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Try
            SPVAct.listdata = SqlHelper.ExecuteDataset(SPVAct.strConnection, CommandType.StoredProcedure, SP_COMBO_COLLECTOR).Tables(0)
            Return SPVAct
        Catch exp As Exception
            WriteException("SPVActivity", "GetComboCollector", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckCollectorType(ByVal SPVAct As Parameter.SPVActivity) As String
        Dim params(1) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = .CGID
            params(1) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
            params(1).Value = .CollectorID
        End With
        Try
            Dim CollType As String
            CollType = CStr(SqlHelper.ExecuteScalar(SPVAct.strConnection, CommandType.StoredProcedure, SPCheckCollectorType, params))
            Return CollType
        Catch exp As Exception
            WriteException("SPVActivity", "CheckCollectorType", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SPVActivityFixed(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(SPVAct.strConnection)
        Dim params(2) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
            params(0).Value = .WhereAgreementNo
            params(1) = New SqlParameter("@Action", SqlDbType.Char, 20)
            params(1).Value = .Action
            params(2) = New SqlParameter("@ReassignTo", SqlDbType.Char, 12)
            params(2).Value = .CollectorID
        End With
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, SPVAct_Fixed, params)
            oSaveTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityFixed", exp.Message + exp.StackTrace)
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

    Public Sub SPVActivityCases(ByVal SPVAct As Parameter.SPVActivity)

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(SPVAct.strConnection)
        Dim params(1) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
            params(0).Value = .WhereAgreementNo
            params(1) = New SqlParameter("@CasesID", SqlDbType.Char, 10)
            params(1).Value = .casesID
        End With
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, SPVAct_Cases, params)
            oSaveTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityCases", exp.Message + exp.StackTrace)
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

    Public Sub SPVActivityRAL(ByVal SPVAct As Parameter.SPVActivity)

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(SPVAct.strConnection)
        Dim params(2) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
            params(0).Value = .WhereAgreementNo
            params(1) = New SqlParameter("@Action", SqlDbType.Char, 20)
            params(1).Value = .Action
            params(2) = New SqlParameter("@PlanDateRAL", SqlDbType.DateTime)
            params(2).Value = .PlanDateRAL
        End With
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, SPVAct_RAL, params)
            oSaveTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityRAL", exp.Message + exp.StackTrace)
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

    Public Sub SPVActivityRedDot(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(SPVAct.strConnection)
        Dim params(0) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
            params(0).Value = .WhereAgreementNo
        End With

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, SPVAct_RedDot, params)
            oSaveTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityRedDot", exp.Message + exp.StackTrace)
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

    Public Function SPVListResultAction(ByVal SPVAct As Parameter.SPVActivity) As DataSet
        Dim params As SqlParameter
        Try

            params = New SqlParameter("@strKey", SqlDbType.Char, 10)
            params.Value = ""
            Return SqlHelper.ExecuteDataset(SPVAct.strConnection, CommandType.StoredProcedure, "spListResultAction", params)
        Catch exp As Exception
            WriteException("SPVActivity", "SPVListResultAction", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SPVActivityPlanSave(ByVal SPVAct As Parameter.SPVActivity) As Boolean
        Dim params(2) As SqlParameter
        With SPVAct
            params(0) = New SqlParameter("@WhereAgreementNo", SqlDbType.VarChar, 8000)
            params(0).Value = .WhereAgreementNo
            params(1) = New SqlParameter("@PlanActivity", SqlDbType.Char, 10)
            params(1).Value = .Action
            params(2) = New SqlParameter("@PlanDate", SqlDbType.DateTime)
            params(2).Value = .PlanDateRAL
        End With
        Try
            SqlHelper.ExecuteNonQuery(SPVAct.strConnection, CommandType.StoredProcedure, "spSPVActPlan", params)
            Return True
        Catch exp As Exception
            WriteException("SPVActivity", "SPVActivityFixed", exp.Message + exp.StackTrace)
            Return False
        End Try
    End Function
End Class
