

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCRequestPrint : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spListPDCRequest As String = "spPDCRequestPrintPaging"
    Private Const spSavePDCRequest As String = "spPDCRequestPrintSave"
    Private Const spReportPDCRequest As String = "spPDCRequestPrintReport"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_LETTER As String = "@Letter"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_NEXTINSTALLMENTNO As String = "@NextInstallmentNo"

    Public Function PDCRequestListCG(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRequestPrint", "PDCRequestListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PDCRequestList(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCRequest, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRequestPrint", "PDCRequestList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PDCRequestSave(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oTrans As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        'Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(2) {}

            params(0) = New SqlParameter(PARAM_LETTER, SqlDbType.Char, 50)
            params(0).Value = customclass.Letter

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePDCRequest, params)
            'Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("PDCRequestPrint", "PDCRequestSave", exp.Message + exp.StackTrace)
            customclass.hasil = 0
            Return customclass
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
        End Try
    End Function

    Public Function PDCRequestReport(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPDCRequest, params)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRequestPrint", "PDCRequestReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function PDCRequestSurat(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSPDCRequest", params)
            Return customclass
        Catch ex As Exception
            WriteException("PDCRequestPrint", "PDCRequestSurat", ex.Message + ex.StackTrace)
        End Try
    End Function

End Class
