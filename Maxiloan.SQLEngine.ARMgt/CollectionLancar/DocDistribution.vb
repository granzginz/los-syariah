


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class DocDistribution : Inherits DataAccessBase
    Private Const spDocDistributionPaging As String = "spDocDistributionPaging"
    Private Const spInqDocDistributionPaging As String = "spInqDocDistributionPaging"
    Private Const spRequestViewData As String = "spDocumentDistributionView"
    Private Const spSaveDocumentDistribution As String = "spDocumentDistributionSave"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"

    Public Function InqDocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListDistribution = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqDocDistributionPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("DocumentDistribution", "InqDocumentDistributionPaging", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function DocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListDistribution = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDocDistributionPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("DocumentDistribution", "DocumentDistributionPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RequestDataDocDistribution(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(1).Value = customclass.BranchId



            customclass.ListView = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRequestViewData, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DocumentDistribution", "RequestDataDocDistribution", exp.Message + exp.StackTrace)
        End Try
    End Function



    Public Function DocumentDistributionSave(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
             
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID})

            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId}) 
            params.Add(New SqlParameter("@AgreementSentDate", SqlDbType.DateTime) With {.Value = customclass.AgreementSentDate})
            params.Add(New SqlParameter("@SlipSentDate", SqlDbType.DateTime) With {.Value = customclass.SlipSetoranDate}) 
            params.Add(New SqlParameter("@PolicySentDate", SqlDbType.DateTime) With {.Value = customclass.Insurancedate}) 
            params.Add(New SqlParameter("@AgreementReceiveBy", SqlDbType.Char, 50) With {.Value = customclass.AgreementReceive}) 
            params.Add(New SqlParameter("@SlipReceiveBy", SqlDbType.Char, 50) With {.Value = customclass.SlipSetoranReceive}) 
            params.Add(New SqlParameter("@PolicyReceiveBy", SqlDbType.Char, 50) With {.Value = customclass.InsuranceReceive}) 
            params.Add(New SqlParameter("@WelcomeLetter", SqlDbType.DateTime) With {.Value = customclass.WelcomeLetter}) 
            params.Add(New SqlParameter("@WelcomeLetterReceiveBy", SqlDbType.Char, 50) With {.Value = customclass.WelcomeLetterReceiveBy})
            params.Add(New SqlParameter("@CardInstallment", SqlDbType.DateTime) With {.Value = customclass.CardInstallment})

            params.Add(New SqlParameter("@CardInstallmentReceiveBy", SqlDbType.Char, 50) With {.Value = customclass.CardInstallmentReceiveBy})
            params.Add(New SqlParameter("@Courier", SqlDbType.Char, 50) With {.Value = customclass.Courier})

            Dim prmo = New SqlParameter("@msg", SqlDbType.Char, 50) With {.Direction = ParameterDirection.Output}
            params.Add(prmo)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSaveDocumentDistribution, params.ToArray)
            Return New Parameter.DocDistribution() With {.approveby = prmo.Value.ToString}
        Catch exp As Exception
            Throw New Exception(exp.Message)
            WriteException("DocumentDistributionSave", exp.Message + exp.StackTrace)
        End Try
    End Function




End Class
