


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ReceiptNotesOnRequest : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spReceiptNotesOnRequestList As String = "spReceiptNotesOnRequestPaging"
    Private Const spRequestViewDataSelect As String = "spReceiptNotesOnRequestView"
    Private Const spViewInstallment As String = "spViewDataInstallment"
    Private Const spGenerateKuitansiTagihOnRequest As String = "spGenerateKuitansiTagihOnRequest"
    'Private Const spRALSaveRALPrint As String = "spRALPrintingSavePrintRAL"
    Private Const spRALDataExtend As String = "spRALDataChangeExec"
    Protected Const PARAM_RALN As String = "@RALN"
    Protected Const PARAM_EXECUTORIDOLD As String = "@ExecutorIDOld"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_EXECUTORID As String = "@ExecutorID"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_NEXTBUSINESSDATE As String = "@NextBD"
    Protected Const PARAM_NEXTINSTALLMENT As String = "@NextInstallment"
    Protected Const PARAM_BUSINESSDATE1 As String = "@BusinessDate"
    Protected Const PARAM_SELECTINSTALLMENT As String = "@SelectInstallment"
    Protected Const PARAM_DAYSOVERDUE As String = "@DaysOverdue"
    Protected Const PARAM_COLLECTORID As String = "@CollectorID"
    Protected Const PARAM_RECEIPTDATES As String = "@ReceiptDates"
    Protected Const PARAM_BRANCHID1 As String = "@BranchID"
    ''================ RAL On Request ==========================


    Public Function ReceiptNotesOnRequestListCG(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ReceiptNotesOnRequest", "ReceiptNotesOnRequestListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReceiptNotesOnRequestList(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReceiptNotesOnRequestList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("ReceiptNotesOnRequest", "ListReceiptNotesOnRequest", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function ViewDataInstallment(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_NEXTINSTALLMENT, SqlDbType.Int)
            params(2).Value = customclass.NextInstallment


            params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output


            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewInstallment, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("ReceiptNotesOnRequest", "ViewDataInstallment", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GenerateKuitansiTagihOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim oTrans As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        Try
            oTrans = oConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(0).Value = customclass.BusinessDate

            params(1) = New SqlParameter(PARAM_SELECTINSTALLMENT, SqlDbType.Int)
            params(1).Value = customclass.NextInstallment

            params(2) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(2).Value = customclass.ApplicationID

            params(3) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(3).Value = customclass.CGID

            params(4) = New SqlParameter(PARAM_DAYSOVERDUE, SqlDbType.Int)
            params(4).Value = customclass.daysoverdue

            params(5) = New SqlParameter(PARAM_COLLECTORID, SqlDbType.VarChar, 12)
            params(5).Value = customclass.ExecutorID


            params(6) = New SqlParameter(PARAM_BRANCHID1, SqlDbType.Char, 3)
            params(6).Value = customclass.BranchId

            params(7) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_RECEIPTDATES, SqlDbType.DateTime)
            params(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spGenerateKuitansiTagihOnRequest, params)
            oTrans.Commit()
            customclass.hasil = CInt(params(7).Value)
            customclass.receiptdates = CDate(params(8).Value)
            Return customclass
        Catch exp As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            WriteException("ReceiptNotesOnRequest", "GenerateKuitansiTagihOnRequest", exp.Message + exp.StackTrace)
            Return customclass
        End Try

    End Function

    'Public Function RALSaveDataPrintRAL(ByVal customclass As Entities.RALChangeExec) As Entities.RALChangeExec
    '    Dim params() As SqlParameter = New SqlParameter(4) {}
    '    Dim oConnection As New SqlConnection(customclass.strConnection)
    '    Dim objtrans As SqlTransaction
    '    Try
    '        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
    '        objtrans = oConnection.BeginTransaction
    '        params(0) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
    '        params(0).Value = customclass.RALNO

    '        params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
    '        params(1).Value = customclass.BusinessDate

    '        params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
    '        params(2).Value = customclass.LoginId

    '        params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
    '        params(3).Value = customclass.ApplicationID

    '        params(4) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
    '        params(4).Direction = ParameterDirection.Output

    '        SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRALSaveRALPrint, params)
    '        customclass.hasil = CInt(params(4).Value)
    '        objtrans.Commit()
    '        Return customclass
    '    Catch exp As Exception
    '        objtrans.Commit()
    '        WriteException("RALChangeExec", "RALSaveDataPrintRAL", exp.Message + exp.StackTrace)
    '    Finally
    '        If oConnection.State = ConnectionState.Open Then oConnection.Close()
    '        oConnection.Dispose()
    '    End Try
    'End Function

    Public Function DataReceiptNotesOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        'Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALDataExtend).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ReceiptNotesOnRequest", "DataReceiptNotesOnRequest", exp.Message + exp.StackTrace)
        End Try
    End Function


    '=================== RAL On Request ============================
    Public Function RequestDataSelect(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID



            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRequestViewDataSelect, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ReceiptNotesOnRequest", "ReceiptNotesOnRequestViewDataSelectRequest", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
