
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class RptIncOverDueRating : Inherits DataAccessBase
#Region "Constanta"
    Private Const OVER_DUE_RATING_REPORT As String = "spIncOverDueRatingReport"
#End Region
#Region "IncOverDueRating Reports"
    Public Function IncOverDueRatingReports(ByVal oCustomClass As Parameter.RptIncOverDueRating) As Parameter.RptIncOverDueRating
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@where", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomClass.WhereCond
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, OVER_DUE_RATING_REPORT, params)
            Return oCustomClass
        Catch exp As Exception
            WriteException("RptIncBillingCharges", "IncBillingChargesReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

End Class
