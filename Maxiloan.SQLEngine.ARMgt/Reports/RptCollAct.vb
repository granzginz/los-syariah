
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RptCollAct : Inherits DataAccessBase
    Private Const spViewDataCollector As String = "spViewDataCollector"
    Private Const spListReport As String = "spRptCollActivity"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"

    Public Function ViewDataCollector(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.VarChar, 10)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListCollector = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewDataCollector, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RptCollAct", "ViewDataCollector", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ViewReportCollAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("RptCollAct", "ViewReportCollAct", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ViewSubReportCollAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRAlHistorySubReport", params)
            Return customclass
        Catch exp As Exception
            WriteException("RptCollAct", "ViewSubReportCollAct", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
