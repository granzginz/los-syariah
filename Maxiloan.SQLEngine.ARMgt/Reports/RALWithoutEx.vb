
#Region "Imports"

Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALWithoutEx : Inherits DataAccessBase
    Public Function ViewReportRALwithoutEx(ByVal customclass As Parameter.RALWithoutEx) As Parameter.RALWithoutEx
        Dim params As SqlParameter = New SqlParameter
        Try
            params = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params.Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRptRALWithoutEX", params)
            Return customclass
        Catch exp As Exception
            WriteException("RALWithoutEx", "ViewReportRALWithoutEx", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
