
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class RptIncReceiptNotes : Inherits DataAccessBase
#Region "Constanta"
    Private Const RECEIPT_NOTES_REPORT As String = "spIncReceiptNotesReport"
    
#End Region

#Region "ReceiptNotes Reports"

    Public Function IncReceiptNotesReports(ByVal oCustomClass As Parameter.RptIncReceiptNotes) As Parameter.RptIncReceiptNotes
        Dim params(0) As SqlParameter

        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 8000)
            params(0).Value = oCustomClass.WhereCond

            oCustomClass.ListReports = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, RECEIPT_NOTES_REPORT, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("RptIncReceiptNotes", "IncReceiptNotesReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

End Class
