
#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region
Public Class RptIncBucketMovement : Inherits DataAccessBase
#Region "Constanta"
    Private Const BUCKET_MOVEMENT_REPORT As String = "spIncBucketMovementReport"
    Private Const BUCKET_MOVEMENT_GET_BEGINHOLDER As String = "spGetBeginHolder"
    Private Const GET_PERIOD As String = "select Period from collincentiveperiod"
    Private Const GET_BEGIN_STATUS As String = "select Status as ID,Description as Name from collincentivebmstatus where BeginEndStatus='B'"

#End Region

#Region "BucketMovement Reports"

    Public Function IncBucketMovementReports(ByVal oCustomClass As Parameter.RptIncBucketMovement) As Parameter.RptIncBucketMovement
        Dim params(0) As SqlParameter

        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomClass.WhereCond

            oCustomClass.ListReports = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, BUCKET_MOVEMENT_REPORT, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("RptIncBucketMovement", "IncBucketMovementReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
#Region "GetPeriod"

    Public Function GetPeriod(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = GET_PERIOD

        dttResult.Columns.Add("Period", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("Period") = oReader("Period").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetPeriod", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "Begin Status"

    Public Function GetBeginStatus(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = GET_BEGIN_STATUS

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetBeginStatus", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "Begin Holder"
    Public Function GetBeginHolder(ByVal oCustomClass As Parameter.RptIncBucketMovement) As DataTable
        Dim params(0) As SqlParameter

        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomClass.WhereCond

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, BUCKET_MOVEMENT_GET_BEGINHOLDER, params).Tables(0)

        Catch exp As Exception
            WriteException("RptIncBucketMovement", "IncBucketMovementReports", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region


End Class
