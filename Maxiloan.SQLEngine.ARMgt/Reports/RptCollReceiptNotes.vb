

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RptCollReceiptNotes : Inherits DataAccessBase

    Private Const spViewReport As String = "spReceiptNotesReport"
    Private Const spOutStanding As String = "spOutStanding"

    Public Function ViewReport(ByVal customclass As Parameter.RptCollReceiptNotes) As Parameter.RptCollReceiptNotes
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000)
            params(0).Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewReport, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RptCollReceiptNotes", "ViewReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function OutStandingReport(ByVal customclass As Parameter.RptCollReceiptNotes) As Parameter.RptCollReceiptNotes
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = customclass.CGID
            params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
            params(1).Value = customclass.CollectorID
            params(2) = New SqlParameter("@vReceiptPeriodeFrom", SqlDbType.VarChar, 13)
            params(2).Value = customclass.ReceiptPeriodeFrom
            params(3) = New SqlParameter("@vReceiptPeriodeTo", SqlDbType.VarChar, 13)
            params(3).Value = customclass.ReceiptPeriodeTo
            params(4) = New SqlParameter("@IsPrint", SqlDbType.Bit)
            params(4).Value = customclass.IsPrint

            customclass.OutStandingReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spOutStanding, params)
            Return customclass
        Catch exp As Exception
            WriteException("RptCollReceiptNotes", "OutStandingReport", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
