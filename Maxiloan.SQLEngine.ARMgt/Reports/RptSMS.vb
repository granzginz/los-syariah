

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RptSMS : Inherits DataAccessBase
    Private Const spListReport As String = "spReportSMS"

    Public Function ListReport(ByVal customclass As Parameter.RptSMS) As Parameter.RptSMS
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("RptSMS", "ListReport", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
