
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class RptIncBillingCharges : Inherits DataAccessBase
#Region "Constanta"
    Private Const BILLING_CHARGES_REPORT As String = "spIncBillingChargesReport"

#End Region
#Region "IncBillingCharges Reports"

    Public Function IncBillingChargesReports(ByVal oCustomClass As Parameter.RptIncBillingCharges) As Parameter.RptIncBillingCharges
        Dim params(0) As SqlParameter

        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.WhereCond

            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, BILLING_CHARGES_REPORT, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("RptIncBillin    gCharges", "IncBillingChargesReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

End Class
