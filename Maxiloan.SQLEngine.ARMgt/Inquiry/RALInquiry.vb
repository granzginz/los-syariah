
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALInquiry : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spRALInqList As String = "spRALInquiryPaging"
    Private Const spRALInqListExtend As String = "spRALInquiryHistory"
    Private Const spRALViewDataCollector As String = "spRALPrintingViewCollector"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_CGID As String = "@CGID"


    Public Function ListRALInq(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALInqList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALInquiry", "InqZipCode", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALInqListCG(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALInquiry", "RALInqListCG", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function RALInqListExecutorCombo(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim params As New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter(PARAM_CGID, SqlDbType.Char, 3) With {.Value = customclass.CGID}) 
            If (customclass.CollectorType.Trim = "EP") Then
                params.Add(New SqlParameter("@CollectorType", SqlDbType.Char, 3) With {.Value = "EP"})
            End If
  
            customclass.ListInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRALPrintingViewCollectorInt", params.ToArray).Tables(0)

            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
         
    End Function



    Public Function RALInqListExecutor(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim params As New List(Of SqlParameter)
        Try
            'ngak tahu kenapa di store procedurenya dihilangkan paging parameter

            'params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            'params(0).Value = customclass.CurrentPage

            'params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            'params(1).Value = customclass.PageSize

            params.Add(New SqlParameter(PARAM_CGID, SqlDbType.Char, 3) With {.Value = customclass.CGID})

            If (customclass.CollectorType.Trim = "EP") Then
                params.Add(New SqlParameter("@CollectorType", SqlDbType.Char, 3) With {.Value = "EP"})
            End If


            'params(3) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            'params(3).Direction = ParameterDirection.Output


            customclass.ListInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewDataCollector, params.ToArray).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("RALInquiry", "RALInqListExecutor", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListRALInqExtend(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALInqListExtend, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALInquiry", "ListRALInqExtend", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
