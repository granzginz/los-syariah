
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class DeskCollPerformance : Inherits DataAccessBase
#Region " Private Const "
    Private Const DESKCOLL_PAGING As String = "spDeskCollPerformancePaging"
    Private Const QUERY_COMBO_CG As String = "select cgid as ID, CGName as Name from collectiongroup order by CGID"
    Private Const GET_PERIOD As String = "select Period from collincentiveperiod"
    Private Const GET_BEGIN_STATUS As String = "select Status as ID,Description as Name from collincentivebmstatus where BeginEndStatus='B'"
    Private Const GET_END_STATUS As String = "select Status as ID,Description as Name from collincentivebmstatus where BeginEndStatus='E'"
#End Region
#Region "DeskCollPerformancePaging"
    Public Function DeskCollPerformancePaging(ByVal oCustomClass As Parameter.DeskCollPerformance) As Parameter.DeskCollPerformance
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, DESKCOLL_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("DeskCollPerformance", "DeskCollPerformancePaging", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region
#Region "GetComboCG"

    Public Function GetComboCG(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = QUERY_COMBO_CG

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetComboCG", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetPeriod"

    Public Function GetPeriod(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = GET_PERIOD

        dttResult.Columns.Add("Period", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("Period") = oReader("Period").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetPeriod", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "Begin Status"

    Public Function GetBeginStatus(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = GET_BEGIN_STATUS

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetBeginStatus", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "EndStatus"
    Public Function GetEndStatusStatus(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = GET_END_STATUS

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DeskCollPerformance", "GetEndStatusStatus", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

End Class
