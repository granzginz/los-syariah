
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class InqZipCode : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const COLLZIPCODE_LIST As String = "spInqZipCodePaging"
    Private Const GET_CITY As String = "spGetCity"

#End Region
    Public Function InqZipCode(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode

        Dim params(5) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@City", SqlDbType.VarChar, 30)
                params(0).Value = .City
                params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oCollZipCode.ListData = SqlHelper.ExecuteDataset(oCollZipCode.strConnection, CommandType.StoredProcedure, COLLZIPCODE_LIST, params).Tables(0)
            oCollZipCode.TotalRecord = CType(params(5).Value, Int16)
            Return oCollZipCode
        Catch exp As Exception
            WriteException("InqZipCode", "InqZipCode", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetCityCombo(ByVal strconn As String) As DataTable
        Dim dtZipCode As New DataTable
        Try
            dtZipCode = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, GET_CITY).Tables(0)
            Return dtZipCode
        Catch exp As Exception
            WriteException("InqZipCode", "GetCityCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
