#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class ViewCollection : Inherits DataAccessBase

    Private Const spViewCollection As String = "spViewCollection"
    Private Const spViewKewajibanNasabah As String = "spViewAmortizationDaftarKewajibanNasabah"

    Public Function ViewCollection(ByVal ocustomclass As Parameter.ViewCollection) As Parameter.ViewCollection
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = ocustomclass.BranchID1

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = ocustomclass.ApplicationID

            ocustomclass.listData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, spViewCollection, params).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("ViewCollection", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ViewKewajibanNasabah(ByVal ocustomclass As Parameter.ViewCollection) As Parameter.ViewCollection
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = ocustomclass.ApplicationID

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = ocustomclass.BusinessDate

            params(2) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(2).Value = ocustomclass.PlanDate

            ocustomclass.listData = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, spViewKewajibanNasabah, params).Tables(0)
            Return ocustomclass
        Catch exp As Exception
            WriteException("ViewKewajibanNasabah", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
