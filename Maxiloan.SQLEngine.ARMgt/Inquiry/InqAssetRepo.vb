

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class InqInqAssetRepo : Inherits DataAccessBase

    Private Const spInqAssetRepoList As String = "spInqAssetRepoList"
    Private Const spInqAssetRepoDetail As String = "spInqAssetRepoDetail"

    Public Function InqAssetRepoList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqAssetRepoList, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass

        Catch exp As Exception
            WriteException("InqInqAssetRepo", "InqAssetRepoList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function InqAssetRepoDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
        params(0).Value = customclass.CGID

        params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(1).Value = customclass.AgreementNo

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqAssetRepoDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InqInqAssetRepo", "InqAssetRepoList", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
