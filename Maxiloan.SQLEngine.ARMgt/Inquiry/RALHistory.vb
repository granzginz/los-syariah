#Region " Imports "
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class RALHistory : Inherits DataAccessBase
    Public Function RALHistoryList(ByVal customclass As Parameter.RALHistory) As Parameter.RALHistory
        Dim params(1) As SqlParameter
        Try
            With customclass
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = .AppliactionID
                params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1).Value = .BranchId
                'params(2) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                'params(2).Value = .AgreementNo
            End With
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRAlHistory", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALHistory", "RALHistoryList", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
