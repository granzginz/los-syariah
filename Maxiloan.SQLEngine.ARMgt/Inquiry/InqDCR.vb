

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InqDCR : Inherits DataAccessBase

#Region "Const"
    Private Const DCR_LIST As String = "spInqDCRList"
    Private Const DCR_RPT As String = "spDCRReport"
    Private Const QUERY_COMBO_TASKTYPE As String = "select ID, description as Name from tblColltaskType"
    Private Const QUERY_COMBO_CG As String = "select cgid as ID, CGName as Name from collectiongroup order by CGID"

#End Region

    Public Function GetInqDCRList(ByVal oDCR As Parameter.InqDCR) As Parameter.InqDCR
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            With oDCR
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With

            Dim oDCRList As New Parameter.InqDCR

            oDCRList.listdata = SqlHelper.ExecuteDataset(oDCR.strConnection, CommandType.StoredProcedure, DCR_LIST, params).Tables(0)
            oDCRList.TotalRecord = CType(params(5).Value, Int16)
            Return oDCRList
        Catch exp As Exception
            WriteException("InqDCR", "GetInqDCRList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetComboTaskType(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = QUERY_COMBO_TASKTYPE

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("InqDCR", "GetComboTaskType", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetComboCG(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = QUERY_COMBO_CG

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("InqDCR", "GetComboCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListReport(ByVal oDCR As Parameter.InqDCR) As DataTable
        Dim params(1) As SqlParameter
        Try
            With oDCR
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(1).Value = .WhereCond
            End With
            Dim oDCRList As New Parameter.InqDCR
            oDCRList.listdata = SqlHelper.ExecuteDataset(oDCR.strConnection, CommandType.StoredProcedure, DCR_RPT, params).Tables(0)
            Return oDCRList.listdata
        Catch exp As Exception
            WriteException("InqDCR", "ListReport", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
