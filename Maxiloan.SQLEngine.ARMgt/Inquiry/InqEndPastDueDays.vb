﻿#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class InqEndPastDueDays : Inherits DataAccessBase
#Region "Private Const"

    Private Const spInqEndPastDueDaysPaging As String = "spInqEndPastDueDaysPaging"

#End Region

    Public Function InqEndPastDueDays(ByVal oInqEndPastDueDays As Parameter.InqEndPastDueDays) As Parameter.InqEndPastDueDays
        Dim params(5) As SqlParameter
        Try
            With oInqEndPastDueDays
                params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(0).Value = .BranchId
                params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oInqEndPastDueDays.ListData = SqlHelper.ExecuteDataset(oInqEndPastDueDays.strConnection, CommandType.StoredProcedure, spInqEndPastDueDaysPaging, params).Tables(0)
            oInqEndPastDueDays.TotalRecord = CType(params(5).Value, Int16)
            Return oInqEndPastDueDays
        Catch exp As Exception
            WriteException("InqEndPastDueDays", "InqEndPastDueDays", exp.Message + exp.StackTrace)
        End Try
    End Function


End Class
