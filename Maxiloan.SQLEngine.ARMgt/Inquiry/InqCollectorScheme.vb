

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InqCollectorScheme : Inherits DataAccessBase
    Private Const spInqCollectorScheme As String = "spInqCollSchemePaging"
    'Private const spViewCollection as String = "spViewCollection"
    Protected Const PARAM_SEARCHBY As String = "@SearchBy"

    Public Function ListCollectorScheme(ByVal customclass As Parameter.InqCollectorScheme) As Parameter.InqCollectorScheme
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_SEARCHBY, SqlDbType.VarChar, 50)
            params(4).Value = customclass.Criteria

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.CollectorSchemeList = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqCollectorScheme, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("InqCollectorScheme", "ListCollectorScheme", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
