

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CollActivity : Inherits DataAccessBase

#Region "Const"
    Private Const GET_COLLACT_LIST As String = "spCollActivityList"
    Private Const GET_COLLACTHISTORY_LIST As String = "spCollActivityHistory"
    Private Const GET_COLLACT_DETAIL As String = "spCollActivityDetail"
    Private Const GET_COLLACT_HISTORY As String = "spCollectionHistory"

    Private Const GET_COLLECTOR As String = "spGetCollectorCombo"

    Private Const QUERY_COMBO_COLLECTOR As String = "select ID, description as Name from tblColltaskType"
    Private Const QUERY_COMBO_CG As String = "select cgid as ID, CGName as Name from collectiongroup order by CGID"
    Private Const GET_COLLACTHISTORY_LIST_PRINT As String = "spViewCollActivityPrint"
#End Region

    Public Function GetCollActivityList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity

        Dim params(6) As SqlParameter
        Try
            With oCollAct
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(2).Value = .CurrentPage
                params(3) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(3).Value = .PageSize
                params(4) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(4).Value = .WhereCond
                params(5) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(5).Value = .SortBy
                params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(6).Direction = ParameterDirection.Output
            End With


            Dim oCollActList As New Parameter.CollActivity

            oCollActList.ListData = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLACT_LIST, params).Tables(0)
            oCollActList.TotalRecord = CType(params(6).Value, Int16)
            Return oCollActList
        Catch exp As Exception
            WriteException("CollActivity", "GetCollActivityList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollActivityResultList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity

        Dim params(6) As SqlParameter
        Try
            With oCollAct
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
                params(0).Value = .AgreementNo
                params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params(1).Value = .CustomerID
                params(2) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(2).Value = .CurrentPage
                params(3) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(3).Value = .PageSize
                params(4) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(4).Value = .WhereCond
                params(5) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(5).Value = .SortBy
                params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(6).Direction = ParameterDirection.Output
            End With

            Dim oCollActList As New Parameter.CollActivity
            oCollActList.ListData = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLACTHISTORY_LIST, params).Tables(0)
            oCollActList.TotalRecord = CType(params(6).Value, Int16)
            Return oCollActList
        Catch exp As Exception
            WriteException("CollActivity", "GetCollActivityList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollActivityDetail(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim params(4) As SqlParameter
        Dim oReturn As New Parameter.CollActivity
        Try
            With oCollAct
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
                params(0).Value = .AgreementNo
                params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params(1).Value = .CustomerID
                params(2) = New SqlParameter("@ActivityDate", SqlDbType.DateTime)
                params(2).Value = .ActivityDate
                params(3) = New SqlParameter("@CollectorId", SqlDbType.Char, 12)
                params(3).Value = .CollectorID
                params(4) = New SqlParameter("@CollSeqNo", SqlDbType.Int)
                params(4).Value = .CollSeqNo
            End With
            oReturn.ListData = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLACT_DETAIL, params).Tables(0)
            Return oReturn
        Catch exp As Exception
            WriteException("CollActivity", "GetCollActivityDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollActivityHistory(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim params(6) As SqlParameter
        Try
            With oCollAct
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
                params(0).Value = .AgreementNo
                params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params(1).Value = .CustomerID
                params(2) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(2).Value = .CurrentPage
                params(3) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(3).Value = .PageSize
                params(4) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(4).Value = .WhereCond
                params(5) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(5).Value = .SortBy
                params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(6).Direction = ParameterDirection.Output
            End With


            Dim oCollActList As New Parameter.CollActivity

            oCollActList.ListData = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLACT_HISTORY, params).Tables(0)
            oCollActList.TotalRecord = CType(params(6).Value, Int16)
            Return oCollActList
        Catch exp As Exception
            WriteException("CollActivity", "GetCollActivityHistory", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetComboCG(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String

        sqlQuery = QUERY_COMBO_CG

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("CollActivity", "GetComboCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorCombo(ByVal oCollAct As Parameter.CollActivity) As DataTable
        Dim params(1) As SqlParameter
        Try
            With oCollAct
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
                params(1).Value = .CollectorType
            End With
            Dim dtZipCode As New DataTable
            dtZipCode = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLECTOR, params).Tables(0)
            Return dtZipCode
        Catch exp As Exception
            WriteException("CollActivity", "GetCollectorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollActivityResultListPrint(ByVal oCollAct As Parameter.CollActivity) As DataTable

        Dim params(1) As SqlParameter
        Try
            With oCollAct
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(1).Value = .WhereCond
            End With

            Dim oCollActList As New Parameter.CollActivity
            oCollActList.ListData = SqlHelper.ExecuteDataset(oCollAct.strConnection, CommandType.StoredProcedure, GET_COLLACTHISTORY_LIST_PRINT, params).Tables(0)

            Return oCollActList.ListData
        Catch exp As Exception
            WriteException("CollActivity", "GetCollActivityListPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
