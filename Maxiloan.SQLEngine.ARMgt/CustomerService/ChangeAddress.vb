
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ChangeAddress : Inherits DataAccessBase

    Private Const spListAddress As String = "spChangeAddressPaging"
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spViewAddress As String = "spChangeAddressView"
    Private Const spSaveAddress As String = "spChangeAddressSave"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_MAILINGADDRESS As String = "@MailingAddress"
    Protected Const PARAM_MAILINGKELURAHAN As String = "@MailingKelurahan"
    Protected Const PARAM_MAILINGKECAMATAN As String = "@MailingKecamatan"
    Protected Const PARAM_MAILINGCITY As String = "@MailingCity"
    Protected Const PARAM_MAILINGZIPCODE As String = "@MailingZipCode"
    Protected Const PARAM_MAILINGRT As String = "@MailingRT"
    Protected Const PARAM_MAILINGRW As String = "@MailingRW"
    Protected Const PARAM_MAILINGAREAPHONE1 As String = "@MailingAreaPhone1"
    Protected Const PARAM_MAILINGAREAPHONE2 As String = "@MailingAreaPhone2"
    Protected Const PARAM_MAILINGAREAFAX As String = "@MailingAreaFax"
    Protected Const PARAM_MAILINGPHONE1 As String = "@MailingPhone1"
    Protected Const PARAM_MAILINGPHONE2 As String = "@MailingPhone2"
    Protected Const PARAM_MAILINGFAX As String = "@MailingFax"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_NOTES As String = "@Notes"


    Public Function ListAddress(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAddress = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAddress, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("ChangeAddress", "ListAddress", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddressListCG(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try

            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListAddress = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ChangeAddress", "AddressListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ViewAddress(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.AgreementNo

            customclass.ListAddress = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewAddress, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ChangeAddress", "ViewAddress", exp.Message + exp.StackTrace)
        End Try

    End Function


    Public Function SaveAddress(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim params() As SqlParameter = New SqlParameter(18) {}
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.AgreementNo

            params(2) = New SqlParameter(PARAM_MAILINGADDRESS, SqlDbType.VarChar, 100)
            params(2).Value = customclass.MailingAddress

            params(3) = New SqlParameter(PARAM_MAILINGKELURAHAN, SqlDbType.VarChar, 30)
            params(3).Value = customclass.MailingKelurahan

            params(4) = New SqlParameter(PARAM_MAILINGKECAMATAN, SqlDbType.VarChar, 30)
            params(4).Value = customclass.MailingKecamatan

            params(5) = New SqlParameter(PARAM_MAILINGZIPCODE, SqlDbType.VarChar, 5)
            params(5).Value = customclass.MailingZipCode

            params(6) = New SqlParameter(PARAM_MAILINGAREAPHONE1, SqlDbType.Char, 4)
            params(6).Value = customclass.MailingAreaPhone1

            params(7) = New SqlParameter(PARAM_MAILINGPHONE1, SqlDbType.Char, 10)
            params(7).Value = customclass.MailingPhone1

            params(8) = New SqlParameter(PARAM_MAILINGAREAPHONE2, SqlDbType.Char, 4)
            params(8).Value = customclass.MailingAreaPhone2

            params(9) = New SqlParameter(PARAM_MAILINGPHONE2, SqlDbType.Char, 10)
            params(9).Value = customclass.MailingPhone2

            params(10) = New SqlParameter(PARAM_AREAFAX, SqlDbType.Char, 4)
            params(10).Value = customclass.MailingAreaFax

            params(11) = New SqlParameter(PARAM_MAILINGFAX, SqlDbType.Char, 10)
            params(11).Value = customclass.MailingFax

            params(12) = New SqlParameter(PARAM_MAILINGCITY, SqlDbType.Char, 50)
            params(12).Value = customclass.MailingCity

            params(13) = New SqlParameter(PARAM_MAILINGRT, SqlDbType.Char, 3)
            params(13).Value = customclass.MailingRT

            params(14) = New SqlParameter(PARAM_MAILINGRW, SqlDbType.Char, 3)
            params(14).Value = customclass.MailingRW

            params(15) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(15).Value = customclass.Notes

            params(16) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(16).Value = customclass.BusinessDate

            params(17) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(17).Direction = ParameterDirection.Output

            params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            params(18).Value = customclass.MobilePhone

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveAddress, params)
            customclass.hasil = CInt(params(17).Value)
            objtrans.Commit()
            Return customclass
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("ChangeAddress", "SaveAddress", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function GetAddress(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oReturnValue As New Parameter.ChangeAddress
        Dim params(0) As SqlParameter
        Dim spName As String
        spName = "spApplicationCopy"
        Try
            params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.CustomerID
            oReturnValue.ListAddress = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("ChangeAddress", "ViewAddress", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
