

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetRepo : Inherits DataAccessBase

#Region "Constanta"
    Private Const spAssetRepoList As String = "spAssetRepoList"
    Private Const spAssetRepoDetail As String = "spAssetRepoDetail"
    Private Const spAssetRepoSave As String = "spAssetRepoSave"
    Private Const spAssetRepoCheckList As String = "spAssetRepoCheckList"
    Private Const spAssetRepoCheckListSave As String = "spAssetRepoCheckSave"
    Private Const spAssetRepoCLPrintList As String = "spAssetRepoCLPrintList"
    Private Const spAssetRepoCLPrintReport As String = "spAssetRepoCLPrintReport"
    Private Const spAssetRepoPrepaymentLetterPrint As String = "spAssetRepoPrepaymentLetterPrint"
#End Region
#Region "AssetRepoList"
    Public Function AssetRepoList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
            WriteException("AssetRepo", "AssetRepoList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function AssetRepoApprovalList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAssetRepoApprovalList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
            WriteException("AssetRepo", "AssetRepoList", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region
#Region "AssetRepoCheckList"
    Public Function AssetRepoCheckList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@AssetTypeID", SqlDbType.Char, 10) With {.Value = customclass.AssetTypeID})

            If (customclass.ApplicationID <> "") Then
                params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID})
            End If
             
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoCheckList, params.ToArray).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "AssetRepoCheckListSave"
    Public Sub AssetRepoCheckListSave(ByVal customclass As Parameter.AssetRepo)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(10) {}

        params(0) = New SqlParameter("@BAPKNo", SqlDbType.Char, 20)
        params(0).Value = customclass.BAPKNo

        params(1) = New SqlParameter("@CheckListID", SqlDbType.Char, 10)
        params(1).Value = customclass.CheckListID

        params(2) = New SqlParameter("@CGID", SqlDbType.Char, 3)
        params(2).Value = customclass.CGID

        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(3).Value = customclass.AssetSeqNo

        params(4) = New SqlParameter("@YNQuestion", SqlDbType.Char, 1)
        params(4).Value = customclass.YNQuestion

        params(5) = New SqlParameter("@Quantity", SqlDbType.Char, 10)
        params(5).Value = customclass.Quantity

        params(6) = New SqlParameter("@Notes", SqlDbType.Text)
        params(6).Value = customclass.Notes

        params(7) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(7).Value = customclass.AgreementNo

        params(8) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(8).Value = customclass.AssetTypeID

        params(9) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(9).Value = customclass.ApplicationID

        params(10) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(10).Value = customclass.BranchId

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoCheckListSave, params)
            oSaveTransaction.Commit()
        Catch exp As Exception
            oSaveTransaction.Rollback()
            WriteException("AssetRepo", "AssetRepoCheckListSave", exp.Message + exp.StackTrace)
        End Try

    End Sub
#End Region
#Region "AssetRepoDetail"
    Public Function AssetRepoDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter("@RALNo", SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "AssetRepoSave"
    Public Function AssetRepoSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim oSaveTransaction As SqlTransaction = Nothing
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@CGID", SqlDbType.Char, 10) With {.Value = customclass.CGID}) 
            params.Add(New SqlParameter("@AssetSeqNo", SqlDbType.Int) With {.Value = customclass.AssetSeqNo})
            params.Add(New SqlParameter("@RepossessDate", SqlDbType.DateTime) With {.Value = customclass.RepossesDate}) 
            params.Add(New SqlParameter("@untilDate", SqlDbType.DateTime) With {.Value = customclass.UntilDate}) 
            params.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = customclass.AgreementNo}) 
            params.Add(New SqlParameter("@AssetDescription", SqlDbType.VarChar, 50) With {.Value = customclass.AssetDescription})
            params.Add(New SqlParameter("@endpastduedays", SqlDbType.Int) With {.Value = customclass.EndpastDueDays})
            params.Add(New SqlParameter("@Collectorid", SqlDbType.Char, 12) With {.Value = customclass.CollectorID})
            params.Add(New SqlParameter("@AssetLocation", SqlDbType.Text) With {.Value = customclass.AssetLocation})
            params.Add(New SqlParameter("@AssetCondition", SqlDbType.Text) With {.Value = customclass.AssetCondition}) 
            params.Add(New SqlParameter("@Checker", SqlDbType.VarChar, 50) With {.Value = customclass.Checker})
            params.Add(New SqlParameter("@CheckerJobTitle", SqlDbType.VarChar, 50) With {.Value = customclass.CheckerJob})
            params.Add(New SqlParameter("@CheckerDate", SqlDbType.DateTime) With {.Value = customclass.Checkdate})
            params.Add(New SqlParameter("@Notes", SqlDbType.Text) With {.Value = customclass.Notes})
            params.Add(New SqlParameter("@RALNo", SqlDbType.Char, 20) With {.Value = customclass.RALNo})
            params.Add(New SqlParameter("@BussinessDate", SqlDbType.DateTime) With {.Value = customclass.BussinessDate}) 
            params.Add(New SqlParameter("@RALDate", SqlDbType.DateTime) With {.Value = customclass.RALDate}) 
            params.Add(New SqlParameter("@RALEndDate", SqlDbType.DateTime) With {.Value = customclass.RALEndDate}) 
            params.Add(New SqlParameter("@BAPKNo", SqlDbType.Char, 20) With {.Value = customclass.BAPKNo}) 
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID}) 
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId}) 
            params.Add(New SqlParameter("@Color", SqlDbType.VarChar, 20) With {.Value = customclass.Color}) 
            params.Add(New SqlParameter("@LicensePlate", SqlDbType.VarChar, 20) With {.Value = customclass.LisencePlate})  
            params.Add(New SqlParameter("@STNKEndDate", SqlDbType.VarChar, 100) With {.Value = customclass.STNKEndDate}) 
            params.Add(New SqlParameter("@STNKName", SqlDbType.VarChar, 100) With {.Value = customclass.STNKName}) 
            params.Add(New SqlParameter("@BBM", SqlDbType.VarChar, 20) With {.Value = customclass.BBM})  
            params.Add(New SqlParameter("@CollectionExpense", SqlDbType.Decimal) With {.Value = customclass.DebitAmount}) 
            params.Add(New SqlParameter("@BranchAssetReposes", SqlDbType.Char, 3) With {.Value = customclass.BranchAssetReposes}) 
            params.Add(New SqlParameter("@KMAsset", SqlDbType.Int) With {.Value = customclass.KMAsset}) 
            params.Add(New SqlParameter("@AdaSTNK", SqlDbType.Bit) With {.Value = CType(customclass.AdaAsset, Boolean)})
            params.Add(New SqlParameter("@IDLokasiAsset", SqlDbType.VarChar) With {.Value = customclass.IDLokasiAsset}) 
            params.Add(New SqlParameter("@Grade", SqlDbType.VarChar) With {.Value = customclass.Grade}) 
            params.Add(New SqlParameter("@AssetRepossesionCheckList", SqlDbType.Structured) With {.Value = customclass.ToAssetRepoChecksTable})

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spAssetRepoSave, params.ToArray)
            oSaveTransaction.Commit() 
            Return customclass
        Catch exp As Exception
            oSaveTransaction.Rollback()
            Throw New Exception(exp.Message)
            WriteException("AssetRepo", "AssetRepoSave", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
        Return Nothing
    End Function
#End Region


    Public Function AssetRepoApproveReject(ByVal customclass As Parameter.AssetRepo, status As String) As Parameter.AssetRepo

        Dim oSaveTransaction As SqlTransaction = Nothing
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@CGID", SqlDbType.Char, 10) With {.Value = customclass.CGID})
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId})
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID})
            params.Add(New SqlParameter("@AssetSeqNo", SqlDbType.Int) With {.Value = customclass.AssetSeqNo})
            params.Add(New SqlParameter("@RALNo", SqlDbType.Char, 20) With {.Value = customclass.RALNo})
            params.Add(New SqlParameter("@Collectorid", SqlDbType.Char, 12) With {.Value = customclass.CollectorID})
            params.Add(New SqlParameter("@BussinessDate", SqlDbType.DateTime) With {.Value = customclass.BussinessDate})
            params.Add(New SqlParameter("@RepossesionStatus", SqlDbType.Char, 20) With {.Value = status}) 
            params.Add(New SqlParameter("@loginId", SqlDbType.Char, 20) With {.Value = customclass.LoginId})
            params.Add(New SqlParameter("@Notes", SqlDbType.Char, 20) With {.Value = customclass.Notes})

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, "spAssetRepoRejectApprove", params.ToArray)
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            oSaveTransaction.Rollback()
            Throw New Exception(exp.Message)
            WriteException("AssetRepo", "AssetRepoSave", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
        Return Nothing
    End Function




#Region "AssetRepoCLPrintLIst"
    Public Function AssetRepoCLPrintList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoCLPrintList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoSave", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "AssetRepoCLPrintReport"
    Public Function AssetRepoCLPrintReport(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@ExecutorID", SqlDbType.Char, 20)
            params(1).Value = customclass.ExecutorID

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoCLPrintReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoCLPrintReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function AssetRepoPrepaymentLetterPrint(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@BussinesDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepoPrepaymentLetterPrint, params)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoPrepaymentLetterPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function AssetRepoChangeLocList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAssetRepoChangeLocList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            Throw New ExecutionEngineException(exp.Message)
        End Try

    End Function

    Public Function AssetRepoChangeLocSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try

            params(0) = New SqlParameter("@RALNo", SqlDbType.Char, 20)
            params(0).Value = customclass.RALNo

            params(1) = New SqlParameter("@BussinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BussinessDate

            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationID

            params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@BranchAssetInTransit", SqlDbType.Char, 3)
            params(4).Value = customclass.BranchAssetInTransit


            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, "spAssetRepoChangeLocSave", params)
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

    Public Function AssetRepoChangeLocDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter("@RALNo", SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAssetRepoChangeLocDetail", params).Tables(0)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function AssetRepoChangeLocRecList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAssetRepoChangeLocRecList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Function

    Public Function AssetRepoChangeLocRecSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try

            params(0) = New SqlParameter("@RALNo", SqlDbType.Char, 20)
            params(0).Value = customclass.RALNo

            params(1) = New SqlParameter("@BussinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BussinessDate

            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationID

            params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@BranchAssetReposes", SqlDbType.Char, 3)
            params(4).Value = customclass.BranchAssetReposes


            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, "spAssetRepoChangeLocRecSave", params)
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function
    Public Sub GetLokasiPenyimpanan(ByRef p_Class As Parameter.AssetRepo)

        Try
            p_Class.listData = SqlHelper.ExecuteDataset(p_Class.strConnection, CommandType.StoredProcedure, "spGetLokasiPenyimpanan").Tables(0)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub
    Public Function BiayaPenangananSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        'Dim params() As SqlParameter = New SqlParameter(2) {}
        Try

            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId}) 
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID})
          
            params.Add(New SqlParameter("@TanggalPembebanan", SqlDbType.DateTime) With {.Value = customclass.TanggalPembebanan})

            params.Add(New SqlParameter("@BiayaCollection", SqlDbType.Decimal) With {.Value = customclass.BiayaCollection})
            params.Add(New SqlParameter("@BiayaTebus", SqlDbType.Decimal) With {.Value = customclass.BiayaTebus})

            params.Add(New SqlParameter("@assetRepoBOP", SqlDbType.Structured) With {.Value = customclass.ToBOPTable})

            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)


            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            ' SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, "spBiayaPenangananSave", params)
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, "spBiayaPenangananBOPSave", params.ToArray)

            'If CType(errPrm.Value, String) <> "" Then
            '    Return customclass
            'End If

            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function
    Function rederCommonValueText(reader As SqlDataReader) As IList(Of Parameter.CommonValueText)
        Dim result = New List(Of Parameter.CommonValueText)
        If (reader.HasRows) Then
            If (reader Is Nothing) Then Return Nothing
            If (reader.IsClosed) Then Return Nothing 
            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text") 
            While (reader.Read())
                result.Add(New Parameter.CommonValueText(IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)), IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
            End While 
        End If
        Return result
    End Function
    Public Function AssetGrade(cnn As String) As IList(Of Parameter.CommonValueText) 
        Dim str As String = "select GradeCode Value,GradeCode Text from assetgrade" 
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, str)
        Return rederCommonValueText(reader) 
    End Function

    Public Function AssetGradeValue(cnn As String) As IList(Of Parameter.CommonValueText)
        Dim str As String = "select cast (cast(GradeValue as smallint ) as char(2)) Value,GradeCode Text from assetgrade"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, str)
        Return rederCommonValueText(reader)
    End Function

    Public Function JenisBiayaBOP(cnn As String) As IList(Of Parameter.CommonValueText)
        Dim str As String = "select ID as Value ,description as Text from tblBOP order by description" 
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, str)
        Return rederCommonValueText(reader)

        'If (reader.HasRows) Then
        '    If (reader Is Nothing) Then Return Nothing 
        '    If (reader.IsClosed) Then Return Nothing

        '    Dim _cID = reader.GetOrdinal("Value")
        '    Dim _desc = reader.GetOrdinal("Text")


        '    While (reader.Read())
        '        result.Add(New Parameter.CommonValueText( IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)), IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
        '    End While

        'End If
        'Return result
    End Function
End Class
