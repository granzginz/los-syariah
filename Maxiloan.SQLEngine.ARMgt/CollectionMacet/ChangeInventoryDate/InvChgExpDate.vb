

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InvChgExpDate : Inherits DataAccessBase
    Private Const spInvExpDateList As String = "spInvChgExpDateList"
    Private Const spInvExpDateDetail As String = "spInvChgExpDateDetail"
    Private Const spInvExpDateSave As String = "spInvChgExpDateSave"
    Private Const spInvExpDateView As String = "spInvChgExpDateView"
    Private Const QueryReason As String = "select reasonID as ID, description as Name from reason where reasontypeid='CHINV'"


    Public Function InvExpDateList(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output


            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInvExpDateList, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("InvChgExpDate", "InvExpDateList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function InvExpDateView(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@ApprovalNo", SqlDbType.Char, 50)
            params(2).Value = customclass.Approvalno


            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInvExpDateView, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InvChgExpDate", "InvExpDateView", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function InvExpDateDetail(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(1).Value = customclass.CGID

            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationID

            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInvExpDateDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InvChgExpDate", "InvExpDateDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function InvExpDateSave(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = oSaveTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "CH1"
                .RequestDate = customclass.BussinessDate
                .UserRequest = customclass.RequestBy
                .TransactionNo = customclass.ApplicationID
                .ApprovalNote = customclass.Notes
                .ApprovalValue = customclass.RefundAmount
                .UserRequest = customclass.LoginId
                .UserApproval = customclass.ApproveBy
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.ReFund_Approval
            End With
            customclass.Approvalno = oApprovalID.RequestForApproval(oEntitiesApproval)
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@Requestdate", SqlDbType.DateTime)
            params(2).Value = customclass.BussinessDate

            params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@PrevInvExpectedDate", SqlDbType.DateTime)
            params(4).Value = customclass.PrevInvExpDate

            params(5) = New SqlParameter("@NewInvExpectedDate", SqlDbType.DateTime)
            params(5).Value = customclass.NewInvExpDate

            params(6) = New SqlParameter("@Reason", SqlDbType.VarChar, 10)
            params(6).Value = customclass.Reason

            params(7) = New SqlParameter("@ApproveBy", SqlDbType.VarChar, 12)
            params(7).Value = customclass.ApproveBy

            params(8) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(8).Value = customclass.Approvalno

            params(9) = New SqlParameter("@Result", SqlDbType.VarChar, 1)
            params(9).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spInvExpDateSave, params)

            If CStr(params(9).Value).Trim = "0" Then
                customclass.Result = "There are already a request waiting to be approved"
            Else
                customclass.Result = ""
            End If
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            oSaveTransaction.Rollback()
            WriteException("InvChgExpDate", "InvExpDateSave", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Function


    Public Function GetComboReason(ByVal customclass As Parameter.InvChgExpDate) As DataTable
        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.Text, QueryReason).Tables(0)
            Return customclass.listData
        Catch exp As Exception
            WriteException("InvChgExpDate", "GetComboReason", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
