#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class CollInvReturn : Inherits DataAccessBase
    Public Function InvReturnList(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCollInvReturnList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CollInvSelling", "InvReturnList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function InvReturnDetail(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@RepossesSeqNo", SqlDbType.VarChar, 20)
        params(2).Value = customclass.RepossesSeqNo

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCollInvReturnDetail", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InvReturnDetail", "InvReturnDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function InvReturnSave(ByVal customclass As Parameter.CollInvSelling) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = customclass.BusinessDate

        params(3) = New SqlParameter("@RepossesSeqNo", SqlDbType.VarChar, 20)
        params(3).Value = customclass.RepossesSeqNo

        params(4) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(4).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCollInvReturnSave", params)
            Return CType(params(4).Value, Boolean)
        Catch exp As Exception
            WriteException("InvReturnSave", "InvReturnSave", exp.Message + exp.StackTrace)
            Return False
        End Try
    End Function



End Class
