

#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class CollInvOnRequest : Inherits DataAccessBase

    Public Function CollInvOnRequest(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter( _
            "@BusinessDate", SqlDbType.DateTime)
        params(0).Value = customclass.BusinessDate
        params(1) = New SqlParameter( _
            "@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter( _
                    "@BranchID", SqlDbType.Char, 3)
        params(2).Value = customclass.BranchId
        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetInventoryOnRequest", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CollInvRequest", "CollInvOnRequest", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub InvOnRequestProses(ByVal customclass As Parameter.CollInvSelling)
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter( _
            "@BusinessDate", SqlDbType.DateTime)
        params(0).Value = customclass.BusinessDate
        params(1) = New SqlParameter( _
            "@Applicationid", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter( _
                    "@BranchID", SqlDbType.Char, 3)
        params(2).Value = customclass.BranchId

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInventoryOnRequestProcess", params)
        Catch exp As Exception
            WriteException("CollInvRequest", "CollInvOnRequest", exp.Message + exp.StackTrace)
        End Try
    End Sub

End Class
