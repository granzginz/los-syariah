


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InventoryAppraisal : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spInventoryAppraisalList As String = "spInventoryAppraisalPaging"
    Private Const spRequestViewDataSelect As String = "spInventoryAppraisalView"
    Private Const spSaveInventoryAppraisal As String = "spSaveInventoryAppraisal"
    Private Const spSaveInventoryAppraisal1 As String = "spSaveInventoryAppraisal1"
    Private Const spSaveInventoryAppraisal1a As String = "spSaveInventoryAppraisal1a"
    Private Const spSaveInventoryAppraisal3 As String = "spSaveInventoryAppraisal3"
    Private Const spViewAppraisal As String = "spViewAppraisal"
    Private Const spAccruedInterest As String = "spAccruedInterest"
    Private Const spViewAppraisalBidder As String = "spViewAppraisalBidder"
    'Private Const spGenerateKuitansiTagihOnRequest As String = "spGenerateKuitansiTagihOnRequest"
    ''Private Const spRALSaveRALPrint As String = "spRALPrintingSavePrintRAL"
    Private Const spRALDataExtend As String = "spRALDataChangeExec"
    Protected Const PARAM_RALN As String = "@RALN"
    Protected Const PARAM_EXECUTORIDOLD As String = "@ExecutorIDOld"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_EXECUTORID As String = "@ExecutorID"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_NEXTBUSINESSDATE As String = "@NextBD"
    Protected Const PARAM_NEXTINSTALLMENT As String = "@NextInstallment"
    Protected Const PARAM_BUSINESSDATE1 As String = "@BusinessDate"
    Protected Const PARAM_SELECTINSTALLMENT As String = "@SelectInstallment"
    Protected Const PARAM_DAYSOVERDUE As String = "@DaysOverdue"
    Protected Const PARAM_COLLECTORID As String = "@CollectorID"
    Protected Const PARAM_RECEIPTDATES As String = "@ReceiptDates"
    Protected Const PARAM_BRANCHID1 As String = "@BranchID"
    Protected Const PARAM_ASSETSEQNO As String = "@AssetSeqno"
    Protected Const PARAM_REPOSSESSEQNO As String = "@RepossesSeqno"
    Protected Const PARAM_BIDDERDATE As String = "@BidderDate"
    Protected Const PARAM_BIDDERNO As String = "@BidderNo"
    Protected Const PARAM_BIDDERNAME As String = "@BidderName"
    Protected Const PARAM_BIDDERAMOUNT As String = "@BidderAmount"
    Protected Const PARAM_FORMBIDDER As String = "@FormBidder"
    Protected Const PARAM_NOTELEPON As String = "@NoTelepon"

    Protected Const PARAM_ESTIMATIONDATE As String = "@EstimationDate"
    Protected Const PARAM_ESTIMATIONPRICE As String = "@EstimationPrice"
    Protected Const PARAM_ESTIMATIONBY As String = "@EstimationBy"
    Protected Const PARAM_ESTIMATIONAMOUNT As String = "@EstimationAmount"
    Protected Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Protected Const PARAM_NOTES As String = "@Notes"
    Protected Const PARAM_FLAG As String = "@Flag"
    Protected Const PARAM_APPROVEBY As String = "@ApproveBy"


    '''================ RAL On Request ==========================


    Public Function InventoryAppraisalListCG(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InventoryAppraisal", "InventoryAppraisalListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function InventoryAppraisalList(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInventoryAppraisalList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("InventoryAppraisal", "ListInventoryAppraisal", exp.Message + exp.StackTrace)
        End Try
    End Function


    'Public Function ViewDataInstallment(ByVal customclass As Entities.ReceiptNotesOnRequest) As Entities.ReceiptNotesOnRequest
    '    Dim params() As SqlParameter = New SqlParameter(4) {}
    '    Try
    '        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
    '        params(0).Value = customclass.CurrentPage

    '        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
    '        params(1).Value = customclass.PageSize

    '        params(2) = New SqlParameter(PARAM_NEXTINSTALLMENT, SqlDbType.Int)
    '        params(2).Value = customclass.NextInstallment


    '        params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
    '        params(3).Value = customclass.ApplicationID

    '        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
    '        params(4).Direction = ParameterDirection.Output


    '        customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewInstallment, params).Tables(0)
    '        customclass.TotalRecord = CInt(params(4).Value)
    '        Return customclass
    '    Catch exp As Exception
    '        WriteException("ReceiptNotesOnRequest", "ViewDataInstallment", exp.Message + exp.StackTrace)
    '    End Try
    'End Function

    Public Function SaveInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim params1() As SqlParameter = New SqlParameter(9) {}
        Dim params2 As IList(Of SqlParameter) = New List(Of SqlParameter) 'As SqlParameter = New SqlParameter(19) {}
        Dim oTrans As SqlTransaction

        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim i As Integer
        Dim strApprovalNo As String
        'Dim TotalAmountBidder As Double
        'Dim totalSum As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction
            strApprovalNo = ""

            'If (customclass.EstimationPrice > customclass.bidderamount) Then

            Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
            Dim oEntitiesApproval As New Parameter.Approval

            With oEntitiesApproval
                .ApprovalTransaction = oTrans
                .BranchId = customclass.BranchId
                .SchemeID = "SELL"
                .RequestDate = customclass.BusinessDate
                .TransactionNo = customclass.ApplicationID
                .ApprovalValue = customclass.bidderamount
                .UserRequest = customclass.LoginId
                .UserApproval = customclass.approveby
                .ApprovalNote = customclass.EstimationNotes
                .AprovalType = Parameter.Approval.ETransactionType.InventorySelling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            'End If

            params(0) = New SqlParameter(PARAM_BRANCHID1, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(2).Value = customclass.CGID

            params(3) = New SqlParameter(PARAM_ASSETSEQNO, SqlDbType.Int)
            params(3).Value = customclass.AssetSeqno

            params(4) = New SqlParameter(PARAM_REPOSSESSEQNO, SqlDbType.Int)
            params(4).Value = customclass.RepossessseqNo

            params(5) = New SqlParameter(PARAM_FLAG, SqlDbType.Bit)
            params(5).Value = customclass.flag



            params(6) = New SqlParameter(PARAM_APPROVEBY, SqlDbType.VarChar, 20)
            params(6).Value = customclass.approveby

            params(7) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(7).Value = strApprovalNo

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveInventoryAppraisal1, params)


            params1(0) = New SqlParameter(PARAM_BRANCHID1, SqlDbType.Char, 3)
            params1(1) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params1(2) = New SqlParameter(PARAM_ASSETSEQNO, SqlDbType.Int)
            params1(3) = New SqlParameter(PARAM_REPOSSESSEQNO, SqlDbType.Int)
            params1(4) = New SqlParameter(PARAM_BIDDERNO, SqlDbType.Int)
            params1(5) = New SqlParameter(PARAM_BIDDERNAME, SqlDbType.VarChar, 50)
            params1(6) = New SqlParameter(PARAM_BIDDERAMOUNT, SqlDbType.Money)
            params1(7) = New SqlParameter(PARAM_BIDDERDATE, SqlDbType.DateTime)
            params1(8) = New SqlParameter(PARAM_FORMBIDDER, SqlDbType.Bit)
            params1(9) = New SqlParameter(PARAM_NOTELEPON, SqlDbType.VarChar)

            For i = 0 To customclass.ListBidder.Rows.Count - 1
                params1(0).Value = customclass.BranchId
                params1(1).Value = customclass.ApplicationID
                params1(2).Value = customclass.AssetSeqno
                params1(3).Value = customclass.RepossessseqNo
                params1(4).Value = i + 1
                params1(5).Value = customclass.ListBidder.Rows(i).Item("BidderName")
                params1(6).Value = customclass.ListBidder.Rows(i).Item("BidderAmount")
                params1(7).Value = customclass.ListBidder.Rows(i).Item("BidderDate")
                params1(8).Value = customclass.ListBidder.Rows(i).Item("FormBidder")
                params1(9).Value = customclass.ListBidder.Rows(i).Item("NoTelepon")

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveInventoryAppraisal1a, params1)
            Next

             
            params2.Add(New SqlParameter(PARAM_ESTIMATIONDATE, SqlDbType.DateTime) With {.Value = customclass.EstimationDate}) 
            params2.Add(New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)With {.Value = customclass.ApplicationID})
            params2.Add(New SqlParameter(PARAM_ESTIMATIONPRICE, SqlDbType.Decimal) With {.Value = customclass.EstimationPrice})
            params2.Add(New SqlParameter(PARAM_ESTIMATIONBY, SqlDbType.VarChar, 20)With {.Value = customclass.EstimationBy}) 
            params2.Add(New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)With {.Value = customclass.EstimationNotes})
            params2.Add(New SqlParameter(PARAM_BIDDERAMOUNT, SqlDbType.Decimal) With {.Value = customclass.bidderamount})
            params2.Add(New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)With {.Value = customclass.BusinessDate}) 
            params2.Add(New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50) With {.Value = strApprovalNo})


            params2.Add(New SqlParameter("@MRP", SqlDbType.Decimal) With {.Value = customclass.MRP})
            params2.Add(New SqlParameter("@Grade", SqlDbType.VarChar) With {.Value = customclass.Grade})
            params2.Add(New SqlParameter("@GradeValue", SqlDbType.Int) With {.Value = customclass.GradeValue})

            params2.Add(New SqlParameter("@BiayaTebus", SqlDbType.Decimal) With {.Value = customclass.BiayaRepossess})
            params2.Add(New SqlParameter("@BiayaKoordinasi", SqlDbType.Decimal) With {.Value = customclass.BiayaKoordinasi})
            params2.Add(New SqlParameter("@BiayaMobilisasi", SqlDbType.Decimal) With {.Value = customclass.BiayaMobilisasi})

            params2.Add(New SqlParameter("@PenawarTerbaik", SqlDbType.VarChar)With {.Value = customclass.PenawarTerbaik}) 
            params2.Add(New SqlParameter("@PenawarHarga", SqlDbType.Decimal)With {.Value = customclass.PenawarHarga}) 
            params2.Add(New SqlParameter("@CaraPembayaran", SqlDbType.VarChar)With {.Value = customclass.CaraPembayaran}) 
            params2.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar)With {.Value = customclass.AccountID})  

             
            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveInventoryAppraisal3, params2.ToArray)


            oTrans.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()

        Catch exp As Exception
            oTrans.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("InventoryAppraisal", "SaveInventoryAppraisal", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try

    End Function

    ''Public Function RALSaveDataPrintRAL(ByVal customclass As Entities.RALChangeExec) As Entities.RALChangeExec
    ''    Dim params() As SqlParameter = New SqlParameter(4) {}
    ''    Dim oConnection As New SqlConnection(customclass.strConnection)
    ''    Dim objtrans As SqlTransaction
    ''    Try
    ''        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
    ''        objtrans = oConnection.BeginTransaction
    ''        params(0) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
    ''        params(0).Value = customclass.RALNO

    ''        params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
    ''        params(1).Value = customclass.BusinessDate

    ''        params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
    ''        params(2).Value = customclass.LoginId

    ''        params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
    ''        params(3).Value = customclass.ApplicationID

    ''        params(4) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
    ''        params(4).Direction = ParameterDirection.Output

    ''        SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRALSaveRALPrint, params)
    ''        customclass.hasil = CInt(params(4).Value)
    ''        objtrans.Commit()
    ''        Return customclass
    ''    Catch exp As Exception
    ''        objtrans.Commit()
    ''        WriteException("RALChangeExec", "RALSaveDataPrintRAL", exp.Message + exp.StackTrace)
    ''    Finally
    ''        If oConnection.State = ConnectionState.Open Then oConnection.Close()
    ''        oConnection.Dispose()
    ''    End Try
    ''End Function

    Public Function DataInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        'Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALDataExtend).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InventoryAppraisal", "DataInventoryAppraisal", exp.Message + exp.StackTrace)
        End Try
    End Function


    '=================== RAL On Request ============================
    Public Function RequestDataSelectInventory(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID



            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRequestViewDataSelect, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InventoryAppraisal", "InventoryAppraisalViewDataSelectRequest", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function ViewAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApprovalNo", SqlDbType.Char, 50)
            params(0).Value = customclass.ApprovalNo

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            customclass.Listappraisal = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewAppraisal, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ViewAppraisal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetAccruedInterest(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try


            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter("@dblAccruedInterest", SqlDbType.BigInt)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter("@dblECI", SqlDbType.BigInt)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter("@intDayReg", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spAccruedInterest, params)
            customclass.accruedInterest = CInt(params(2).Value)
            Return customclass
        Catch exp As Exception
            WriteException("GetAccruedItnerest", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ViewAppraisalbidder(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim spName As String
        Try
            params(0) = New SqlParameter("@ApprovalNo", SqlDbType.Char, 50)
            params(0).Value = customclass.approvalno

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            If customclass.TableName = "spViewAppraisalBidderApproval" Then spName = customclass.TableName Else spName = spViewAppraisalBidder

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ViewAppraisal", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub GetMRP(ByRef p_Class As Parameter.InventoryAppraisal)
        Dim m_Table As DataTable
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@AssetCode", SqlDbType.VarChar)
            params(0).Value = p_Class.AssetCode

            params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(1).Value = p_Class.BranchId
            params(2) = New SqlParameter("@ManufacturingYear", SqlDbType.Int)
            params(2).Value = p_Class.ManufacturingYear

            m_Table = SqlHelper.ExecuteDataset(p_Class.strConnection, CommandType.StoredProcedure, "spGetMRP", params).Tables(0)
            If m_Table.Rows.Count > 0 Then
                p_Class.MRP = CDbl(m_Table.Rows(0)(0))
            Else
                p_Class.MRP = 0
            End If
        Catch exp As Exception
            WriteException("GetMRP", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub PrintFormPersetujuanAppraisal(p_Class As Parameter.InventoryAppraisal)

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = p_Class.ApplicationID

            params(1) = New SqlParameter("@BussinessDate", SqlDbType.DateTime)
            params(1).Value = p_Class.BusinessDate

            p_Class.ListReport = SqlHelper.ExecuteDataset(p_Class.strConnection, CommandType.StoredProcedure, "spPrintFormPersetujuanAppraisal", params).Tables(0).DataSet
        Catch exp As Exception
            WriteException("PrintFormPersetujuanAppraisal", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub PrintFormPersetujuanAppraisalSubBid(p_Class As Parameter.InventoryAppraisal)

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = p_Class.ApplicationID
 
            p_Class.ListReport = SqlHelper.ExecuteDataset(p_Class.strConnection, CommandType.StoredProcedure, "spPrintFormPersetujuanAppraisalSubBid", params).Tables(0).DataSet
        Catch exp As Exception
            WriteException("PrintFormPersetujuanAppraisalSubBid", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub PrintFormPersetujuanList(p_Class As Parameter.InventoryAppraisal)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = p_Class.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = p_Class.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = p_Class.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = p_Class.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            p_Class.Listappraisal = SqlHelper.ExecuteDataset(p_Class.strConnection, CommandType.StoredProcedure, "spPrintFormPersetujuan", params).Tables(0)
            p_Class.TotalRecord = CInt(params(4).Value)

        Catch exp As Exception
            WriteException("PrintFormPersetujuanList", exp.Message + exp.StackTrace)
        End Try
    End Sub
End Class
