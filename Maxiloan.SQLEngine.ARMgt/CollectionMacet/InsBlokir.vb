﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsBlokir : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spPagingInsBlokir"
    Private Const LIST_UPDATE As String = "spReleaseBlokirBayar"
#End Region

    Public Function GetInsBlokir(ByVal oCustomClass As Parameter.InsBlokir) As Parameter.InsBlokir
        Dim oReturnValue As New Parameter.InsBlokir
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CollectionMacet.GetInsBlokir")
        End Try
    End Function

    Public Sub ReleaseBlokirBayar(ByVal ocustomClass As Parameter.InsBlokir)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationId
        params(1) = New SqlParameter("@SPNo", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.SPNo
       
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CollectionMacet.ReleaseBlokirBayar")
        End Try
    End Sub

End Class
