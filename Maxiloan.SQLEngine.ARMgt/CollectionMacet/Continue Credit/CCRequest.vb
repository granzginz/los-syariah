

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CCRequest : Inherits DataAccessBase
#Region "Private Const"
    Private Const spCCRequestList As String = "spContinueCreditRequestList"
    Private Const spCCRequestDetail As String = "spContinueCreditRequestDetail"
    Private Const spCCRequestSave As String = "spContinueCreditRequestSave"
    Private Const spCCRequestView As String = "spContinueCreditApprovalView"
    Private Const spRptContinueCreditReq As String = "spRptContinueCreditReq"
    Private Const QueryReason As String = "select reasonID as ID, description as Name from reason where reasontypeid='CC'"
    Private Const spCCRequestListInq As String = "spContinueCreditRequestInquiry"
#End Region
    Public Function CCRequestList(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCCRequestList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CCRequest", "CCRequestList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function CCRequestView(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ApprovalNo", SqlDbType.Char, 50)
        params(2).Value = customclass.Approvalno

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCCRequestView, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CCRequest", "CCRequestView", exp.Message + exp.StackTrace)
        End Try


    End Function

    Public Function CCRequestDetail(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCCRequestDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CCRequest", "CCRequestList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CCRequestSave(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest

        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval


        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = oSaveTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "CC1"
                .RequestDate = customclass.BussinessDate
                .UserRequest = customclass.RequestBy
                .TransactionNo = customclass.ApplicationID
                .ApprovalNote = customclass.Notes
                .ApprovalValue = customclass.RefundAmount
                .UserRequest = customclass.RequestBy
                .UserApproval = customclass.ApproveBy
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.ContinueCredit_Approval
            End With

            customclass.Approvalno = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(1).Value = customclass.CGID

            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationID

            params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params(3).Value = customclass.AssetSeqNo

            params(4) = New SqlParameter("@RepossesSeqNo", SqlDbType.SmallInt)
            params(4).Value = customclass.RepossesSeqNo

            params(5) = New SqlParameter("@Reason", SqlDbType.Char, 10)
            params(5).Value = customclass.Reason

            params(6) = New SqlParameter("@ApproveBy", SqlDbType.VarChar, 12)
            params(6).Value = customclass.ApproveBy

            params(7) = New SqlParameter("@ApproveVia", SqlDbType.VarChar, 10)
            params(7).Value = customclass.ApproveVia

            params(8) = New SqlParameter("@FaxNo", SqlDbType.Char, 10)
            params(8).Value = customclass.FaxNo

            params(9) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
            params(9).Value = customclass.Notes

            params(10) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(10).Value = customclass.Approvalno

            params(11) = New SqlParameter("@ApprovalStatus", SqlDbType.Text)
            params(11).Value = "REQ"

            params(12) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(12).Value = customclass.BussinessDate
            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spCCRequestSave, params)
            oSaveTransaction.Commit()
            Return customclass
        Catch exp As Exception
            oSaveTransaction.Rollback()
            WriteException("CCRequest", "CCRequestSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
        End Try
    End Function

    Public Function GetComboReason(ByVal customclass As Parameter.CCRequest) As DataTable

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.Text, QueryReason).Tables(0)
            Return customclass.listData
        Catch exp As Exception
            WriteException("CCRequest", "GetComboReason", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ContinueCreditReqReport(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = customclass.ApplicationID

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRptContinueCreditReq, params)
        Return customclass
    End Function

    Public Function CCRequestListInq(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCCRequestListInq, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CCRequest", "spCCRequestListInq", exp.Message + exp.StackTrace)
        End Try

    End Function
End Class
