
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALPrinting : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spRALPrintingList As String = "spRALPrintingPaging"
    Private Const spRALViewDataSelect As String = "spRALPrintingView"
    Private Const spRALViewDataCollector As String = "spRALPrintingViewCollector"
    Private Const spRALSaveDataExecutor As String = "spRALPrintingSaveExecutor"
    Private Const spRALViewHistoryExecutor As String = "spRALPrintingViewHistory"
    Private Const spRALSaveRALPrint As String = "spRALPrintingSavePrintRAL"
    Private Const spRALListReport As String = "spRALPrintingRptRAL"
    Private Const spRALListReportCheckList As String = "spRALPrintingRptCheckList"
    Private Const spViewPemberiKuasaSKE As String = "spViewPemberiKuasaSKE"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_EXECUTORID As String = "@ExecutorID"
    Protected Const PARAM_MASABERLAKUSKE As String = "@MASASKE"
    Protected Const PARAM_HASIL As String = "@hasil"
    '================ RAL On Request ==========================
    Private Const spRequestViewDataSelect As String = "spRALOnRequestView"

    Public Function RALListCG(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALListPrinting(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALPrintingList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALListPrinting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALViewDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewDataSelect, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALViewDataSelect", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function RALViewDataCollector(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Try
            If customclass.SPType = "HISTORY" Then
                Dim params() As SqlParameter = New SqlParameter(4) {}

                params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
                params(0).Value = customclass.CurrentPage

                params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
                params(1).Value = customclass.PageSize

                params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
                params(2).Value = customclass.WhereCond

                params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
                params(3).Value = customclass.SortBy

                params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
                params(4).Direction = ParameterDirection.Output

                customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewDataCollector, params).Tables(0)
                customclass.TotalRecord = CInt(params(3).Value)

            ElseIf customclass.SPType = "PEMBERIKUASA" Then
                Dim params() As SqlParameter = New SqlParameter(0) {}

                params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
                params(0).Value = customclass.CGID

                customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPemberiKuasaSKE, params).Tables(0)

            Else
                Dim params() As SqlParameter = New SqlParameter(0) {}

                params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
                params(0).Value = customclass.CGID

                customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewDataCollector, params).Tables(0)
            End If
            
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALViewDataCollector", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALSaveDataExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim oTrans As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter(PARAM_EXECUTORID, SqlDbType.Char, 12)
            params(2).Value = customclass.ExecutorID

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_MasaBerlakuSKE, SqlDbType.SmallInt)
            params(4).Value = customclass.MASASKE

            params(5) = New SqlParameter("@IdPemberiKuasa", SqlDbType.Char, 20)
            params(5).Value = customclass.IdPemberiKuasa

            params(6) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(6).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spRALSaveDataExecutor, params)
            oTrans.Commit()
            customclass.hasil = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            WriteException("AgreementTransfer", "RALSaveDataExecutor", exp.Message + exp.StackTrace)
            Return customclass
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Function

    Public Function RALViewHistoryExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_EXECUTORID, SqlDbType.Char, 12)
            params(0).Value = customclass.ExecutorID

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewHistoryExecutor, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALViewHistoryExecutor", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objtrans = oConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(0).Value = customclass.RALNo

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRALSaveRALPrint, params)
            customclass.hasil = CInt(params(4).Value)
            objtrans.Commit()
            Return customclass
        Catch exp As Exception
            objtrans.Commit()
            WriteException("RALPrinting", "RALSaveDataPrintRAL", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

    Public Function RALListReport(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_EXECUTORID, SqlDbType.Char, 12)
            params(0).Value = customclass.ExecutorID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(2).Value = customclass.ApplicationID

            params(3) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(3).Value = customclass.CGID

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALListReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALListReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALListReportCheckList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_EXECUTORID, SqlDbType.Char, 12)
            params(1).Value = customclass.ExecutorID

            params(2) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(2).Value = customclass.CGID

            params(3) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(3).Value = customclass.RALNo

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALListReportCheckList, params)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALListReportCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function


    '=================== RAL On Request ============================
    Public Function RALViewDataSelectRequest(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRequestViewDataSelect, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALViewDataSelectRequest", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
