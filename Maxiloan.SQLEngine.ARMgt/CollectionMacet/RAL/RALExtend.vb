


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALExtend : Inherits DataAccessBase
    Private Const spRALExtendView As String = "spRALExtendView"
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spListRALExtend As String = "spRALExtendPaging"
    Private Const spRALExtendSave As String = "spRALExtendSave"
    Private Const spRALDataExtend As String = "spRALDataExtend"
    Private Const spRALSaveRALPrint As String = "spRALPrintingSavePrintRAL"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_ENDDATE As String = "@EndDate"
    Protected Const PARAM_NOTES As String = "@Notes"
    Protected Const PARAM_ONHAND As String = "@OnHand"
    Protected Const PARAM_HASIL As String = "@hasil"


    Public Function RALListCG(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALExtendList(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRALExtend, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALExtendList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALExtendView(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALExtendView, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALExtendView", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALExtendSave(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.Char, 10)
            params(2).Value = customclass.BusinessDate

            params(3) = New SqlParameter(PARAM_ENDDATE, SqlDbType.Char, 10)
            params(3).Value = customclass.EndDate

            params(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(4).Value = customclass.Notes

            params(5) = New SqlParameter(PARAM_ONHAND, SqlDbType.Int)
            params(5).Value = customclass.OnHand

            params(6) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(6).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spRALExtendSave, params)
            customclass.hasil = CInt(params(6).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALExtendSave", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALDataExtend(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        'Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALDataExtend).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALDataExtend", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALSaveDataPrint(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(0).Value = customclass.RALNo

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spRALSaveRALPrint, params)
            customclass.hasil = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALExtend", "RALSaveDataPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
