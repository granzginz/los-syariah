
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALRelease : Inherits DataAccessBase
    Private Const spRALReleaseView As String = "spRALExtendView"
    Private Const spRALReleaseSave As String = "spRALReleaseSave"
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spListRALRelease As String = "spRALReleasePaging"
    Private Const spRALDataExtend As String = "spRALDataExtend"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_AGREEMENTNO As String = "@AgreementNo"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationId"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_NOTES As String = "@Notes"
    Protected Const PARAM_HASIL As String = "@hasil"


    Public Function RALListCG(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALRelease", "RALListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALReleaseList(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRALRelease, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALRelease", "RALReleaseList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALDataExtend(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
        Try
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALDataExtend).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALRelease", "RALDataExtend", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALReleaseView(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALReleaseView, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALRelease", "RALReleaseView", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALReleaseSave(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(0).Value = customclass.BusinessDate

            params(1) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.strReasonID

            params(2) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(2).Value = customclass.RALNo

            params(3) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(3).Value = customclass.Notes

            params(4) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
            params(4).Value = customclass.ApplicationID

            params(5) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spRALReleaseSave, params)
            customclass.hasil = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALRelease", "RALReleaseSave", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function CekPrepayment(ByVal customclass As Parameter.RALRelease) As Boolean
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BrancID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter("@SaveStatus", SqlDbType.Bit)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCekPrepayment", params)
            Return CType(params(2).Value, Boolean)
        Catch exp As Exception
            WriteException("RALRelease", "CekPrepayment", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
