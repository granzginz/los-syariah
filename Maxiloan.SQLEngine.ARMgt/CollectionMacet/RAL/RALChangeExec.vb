


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RALChangeExec : Inherits DataAccessBase
    Private Const spListCG As String = "spViewDataCollector"
    Private Const spRALChangeExecList As String = "spRALChangeExecPaging"
    Private Const spRequestViewDataSelect As String = "spChangeExecView"
    Private Const spRALViewDataCollector As String = "spRALChangeExecViewCollector"
    Private Const spRALSaveDataExecutor As String = "spRALChangeExecutorSave"
    Private Const spRALSaveRALPrint As String = "spRALPrintingSavePrintRAL"
    Private Const spRALDataExtend As String = "spRALDataChangeExec"
    Protected Const PARAM_RALN As String = "@RALN"
    Protected Const PARAM_EXECUTORIDOLD As String = "@ExecutorIDOld"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_AGREEMENTNO As String = "@ApplicationID"
    Protected Const PARAM_RALNO As String = "@RALNo"
    Protected Const PARAM_EXECUTORID As String = "@ExecutorID"
    Protected Const PARAM_HASIL As String = "@hasil"
    Protected Const PARAM_NEXTBUSINESSDATE As String = "@NextBD"

    '================ RAL On Request ==========================


    Public Function RALListCG(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(0).Value = customclass.CGID

            params(1) = New SqlParameter(PARAM_STRKEY, SqlDbType.Char, 50)
            params(1).Value = customclass.strKey

            params(2) = New SqlParameter(PARAM_COLLECTORTYPE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.CollectorType

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCG, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALChangeExec", "RALListCG", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALChangeExecList(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALChangeExecList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALChangeExec", "RALListChangeExec", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function RALViewDataCollector(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_CGID, SqlDbType.Char, 3)
            params(2).Value = customclass.CGID

            params(3) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(3).Direction = ParameterDirection.Output

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALViewDataCollector, params).Tables(0)
            customclass.TotalRecord = CInt(params(3).Value)
            Return customclass
        Catch exp As Exception
            WriteException("RALChangeExec", "RALViewDataCollector", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RALSaveDataExecutor(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim oTrans As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        Try
            oTrans = oConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_RALN, SqlDbType.Char, 20)
            params(0).Value = customclass.RALNOld

            params(1) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_EXECUTORID, SqlDbType.Char, 12)
            params(2).Value = customclass.ExecutorID

            params(3) = New SqlParameter(PARAM_EXECUTORIDOLD, SqlDbType.Char, 12)
            params(3).Value = customclass.CollectorIDOld

            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter(PARAM_NEXTBUSINESSDATE, SqlDbType.DateTime)
            params(5).Value = customclass.NextBD


            params(6) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(6).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spRALSaveDataExecutor, params)
            oTrans.Commit()
            customclass.hasil = CInt(params(6).Value)
            Return customclass
        Catch exp As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            WriteException("RALOnRequest", "RALSaveDataExecutor", exp.Message + exp.StackTrace)
            Return customclass
        End Try

    End Function

    Public Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objtrans = oConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(0).Value = customclass.RALNo

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter(PARAM_HASIL, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRALSaveRALPrint, params)
            customclass.hasil = CInt(params(4).Value)
            objtrans.Commit()
            Return customclass
        Catch exp As Exception
            objtrans.Commit()
            WriteException("RALChangeExec", "RALSaveDataPrintRAL", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

    Public Function RALDataChangeExec(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        'Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRALDataExtend).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALChangeExec", "RALDataChangeExec", exp.Message + exp.StackTrace)
        End Try
    End Function


    '=================== RAL On Request ============================
    Public Function RequestDataSelect(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_RALNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RALNo

            customclass.ListRAL = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRequestViewDataSelect, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("RALChangeExec", "RALViewDataSelectRequest", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
