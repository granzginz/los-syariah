#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CollExpense : Inherits DataAccessBase
#Region "Private Const"
    Private Const spAssetExpenseSave As String = "spAssetExpenseSave"
#End Region
    Public Function CollExpenseList(ByVal customclass As Parameter.CollExpense) As Parameter.CollExpense
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCollExpenseList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoList", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function CollExpenseReqList(ByVal customclass As Parameter.CollExpense) As Parameter.CollExpense
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCollExpenseReqList", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRepo", "AssetRepoList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub CollExpenseSave(ByVal customclass As Parameter.CollExpense)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = oSaveTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "CLEX"
                .RequestDate = customclass.BusinessDate
                .TransactionNo = customclass.ApplicationID
                .ApprovalValue = customclass.ExpenseAmount
                .UserRequest = customclass.LoginId
                .UserApproval = customclass.ApproveBy
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Collection_Expense
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(11) {}

            params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.AgreementNo

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            params(3) = New SqlParameter("@RALNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.RALNo

            params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(4).Value = customclass.Notes

            params(5) = New SqlParameter("@CollectorId", SqlDbType.VarChar, 12)
            params(5).Value = customclass.CollectorID

            params(6) = New SqlParameter("@ExpenseAmount", SqlDbType.Decimal)
            params(6).Value = customclass.ExpenseAmount

            params(7) = New SqlParameter("@ApproveBy", SqlDbType.VarChar, 50)
            params(7).Value = customclass.ApproveBy

            params(8) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(8).Value = strApprovalNo

            params(9) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(9).Value = customclass.BranchId

            params(10) = New SqlParameter("@BankAccountId", SqlDbType.VarChar, 10)
            params(10).Value = customclass.BankAccountID

            params(11) = New SqlParameter("@RequestBy", SqlDbType.VarChar, 12)
            params(11).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spAssetExpenseSave, params)
            oSaveTransaction.Commit()

        Catch exp As Exception
            oSaveTransaction.Rollback()
            WriteException("CollExpense", "CollExpenseSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

End Class
