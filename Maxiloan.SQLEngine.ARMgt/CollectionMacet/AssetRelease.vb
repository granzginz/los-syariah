

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetRelease : Inherits DataAccessBase

    Private Const spAssetReleaseList As String = "spAssetReleaseList"
    Private Const spAssetReleaseDetail As String = "spAssetReleaseDetail"
    Private Const spAssetReleaseSave As String = "spAssetReleaseSave"
    Private Const spAssetReleaseCheckList As String = "spAssetReleaseCheckList"
    Private Const spAssetReleaseCheckListSave As String = "spAssetReleaseCheckSave"


    Public Function AssetReleaseList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output


            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetReleaseList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRelease", "AssetReleaseList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AssetReleaseCheckList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@Applicationid", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params(2).Value = customclass.AssetSeqNo

            params(3) = New SqlParameter("@RepossesSeqNo", SqlDbType.SmallInt)
            params(3).Value = customclass.RepossesSeqNo

            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetReleaseCheckList, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRelease", "AssetReleaseList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function AssetReleaseCheckListSave(ByVal customclass As Parameter.AssetRelease) As Boolean
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        oSaveTransaction = oConnection.BeginTransaction
        If Not AssetReleaseSave(customclass, oSaveTransaction) Then
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            Return False
            Exit Function
        End If

        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(2).Value = customclass.AssetSeqNo

            params(3) = New SqlParameter("@RepossesSeqNo", SqlDbType.SmallInt)
            params(3).Value = customclass.RepossesSeqNo

            For intloop = 0 To customclass.listData.Rows.Count - 1
                With customclass.listData.Rows(intloop)
                    params(4) = New SqlParameter("@CheckListID", SqlDbType.Char, 10)
                    params(4).Value = .Item("CheckListID")

                    params(5) = New SqlParameter("@YNQuestion", SqlDbType.Char, 1)
                    params(5).Value = .Item("YNQuestion")

                    params(6) = New SqlParameter("@Quantity", SqlDbType.Char, 10)
                    params(6).Value = .Item("Quantity")

                    params(7) = New SqlParameter("@Notes", SqlDbType.Text)
                    params(7).Value = .Item("Notes")
                End With
                SqlHelper.ExecuteDataset(oSaveTransaction, CommandType.StoredProcedure, spAssetReleaseCheckListSave, params)
            Next
            oSaveTransaction.Commit()
            Return True
        Catch exp As Exception
            oSaveTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            Return False
            WriteException("AssetRelease", "AssetReleaseList", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

    Public Function AssetReleaseDetail(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(0).Value = customclass.AgreementNo
        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetReleaseDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetRelease", "AssetReleaseList", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function AssetReleaseSave(ByVal customclass As Parameter.AssetRelease, ByRef oSaveTransaction As SqlTransaction) As Boolean
        Dim params() As SqlParameter = New SqlParameter(10) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params(2).Value = customclass.AssetSeqNo

            params(3) = New SqlParameter("@RepossesSeqNo", SqlDbType.SmallInt)
            params(3).Value = customclass.RepossesSeqNo

            params(4) = New SqlParameter("@ReleaseDate", SqlDbType.DateTime)
            params(4).Value = customclass.ReleaseDate

            params(5) = New SqlParameter("@ReleaseBy", SqlDbType.VarChar, 30)
            params(5).Value = customclass.ReleaseBy

            params(6) = New SqlParameter("@ReleaseTo", SqlDbType.VarChar, 30)
            params(6).Value = customclass.ReleaseTo

            params(7) = New SqlParameter("@ReleaseNotes", SqlDbType.Text)
            params(7).Value = customclass.ReleaseNotes

            params(8) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(8).Value = customclass.CGID

            params(9) = New SqlParameter("@Collectorid", SqlDbType.Char, 12)
            params(9).Value = customclass.CollectorID

            params(10) = New SqlParameter("@BussinessDate", SqlDbType.DateTime)
            params(10).Value = customclass.BussinessDate

            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, spAssetReleaseSave, params)
            Return True
        Catch exp As Exception
            'oSaveTransaction.Rollback()
            Return False
            WriteException("AssetRelease", "AssetReleaseList", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
