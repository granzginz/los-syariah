#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class CollPrintBPK : Inherits DataAccessBase
#Region "Constanta"
    
    Private Const spPrinBPKList As String = "spPrintBPKList"
#End Region
#Region "PrintBPKList"
    Public Function PrintBPKList(ByVal customclass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPrinBPKList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CollPrintBPK", "CollPrintBPKList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "PrintBPKReport"
    Public Function PrintBPKReport(ByVal customclass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@ExecutorID", SqlDbType.Char, 20)
            params(1).Value = customclass.ExecutorID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = customclass.BranchId
            params(3) = New SqlParameter("@RepossesSeqNo", SqlDbType.Char, 2)
            params(3).Value = customclass.RepossesSeqNo

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPrintBPKReport", params)
            Return customclass
        Catch exp As Exception
            WriteException("CollPrintBPK", "CollPrintBPKViewer", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
End Class
