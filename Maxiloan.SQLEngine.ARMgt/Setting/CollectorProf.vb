

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class CollectorProf : Inherits DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const Collector_LIST As String = "spCollProfPaging"
    Private Const Collector_BYID As String = "spCollProfByID"
    Private Const Collector_ADD As String = "spCollProfAdd"
    Private Const Collector_EDT As String = "spCollProfEdit"
    Private Const Collector_DEL As String = "spCollProfDelete"
    Private Const Collector_RPT As String = "spCollProfReport"
    Private Const Collector_ZIPCODE As String = "spCollProfZipCode"
    Private Const Collector_DETAIL As String = "spCollectorProfDetail"
    Private Const Collector_RAL As String = "spCollSettingRAL"

    Private Const GET_BRANCHADDRESS As String = "spGetBranchAddress"
    Private Const Supervisor_CBO As String = "spGetSupervisorCombo"


    'CollectorMOU
    Private Const spCollSettingMOUPaging As String = "spCollSettingMOUPaging"
    Private Const spCollSettingMOUByID As String = "spCollSettingMOUByID"
    Private Const spCollSettingMOUAdd As String = "spCollSettingMOUAdd"
    Private Const spCollSettingMOUEdit As String = "spCollSettingMOUEdit"
    Private Const spCollSettingMOUDelete As String = "spCollSettingMOUDelete"
    Private Const spCollectorMOUProfDetail As String = "spCollectorMOUProfDetail"
    Private Const spCollSettingMOUExtendUpdate As String = "spCollSettingMOUExtendUpdate"
    Private Const spCollSettingMOUExtendHistory As String = "spCollSettingMOUExtendHistory"


    ' Private Const GET_BRANCHADDRESS As String = "spGetBranchAddress"
    Private m_connection As SqlConnection
#End Region

    Public Function GetCollectorProfList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(5) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_LIST, params).Tables(0)
            oCollector.TotalRecord = CType(params(5).Value, Int16)
            Return oCollector
        Catch exp As Exception
            WriteException("CollectorProf", "GetCollectorProfList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorProfByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = oCollector.CGID

            params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
            params(1).Value = oCollector.CollectorID
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_BYID, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("CollectorProf", "GetCollectorProfByID", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorProfReport(ByVal strConnection As String) As Parameter.Collector
        Try
            Dim oCollectorList As New Parameter.Collector
            oCollectorList.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, Collector_RPT).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("CollectorProf", "GetCollectorProfReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorProfAdd(ByVal oCollector As Parameter.Collector, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params(28) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params(2).Value = .CollectorName
                params(3) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
                params(3).Value = .CollectorType
                params(4) = New SqlParameter("@Supervisor", SqlDbType.Char, 10)
                params(4).Value = .Supervisor
                params(5) = New SqlParameter("@Active", SqlDbType.Char, 1)
                params(5).Value = .Active
                params(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
                params(6).Value = .Notes
            End With
            With oClassAddress
                params(7) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params(7).Value = .Address

                params(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(8).Value = .RT
                params(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(9).Value = .RW

                params(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(10).Value = .Kelurahan
                params(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(11).Value = .Kecamatan
                params(12) = New SqlParameter("@City", SqlDbType.VarChar, 30)
                params(12).Value = .City
                params(13) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(13).Value = .ZipCode
                params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
                params(14).Value = .AreaPhone1
                params(15) = New SqlParameter("@PhoneNo1", SqlDbType.Char, 10)
                params(15).Value = .Phone1
                params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
                params(16).Value = .AreaPhone2
                params(17) = New SqlParameter("@PhoneNo2", SqlDbType.Char, 10)
                params(17).Value = .Phone2

                params(18) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
                params(18).Value = oClassAddress.AreaFax
                params(19) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
                params(19).Value = oClassAddress.Fax
            End With

            With oClassPersonal
                params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
                params(20).Value = .MobilePhone
                params(21) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
                params(21).Value = .Email
            End With

            With oCollector   
                params(22) = New SqlParameter("@BankID", SqlDbType.Char, 5)
                params(22).Value = .collBankID
                params(23) = New SqlParameter("@BankBranchId", SqlDbType.Int)
                params(23).Value = .collBankBranchID
                params(24) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
                params(24).Value = .collAcountName
                params(25) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
                params(25).Value = .collAccountNo
                params(26) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
                params(26).Value = .IDNumber
                params(27) = New SqlParameter("@CollectorStatus", SqlDbType.Char, 1)
                params(27).Value = .CollectorStatus
                params(28) = New SqlParameter("@CollectorNPWP", SqlDbType.VarChar, 35)
                params(28).Value = .CollectorNPWP
            End With


            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_ADD, params)
        Catch exp As Exception
            WriteException("CollectorProf", "CollectorProfAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Sub CollectorProfEdit(ByVal oCollector As Parameter.Collector, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params(28) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params(2).Value = .CollectorName
                params(3) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
                params(3).Value = .CollectorType
                params(4) = New SqlParameter("@Supervisor", SqlDbType.Char, 10)
                params(4).Value = .Supervisor
                params(5) = New SqlParameter("@Active", SqlDbType.Char, 1)
                params(5).Value = .Active
                params(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
                params(6).Value = .Notes
            End With


            With oClassAddress
                params(7) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params(7).Value = .Address
                params(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(8).Value = .RT
                params(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(9).Value = .RW
                params(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(10).Value = .Kelurahan
                params(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(11).Value = .Kecamatan
                params(12) = New SqlParameter("@City", SqlDbType.VarChar, 30)
                params(12).Value = .City
                params(13) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(13).Value = .ZipCode
                params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
                params(14).Value = .AreaPhone1
                params(15) = New SqlParameter("@PhoneNo1", SqlDbType.Char, 10)
                params(15).Value = .Phone1
                params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
                params(16).Value = .AreaPhone2
                params(17) = New SqlParameter("@PhoneNo2", SqlDbType.Char, 10)
                params(17).Value = .Phone2
                params(18) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
                params(18).Value = oClassAddress.AreaFax
                params(19) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
                params(19).Value = oClassAddress.Fax
            End With

            With oClassPersonal
                params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
                params(20).Value = .MobilePhone
                params(21) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
                params(21).Value = .Email
            End With

            With oCollector
                params(22) = New SqlParameter("@BankID", SqlDbType.Char, 5)
                params(22).Value = .collBankID
                params(23) = New SqlParameter("@BankBranchId", SqlDbType.Int)
                params(23).Value = .collBankBranchID
                params(24) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
                params(24).Value = .collAcountName
                params(25) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
                params(25).Value = .collAccountNo
                params(26) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
                params(26).Value = .IDNumber
                params(27) = New SqlParameter("@CollectorStatus", SqlDbType.Char, 1)
                params(27).Value = .CollectorStatus
                params(28) = New SqlParameter("@CollectorNPWP", SqlDbType.VarChar, 35)
                params(28).Value = .CollectorNPWP
            End With


            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_EDT, params)
        Catch exp As Exception
            WriteException("CollectorProf", "CollectorProfEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Sub CollectorProfDelete(ByVal oCollector As Parameter.Collector)
        Dim params(1) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_DEL, params)
        Catch exp As Exception
            WriteException("CollectorProf", "CollectorProfDelete", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Function GetCollectorProfDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
            params(0).Value = oCollector.CollectorID
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_DETAIL, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("CollectorProf", "GetCollectorProfDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = oCollector.CGID
            params(1) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
            params(1).Value = oCollector.CollectorType
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Supervisor_CBO, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("CollectorProf", "GetSupervisorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function




#Region "CollectorMOU"


    'CollectorMOU
    Public Function GetCollectorMOUList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(5) As SqlParameter
        Try
            With oCollector

                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond2
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output

            End With
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUPaging, params).Tables(0)
            oCollector.TotalRecord = CType(params(5).Value, Int16)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorMOUByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
        params(0).Value = oCollector.CGID
        params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
        params(1).Value = oCollector.CollectorID
        Try
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUByID, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorByID", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function GetBranchAddress(ByVal Collector As Parameter.Collector) As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = Collector.BranchId

            Dim branchaddress As String
            branchaddress = CStr(SqlHelper.ExecuteScalar(Collector.strConnection, CommandType.StoredProcedure, GET_BRANCHADDRESS, params))
            Return branchaddress
        Catch exp As Exception
            WriteException("Collector", "GetBranchAddress", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorMOUAdd(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = .BranchId
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@NoMOU", SqlDbType.Char, 9)
                params(2).Value = .NoMOU
                params(3) = New SqlParameter("@TglMOU", SqlDbType.SmallDateTime)
                params(3).Value = .TglMOU
                params(4) = New SqlParameter("@TglExpiredMOU", SqlDbType.SmallDateTime)
                params(4).Value = .TglExpiredMOU
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUAdd, params)
        Catch exp As Exception
            WriteException("Collector", "CollectorAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Sub CollectorMOUEdit(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = oCollector.BranchId
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 13)
                params(1).Value = oCollector.CollectorID
                params(2) = New SqlParameter("@NoMOU", SqlDbType.Char, 9)
                params(2).Value = oCollector.NoMOU
                params(3) = New SqlParameter("@TglMOU", SqlDbType.SmallDateTime)
                params(3).Value = oCollector.TglMOU
                params(4) = New SqlParameter("@TglExpiredMOU", SqlDbType.SmallDateTime)
                params(4).Value = oCollector.TglExpiredMOU
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUEdit, params)
        Catch ex As Exception
            WriteException("collector", "CollectorMOUEdit", ex.Message + ex.StackTrace)
            Throw New Exception("Unable to edit selected record")
        End Try
    End Sub

    Public Sub CollectorMOUDelete(ByVal oCollector As Parameter.Collector)
        Dim params(2) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = .BranchId
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@NoMOU", SqlDbType.Char, 12)
                params(2).Value = .NoMOU
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUDelete, params)

        Catch exp As Exception
            WriteException("Collector", "CollectorDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try

    End Sub


    Public Function GetCollectorMOUDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector

        Dim params(0) As SqlParameter
        Try

            params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
            params(0).Value = oCollector.CollectorID

            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, spCollectorMOUProfDetail, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorMOUExtendUpdate(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = .BranchId
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@NoMOU", SqlDbType.Char, 9)
                params(2).Value = .NoMOU
                params(3) = New SqlParameter("@TglMOU", SqlDbType.SmallDateTime)
                params(3).Value = .TglMOU
                params(4) = New SqlParameter("@ExtendFrom", SqlDbType.SmallDateTime)
                params(4).Value = .TglExpiredMOU
                params(5) = New SqlParameter("@ExtendTo", SqlDbType.SmallDateTime)
                params(5).Value = .TglExpiredMOUNew
                params(6) = New SqlParameter("@Keterangan", SqlDbType.VarChar, 255)
                params(6).Value = .ket
                params(7) = New SqlParameter("@TglExpiredMOU", SqlDbType.SmallDateTime)
                params(7).Value = .TglExpiredMOUNew
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUExtendUpdate, params)
        Catch exp As Exception
            WriteException("Collector", "CollectorMOUExtendUpdate", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Function CollectorMOUExtendHistory(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .BranchId
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@NoMOU", SqlDbType.Char, 9)
                params(2).Value = .NoMOU
                ' params(3) = New SqlParameter("@TglMOU", SqlDbType.SmallDateTime)
                'params(3).Value = .TglMOU
                'params(4) = New SqlParameter("@TglExpiredMOU", SqlDbType.SmallDateTime)
                'params(4).Value = .TglExpiredMOU
                'params(5) = New SqlParameter("@Keterangan", SqlDbType.VarChar, 255)
                'params(5).Value = .ket
                'params(7) = New SqlParameter("@TglExpiredMOU", SqlDbType.SmallDateTime)
                'params(7).Value = .TglExpiredMOUNew
            End With
            'SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUExtendHistory, params)
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, spCollSettingMOUExtendHistory, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "CollectorMOUExtendHistory", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Function


#End Region

End Class
