

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CasesSetting : Inherits DataAccessBase
    Private Const spListCasesSetting As String = "spCasesSettingPaging"
    Private Const spReportCasesSetting As String = "spCasesSettingReport"
    Private Const spCasesSettingAdd As String = "spCasesSettingAdd"
    Private Const spCasesSettingEdit As String = "spCasesSettingEdit"
    Private Const spCasesSettingDelete As String = "spCasesSettingDelete"
    Protected Const PARAM_CASESID As String = "@casesid"
    Protected Const PARAM_DESCRIPTION As String = "@description"




    Public Function ListCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListCasesSetting = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCasesSetting, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CasesSetting", "ListCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddCasesSetting(ByVal customclass As Parameter.CasesSetting) As String
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CASESID, SqlDbType.Char, 10)
            params(0).Value = customclass.CasesID


            params(1) = New SqlParameter("@Konsumen", SqlDbType.Bit)
            params(1).Value = customclass.konsumen

            params(2) = New SqlParameter("@Unit", SqlDbType.Bit)
            params(2).Value = customclass.unit

            params(3) = New SqlParameter("@Klas", SqlDbType.Char, 1)
            params(3).Value = customclass.klas

            params(4) = New SqlParameter("@Penyebab", SqlDbType.VarChar, 50)
            params(4).Value = customclass.penyebab

            params(5) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCasesSettingAdd, params)
            Return CStr(params(5).Value)
        Catch exp As Exception
            WriteException("CasesSetting", "AddCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function EditCasesSetting(ByVal customclass As Parameter.CasesSetting) As String
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter(PARAM_CASESID, SqlDbType.Char, 10)
            params(0).Value = customclass.CasesID

            params(1) = New SqlParameter("@Konsumen", SqlDbType.Bit)
            params(1).Value = customclass.konsumen

            params(2) = New SqlParameter("@Unit", SqlDbType.Bit)
            params(2).Value = customclass.unit

            params(3) = New SqlParameter("@Klas", SqlDbType.Char, 1)
            params(3).Value = customclass.klas

            params(4) = New SqlParameter("@Penyebab", SqlDbType.VarChar, 50)
            params(4).Value = customclass.penyebab

            params(5) = New SqlParameter("@casesidnew", SqlDbType.Char, 10)
            params(5).Value = customclass.CasesIDnew

            params(6) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCasesSettingEdit, params)
            Return CStr(params(6).Value)
        Catch exp As Exception
            WriteException("CasesSetting", "EditCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DeleteCasesSetting(ByVal customclass As Parameter.CasesSetting) As String

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_CASESID, SqlDbType.Char, 10)
            params(0).Value = customclass.CasesID

            params(1) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCasesSettingDelete, params)
            Return CStr(params(1).Value)
        Catch exp As Exception
            WriteException("CasesSetting", "DeleteCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
        Try
            customclass.ListCasesSetting = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportCasesSetting).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CasesSetting", "ReportCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListCases(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting 
        Try
            customclass.ListCasesSetting = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spListKasus").Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CasesSetting", "ReportCasesSetting", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class

