
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class IncentiveBL : Inherits DataAccessBase
#Region " Private Const "
    Private Const INCENTIVEBL_PAGING As String = "spIncentiveBLPaging"
    Private Const INCENTIVEBL_SAVE As String = "spIncentiveBLSave"
    Private Const INCENTIVEBL_UPDATE As String = "spIncentiveBLUpdate"
    Private Const INCENTIEBL_DELETE As String = "spIncentiveBLDelete"
    Private Const INCENTIVEBL_REPORTS As String = "spIncentiveBLReport"
    Private m_connection As SqlConnection
#End Region
#Region "IncentiveBLPaging"

    Public Function IncentiveBLPaging(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBL_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBL", "IncentiveBLPaging", exp.Message + exp.StackTrace)

        End Try
    End Function

#End Region
#Region "IncentiveBLSave"

    Public Sub IncentiveBLSave(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim objtrans As SqlTransaction
        Dim params(3) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@minPercentage", SqlDbType.Decimal)
            params(0).Value = oCustomClass.MinPercentage
            params(1) = New SqlParameter("@MaxPercentage", SqlDbType.Decimal)
            params(1).Value = oCustomClass.MaxPercentage
            params(2) = New SqlParameter("@Point", SqlDbType.Int)
            params(2).Value = oCustomClass.Point
            params(3) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(3).Value = oCustomClass.Tariff

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBL_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBL", "IncentiveBLSave", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region

#Region "IncentiveBLUpdate"

    Public Sub IncentiveBLUpdate(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim objtrans As SqlTransaction
        Dim params(3) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@minPercentage", SqlDbType.Decimal)
            params(0).Value = oCustomClass.MinPercentage
            params(1) = New SqlParameter("@MaxPercentage", SqlDbType.Decimal)
            params(1).Value = oCustomClass.MaxPercentage
            params(2) = New SqlParameter("@Point", SqlDbType.Int)
            params(2).Value = oCustomClass.Point
            params(3) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(3).Value = oCustomClass.Tariff


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBL_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBL", "IncentiveBLUpdate", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveBLDelete"

    Public Sub IncentiveBLDelete(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim objtrans As SqlTransaction
        Dim params(0) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@minPercentage", SqlDbType.Decimal)
            params(0).Value = oCustomClass.MinPercentage

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIEBL_DELETE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBL", "IncentiveBLDELETE", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveBL Reports"
    Public Function IncentiveBLReports(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL

        Try
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBL_REPORTS)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBL", "IncentiveBLReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region


End Class
