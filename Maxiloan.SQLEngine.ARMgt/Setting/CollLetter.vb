
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class CollLetter : Inherits DataAccessBase
#Region " Private Const "
    Private Const COLLLETTER_PAGING As String = "spCollLetterPaging"
    Private Const COLLLETTER_FIELD_PAGING As String = "spCollLetterFieldPaging"
    Private Const COLLLETTER_SAVE As String = "spCollLetterSave"
    Private Const COLLLETTER_UPDATE As String = "spCollLetterUpdate"
    Private Const COLLLETTER_DELETE As String = "spCollLetterDelete"
    Private Const COLLLETTER_REPORTS As String = "spCollLetterReports"
    Private m_connection As SqlConnection
#End Region
#Region "CollLetterFieldPaging"
    Public Function CollLetterFieldPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, COLLLETTER_FIELD_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("CollLetterField", "CollLetterFieldPaging", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region


#Region "CollLetterPaging"
    Public Function CollLetterPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, COLLLETTER_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("CollLetter", "CollLetterPaging", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region


#Region "CollLetterSave"

    Public Sub CollLetterSave(ByVal oCustomClass As Parameter.CollLetter)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.LetterId
            params(1) = New SqlParameter("@LetterName", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.LetterName
            params(2) = New SqlParameter("@Template", SqlDbType.Text)
            params(2).Value = oCustomClass.Template


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, COLLLETTER_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("CollLetter", "CollLetterSave", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "CollLetterUpdate"

    Public Sub CollLetterUpdate(ByVal oCustomClass As Parameter.CollLetter)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.LetterId
            params(1) = New SqlParameter("@LetterName", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.LetterName
            params(2) = New SqlParameter("@Template", SqlDbType.Text)
            params(2).Value = oCustomClass.Template


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, COLLLETTER_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("CollLetter", "CollLetterUpdate", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "CollLetterDelete"

    Public Sub CollLetterDelete(ByVal oCustomClass As Parameter.CollLetter)
        Dim objtrans As SqlTransaction
        Dim params(0) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@LetterId", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.LetterId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, COLLLETTER_DELETE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("CollLetter", "CollLetterDELETE", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "CollLetter Reports"
    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim params(1) As SqlParameter

        Try
            params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomClass.WhereCond
            params(1) = New SqlParameter("@Sort", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.SortBy
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, COLLLETTER_REPORTS, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("CollLetter", "CollLetterReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
End Class
