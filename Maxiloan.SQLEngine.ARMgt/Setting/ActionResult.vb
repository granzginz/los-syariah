

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class ActionResult : Inherits DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const ACTION_LIST As String = "spCollActionList"
    Private Const ACTION_BYID As String = "spCollActionByID"
    Private Const ACTION_ADD As String = "spCollActionAdd"
    Private Const ACTION_EDT As String = "spCollActionEdit"
    Private Const ACTION_DEL As String = "spCollActionDelete"
    Private Const ACTION_RPT As String = "spCollActionReport"

    Private Const RESULT_LIST As String = "spCollResultList"
    Private Const RESULT_BYID As String = "spCollResultByID"
    Private Const RESULT_ADD As String = "spCollResultAdd"
    Private Const RESULT_EDT As String = "spCollResultEdit"
    Private Const RESULT_DEL As String = "spCollResultDelete"

    Private Const RESULT_CAT As String = "spCollGetResultCategory"

#End Region

#Region "ACTION"

    Public Function GetActionList(ByVal oAction As Parameter.ActionResult) As Parameter.ActionResult

        Dim params(4) As SqlParameter
        Try
            With oAction
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With


            Dim oActionList As New Parameter.ActionResult

            oActionList.ListData = SqlHelper.ExecuteDataset(oAction.strConnection, CommandType.StoredProcedure, ACTION_LIST, params).Tables(0)
            oActionList.TotalRecord = CType(params(4).Value, Int16)
            Return oActionList
        Catch exp As Exception
            WriteException("ActionResult", "GetActionList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetActionByID(ByVal oAction As Parameter.ActionResult) As Parameter.ActionResult

        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
        params(0).Value = oAction.actionid

        Try
            Dim oActionList As New Parameter.ActionResult

            oActionList.ListData = SqlHelper.ExecuteDataset(oAction.strConnection, CommandType.StoredProcedure, ACTION_BYID, params).Tables(0)
            Return oActionList

        Catch exp As Exception
            WriteException("ActionResult", "GetActionByID", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetActionReport(ByVal strConnection As String) As Parameter.ActionResult

        Try
            Dim oActionList As New Parameter.ActionResult

            oActionList.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, ACTION_RPT).Tables(0)

            Return oActionList
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch exp As Exception
            WriteException("ActionResult", "GetActionReport", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Sub ActionAdd(ByVal oAction As Parameter.ActionResult)
        Dim params(1) As SqlParameter
        With oAction
            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@ActionName", SqlDbType.VarChar, 50)
            params(1).Value = .actionname
        End With
        Try
            SqlHelper.ExecuteNonQuery(oAction.strConnection, CommandType.StoredProcedure, ACTION_ADD, params)
        Catch exp As Exception
            WriteException("ActionResult", "ActionAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Sub ActionEdit(ByVal oAction As Parameter.ActionResult)
        Dim params(1) As SqlParameter
        With oAction
            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@ActionName", SqlDbType.VarChar, 50)
            params(1).Value = .actionname
        End With
        Try
            SqlHelper.ExecuteNonQuery(oAction.strConnection, CommandType.StoredProcedure, ACTION_EDT, params)

        Catch exp As Exception
            WriteException("ActionResult", "ActionEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO : record cannot be edit ")

        End Try

    End Sub

    Public Sub ActionDelete(ByVal oAction As Parameter.ActionResult)

        Dim params(0) As SqlParameter

        With oAction

            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid

        End With


        Try
            SqlHelper.ExecuteNonQuery(oAction.strConnection, CommandType.StoredProcedure, ACTION_DEL, params)

        Catch exp As Exception
            WriteException("ActionResult", "ActionDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub

#End Region

#Region "RESULT"

    Public Function GetResultList(ByVal oResult As Parameter.ActionResult) As Parameter.ActionResult
        Dim params(5) As SqlParameter
        With oResult
            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(1).Value = .CurrentPage
            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(2).Value = .PageSize
            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(3).Value = .WhereCond
            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(4).Value = .SortBy
            params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output
        End With

        Try
            Dim oResultList As New Parameter.ActionResult

            oResultList.ListData = SqlHelper.ExecuteDataset(oResult.strConnection, CommandType.StoredProcedure, RESULT_LIST, params).Tables(0)
            oResultList.TotalRecord = CType(params(5).Value, Int16)

            Return oResultList
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch exp As Exception
            WriteException("ActionResult", "GetResultList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetResultByID(ByVal oResult As Parameter.ActionResult) As Parameter.ActionResult

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
        params(0).Value = oResult.resultid
        params(1) = New SqlParameter("@ResultID", SqlDbType.Char, 10)
        params(1).Value = oResult.resultid


        Try
            Dim oResultList As New Parameter.ActionResult

            oResultList.ListData = SqlHelper.ExecuteDataset(oResult.strConnection, CommandType.StoredProcedure, RESULT_BYID, params).Tables(0)
            Return oResultList

        Catch exp As Exception
            WriteException("ActionResult", "GetResultByID", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub ResultAdd(ByVal oResult As Parameter.ActionResult)

        Dim params(3) As SqlParameter

        With oResult
            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@ResultID", SqlDbType.Char, 10)
            params(1).Value = .resultid
            params(2) = New SqlParameter("@ResultName", SqlDbType.VarChar, 100)
            params(2).Value = .resultname
            params(3) = New SqlParameter("@ResultCategory", SqlDbType.Char, 10)
            params(3).Value = .resultcategory
        End With
        Try
            SqlHelper.ExecuteNonQuery(oResult.strConnection, CommandType.StoredProcedure, RESULT_ADD, params)
        Catch exp As Exception
            WriteException("ActionResult", "ResultAdd", exp.Message + exp.StackTrace)
        End Try

    End Sub

    Public Sub ResultEdit(ByVal oResult As Parameter.ActionResult)

        Dim params(3) As SqlParameter

        With oResult
            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@ResultID", SqlDbType.Char, 10)
            params(1).Value = .resultid
            params(2) = New SqlParameter("@ResultName", SqlDbType.VarChar, 100)
            params(2).Value = .resultname
            params(3) = New SqlParameter("@ResultCategory", SqlDbType.Char, 10)
            params(3).Value = .resultcategory
        End With

        Try

            SqlHelper.ExecuteNonQuery(oResult.strConnection, CommandType.StoredProcedure, RESULT_EDT, params)

        Catch exp As Exception
            WriteException("ActionResult", "ResultEdit", exp.Message + exp.StackTrace)
        End Try

    End Sub

    Public Sub ResultDelete(ByVal oResult As Parameter.ActionResult)

        Dim params(1) As SqlParameter

        With oResult

            params(0) = New SqlParameter("@ActionID", SqlDbType.Char, 10)
            params(0).Value = .actionid
            params(1) = New SqlParameter("@ResultID", SqlDbType.Char, 10)
            params(1).Value = .resultid
        End With
        Try
            SqlHelper.ExecuteNonQuery(oResult.strConnection, CommandType.StoredProcedure, RESULT_DEL, params)
        Catch exp As Exception
            WriteException("ActionResult", "ResultDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try

    End Sub

    Public Function GetResultCategory(ByVal strconn As String) As Parameter.ActionResult
        Try
            Dim oResultList As New Parameter.ActionResult
            oResultList.ListData = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, RESULT_CAT).Tables(0)
            Return oResultList
        Catch exp As Exception
            WriteException("ActionResult", "GetResultCategory", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
End Class
