
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class Collector : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const Collector_LIST As String = "spCollSettingPaging"
    Private Const Collector_BYID As String = "spCollSettingByID"
    Private Const Collector_ADD As String = "spCollSettingAdd"
    Private Const Collector_EDT As String = "spCollSettingEdit"
    Private Const Collector_EDT_MAXACC As String = "spCollSettingEditMaxAcc"
    Private Const Collector_DEL As String = "spCollSettingDelete"
    Private Const Collector_RPT As String = "spCollSettingReport"
    Private Const Collector_ZIPCODE As String = "spCollSettingZipCode"
    Private Const Collector_DETAIL As String = "spCollectorProfDetail"
    Private Const Collector_RAL As String = "spCollSettingRAL"

    Private Const GET_BRANCHADDRESS As String = "spGetBranchAddress"
    Private Const Collector_CBO As String = "spGetComboUser"

    Private Const CollectorType_CBO As String = "spGetCollectorType"
    Private Const CollectorSPV_CBO As String = "spGetSupervisor"


    Private m_connection As SqlConnection
#End Region
    Public Function GetCollectorList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(5) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_LIST, params).Tables(0)
            oCollector.TotalRecord = CType(params(5).Value, Int16)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
        params(0).Value = oCollector.CGID
        params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
        params(1).Value = oCollector.CollectorID
        params(2) = New SqlParameter("@CollectorType", SqlDbType.Char, 10)
        params(2).Value = oCollector.CollectorType

        Try
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_BYID, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorByID", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorReport(ByVal strConnection As String) As Parameter.Collector
        Try
            Dim oCollectorList As New Parameter.Collector
            oCollectorList.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, Collector_RPT).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("Collector", "GetCollectorReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetBranchAddress(ByVal Collector As Parameter.Collector) As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = Collector.BranchId

            Dim branchaddress As String
            branchaddress = CStr(SqlHelper.ExecuteScalar(Collector.strConnection, CommandType.StoredProcedure, GET_BRANCHADDRESS, params))
            Return branchaddress
        Catch exp As Exception
            WriteException("Collector", "GetBranchAddress", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorAdd(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@CollectorType", SqlDbType.Char, 9)
                params(2).Value = .CollectorType
                params(3) = New SqlParameter("@Supervisor", SqlDbType.Char, 20)
                params(3).Value = .Supervisor
                params(4) = New SqlParameter("@Active", SqlDbType.Char, 1)
                params(4).Value = .Active
                params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(5).Value = getReferenceDBName()
                params(6) = New SqlParameter("@PasswordMobile", SqlDbType.Char, 20)
                params(6).Value = .PasswordMobile
                params(7) = New SqlParameter("@ImeiMobile", SqlDbType.Char, 100)
                params(7).Value = .ImeiMobile

            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_ADD, params)
        Catch exp As Exception
            WriteException("Collector", "CollectorAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Sub CollectorEdit(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@CollectorType", SqlDbType.VarChar, 10)
                params(2).Value = .CollectorType
                params(3) = New SqlParameter("@Supervisor", SqlDbType.Char, 20)
                params(3).Value = .Supervisor
                params(4) = New SqlParameter("@Active", SqlDbType.Char, 1)
                params(4).Value = .Active
                params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(5).Value = getReferenceDBName()
                params(6) = New SqlParameter("@OldCollectorType", SqlDbType.VarChar, 10)
                params(6).Value = .OldCollectorType
                params(7) = New SqlParameter("@PasswordMobile", SqlDbType.Char, 20)
                params(7).Value = .PasswordMobile
                params(8) = New SqlParameter("@ImeiMobile", SqlDbType.Char, 100)
                params(8).Value = .ImeiMobile

            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_EDT, params)
        Catch exp As Exception
            WriteException("Collector", "CollectorEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Sub CollectorDelete(ByVal oCollector As Parameter.Collector)
        Dim params(2) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@CollectorType", SqlDbType.VarChar, 10)
                params(2).Value = .CollectorType
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_DEL, params)

        Catch exp As Exception
            WriteException("Collector", "CollectorDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try

    End Sub

    Public Function GetCollectorZipCode(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(6) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(2).Value = .CurrentPage
                params(3) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(3).Value = .PageSize
                params(4) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(4).Value = .WhereCond
                params(5) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(5).Value = .SortBy
                params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(6).Direction = ParameterDirection.Output
            End With

            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_ZIPCODE, params).Tables(0)
            oCollector.TotalRecord = CType(params(6).Value, Int16)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorZipCode", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetCollectorDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector

        Dim params(0) As SqlParameter
        Try

            params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
            params(0).Value = oCollector.CollectorID

            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_DETAIL, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorRAL(ByVal oCollector As Parameter.Collector) As Parameter.Collector

        Dim params(6) As SqlParameter

        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
            params(0).Value = oCollector.CGID
            params(1) = New SqlParameter("@ExecutorID", SqlDbType.Char, 20)
            params(1).Value = oCollector.CollectorID
            params(2) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(2).Value = oCollector.CurrentPage
            params(3) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(3).Value = oCollector.PageSize
            params(4) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(4).Value = oCollector.WhereCond
            params(5) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(5).Value = oCollector.SortBy
            params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(6).Direction = ParameterDirection.Output

            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_RAL, params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("CasesSetting", "GetCollectorRAL", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Try
            Dim par() As SqlParameter = New SqlParameter(0) {}

            par(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            par(0).Value = getReferenceDBName()

            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_CBO, par).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorTypeCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params As SqlParameter
        Try
            params = New SqlParameter("@CGID", SqlDbType.Char, 10)
            params.Value = oCollector.CGID
            'params(1) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 100)
            'params(1).Value = IIf(IsDBNull(oCollector.WhereCond), "", oCollector.WhereCond).ToString

            Dim oCollectorList As New Parameter.Collector

            oCollectorList.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, CollectorType_CBO, params).Tables(0)
            Return oCollectorList

        Catch exp As Exception
            WriteException("Collector", "GetCollectorTypeCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
        params(0).Value = oCollector.CGID
        params(1) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 100)
        params(1).Value = IIf(IsDBNull(oCollector.WhereCond), "", oCollector.WhereCond).ToString

        Try
            Dim oCollectorList As New Parameter.Collector
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, CollectorSPV_CBO, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("Collector", "GetSupervisorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorAllocationPaging(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(5) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
            params(0).Value = oCollector.BranchId
            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(1).Value = oCollector.CurrentPage
            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(2).Value = oCollector.PageSize
            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(3).Value = oCollector.WhereCond
            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(4).Value = oCollector.SortBy
            params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, _
            "spCollectorAllocationPaging", params).Tables(0)
            oCollector.TotalRecord = CInt(params(5).Value)
            Return oCollector
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub CollectorAllocationSave(ByVal oCollector As Parameter.Collector)
        Dim params(4) As SqlParameter
        Try
            With oCollector
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = .AppID
                params(1) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(1).Value = .CGID
                params(2) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(2).Value = .CollectorID
                params(3) = New SqlParameter("@DeskCollID", SqlDbType.Char, 20)
                params(3).Value = .DeskCollID
                params(4) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(4).Value = .ZipCode
            End With
            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, _
                "spCollectorAllocationSave", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Function GetCollectorZPList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim oReturnValue As New Parameter.Collector
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCollector.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCollector.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCollector.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCollector.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, "spCollectorZPPaging", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int16)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function CollectorZPSave(ByVal oCollector As Parameter.Collector, ByVal oData As DataTable) As Parameter.Collector
        Dim conn As New SqlConnection(oCollector.strConnection)
        Dim transaction As SqlTransaction = Nothing            
        Dim params(7) As SqlParameter
        Dim oReturn As New Parameter.Collector
        Dim Delete As String = "1"
        Dim intLoop As Integer = 0

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            If oData.Rows.Count > 0 Then
                params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(3) = New SqlParameter("@UsrCr", SqlDbType.VarChar, 50)
                params(4) = New SqlParameter("@DtmCr", SqlDbType.DateTime)
                params(5) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params(6) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 40)
                params(7) = New SqlParameter("@Kota", SqlDbType.VarChar, 40)

                For intLoop = 0 To oData.Rows.Count - 1
                    params(0).Value = oData.Rows(intLoop).Item("CollectorID")
                    params(1).Value = oCollector.BranchId
                    params(2).Value = oData.Rows(intLoop).Item("ZipCode")
                    params(3).Value = oCollector.LoginId
                    params(4).Value = oCollector.BusinessDate
                    params(5).Value = Delete
                    params(6).Value = oCollector.Kecamatan
                    params(7).Value = oCollector.Kota
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCollectorZPSave", params)
                    Delete = "0"
                Next
            Else
                params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(0).Value = ""
                params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1).Value = ""
                params(2) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(2).Value = ""
                params(3) = New SqlParameter("@UsrCr", SqlDbType.VarChar, 50)
                params(3).Value = ""
                params(4) = New SqlParameter("@DtmCr", SqlDbType.DateTime)
                params(4).Value = CDate("01/01/1900")
                params(5) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params(5).Value = "3"
                params(6) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 40)
                params(6).Value = oCollector.Kecamatan
                params(7) = New SqlParameter("@Kota", SqlDbType.VarChar, 40)                
                params(7).Value = oCollector.Kota
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCollectorZPSave", params)
            End If

            transaction.Commit()            
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()            
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function GetCollectorZPDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim oReturnValue As New Parameter.Collector
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCollector.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCollector.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCollector.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCollector.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, "spCollectorZPDetailPaging", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int16)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetCollectorZPByKecamatan(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim oReturnValue As New Parameter.Collector
        Dim params(1) As SqlParameter
        Dim SP As String = ""


        If oCollector.Status = "1" Then
            SP = "spGetCollectorZPByKecamatan"
        ElseIf oCollector.Status = "2" Then
            SP = "spGetKodePosCollectorZP"
        End If


        params(0) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 40)
        params(0).Value = oCollector.Kecamatan
        params(1) = New SqlParameter("@Kota", SqlDbType.VarChar, 40)
        params(1).Value = oCollector.Kota

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, SP, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub CollectorEditMaxAcc(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@MaksimalKontrak", SqlDbType.SmallInt)
                params(2).Value = .MaksimalKontrak
            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, Collector_EDT_MAXACC, params)
        Catch exp As Exception
            WriteException("Collector", "CollectorEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Function GetCollectorDeskCollByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
        params(0).Value = oCollector.CGID
        params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
        params(1).Value = oCollector.CollectorID
        Try
            oCollector.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, "spGetCollectorDeskColl", params).Tables(0)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetCollectorDeskCollByID", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorDeskCollSave(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorID", SqlDbType.Char, 20)
                params(1).Value = .CollectorID
                params(2) = New SqlParameter("@DeskCollID", SqlDbType.Char, 20)
                params(2).Value = .DeskCollID
                params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                params(3).Direction = ParameterDirection.Output
            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spCollectorDeskCollSave", params)
        Catch exp As Exception
            WriteException("Collector", "CollectorDeskCollSave", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Sub RollingWilayahSave(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CollectorA", SqlDbType.Structured)
                params(0).Value = .CollectorA
                params(1) = New SqlParameter("@CollectorB", SqlDbType.Structured)
                params(1).Value = .CollectorB
            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spRollingWilayahSave", params)
        Catch exp As Exception            
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Sub RollingCollectorSave(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CollectorIDA", SqlDbType.Char, 20)
                params(0).Value = .CollectorIDA
                params(1) = New SqlParameter("@CollectorIDB", SqlDbType.Char, 20)
                params(1).Value = .CollectorIDB
            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spRollingCollectorSave", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Sub GantiCollectorSave(ByVal oCollector As Parameter.Collector)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            With oCollector
                params(0) = New SqlParameter("@CollectorA", SqlDbType.Structured)
                params(0).Value = .CollectorA
                params(1) = New SqlParameter("@CollectorIDB", SqlDbType.Char, 12)
                params(1).Value = .CollectorIDB
            End With

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spGantiCollectorSave", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Function GetPemberiKuasaSKEList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = oCollector.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = oCollector.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = oCollector.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = oCollector.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCollector.listPemberiKuasaSKE = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, "spGetPemberiKuasaSKEList", params).Tables(0)
            oCollector.TotalRecord = CInt(params(4).Value)
            Return oCollector
        Catch exp As Exception
            WriteException("Collector", "GetPemberiKuasaSKEList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function PemberiKuasaSKEAdd(ByVal oCollector As Parameter.Collector) As String
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
            params(0).Value = oCollector.EmployeeID

            params(1) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
            params(1).Value = oCollector.EmployeeName

            params(2) = New SqlParameter("@Position", SqlDbType.VarChar, 10)
            params(2).Value = oCollector.Position

            params(3) = New SqlParameter("@strError", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spPemberiKuasaSKEAdd", params)
            Return CType(params(3).Value, String)

        Catch exp As Exception
            WriteException("Collector", "PemberiKuasaSKEAdd", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PemberiKuasaSKEEdit(ByVal oCollector As Parameter.Collector) As String
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
            params(0).Value = oCollector.EmployeeID

            params(1) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
            params(1).Value = oCollector.EmployeeName

            params(2) = New SqlParameter("@Position", SqlDbType.VarChar, 10)
            params(2).Value = oCollector.Position

            params(3) = New SqlParameter("@strError", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spPemberiKuasaSKEEdit", params)
            Return CType(params(3).Value, String)

        Catch exp As Exception
            WriteException("Collector", "PemberiKuasaSKEEdit", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PemberiKuasaSKEDelete(ByVal oCollector As Parameter.Collector) As String
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 10)
            params(0).Value = oCollector.EmployeeID

            params(1) = New SqlParameter("@strError", SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCollector.strConnection, CommandType.StoredProcedure, "spPemberiKuasaSKEDelete", params)
            Return CType(params(1).Value, String)

        Catch exp As Exception
            WriteException("Collector", "PemberiKuasaSKEDelete", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
