

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class IncentiveBM : Inherits DataAccessBase
#Region " Private Const "
    Private Const INCENTIVEBM_PAGING As String = "spIncentiveBMPaging"
    Private Const INCENTIVEBM_SAVE As String = "spIncentiveBMSave"
    Private Const INCENTIVEBM_UPDATE As String = "spIncentiveBMUpdate"
    Private Const INCENTIEBM_COMBO_LIST As String = "spIncentiveBMComboList"
    Private Const INCENTIVEBM_REPORTS As String = "spIncentiveBMReport"
    Private m_connection As SqlConnection
    
#End Region
#Region "IncentiveBMCombolist"
    Public Function IncentiveBMComboList(ByVal oCustomClass As Parameter.IncentiveBM) As DataTable
        Dim params(0) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@BeginEndStatus", SqlDbType.Char, 2)
                params(0).Value = .BeginEndStatus

                Return SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, INCENTIEBM_COMBO_LIST, params).Tables(0)


            End With

        Catch exp As Exception
            WriteException("IncentiveBM", "IncentiveBMComboList", exp.Message + exp.StackTrace)

        End Try
    End Function
#End Region
#Region "IncentiveBMPaging"

#End Region
    Public Function IncentiveBMPaging(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBM_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBM", "IncentiveBMPaging", exp.Message + exp.StackTrace)

        End Try
    End Function
    Public Function IncentiveBMReports(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM
        Dim params(0) As SqlParameter
        Try
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBM_REPORTS)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBM", "IncentiveBMReports", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Sub IncentiveBMSave(ByVal oCustomClass As Parameter.IncentiveBM)
        Dim objtrans As SqlTransaction
        Dim params(4) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BeginStatus", SqlDbType.Char, 2)
            params(0).Value = oCustomClass.BeginStatus
            params(1) = New SqlParameter("@EndStatus", SqlDbType.Char, 2)
            params(1).Value = oCustomClass.EndStatus
            params(2) = New SqlParameter("@Percentage", SqlDbType.Decimal)
            params(2).Precision = 18
            params(2).Scale = 2
            params(2).Value = oCustomClass.Percentage
            params(3) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(3).Value = oCustomClass.Tariff
            params(4) = New SqlParameter("@Active", SqlDbType.Char, 1)
            params(4).Value = oCustomClass.Active

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBM_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBM", "IncentiveBMSave", exp.Message + exp.StackTrace)

        End Try
    End Sub
    Public Sub IncentiveBmUpdate(ByVal oCustomClass As Parameter.IncentiveBM)
        Dim objtrans As SqlTransaction
        Dim params(5) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(0).Value = oCustomClass.Seqno
            params(1) = New SqlParameter("@BeginStatus", SqlDbType.Char, 2)
            params(1).Value = oCustomClass.BeginStatus
            params(2) = New SqlParameter("@EndStatus", SqlDbType.Char, 2)
            params(2).Value = oCustomClass.EndStatus
            params(3) = New SqlParameter("@Percentage", SqlDbType.Decimal)
            params(3).Precision = 18
            params(3).Scale = 2
            params(3).Value = oCustomClass.Percentage
            params(4) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(4).Value = oCustomClass.Tariff
            params(5) = New SqlParameter("@Active", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.Active

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBM_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBM", "IncentiveBMUpdate", exp.Message + exp.StackTrace)
        End Try
    End Sub
End Class
