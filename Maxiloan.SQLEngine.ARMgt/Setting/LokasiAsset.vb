﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LokasiAsset : Inherits DataAccessBase
#Region " Private Const"
    'Stored Procedure Name
    Private Const spListLokasiAsset As String = "spLokasiAssetPaging"
    Private Const spAddLokasiAsset As String = "spLokasiAssetAdd"
    Private Const spEditLokasiAsset As String = "spLokasiAssetEdit"
    Private Const spDeleteLokasiAsset As String = "spLokasiAssetDelete"
    Private Const spReportLokasiAsset As String = "spLokasiAssetReport"
    Private m_connection As SqlConnection

#End Region
    Public Function ListLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listLokasiAsset = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListLokasiAsset, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("LokasiAsset", "ListLokasiAsset", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function EditLokasiAsset(ByVal customclass As Parameter.LokasiAsset, ByVal oClassAddress As Parameter.Address) As String
        Dim params(14) As SqlParameter
        Try
            params(0) = New SqlParameter("@Id", SqlDbType.VarChar, 10)
            params(0).Value = customclass.Id

            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params(1).Value = customclass.Description

            params(2) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(2).Value = ParameterDirection.Output

            With oClassAddress
                params(3) = New SqlParameter("@Alamat", SqlDbType.VarChar, 100)
                params(3).Value = .Address

                params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(4).Value = .RT

                params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(5).Value = .RW

                params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(6).Value = .Kelurahan

                params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(7).Value = .Kecamatan

                params(8) = New SqlParameter("@Kota", SqlDbType.VarChar, 30)
                params(8).Value = .City

                params(9) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
                params(9).Value = .ZipCode
            End With

            params(10) = New SqlParameter("@Kontak", SqlDbType.VarChar, 20)
            params(10).Value = customclass.Kontak

            params(11) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 35)
            params(11).Value = customclass.Jabatan

            params(12) = New SqlParameter("@Handphone", SqlDbType.VarChar, 20)
            params(12).Value = customclass.Handphone

            params(13) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(13).Value = customclass.Email

            params(14) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(14).Value = customclass.Status


            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spEditLokasiAsset, params)

        Catch exp As Exception
            WriteException("LokasiAsset", "EditLokasiAsset", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddLokasiAsset(ByVal customclass As Parameter.LokasiAsset, ByVal oClassAddress As Parameter.Address) As String
        Dim params(14) As SqlParameter
        Try
            params(0) = New SqlParameter("@Id", SqlDbType.VarChar, 10)
            params(0).Value = customclass.Id

            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params(1).Value = customclass.Description

            params(2) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(2).Value = ParameterDirection.Output

            With oClassAddress
                params(3) = New SqlParameter("@Alamat", SqlDbType.VarChar, 100)
                params(3).Value = .Address

                params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(4).Value = .RT

                params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(5).Value = .RW

                params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(6).Value = .Kelurahan

                params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(7).Value = .Kecamatan

                params(8) = New SqlParameter("@Kota", SqlDbType.VarChar, 30)
                params(8).Value = .City

                params(9) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
                params(9).Value = .ZipCode
            End With

            params(10) = New SqlParameter("@Kontak", SqlDbType.VarChar, 20)
            params(10).Value = customclass.Kontak

            params(11) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 35)
            params(11).Value = customclass.Jabatan

            params(12) = New SqlParameter("@Handphone", SqlDbType.VarChar, 20)
            params(12).Value = customclass.Handphone

            params(13) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(13).Value = customclass.Email

            params(14) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(14).Value = customclass.Status

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spAddLokasiAsset, params)

        Catch exp As Exception
            WriteException("LokasiAsset", "AddLokasiAsset", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DeleteLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As String
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@Id", SqlDbType.VarChar, 10)
            params(0).Value = customclass.Id

            params(1) = New SqlParameter("@strerror", SqlDbType.VarChar, 50)
            params(1).Value = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spDeleteLokasiAsset, params)

        Catch exp As Exception
            WriteException("LokasiAsset", "DeleteLokasiAsset", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ReportLokasiAsset = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportLokasiAsset, params)
            Return customclass
        Catch exp As Exception
            WriteException("LokasiAsset", "ReportLokasiAsset", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
