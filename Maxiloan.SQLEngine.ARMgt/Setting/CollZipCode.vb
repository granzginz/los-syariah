

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region



Public Class CollZipCode : Inherits DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const COLLZIPCODE_LIST As String = "spCOLLZIPCODEPaging"
    Private Const COLLZIPCODE_EDT As String = "spCOLLZIPCODEEdit"
    Private Const COLLZIPCODE_RPT As String = "spCOLLZIPCODEReport"
    Private Const GET_CITY As String = "spGetCity"
    Private Const GET_CG As String = "spGetCollectionGroup"

    Private Const COLLECTORZIPCODE_LIST As String = "spCollectorZipCodePaging"
    Private Const COLLECTORZIPCODE_EDT As String = "spCollectorZipCodeEdit"
    Private Const COLLECTORZIPCODE_RPT As String = "spCollectorZipCodeReport"

    Private Const GET_COLLECTOR As String = "spGetCollectorCombo"

#End Region

    Public Function CollZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode

        Dim params(5) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@City", SqlDbType.Char, 30)
                params(0).Value = .City
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oCollZipCode.ListData = SqlHelper.ExecuteDataset(oCollZipCode.strConnection, CommandType.StoredProcedure, COLLZIPCODE_LIST, params).Tables(0)
            oCollZipCode.TotalRecord = CType(params(5).Value, Int16)

            Return oCollZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "CollZipCodeList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCGCombo(ByVal strconn As String) As DataTable

        Dim dtZipCode As New DataTable
        Try
            dtZipCode = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, GET_CG).Tables(0)
            Return dtZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "GetCGCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCityCombo(ByVal strconn As String) As DataTable

        Dim dtZipCode As New DataTable
        Try
            dtZipCode = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, GET_CITY).Tables(0)
            Return dtZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "GetCityCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)
        Dim params(4) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@City", SqlDbType.Char, 30)
                params(0).Value = .City
                params(1) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(1).Value = .CGID
                params(2) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(2).Value = .ZipCode
                params(3) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(3).Value = .Kecamatan
                params(4) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(4).Value = .Kelurahan
            End With
            SqlHelper.ExecuteNonQuery(oCollZipCode.strConnection, CommandType.StoredProcedure, COLLZIPCODE_EDT, params)
        Catch exp As Exception
            WriteException("CollZipCode", "CollZipCodeEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Function CollectorZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
        Dim params(5) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(1).Value = .CurrentPage
                params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(2).Value = .PageSize
                params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(3).Value = .WhereCond
                params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(4).Value = .SortBy
                params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(5).Direction = ParameterDirection.Output
            End With
            oCollZipCode.ListData = SqlHelper.ExecuteDataset(oCollZipCode.strConnection, CommandType.StoredProcedure, COLLECTORZIPCODE_LIST, params).Tables(0)
            oCollZipCode.TotalRecord = CType(params(5).Value, Integer)
            Return oCollZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "CollectorZipCodeList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollectorCombo(ByVal oCollZipCode As Parameter.CollZipCode) As DataTable
        Dim params(1) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
                params(1).Value = .CollectorType
            End With
            Dim dtZipCode As New DataTable
            dtZipCode = SqlHelper.ExecuteDataset(oCollZipCode.strConnection, CommandType.StoredProcedure, GET_COLLECTOR, params).Tables(0)
            Return dtZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "GetCollectorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollectorZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)
        Dim params(4) As SqlParameter
        Try
            With oCollZipCode
                params(0) = New SqlParameter("@CollectorID", SqlDbType.Char, 12)
                params(0).Value = .CollectorID
                params(1) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(1).Value = .CGID
                params(2) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(2).Value = .ZipCode
                params(3) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(3).Value = .Kecamatan
                params(4) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(4).Value = .Kelurahan
            End With
            SqlHelper.ExecuteNonQuery(oCollZipCode.strConnection, CommandType.StoredProcedure, COLLECTORZIPCODE_EDT, params)
        Catch exp As Exception
            WriteException("CollZipCode", "CollectorZipCodeEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Function CollGroupZipCodeReport(ByVal strconn As String, ByVal city As String) As Parameter.CollZipCode
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@City", SqlDbType.Char, 30)
            params(0).Value = city
            Dim CollZipCode As New Parameter.CollZipCode
            CollZipCode.ListData = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, COLLZIPCODE_RPT, params).Tables(0)
            Return CollZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "CollGroupZipCodeReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CollectorZipCodeReport(ByVal strconn As String, ByVal cgid As String) As Parameter.CollZipCode
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = cgid
            Dim CollZipCode As New Parameter.CollZipCode
            CollZipCode.ListData = SqlHelper.ExecuteDataset(strconn, CommandType.StoredProcedure, COLLECTORZIPCODE_RPT, params).Tables(0)
            Return CollZipCode
        Catch exp As Exception
            WriteException("CollZipCode", "CollectorZipCodeReport", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
