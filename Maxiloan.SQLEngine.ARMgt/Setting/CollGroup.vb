

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region

Public Class CollGroup : Inherits DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const COLLGROUP_LIST As String = "spCollGroupList"
    Private Const COLLGROUP_BYID As String = "spCollGroupByID"
    Private Const COLLGROUP_ADD As String = "spCollGroupAdd"
    Private Const COLLGROUP_EDT As String = "spCollGroupEdit"
    Private Const COLLGROUP_DEL As String = "spCollGroupDelete"
    Private Const COLLGROUP_RPT As String = "spCollGroupReport"

    Private Const GET_BRANCHADDRESS As String = "spGetBranchAddress"
    Private Const GET_BRANCHCOMBO As String = "spGetAllBranch"
#End Region

    Public Function GetCollGroupList(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup

        Dim params(4) As SqlParameter
        Try
            With oCollGroup
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCollGroup.ListData = SqlHelper.ExecuteDataset(oCollGroup.strConnection, CommandType.StoredProcedure, COLLGROUP_LIST, params).Tables(0)
            oCollGroup.TotalRecord = CType(params(4).Value, Int16)

            Return oCollGroup
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch exp As Exception
            WriteException("CollGroup", "GetCollGroupList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollGroupByID(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
            params(0).Value = oCollGroup.CGID
            oCollGroup.ListData = SqlHelper.ExecuteDataset(oCollGroup.strConnection, CommandType.StoredProcedure, COLLGROUP_BYID, params).Tables(0)
            Return oCollGroup
        Catch exp As Exception
            WriteException("CollGroup", "GetCollGroupList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCollGroupReport(ByVal strConnection As String) As Parameter.CollGroup
        Try
            Dim oCollGroup As Parameter.CollGroup
            oCollGroup.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, COLLGROUP_RPT).Tables(0)
            Return oCollGroup
        Catch exp As Exception
            WriteException("CollGroup", "GetCollGroupReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetBranchAddress(ByVal collgroup As Parameter.CollGroup) As DataTable

        Dim params() As SqlParameter = New SqlParameter(0) {}

        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = collgroup.BranchId
            Dim dtbranchaddress As New DataTable
            dtbranchaddress = SqlHelper.ExecuteDataset(collgroup.strConnection, CommandType.StoredProcedure, GET_BRANCHADDRESS, params).Tables(0)
            Return dtbranchaddress
        Catch exp As Exception
            WriteException("CollGroup", "GetBranchAddress", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CollGroupAdd(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCollGroup.strConnection)
        Dim params(20) As SqlParameter
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oCollGroup
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CGName", SqlDbType.VarChar, 50)
                params(1).Value = .CGName
                params(2) = New SqlParameter("@CGHead", SqlDbType.Char, 10)
                params(2).Value = .CGHead
                params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(3).Value = .BranchId
            End With
            With oClassAddress
                params(4) = New SqlParameter("@Address", SqlDbType.Char, 100)
                params(4).Value = .Address
                params(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(5).Value = .RT
                params(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(6).Value = .RW
                params(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(7).Value = .Kelurahan
                params(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(8).Value = .Kecamatan
                params(9) = New SqlParameter("@City", SqlDbType.VarChar, 30)
                params(9).Value = .City
                params(10) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(10).Value = .ZipCode
                params(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
                params(11).Value = .AreaPhone1
                params(12) = New SqlParameter("@PhoneNo1", SqlDbType.VarChar, 20)
                params(12).Value = .Phone1
                params(13) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
                params(13).Value = .AreaPhone2
                params(14) = New SqlParameter("@PhoneNo2", SqlDbType.VarChar, 20)
                params(14).Value = .Phone2
                params(15) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
                params(15).Value = .AreaFax
                params(16) = New SqlParameter("@FaxNo", SqlDbType.VarChar, 20)
                params(16).Value = .Fax
            End With
            With oClassPersonal
                params(17) = New SqlParameter("@ContactPerson", SqlDbType.Char, 50)
                params(17).Value = .PersonName
                params(18) = New SqlParameter("@ContactPersonTitle", SqlDbType.Char, 50)
                params(18).Value = .PersonTitle
                params(19) = New SqlParameter("@ContactPersonMobilePhone", SqlDbType.VarChar, 20)
                params(19).Value = .MobilePhone
                params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                params(20).Value = .Email
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, COLLGROUP_ADD, params)
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("CollGroup", "CollGroupAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        End Try
    End Sub

    Public Sub CollGroupEdit(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params(20) As SqlParameter
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCollGroup.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            With oCollGroup
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@CGName", SqlDbType.VarChar, 50)
                params(1).Value = .CGName
                params(2) = New SqlParameter("@CGHead", SqlDbType.Char, 10)
                params(2).Value = .CGHead
                params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(3).Value = .BranchId
            End With
            With oClassAddress
                params(4) = New SqlParameter("@Address", SqlDbType.Char, 100)
                params(4).Value = .Address
                params(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
                params(5).Value = .RT
                params(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
                params(6).Value = .RW
                params(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
                params(7).Value = .Kelurahan
                params(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
                params(8).Value = .Kecamatan
                params(9) = New SqlParameter("@City", SqlDbType.VarChar, 30)
                params(9).Value = .City
                params(10) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
                params(10).Value = .ZipCode
                params(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
                params(11).Value = .AreaPhone1
                params(12) = New SqlParameter("@PhoneNo1", SqlDbType.VarChar, 20)
                params(12).Value = .Phone1
                params(13) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
                params(13).Value = .AreaPhone2
                params(14) = New SqlParameter("@PhoneNo2", SqlDbType.VarChar, 20)
                params(14).Value = .Phone2
                params(15) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
                params(15).Value = .AreaFax
                params(16) = New SqlParameter("@FaxNo", SqlDbType.VarChar, 20)
                params(16).Value = .Fax
            End With
            With oClassPersonal
                params(17) = New SqlParameter("@ContactPerson", SqlDbType.Char, 50)
                params(17).Value = .PersonName
                params(18) = New SqlParameter("@ContactPersonTitle", SqlDbType.Char, 50)
                params(18).Value = .PersonTitle
                params(19) = New SqlParameter("@ContactPersonMobilePhone", SqlDbType.VarChar, 20)
                params(19).Value = .MobilePhone
                params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                params(20).Value = .Email
            End With

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, COLLGROUP_EDT, params)
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()

            WriteException("CollGroup", "CollGroupEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub

    Public Sub CollGroupDelete(ByVal OCollGroup As Parameter.CollGroup)
        Dim params(0) As SqlParameter
        Try
            With OCollGroup

                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 10)
                params(0).Value = .CGID
            End With

            SqlHelper.ExecuteNonQuery(OCollGroup.strConnection, CommandType.StoredProcedure, COLLGROUP_DEL, params)
        Catch exp As Exception
            WriteException("CollGroup", "CollGroupDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub

    Public Function GetBranchCombo(ByVal collgroup As Parameter.CollGroup) As Parameter.CollGroup
        Try
            collgroup.ListData = SqlHelper.ExecuteDataset(collgroup.strConnection, CommandType.StoredProcedure, GET_BRANCHCOMBO).Tables(0)
            Return collgroup
        Catch exp As Exception
            WriteException("CollGroup", "GetBranchCombo", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
