
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class IncentiveEXE : Inherits DataAccessBase
#Region " Private Const "
    Private Const IncentiveEXE_PAGING As String = "spIncentiveEXEPaging"
    Private Const IncentiveEXE_SAVE As String = "spIncentiveEXESave"
    Private Const IncentiveEXE_UPDATE As String = "spIncentiveEXEUpdate"
    Private Const INCENTIEBL_DELETE As String = "spIncentiveEXEDelete"
    Private Const IncentiveEXE_REPORTS As String = "spIncentiveEXEReport"
    Private m_connection As SqlConnection
#End Region
#Region "IncentiveEXEPaging"

    Public Function IncentiveEXEPaging(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, IncentiveEXE_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveEXE", "IncentiveEXEPaging", exp.Message + exp.StackTrace)

        End Try
    End Function

#End Region
#Region "IncentiveEXESave"

    Public Sub IncentiveEXESave(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim objtrans As SqlTransaction
        Dim params(5) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.AssetStatus
            params(1) = New SqlParameter("@Action", SqlDbType.VarChar, 5)
            params(1).Value = oCustomClass.Action
            params(2) = New SqlParameter("@MinIncentiveEI", SqlDbType.Decimal)
            params(2).Value = oCustomClass.MinIncentiveEI
            params(3) = New SqlParameter("@MaxIncentiveEI", SqlDbType.Decimal)
            params(3).Value = oCustomClass.MaxIncentiveEI
            params(4) = New SqlParameter("@MinIncentiveEP", SqlDbType.Decimal)
            params(4).Value = oCustomClass.MinIncentiveEP
            params(5) = New SqlParameter("@MaxIncentiveEP", SqlDbType.Decimal)
            params(5).Value = oCustomClass.MaxIncentiveEP

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, IncentiveEXE_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveEXE", "IncentiveEXESave", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveEXEUpdate"

    Public Sub IncentiveEXEUpdate(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim objtrans As SqlTransaction
        Dim params(5) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.AssetStatus
            params(1) = New SqlParameter("@Action", SqlDbType.VarChar, 5)
            params(1).Value = oCustomClass.Action
            params(2) = New SqlParameter("@MinIncentiveEI", SqlDbType.Decimal)
            params(2).Value = oCustomClass.MinIncentiveEI
            params(3) = New SqlParameter("@MaxIncentiveEI", SqlDbType.Decimal)
            params(3).Value = oCustomClass.MaxIncentiveEI
            params(4) = New SqlParameter("@MinIncentiveEP", SqlDbType.Decimal)
            params(4).Value = oCustomClass.MinIncentiveEP
            params(5) = New SqlParameter("@MaxIncentiveEP", SqlDbType.Decimal)
            params(5).Value = oCustomClass.MaxIncentiveEP


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, IncentiveEXE_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveEXE", "IncentiveEXEUpdate", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveEXEDelete"

    Public Sub IncentiveEXEDelete(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim objtrans As SqlTransaction
        Dim params(1) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@AssetStatus", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.AssetStatus
            params(1) = New SqlParameter("@Action", SqlDbType.VarChar, 5)
            params(1).Value = oCustomClass.Action

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIEBL_DELETE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveEXE", "IncentiveEXEDELETE", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveEXE Reports"
    Public Function IncentiveEXEReports(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE

        Try
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, IncentiveEXE_REPORTS)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveEXE", "IncentiveEXEReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
End Class
