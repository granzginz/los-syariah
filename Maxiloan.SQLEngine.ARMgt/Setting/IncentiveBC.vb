

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region

Public Class IncentiveBC : Inherits DataAccessBase
#Region " Private Const "
    Private Const INCENTIVEBC_PAGING As String = "spIncentiveBCPaging"
    Private Const INCENTIVEBC_SAVE As String = "spIncentiveBCSave"
    Private Const INCENTIVEBC_UPDATE As String = "spIncentiveBCUpdate"
    Private Const INCENTIEBC_DELETE As String = "spIncentiveBCDelete"
    Private Const INCENTIVEBC_REPORTS As String = "spIncentiveBCReport"
    Private m_connection As SqlConnection

#End Region
#Region "IncentiveBCPaging"

    Public Function IncentiveBCPaging(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBC_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBC", "IncentiveBCPaging", exp.Message + exp.StackTrace)

        End Try
    End Function

#End Region
#Region "IncentiveBCSave"

    Public Sub IncentiveBCSave(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@MaxDays", SqlDbType.SmallInt)
            params(0).Value = oCustomClass.MaxDays
            params(1) = New SqlParameter("@MinDays", SqlDbType.SmallInt)
            params(1).Value = oCustomClass.MinDays
            params(2) = New SqlParameter("@Percentage", SqlDbType.Decimal)
            params(2).Value = oCustomClass.Percentage
            params(2).Precision = 18
            params(2).Scale = 2


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBC_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBC", "IncentiveBCSave", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region


#Region "IncentiveBCUpdate"

    Public Sub IncentiveBCUpdate(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(0).Value = oCustomClass.SeqNo
            params(1) = New SqlParameter("@MaxDays", SqlDbType.SmallInt)
            params(1).Value = oCustomClass.MaxDays
            params(2) = New SqlParameter("@Percentage", SqlDbType.Decimal)
            params(2).Value = oCustomClass.Percentage
            params(2).Precision = 18
            params(2).Scale = 2


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVEBC_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBC", "IncentiveBCUpdate", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveBCDelete"
    Public Sub IncentiveBCDelete(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim objtrans As SqlTransaction
        Dim params(0) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(0).Value = oCustomClass.SeqNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIEBC_DELETE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveBC", "IncentiveBCDELETE", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveBC Reports"
    Public Function IncentiveBCReports(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC
        Dim params(0) As SqlParameter
        Try
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVEBC_REPORTS)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveBC", "IncentiveBCReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

End Class
