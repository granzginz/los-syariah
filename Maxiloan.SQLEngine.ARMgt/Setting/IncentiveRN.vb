
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class IncentiveRN : Inherits DataAccessBase
#Region " Private Const "
    Private Const INCENTIVERN_PAGING As String = "spIncentiveRNPaging"
    Private Const INCENTIVERN_SAVE As String = "spIncentiveRNSave"
    Private Const INCENTIVERN_UPDATE As String = "spIncentiveRNUpdate"
    Private Const INCENTIERN_DELETE As String = "spIncentiveRNDelete"
    Private Const INCENTIVERN_REPORTS As String = "spIncentiveRNReport"
    Private m_connection As SqlConnection

#End Region
#Region "IncentiveBCPaging"

    Public Function IncentiveRNPaging(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
        Dim params(4) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVERN_PAGING, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveRN", "IncentiveRNPaging", exp.Message + exp.StackTrace)

        End Try
    End Function

#End Region
#Region "IncentiveRNSave"

    Public Sub IncentiveRNSave(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@MaxReceipt", SqlDbType.SmallInt)
            params(0).Value = oCustomClass.MaxReceipt
            params(1) = New SqlParameter("@MinReceipt", SqlDbType.SmallInt)
            params(1).Value = oCustomClass.MinReceipt
            params(2) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(2).Value = oCustomClass.Tariff

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVERN_SAVE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveRN", "IncentiveRNSave", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region


#Region "IncentiveRNUpdate"

    Public Sub IncentiveRNUpdate(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim objtrans As SqlTransaction
        Dim params(2) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(0).Value = oCustomClass.SeqNo
            params(1) = New SqlParameter("@MaxReceipt", SqlDbType.SmallInt)
            params(1).Value = oCustomClass.MaxReceipt

            params(2) = New SqlParameter("@Tariff", SqlDbType.Decimal)
            params(2).Value = oCustomClass.Tariff

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIVERN_UPDATE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveRN", "IncentiveRNUpdate", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveBCDelete"

    Public Sub IncentiveRNDelete(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim objtrans As SqlTransaction
        Dim params(0) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(0).Value = oCustomClass.SeqNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INCENTIERN_DELETE, params)
            objtrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("IncentiveRN", "IncentiveRNDELETE", exp.Message + exp.StackTrace)

        End Try
    End Sub
#End Region
#Region "IncentiveRN Reports"
    Public Function IncentiveRNReports(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
        Dim params(0) As SqlParameter
        Try
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, INCENTIVERN_REPORTS)
            Return oCustomClass

        Catch exp As Exception
            WriteException("IncentiveRN", "IncentiveRNReports", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region


End Class
