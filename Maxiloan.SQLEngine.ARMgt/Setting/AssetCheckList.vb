

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetCheckList : Inherits DataAccessBase
    Private Const spListAssetCheckList As String = "spAssetCheckListPaging"
    Private Const spListAssetCheckListLookUp As String = "spAssetCheckListLookUp"
    Private Const spReportAssetCheckList As String = "spAssetCheckListReport"
    Private Const spAddAssetCheckList As String = "spAssetCheckListAdd"
    Private Const spEditAssetCheckList As String = "spAssetCheckListEdit"
    Private Const spDeleteAssetCheckList As String = "spAssetCheckListDelete"
    Protected Const PARAM_ASSETTYPEID As String = "@AssetTypeID"
    Protected Const PARAM_CHECKLISTID As String = "@CheckListID"
    Protected Const PARAM_DESCRIPTION As String = "@Description"
    Protected Const PARAM_ISYNQUESTION As String = "@IsYNQuestion"
    Protected Const PARAM_ISQTYQUESTION As String = "@IsQtyQuestion"
    Protected Const PARAM_ISACTIVE As String = "@IsActive"

    Public Function ListAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAssetCheckList = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAssetCheckList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AssetCheckList", "ListAssetCheckList", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function EditAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter(PARAM_ASSETTYPEID, SqlDbType.Char, 10)
            params(0).Value = customclass.AssetTypeID

            params(1) = New SqlParameter(PARAM_CHECKLISTID, SqlDbType.Char, 10)
            params(1).Value = customclass.CheckListID

            params(2) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 100)
            params(2).Value = customclass.Description

            params(3) = New SqlParameter(PARAM_ISYNQUESTION, SqlDbType.Char, 1)
            params(3).Value = customclass.isYNQuestion

            params(4) = New SqlParameter(PARAM_ISQTYQUESTION, SqlDbType.Char, 1)
            params(4).Value = customclass.isQTYQuestion

            params(5) = New SqlParameter(PARAM_USRUPD, SqlDbType.Char, 20)
            params(5).Value = customclass.LoginId

            params(6) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Char, 1)
            params(7).Value = customclass.IsActive

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spEditAssetCheckList, params)
            Return CStr(params(6).Value)

        Catch exp As Exception
            WriteException("AssetCheckList", "EditAssetCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter(PARAM_ASSETTYPEID, SqlDbType.Char, 10)
            params(0).Value = customclass.AssetTypeID

            params(1) = New SqlParameter(PARAM_CHECKLISTID, SqlDbType.Char, 10)
            params(1).Value = customclass.CheckListID

            params(2) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 100)
            params(2).Value = customclass.Description

            params(3) = New SqlParameter(PARAM_ISYNQUESTION, SqlDbType.Char, 1)
            params(3).Value = customclass.isYNQuestion

            params(4) = New SqlParameter(PARAM_ISQTYQUESTION, SqlDbType.Char, 1)
            params(4).Value = customclass.isQTYQuestion

            params(5) = New SqlParameter(PARAM_USRUPD, SqlDbType.Char, 20)
            params(5).Value = customclass.LoginId

            params(6) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Char, 1)
            params(7).Value = customclass.IsActive

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spAddAssetCheckList, params)
            Return CStr(params(6).Value)
        Catch exp As Exception
            WriteException("AssetCheckList", "AddAssetCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAssetCheckListLookUp(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Try
            customclass.ListAssetCheckList = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAssetCheckListLookUp).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AssetCheckList", "ListAssetCheckListLookUp", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DeleteAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_ASSETTYPEID, SqlDbType.Char, 10)
            params(0).Value = customclass.AssetTypeID

            params(1) = New SqlParameter(PARAM_CHECKLISTID, SqlDbType.Char, 10)
            params(1).Value = customclass.CheckListID

            params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spDeleteAssetCheckList, params)
            Return CStr(params(2).Value)
        Catch exp As Exception
            WriteException("AssetCheckList", "DeleteAssetCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ReportAssetCheck = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportAssetCheckList, params)
            'customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AssetCheckList", "ReportAssetCheckList", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
